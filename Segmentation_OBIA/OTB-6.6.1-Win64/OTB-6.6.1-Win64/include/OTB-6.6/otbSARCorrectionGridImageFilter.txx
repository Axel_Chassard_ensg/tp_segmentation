/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARCorrectionGridImageFilter_txx
#define otbSARCorrectionGridImageFilter_txx

#include "otbSARCorrectionGridImageFilter.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkProgressReporter.h"
#include "itkNumericTraitsPointPixel.h"

#include <cmath>


namespace otb
{
  /** 
   * Constructor with default initialization
   */
  template <class TImageIn, class TImageOut> 
  SARCorrectionGridImageFilter< TImageIn, TImageOut >::SARCorrectionGridImageFilter()
  {
    this->SetNumberOfRequiredInputs(2);

    m_Threshold = 0.3;
    m_MeanRange = 1.;
    m_MeanAzimut = 1.;
    m_Gap = 1.;
  }

  /**
   * Set Master Image
   */ 
  template <class TImageIn, class TImageOut> 
  void
  SARCorrectionGridImageFilter< TImageIn, TImageOut >
  ::SetDEMGridInput(const ImageInType* image )
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(0, const_cast<ImageInType *>(image));
  }

  /**
   * Set Slave Image
   */ 
  template <class TImageIn, class TImageOut> 
  void
  SARCorrectionGridImageFilter< TImageIn, TImageOut >
  ::SetCorGridInput(const ImageInType* image)
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(1, const_cast<ImageInType *>(image));
  }

  /**
   * Get Master Image
   */ 
  template <class TImageIn, class TImageOut> 
  const typename SARCorrectionGridImageFilter< TImageIn, TImageOut >::ImageInType *
  SARCorrectionGridImageFilter< TImageIn, TImageOut >
  ::GetDEMGridInput() const
  {
    if (this->GetNumberOfInputs()<1)
      {
	return 0;
      }
    return static_cast<const ImageInType *>(this->itk::ProcessObject::GetInput(0));
  }

  /**
   * Get Slave Image
   */ 
  template <class TImageIn, class TImageOut> 
  const typename SARCorrectionGridImageFilter< TImageIn, TImageOut >::ImageInType *
  SARCorrectionGridImageFilter< TImageIn, TImageOut >
  ::GetCorGridInput() const
  {
    if (this->GetNumberOfInputs()<2)
      {
	return 0;
      }
    return static_cast<const ImageInType *>(this->itk::ProcessObject::GetInput(1));
  }

  /**
   * Print
   */
  template<class TImageIn, class TImageOut>
  void
  SARCorrectionGridImageFilter< TImageIn, TImageOut >
  ::PrintSelf(std::ostream & os, itk::Indent indent) const
  {
    Superclass::PrintSelf(os, indent);
    
    os << indent << "Mean range : " << m_MeanRange << std::endl;
    os << indent << "Mean azimut : " << m_MeanAzimut << std::endl;
    os << indent << "Threshold : " << m_Threshold << std::endl;
    os << indent << "Gap : " << m_Gap << std::endl;
  }

  /**
   * Method GenerateOutputInformaton()
   **/
  template<class TImageIn, class TImageOut>
  void
  SARCorrectionGridImageFilter< TImageIn, TImageOut >
  ::GenerateOutputInformation()
  {    
    // Call superclass implementation
    Superclass::GenerateOutputInformation();

    // Get pointers to the input and output
    ImageInType * DEMGridPtr  = const_cast<ImageInType * >(this->GetDEMGridInput());
    ImageInType * CorGridPtr  = const_cast<ImageInType * >(this->GetCorGridInput());
    ImageOutPointer outputPtr = this->GetOutput();
  
    // Set the number of Components for the Output VectorImage
    // 2 Components :  
    //                _ range shift between Master and Slave with correction of DEM Grid by correlation grid
    //                _ azimut shift between Master and Slave with correction of DEM Grid by correlation grid
    outputPtr->SetNumberOfComponentsPerPixel(2);

    // Update size and spacing according to grid step
    ImageOutRegionType largestRegion  = static_cast<ImageOutRegionType>(DEMGridPtr->GetLargestPossibleRegion());
    ImageOutSizeType outputSize = static_cast<ImageOutSizeType>(largestRegion.GetSize());
    ImageOutSpacingType outputSpacing = static_cast<ImageOutSpacingType>(DEMGridPtr->GetSpacing());
    ImageOutPointType outputOrigin = static_cast<ImageOutPointType>(DEMGridPtr->GetOrigin());

    // Set spacing
    outputPtr->SetSpacing(outputSpacing);

    // Set origin
    outputPtr->SetOrigin(outputOrigin);

    // Set largest region size
    largestRegion.SetSize(outputSize);
    outputPtr->SetLargestPossibleRegion(largestRegion);

    // Check gridSteps (must be equal)
    ImageKeywordlist demGridKWL = DEMGridPtr->GetImageKeywordlist();
    ImageKeywordlist corGridKWL = CorGridPtr->GetImageKeywordlist();

    int demGridStepRange = atoi(demGridKWL.GetMetadataByKey("support_data.gridstep.range").c_str());
    int demGridStepAzimut = atoi(demGridKWL.GetMetadataByKey("support_data.gridstep.azimut").c_str());
    int corGridStepRange = atoi(corGridKWL.GetMetadataByKey("support_data.gridstep.range").c_str());
    int corGridStepAzimut = atoi(corGridKWL.GetMetadataByKey("support_data.gridstep.azimut").c_str());

    if (demGridStepRange != corGridStepRange || demGridStepAzimut != corGridStepAzimut)
      {
	itkExceptionMacro(<<"GridSteps between the two input gris are not equals.");
      }
    else
      {
	// Add GridSteps into KeyWordList
	ImageKeywordlist outputKWL;
	outputKWL.AddKey("support_data.gridstep.range", std::to_string(demGridStepRange));
	outputKWL.AddKey("support_data.gridstep.azimut", std::to_string(demGridStepAzimut));
	
	outputPtr->SetImageKeywordList(outputKWL); 
      }
  }


  /** 
   * Method GenerateInputRequestedRegion
   */
  template<class TImageIn, class TImageOut>
  void
  SARCorrectionGridImageFilter< TImageIn, TImageOut >
  ::GenerateInputRequestedRegion()
  {
    // Call the superclass' implementation of this method
    Superclass::GenerateInputRequestedRegion();
 
    ImageOutRegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();
    ImageInRegionType inputRequestedRegion = outputRequestedRegion;
   
    // Get pointers to inputs
    ImageInType * DEMGridPtr  = const_cast<ImageInType * >(this->GetDEMGridInput());
    ImageInType * CorGridPtr  = const_cast<ImageInType * >(this->GetCorGridInput());
    
    DEMGridPtr->SetRequestedRegion(inputRequestedRegion);
    CorGridPtr->SetRequestedRegion(inputRequestedRegion);
  }
 

 /**
   * Method BeforeThreadedGenerateData
   */
  template<class TImageIn, class TImageOut>
  void
  SARCorrectionGridImageFilter< TImageIn, TImageOut >
  ::ThreadedGenerateData(const ImageOutRegionType& outputRegionForThread,
			 itk::ThreadIdType threadId)
  {
    // Get pointers to inputs
    const ImageInType * DEMGridPtr = this->GetDEMGridInput();
    const ImageInType * CorGridPtr = this->GetCorGridInput();

    // Compute corresponding input region (same region for DEMGridPtr and CorGridPtr)
    ImageInRegionType inputRegionForThread = outputRegionForThread;

    // Typedef for iterators
    typedef itk::ImageScanlineConstIterator< ImageInType > InputIterator;
    typedef itk::ImageScanlineIterator< ImageOutType > OutputIterator;
        
    // Iterator on output (Grid geometry)
    OutputIterator OutIt(this->GetOutput(), outputRegionForThread);
    OutIt.GoToBegin();

    // Iterators on inputs (DEM geometry)
    InputIterator  InIt_DEMGrid(DEMGridPtr, inputRegionForThread); 
    InputIterator  InIt_CorGrid(CorGridPtr, inputRegionForThread);

    InIt_DEMGrid.GoToBegin();
    InIt_CorGrid.GoToBegin();

    // Support progress methods/callbacks
    itk::ProgressReporter progress(this, threadId, this->GetOutput()->GetRequestedRegion().GetNumberOfPixels());

    ImageOutPixelType pixelCorrectionGrid;
    pixelCorrectionGrid.Reserve(2);
        
    ////////////////////////////// Scan Input and Output Grids //////////////////////////////
    // For each Line of grids 
    while ( !InIt_DEMGrid.IsAtEnd() && !InIt_CorGrid.IsAtEnd() && !OutIt.IsAtEnd())
      {
	InIt_DEMGrid.GoToBeginOfLine();
	InIt_CorGrid.GoToBeginOfLine();
	OutIt.GoToBeginOfLine();

	// For each column
	while (!InIt_DEMGrid.IsAtEndOfLine() && !InIt_CorGrid.IsAtEndOfLine() && !OutIt.IsAtEndOfLine())
	  {
	    // Get elets of DEM Grid
	    double DEMGrid_range = InIt_DEMGrid.Get()[0];
	    double DEMGrid_azimut = InIt_DEMGrid.Get()[1];
	    int DEMGrid_occurence = InIt_DEMGrid.Get()[2];

	    // Distance between DEM Grid deformation with the mean values
	    double Dis_DEMGridRange = std::abs(DEMGrid_range - m_MeanRange); 
	    double Dis_DEMGridAzimut = std::abs(DEMGrid_azimut - m_MeanAzimut);

	    // Check is the value of DEM Grid is acceptable : occurence > 0 and deformations near to the mean 
	    // values  
	    if (!(DEMGrid_occurence > 0 && Dis_DEMGridRange < m_Gap && Dis_DEMGridAzimut < m_Gap))
	      {
		// Change the values by Correlation Grid value or by the mean values
		if (InIt_CorGrid.Get()[2] > m_Threshold)
		  {
		    double Dis_CorGridRange = std::abs(InIt_CorGrid.Get()[0] - m_MeanRange); 
		    double Dis_CorGridAzimut = std::abs(InIt_CorGrid.Get()[1] - m_MeanAzimut);
		    
		    if (Dis_CorGridRange < m_Gap && Dis_CorGridAzimut < m_Gap)
		      {
			// Change by CorGrid values
			pixelCorrectionGrid[0] = InIt_CorGrid.Get()[0];
			pixelCorrectionGrid[1] = InIt_CorGrid.Get()[1];
		      }
		    else
		      {
			// Change by mean values
			pixelCorrectionGrid[0] = m_MeanRange;
			pixelCorrectionGrid[1] = m_MeanAzimut;
		      }
		  }
		else
		  {
		    // Change by mean values
		    pixelCorrectionGrid[0] = m_MeanRange;
		    pixelCorrectionGrid[1] = m_MeanAzimut;
		  }
	      }
	    else
	      {
		// Keep DEM Grid deformations 
		pixelCorrectionGrid[0] = InIt_DEMGrid.Get()[0];
		pixelCorrectionGrid[1] = InIt_DEMGrid.Get()[1];
	      }

	    // Output Assignation
	    OutIt.Set(pixelCorrectionGrid);
	    progress.CompletedPixel();

	    // Next colunm
	    ++InIt_DEMGrid;
	    ++InIt_CorGrid; 
	    ++OutIt;
	  }
	
	// Next line
	InIt_DEMGrid.NextLine();
	InIt_CorGrid.NextLine();
	OutIt.NextLine();
      }

  }

} /*namespace otb*/

#endif
