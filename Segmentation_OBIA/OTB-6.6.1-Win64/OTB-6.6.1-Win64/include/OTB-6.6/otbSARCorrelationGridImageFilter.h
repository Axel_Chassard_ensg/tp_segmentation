/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARCorrelationGridImageFilter_h
#define otbSARCorrelationGridImageFilter_h

#include "itkImageToImageFilter.h"
#include "itkSmartPointer.h"
#include "itkPoint.h"

#include "otbImageKeywordlist.h"
#include "otbGenericRSTransform.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkStatisticsImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"

#include "itkFFTWCommon.h"

namespace otb
{
  /** \class SARCorrelationGridImageFilter 
   * \brief Estimates a deformation grid between two images (master and slave) for the azimut and range 
   * dimension. A correlation rate is also calculated. The DEM can be used. 
   * 
   * This filter performs the estimation of deformations between two images (the images are reals).
   * 
   * \ingroup DiapOTBModule
   */

  template <typename TImageIn,  typename TImageOut> class ITK_EXPORT SARCorrelationGridImageFilter :
    public itk::ImageToImageFilter<TImageIn,TImageOut>
  {
  public:

    // Standard class typedefs
    typedef SARCorrelationGridImageFilter                    Self;
    typedef itk::ImageToImageFilter<TImageIn,TImageOut>    Superclass;
    typedef itk::SmartPointer<Self>                        Pointer;
    typedef itk::SmartPointer<const Self>                  ConstPointer;

    // Method for creation through object factory
    itkNewMacro(Self);
    // Run-time type information
    itkTypeMacro(SARCorrelationGridFilter,ImageToImageFilter);

    /** Typedef to image input type : otb::Image for master and slave images */
    typedef TImageIn                                  ImageInType;
    /** Typedef to describe the inout image pointer type. */
    typedef typename ImageInType::Pointer             ImageInPointer;
    typedef typename ImageInType::ConstPointer        ImageInConstPointer;
    /** Typedef to describe the inout image region type. */
    typedef typename ImageInType::RegionType          ImageInRegionType;
    /** Typedef to describe the type of pixel and point for inout image. */
    typedef typename ImageInType::PixelType           ImageInPixelType;
    typedef typename ImageInType::PointType           ImageInPointType;
    /** Typedef to describe the image index, size types and spacing for inout image. */
    typedef typename ImageInType::IndexType           ImageInIndexType;
    typedef typename ImageInType::IndexValueType      ImageInIndexValueType;
    typedef typename ImageInType::SizeType            ImageInSizeType;
    typedef typename ImageInType::SizeValueType       ImageInSizeValueType;
    typedef typename ImageInType::SpacingType         ImageInSpacingType;
    typedef typename ImageInType::SpacingValueType    ImageInSpacingValueType;
  
    /** Typedef to image output type : otb::VectorImage with three components (range deformations, 
	azimut deformations and correlation rate) */
    typedef TImageOut                                  ImageOutType;
    /** Typedef to describe the output image pointer type. */
    typedef typename ImageOutType::Pointer             ImageOutPointer;
    typedef typename ImageOutType::ConstPointer        ImageOutConstPointer;
    /** Typedef to describe the output image region type. */
    typedef typename ImageOutType::RegionType          ImageOutRegionType;
    /** Typedef to describe the type of pixel and point for output image. */
    typedef typename ImageOutType::PixelType           ImageOutPixelType;
    typedef typename ImageOutType::PointType           ImageOutPointType;
    /** Typedef to describe the image index, size types and spacing for output image. */
    typedef typename ImageOutType::IndexType           ImageOutIndexType;
    typedef typename ImageOutType::IndexValueType      ImageOutIndexValueType;
    typedef typename ImageOutType::SizeType            ImageOutSizeType;
    typedef typename ImageOutType::SizeValueType       ImageOutSizeValueType;
    typedef typename ImageOutType::SpacingType         ImageOutSpacingType;
    typedef typename ImageOutType::SpacingValueType    ImageOutSpacingValueType;


    // Define Point2DType and Point3DType
    using Point2DType = itk::Point<double,2>;
    using Point3DType = itk::Point<double,3>;

    typedef otb::DEMHandler DEMHandlerType;
    typedef typename DEMHandlerType::Pointer DEMHandlerPointerType;

    // Define Filter used here (ie : RSTRansform from Master to Slave)
    typedef typename otb::GenericRSTransform<double,2,2>                                    RSTransformType2D;
    typedef typename itk::MinimumMaximumImageCalculator< ImageInType>			    MinMaxCalculatorType;
    typedef typename itk::RescaleIntensityImageFilter< ImageInType, ImageInType >           RescaleFilterType;

     // ITK proxy to the fftw library
    typedef typename itk::fftw::Proxy<ImageInPixelType> FFTWProxyType;
    typedef typename FFTWProxyType::ComplexType         FFTProxyComplexType;
    typedef typename FFTWProxyType::PixelType           FFTProxyPixelType;
    typedef typename FFTWProxyType::PlanType            FFTProxyPlanType;

    // Setter/Getter
    /** Set/Get the size of the area on which correlation is computed */
    itkSetMacro(PatchSizePerDim, unsigned int);
    itkGetMacro(PatchSizePerDim, unsigned int);
    /** Set/Get subpixel accuracy */
    void SetSubPixelAccuracy(int subPixel)
    {
      m_SubPixelAccuracy = 1.0/subPixel;
    }
    itkGetMacro(SubPixelAccuracy, double);
    /** Set/Get fine coherency threshold */
    itkSetMacro(CorrelationThreshold, ImageInPointType);
    itkGetMacro(CorrelationThreshold, ImageInPointType);
    /** Set/Get Rescale flag */
    itkSetMacro(Rescale, bool);
    itkGetConstMacro(Rescale, bool);  
    /** Set/Get unsigned int grid step */
    void SetGridStep(unsigned int stepRange, unsigned int stepAzimut)
    {
      m_GridStep[0] = stepRange;
      m_GridStep[1] = stepAzimut;
    }
    unsigned int GetGridStepRange()
    {
      return m_GridStep[0];
    }
    unsigned int GetGridStepAzimut()
    {
      return m_GridStep[1];
    }
    /** Set/Get ML factors */
    itkSetMacro(MLran, unsigned int);
    itkGetMacro(MLran, unsigned int);
    itkSetMacro(MLazi, unsigned int);
    itkGetMacro(MLazi, unsigned int);
    

    // Setter/Getter for master and slave images (inputs)
    /** Connect one of the operands for registration */
    void SetMasterInput( const ImageInType* image);

    /** Connect one of the operands for registration */
    void SetSlaveInput(const ImageInType* image);

    /** Get the inputs */
    const ImageInType * GetMasterInput() const;
    const ImageInType * GetSlaveInput() const;

    /** Activate the DEM */
    void activateDEM(const std::string DEMdirectory);
  
  protected:
    // Constructor
    SARCorrelationGridImageFilter();

    // Destructor
    virtual ~SARCorrelationGridImageFilter() ITK_OVERRIDE {};

    // Print
    void PrintSelf(std::ostream & os, itk::Indent indent) const ITK_OVERRIDE;
  
    /** SARCorrelationGridImageFilter produces an vector image to indicate the deformation for the two dimensions 
     * and the correlation rate. The differences between output and input images are the size of images and the 
     * dimensions. The input images a classic images and the output is a otb::VectorImage with three components.
     * As such, SARCorrelationGridImageFilter needs to provide an implementation for 
     * GenerateOutputInformation() in order to inform the pipeline execution model. 
     */ 
    virtual void GenerateOutputInformation() ITK_OVERRIDE;

    /** SARCorrelationGridImageFilter needs input requested regions (for master and slave images) that 
     * corresponds to the projection into the requested region of the deformation grid (our output requested 
     * region).  
     * As such, SARQuadraticAveragingImageFilter needs to provide an implementation for 
     * GenerateInputRequestedRegion() in order to inform the pipeline execution model. 
     * \sa ProcessObject::GenerateInputRequestedRegion() */
    virtual void GenerateInputRequestedRegion() ITK_OVERRIDE;

    /**
     * OutputRegionToInputRegion assigne master and slave regions 
     */
    void OutputRegionToInputRegion(const ImageOutRegionType& outputRegion) const;

    /** SARCorrelationGridImageFilter can be implemented as a multithreaded filter.
     * Therefore, this implementation provides a ThreadedGenerateData() routine
     * which is called for each processing thread. The output image data is
     * allocated automatically by the superclass prior to calling
     * ThreadedGenerateData().  ThreadedGenerateData can only write to the
     * portion of the output image specified by the parameter
     * "outputRegionForThread"
     *
     * \sa ImageToImageFilter::ThreadedGenerateData(),
     *     ImageToImageFilter::GenerateData() */
    void BeforeThreadedGenerateData() override;

    void ThreadedGenerateData(const ImageOutRegionType& outputRegionForThread,
			      itk::ThreadIdType threadId ) override;

  
    void AfterThreadedGenerateData() override;


  private:
    SARCorrelationGridImageFilter(const Self&); // purposely not implemented
    void operator=(const Self &); // purposely not 
    
    /** Coarse correlation threshold */
   ImageInPointType m_CorrelationThreshold;

    /** The size for correlation */
    unsigned int m_PatchSizePerDim;

    /** Search step */
    double m_SubPixelAccuracy;

    /** Grid step */
    ImageInSizeType m_GridStep;

    /** Grid step into ML geometry */
    ImageInSizeType m_GridStep_ML;

    /** ML factors (in range and in azimut) */
    unsigned int m_MLran;
    unsigned int m_MLazi;

    bool m_Rescale;

    // FFT parameters
    FFTProxyPixelType ** m_masterPieceTab;
    FFTProxyPixelType ** m_slavePieceTab;
    FFTProxyComplexType ** m_masterFFTTab;
    FFTProxyComplexType ** m_slaveFFTTab;
    FFTProxyComplexType ** m_multipliedFFTTab;
    FFTProxyPixelType ** m_inverseFFTPieceTab;

    FFTProxyPlanType * m_masterPlanTab; 
    FFTProxyPlanType * m_slavePlanTab;
    FFTProxyPlanType * m_inversePlanTab;

    ImageInSizeType m_pieceSize;
    unsigned int m_sizeFFT;

    // Extract Parameters
    ImageInPointer * m_masterExtractPieceTab;
    ImageInPointer * m_slaveExtractPieceTab;

    // RSTransform
    RSTransformType2D::Pointer m_rsTransform;
  };

} // End namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSARCorrelationGridImageFilter.txx"
#endif



#endif
