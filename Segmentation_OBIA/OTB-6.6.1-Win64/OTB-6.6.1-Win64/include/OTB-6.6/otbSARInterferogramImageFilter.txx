/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARInterferogramImageFilter_txx
#define otbSARInterferogramImageFilter_txx

#include "otbSARInterferogramImageFilter.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkProgressReporter.h"
#include "itkNumericTraitsPointPixel.h"
#include "itkContinuousIndex.h"

#include "ossim/ossimSarSensorModel.h"

#include <cmath>
#include <algorithm>
#include <omp.h>

#ifndef M_1_PI
#define M_1_PI 0.31830988618379067154
#endif

namespace otb
{
  /** 
   * Constructor with default initialization
   */
  template <class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut> 
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >::SARInterferogramImageFilter()
    :  m_MLRan(1), m_MLAzi(1), m_Gain(1), m_UseDEMGeoAsOutput(false), m_nbLinesSAR(0), m_nbColSAR(0), 
       m_MarginRan(1), m_MarginAzi(1), m_nbLinesDEM(0), m_nbColDEM(0), m_ApproxDiapason(false)
  {
    // Inputs required and/or needed
    this->SetNumberOfRequiredInputs(2);
    this->SetNumberOfIndexedInputs(3);

    // Outputs required and/or needed (one required and one optionnal)
    this->SetNumberOfRequiredOutputs(1);
    this->SetNumberOfIndexedOutputs(2);

    this->SetNthOutput(0, ImageOutType::New()); // ML Geo
    this->SetNthOutput(1, ImageOutType::New()); // DEM Geo

    m_OutputCounter = 0;

    m_Mutex = new MutexType();
  }
    
  /** 
   * Destructor
   */
  template <class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut> 
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >::~SARInterferogramImageFilter()
  {
  }

  /**
   * Print
   */
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  void
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::PrintSelf(std::ostream & os, itk::Indent indent) const
  {
    Superclass::PrintSelf(os, indent);

    os << indent << "ML factors : " << m_MLRan << ", " << m_MLAzi << std::endl;
    os << indent << "Gain for amplitude estimation : " << m_Gain << std::endl;
  }

    /**
   * SetDEMPtr
   */
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  void
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::SetDEMPtr(ImageDEMPointer demPtr)
  {
    m_DEMPtr = demPtr;
  }

  /**
   * Set Master Image
   */ 
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  void
 SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::SetMasterInput(const ImageSARType* image )
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(0, const_cast<ImageSARType *>(image));
  }

  /**
   * Set Coregistrated Slave Image
   */ 
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  void
 SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::SetSlaveInput(const ImageSARType* image )
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(1, const_cast<ImageSARType *>(image));
  }

  /**
   * Set Topographic phase
   */ 
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  void
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::SetTopographicPhaseInput(const ImagePhaseType* phaseTopoImage)
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(2, const_cast<ImagePhaseType *>(phaseTopoImage));

    // Adapt the tolerance for inputs
    if (m_MLRan != 1 || m_MLAzi != 1)
      {
	int MLFact = m_MLRan;
	if (m_MLRan < m_MLAzi)
	  {
	    MLFact = m_MLAzi;
	  }
	
	this->SetCoordinateTolerance(MLFact);
	this->SetDirectionTolerance(MLFact);
      }
  }

  /**
   * Get Master Image
   */ 
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  const typename SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >::ImageSARType *
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::GetMasterInput() const
  {
    if (this->GetNumberOfInputs()<1)
      {
	return 0;
      }
    return static_cast<const ImageSARType *>(this->itk::ProcessObject::GetInput(0));
  }

  /**
   * Get Coregistrated Slave Image
   */ 
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  const typename SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >::ImageSARType *
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::GetSlaveInput() const
  {
    if (this->GetNumberOfInputs()<2)
      {
	return 0;
      }
    return static_cast<const ImageSARType *>(this->itk::ProcessObject::GetInput(1));
  }

  /**
   * Get Phase Topographic
   */ 
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  const typename SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >::ImagePhaseType *
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::GetTopographicPhaseInput() const
  {
    if (this->GetNumberOfInputs()<3)
      {
	return 0;
      }
    return static_cast<const ImagePhaseType *>(this->itk::ProcessObject::GetInput(2));
  }


  /**
   * Get Main Output : Interferogram into ML geometry
   */ 
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  TImageOut *
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::GetOutput()
  {
    if (this->GetNumberOfOutputs() < 1)
      {
	return ITK_NULLPTR;
      }
    return static_cast<ImageOutType *>(this->itk::ProcessObject::GetOutput(0));
  }

  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  const TImageOut *
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::GetOutput() const
  {
    if (this->GetNumberOfOutputs() < 1)
      {
	return 0;
      }
    return static_cast<ImageOutType *>(this->itk::ProcessObject::GetOutput(0));
  }

  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  typename SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >::ImageOutType *
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::GetOutputIntoDEMGeo()
  {
    if (this->GetNumberOfOutputs() < 2)
      {
	return ITK_NULLPTR;
      }
    return static_cast<ImageOutType *>(this->itk::ProcessObject::GetOutput(1));
  }

  
  /**
   * Method GenerateOutputInformaton()
   **/
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  void
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::GenerateOutputInformation()
  {
    // Call superclass implementation
    Superclass::GenerateOutputInformation();

    // Get pointers to the inputs and output
    ImageSARConstPointer masterPtr = this->GetMasterInput();
    ImageSARConstPointer slavePtr = this->GetSlaveInput();
    ImageOutPointer outputPtr = this->GetOutput();

    // KeyWordList
    ImageKeywordlist masterKWL = masterPtr->GetImageKeywordlist();

    // Master SAR Dimensions
    m_nbLinesSAR = this->GetMasterInput()->GetLargestPossibleRegion().GetSize()[1];
    m_nbColSAR = this->GetMasterInput()->GetLargestPossibleRegion().GetSize()[0];
  
    //////////////////////////// Main Output : Interferogram into ML Geo ///////////////////////////
    // Vector Image  :
    // At Least 4 Components :  
    //                _ Amplitude
    //                _ Phase
    //                _ Coherence
    //                _ IsData
    outputPtr->SetNumberOfComponentsPerPixel(4);

    // The output is defined with the Master SAR Image and by ML factors 
    // Origin, Spacing and Size (SAR master geometry) in function of ML factors
    ImageOutSizeType outputSize;

    outputSize[0] = std::max<ImageOutSizeValueType>(static_cast<ImageOutSizeValueType>(std::floor( (double) m_nbColSAR / (double) m_MLRan)),1);
    outputSize[1] = std::max<ImageOutSizeValueType>(static_cast<ImageOutSizeValueType>(std::floor( (double) m_nbLinesSAR / (double) m_MLAzi)),1);

    ImageOutPointType outOrigin;
    outOrigin = masterPtr->GetOrigin();
    ImageOutSpacingType outSP;
    outSP = masterPtr->GetSpacing();
    outSP[0] *= m_MLRan;
    outSP[1] *= m_MLAzi;

    // Define Output Largest Region
    ImageOutRegionType outputLargestPossibleRegion = masterPtr->GetLargestPossibleRegion();
    outputLargestPossibleRegion.SetSize(outputSize);
    outputPtr->SetLargestPossibleRegion(outputLargestPossibleRegion);
    outputPtr->SetOrigin(outOrigin);
    outputPtr->SetSpacing(outSP);

    // Add ML factors and bands meaning into keyWordList
    ImageKeywordlist outputKWL = masterKWL;
    outputKWL.AddKey("support_data.band.Amplitude", std::to_string(0));
    outputKWL.AddKey("support_data.band.Phase", std::to_string(1));
    outputKWL.AddKey("support_data.band.Coherency", std::to_string(2));
    outputKWL.AddKey("support_data.band.isData", std::to_string(3));

    outputKWL.AddKey("support_data.ml_ran", std::to_string(m_MLRan));
    outputKWL.AddKey("support_data.ml_azi", std::to_string(m_MLAzi));

    // Set new keyword list to output image
    outputPtr->SetImageKeywordList(outputKWL);    

    ///////// Checks (with input topographic phase keywordlists/metadata) /////////////
    // Check ML Factors
     if (this->GetTopographicPhaseInput() != nullptr)
       {
	 // Get ImagKeyWordList
	 ImageKeywordlist topoKWL = this->GetTopographicPhaseInput()->GetImageKeywordlist();
	 
	 if (topoKWL.HasKey("support_data.ml_ran") && topoKWL.HasKey("support_data.ml_azi"))
	   {
	     // Get Master ML Factors
	     unsigned int topo_MLRan = atoi(topoKWL.GetMetadataByKey("support_data.ml_ran").c_str());
	     unsigned int topo_MLAzi = atoi(topoKWL.GetMetadataByKey("support_data.ml_azi").c_str());
	
	     if ((topo_MLRan != m_MLRan) || (topo_MLAzi != m_MLAzi))
	       {
		 itkExceptionMacro(<<"ML Factor betwwen topographic phase and inputs of this application are different.");
	       }
	   }
       }

     ///////// Checks margin with ML factors (for averaging) /////////////
     if (m_MLRan != 0 || m_MLAzi != 0)
       {
	 if (m_MLRan < m_MarginRan || m_MLAzi < m_MarginAzi)
	   {
	     itkExceptionMacro(<<"Margin for averaging can't be superior to ML Factors.");   
	   } 
       }

    
     //////////////////////////// Optionnal Output : Interferogram into DEM Geo ///////////////////////////
     if (m_UseDEMGeoAsOutput)
       {
	 // Get Pointer
	 ImageOutPointer outputOptionnalPtr = this->GetOutputIntoDEMGeo();

	 // Vector Image  :
	 // At Least 4 Components :  
	 //                _ Amplitude
	 //                _ Phase
	 //                _ Coherence
	 //                _ IsData
	 outputOptionnalPtr->SetNumberOfComponentsPerPixel(4);

	 // This output is defined with the DEM geometry
	 outputOptionnalPtr->SetLargestPossibleRegion(m_DEMPtr->GetLargestPossibleRegion());
	 outputOptionnalPtr->SetOrigin(m_DEMPtr->GetOrigin());
	 outputOptionnalPtr->SetSignedSpacing(m_DEMPtr->GetSignedSpacing());

	 // Projection Ref
	 outputOptionnalPtr->SetProjectionRef(m_DEMPtr->GetProjectionRef());
	
	 // Interferogram Ortho requires Topographic phase in input
	 if (this->GetTopographicPhaseInput() == nullptr || 
	     this->GetTopographicPhaseInput()->GetNumberOfComponentsPerPixel() < 3)
	   {
	     itkExceptionMacro(<<"Interferogram into ortho geometry requires Topographic phase in input with cartesian coordonates.");
	   }

	 if (m_OutputCounter == 0)
	   {
	     if (m_DEMPtr == nullptr)
	       {
		 itkExceptionMacro(<<"To estimate interferogram into ortho geometry, a DEM must be in input.");
	       }

	     // Get DEM's size
	     m_nbLinesDEM = m_DEMPtr->GetLargestPossibleRegion().GetSize()[1];
	     m_nbColDEM = m_DEMPtr->GetLargestPossibleRegion().GetSize()[0];

	     // Allocation of arrays for accumulation and counter into ortho geo
	     m_OrthoRealAcc = new double[m_nbLinesDEM * m_nbColDEM];
	     m_OrthoImagAcc = new double[m_nbLinesDEM * m_nbColDEM];
	     m_OrthoModAcc = new double[m_nbLinesDEM * m_nbColDEM];
	     m_OrthoCounter = new double[m_nbLinesDEM * m_nbColDEM];

	     // Initialize to zero
	     memset(m_OrthoRealAcc, 0, m_nbLinesDEM * m_nbColDEM * sizeof(double));
	     memset(m_OrthoImagAcc, 0, m_nbLinesDEM * m_nbColDEM * sizeof(double));
	     memset(m_OrthoModAcc, 0, m_nbLinesDEM * m_nbColDEM * sizeof(double));
	     memset(m_OrthoCounter, 0, m_nbLinesDEM * m_nbColDEM * sizeof(double));
	   }
       }

     ++m_OutputCounter;
  }

  /** 
   * Method OutputRegionToInputRegion for GenerateInputRequestedRegion
   */
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  typename SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >::ImageSARRegionType 
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::OutputRegionToInputRegion(const ImageOutRegionType& outputRegion, bool withMargin) const
  {
    // Compute the input requested region (size and start index)
    // Use the image transformations to insure an input requested region
    // that will provide the proper range
    const ImageOutSizeType & outputRequestedRegionSize = outputRegion.GetSize();
    ImageOutIndexType outputRequestedRegionIndex = outputRegion.GetIndex();

    // Add a margin for averaging
    int marge_MLRan = m_MarginRan;
    int marge_MLAzi = m_MarginAzi;

    if (!withMargin)
      {
	marge_MLRan = 0;
	marge_MLAzi = 0;
      }

    // Multiply to output region by ML factor to get input SAR region (After coregistration Master and Slave have
    // the same geometry)
    ImageSARIndexType inputRequestedRegionIndex; 
    inputRequestedRegionIndex[0] = static_cast<ImageSARIndexValueType>(outputRequestedRegionIndex[0] * m_MLRan
								       - marge_MLRan);
    inputRequestedRegionIndex[1] = static_cast<ImageSARIndexValueType>(outputRequestedRegionIndex[1] * m_MLAzi 
								       - marge_MLAzi);
    ImageSARSizeType inputRequestedRegionSize;
    inputRequestedRegionSize[0] = static_cast<ImageSARSizeValueType>(outputRequestedRegionSize[0] * m_MLRan + 
								     2*marge_MLRan);
    inputRequestedRegionSize[1] = static_cast<ImageSARSizeValueType>(outputRequestedRegionSize[1] * m_MLAzi + 
								     2*marge_MLAzi);  
   
    // Check Index and Size
    if (inputRequestedRegionIndex[0] < this->GetMasterInput()->GetLargestPossibleRegion().GetIndex()[0])
      {
	inputRequestedRegionIndex[0] = this->GetMasterInput()->GetLargestPossibleRegion().GetIndex()[0];
      }
    if (inputRequestedRegionIndex[1] < this->GetMasterInput()->GetLargestPossibleRegion().GetIndex()[1])
      {
	inputRequestedRegionIndex[1] = this->GetMasterInput()->GetLargestPossibleRegion().GetIndex()[1];
      }
    if ((inputRequestedRegionSize[0] + inputRequestedRegionIndex[0]) > 
	this->GetMasterInput()->GetLargestPossibleRegion().GetSize()[0])
      {
	inputRequestedRegionSize[0] = this->GetMasterInput()->GetLargestPossibleRegion().GetSize()[0] - 
	  inputRequestedRegionIndex[0];
      }
    if ((inputRequestedRegionSize[1] + inputRequestedRegionIndex[1]) > 
	this->GetMasterInput()->GetLargestPossibleRegion().GetSize()[1])
      {
	inputRequestedRegionSize[1] = this->GetMasterInput()->GetLargestPossibleRegion().GetSize()[1] - 
	  inputRequestedRegionIndex[1];
      }

    // Transform into a region
    ImageSARRegionType inputRequestedRegion = outputRegion;
    inputRequestedRegion.SetIndex(inputRequestedRegionIndex);
    inputRequestedRegion.SetSize(inputRequestedRegionSize);
    
    
    return inputRequestedRegion;  
  }

/** 
   * Method OutputRegionToInputRegion for GenerateInputRequestedRegion
   */
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  typename SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >::ImagePhaseRegionType 
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::OutputRegionToInputPhaseRegion(const ImageOutRegionType& outputRegion, 
				   bool & sameGeoAsMainOutput, bool withMargin) const
  {
    // Compute the input requested region (size and start index)
    // Use the image transformations to insure an input grid requested region
    // that will provide the proper range
    const ImageOutSizeType & outputRequestedRegionSize = outputRegion.GetSize();
    ImageOutIndexType outputRequestedRegionIndex = outputRegion.GetIndex();

    // Get Pointer
    ImagePhaseConstPointer topoPhase = this->GetTopographicPhaseInput();
            
    // Get Key into KeyWordList to identify the geometry of Topographic Phase image 
    // Two kinds of geometry : ML (with the same factors m_MLRan and m_MLAzi) or SAR
    ImageKeywordlist topoPhaseKWL = topoPhase->GetImageKeywordlist();

    sameGeoAsMainOutput = false;

    std::string key_MLRan = "support_data.ml_ran";
    std::string key_MLAzi = "support_data.ml_azi";

    ImagePhaseRegionType inputPhaseRequestedRegion;

    if (!topoPhaseKWL.HasKey(key_MLRan) || !topoPhaseKWL.HasKey(key_MLAzi))
      {
	// Add a margin for averaging
	int marge_MLRan = m_MarginRan;
	int marge_MLAzi = m_MarginAzi;
	
	if (!withMargin)
	  {
	    marge_MLRan = 0;
	    marge_MLAzi = 0;
	  }

	// SAR Geometry
	// Multiply to output region by ML factor to get input SAR region 
	ImagePhaseIndexType inputPhaseRequestedRegionIndex; 
	inputPhaseRequestedRegionIndex[0] = static_cast<ImagePhaseIndexValueType>(outputRequestedRegionIndex[0] 
										  * m_MLRan - marge_MLRan);
	inputPhaseRequestedRegionIndex[1] = static_cast<ImagePhaseIndexValueType>(outputRequestedRegionIndex[1] 
										  * m_MLAzi - marge_MLAzi);
	ImagePhaseSizeType inputPhaseRequestedRegionSize;
	inputPhaseRequestedRegionSize[0] = static_cast<ImagePhaseSizeValueType>(outputRequestedRegionSize[0] 
										* m_MLRan + 2*marge_MLRan);
	inputPhaseRequestedRegionSize[1] = static_cast<ImagePhaseSizeValueType>(outputRequestedRegionSize[1] 
										* m_MLAzi + 2*marge_MLAzi);  
   
	// Check Index and Size
	if (inputPhaseRequestedRegionIndex[0] < this->GetMasterInput()->GetLargestPossibleRegion().GetIndex()[0])
	  {
	    inputPhaseRequestedRegionIndex[0] = this->GetMasterInput()->GetLargestPossibleRegion().GetIndex()[0];
	  }
	if (inputPhaseRequestedRegionIndex[1] < this->GetMasterInput()->GetLargestPossibleRegion().GetIndex()[1])
	  {
	    inputPhaseRequestedRegionIndex[1] = this->GetMasterInput()->GetLargestPossibleRegion().GetIndex()[1];
	  }
	if ((inputPhaseRequestedRegionSize[0] + inputPhaseRequestedRegionIndex[0]) > 
	    this->GetMasterInput()->GetLargestPossibleRegion().GetSize()[0])
	  {
	    inputPhaseRequestedRegionSize[0] = this->GetMasterInput()->GetLargestPossibleRegion().GetSize()[0] - 
	      inputPhaseRequestedRegionIndex[0];
	  }
	if ((inputPhaseRequestedRegionSize[1] + inputPhaseRequestedRegionIndex[1]) > 
	    this->GetMasterInput()->GetLargestPossibleRegion().GetSize()[1])
	  {
	    inputPhaseRequestedRegionSize[1] = this->GetMasterInput()->GetLargestPossibleRegion().GetSize()[1] - 
	      inputPhaseRequestedRegionIndex[1];
	  }

	// Transform into a region
	inputPhaseRequestedRegion = outputRegion;
	inputPhaseRequestedRegion.SetIndex(inputPhaseRequestedRegionIndex);
	inputPhaseRequestedRegion.SetSize(inputPhaseRequestedRegionSize);
      }
    else
      {
	// Get ML factors
	unsigned int ml_Ran_Phase = atoi(topoPhaseKWL.GetMetadataByKey(key_MLRan).c_str());
	unsigned int ml_Azi_Phase = atoi(topoPhaseKWL.GetMetadataByKey(key_MLAzi).c_str());
	
	// Check ML factors
	if (ml_Ran_Phase != m_MLRan || ml_Azi_Phase != m_MLAzi)
	  {
	    itkExceptionMacro(<<"ML factors for Topographic phase is not equal to ML factors for interferogram.");
	    return inputPhaseRequestedRegion;
	  }

	// Same region than output
	inputPhaseRequestedRegion = outputRegion;
	sameGeoAsMainOutput = true;
      }

    // Return phase region
    return inputPhaseRequestedRegion; 
  }

  /** 
   * Method GenerateInputRequestedRegion
   */
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  void
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::GenerateInputRequestedRegion()
  {
    // call the superclass' implementation of this method
    Superclass::GenerateInputRequestedRegion();
    
    // Get Output requested region
    ImageOutRegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();

    if (m_OutputCounter == 1)
      {	///////////// Find the region into SAR input images (same geometry for SAR after coregistration ////////////
	ImageSARRegionType inputSARRequestedRegion = OutputRegionToInputRegion(outputRequestedRegion, true);

	ImageSARPointer  masterPtr = const_cast< ImageSARType * >( this->GetMasterInput() );
	ImageSARPointer  slavePtr = const_cast< ImageSARType * >( this->GetSlaveInput() );
    
	masterPtr->SetRequestedRegion(inputSARRequestedRegion);
	slavePtr->SetRequestedRegion(inputSARRequestedRegion);

	///////////// Find the region into topographic phase image (if needed) /////////////
	if (this->GetTopographicPhaseInput() != nullptr)
	  {
	    bool sameGeoAsMainOutput;
	    ImagePhaseRegionType phaseRequestedRegion = OutputRegionToInputPhaseRegion(outputRequestedRegion,
										       sameGeoAsMainOutput, true);
	    ImagePhasePointer  phasePtr = const_cast< ImagePhaseType * >( this->GetTopographicPhaseInput() );
	    phasePtr->SetRequestedRegion(phaseRequestedRegion);
	  }
      }
    else
      {
	// No region requested
	ImageSARRegionType inputSARRequestedRegion;
	ImageSARIndexType index;
	index.Fill(0);
	ImageSARSizeType size;
	size.Fill(0);
	inputSARRequestedRegion.SetIndex(index);
	inputSARRequestedRegion.SetSize(size);

	ImageSARPointer  masterPtr = const_cast< ImageSARType * >( this->GetMasterInput() );
	ImageSARPointer  slavePtr = const_cast< ImageSARType * >( this->GetSlaveInput() );
    
	masterPtr->SetRequestedRegion(inputSARRequestedRegion);
	slavePtr->SetRequestedRegion(inputSARRequestedRegion);


	ImagePhaseRegionType phaseRequestedRegion;
	ImagePhaseIndexType indexPhase;
	indexPhase.Fill(0);
	ImagePhaseSizeType sizePhase;
	sizePhase.Fill(0);
	phaseRequestedRegion.SetIndex(indexPhase);
	phaseRequestedRegion.SetSize(sizePhase);

	ImagePhasePointer  phasePtr = const_cast< ImagePhaseType * >( this->GetTopographicPhaseInput() );
	phasePtr->SetRequestedRegion(phaseRequestedRegion);
      }
  }

  
  /**
   * Method EnlargeOutputRequestedRegion
   */
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  void
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::EnlargeOutputRequestedRegion( itk::DataObject *itkNotUsed(output) )
  {
    // This filter requires all of the optionnal output image (Nth Output = 1).
    if (m_UseDEMGeoAsOutput)
      {
	if ( this->itk::ProcessObject::GetOutput(1) )
	  {
	    this->itk::ProcessObject::GetOutput(1)->SetRequestedRegionToLargestPossibleRegion();
	  }
      }
  }


  /**
   * Method BeforeThreadedGenerateData
   */
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  void
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::BeforeThreadedGenerateData()
  {
    // Allocation for the optionnal image (if needed)
    if (m_UseDEMGeoAsOutput)
      {
	// Allocate optionnal output
	ImageOutPointer outputOptionnalPtr = this->GetOutputIntoDEMGeo();
	outputOptionnalPtr->SetBufferedRegion(outputOptionnalPtr->GetRequestedRegion() );
	outputOptionnalPtr->Allocate();
      }
  }

  /**
   * Method AfterThreadedGenerateData
   */
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  void
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::AfterThreadedGenerateData()
  {    
  }
  

  /**
   * Method ThreadedGenerateData
   */
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  void
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::ThreadedGenerateData(const ImageOutRegionType & outputRegionForThread,
			 itk::ThreadIdType threadId)
  {
    if (this->GetTopographicPhaseInput() == nullptr)
      {
	// Process for line
	this->ThreadedGenerateDataWithoutTopographicPhase(outputRegionForThread,threadId);
      } 
    else
      {
	// Process for column
	this->ThreadedGenerateDataWithTopographicPhase(outputRegionForThread,threadId);
      } 
  }

  /**
   * Method ThreadedGenerateDataWithoutTopographicPhase
   */
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  void
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::ThreadedGenerateDataWithoutTopographicPhase(const ImageOutRegionType & outputRegionForThread,
						itk::ThreadIdType threadId)
  {
    // Compute corresponding input region for SAR images
    ImageSARRegionType inputRegionForThread = OutputRegionToInputRegion(outputRegionForThread, true);
  
    //  Define/declare an iterator that will walk the input regions for this
    // thread. 
    InputSARIterator  inMasterIt(this->GetMasterInput(), inputRegionForThread);
    InputSARIterator  inSlaveIt(this->GetSlaveInput(), inputRegionForThread);

    // Define/declare an iterator that will walk the output region for this
    // thread.
    OutputIterator outIt(this->GetOutput(), outputRegionForThread);

    // Support progress methods/callbacks
    itk::ProgressReporter progress( this, threadId, outputRegionForThread.GetNumberOfPixels() );

    inMasterIt.GoToBegin();
    inSlaveIt.GoToBegin();
    outIt.GoToBegin();

    // Get the number of column of the region
    const ImageOutSizeType &   outputRegionForThreadSize = outputRegionForThread.GetSize();
    unsigned int nbCol = static_cast<unsigned int>(outputRegionForThreadSize[0]); 
  
    // Allocate Arrays for accumulations (size = nbCol)
    double *complexMulConjTab_Re = new double[nbCol];
    double *complexMulConjTab_Im = new double[nbCol];
    double *complexMulConjTab_Mod = new double[nbCol];
    
    // Output Pixel (VectorImage Pixel)
    ImageOutPixelType outPixel;
    outPixel.Reserve(4);

    //// Initialize the indLastL and indFirstL (for spending our averaging with a margin) ////
    int indFirstL = 0;
    int indLastL = m_MLAzi+ m_MarginAzi;
    int marginAzi = (int) m_MarginAzi;
    
    if (inMasterIt.GetIndex()[1] >= marginAzi && marginAzi != 0)
      {
	indFirstL = -marginAzi;
      }
	
    if ((inMasterIt.GetIndex()[1] + indLastL - indFirstL) > (m_nbLinesSAR-1))
      {
	indLastL = (m_nbLinesSAR-1) - inMasterIt.GetIndex()[1] + indFirstL;
      }

    bool backTopreviousLines = false;
    bool backTopreviousColunms = false;
    

    // For each Line
    while ( !inMasterIt.IsAtEnd() && !inSlaveIt.IsAtEnd() && !outIt.IsAtEnd())
      {
	// reinitialize
	for (unsigned int j = 0; j < nbCol; j++) 
	  {
	    complexMulConjTab_Re[j] = 0;
	    complexMulConjTab_Im[j] = 0;
	    complexMulConjTab_Mod[j] = 0;
	  }

	if (backTopreviousLines)
	  {
	    //// Previous index in azimut (for averaging) ////
	    if (inMasterIt.GetIndex()[1] >= marginAzi && marginAzi != 0)
	      {
		indFirstL = -marginAzi;
	      }

	    ImageSARIndexType indSARL;
	    indSARL[0] = inMasterIt.GetIndex()[0];
	    indSARL[1] = inMasterIt.GetIndex()[1] - (2*marginAzi);
	
	    if ((indSARL[1] + indLastL - indFirstL) > (m_nbLinesSAR-1))
	      {
		indLastL = (m_nbLinesSAR - 1) - indSARL[1] + indFirstL;
	      }

	    // Check previous index
	    if (indSARL[1] >= 0 && marginAzi != 0)
	      {
		// Previous index inputs
		inMasterIt.SetIndex(indSARL);
		inSlaveIt.SetIndex(indSARL);
	
		indFirstL = -marginAzi;
	      }
	  }

	backTopreviousLines = true;


	// GenerateInputRequestedRegion function requires an input region with In_nbLine = m_Averaging * Out_nbLine
	for (int i = indFirstL ; i < indLastL; i++) 
	  {
	    inMasterIt.GoToBeginOfLine();
	    inSlaveIt.GoToBeginOfLine();
	    outIt.GoToBeginOfLine();
	  
	    int colCounter = 0;
	    backTopreviousColunms = false;

	    //// Initialize the indLastC and indFirstC (for spending average with a margin) ////
	    int indFirstC = 0;
	    int indLastC = m_MLRan + m_MarginRan;
	    int marginRan = (int) m_MarginRan;
    
	    if (inMasterIt.GetIndex()[0] >= marginRan  && marginRan != 0)
	      {
		indFirstC = -marginRan;
	      }
	
	    if ((inMasterIt.GetIndex()[0] + indLastC - indFirstC) > (m_nbColSAR-1))
	      {
		indLastC = (m_nbColSAR-1) - inMasterIt.GetIndex()[0] + indFirstC;
	      }


	    // For each column
	    while (!inMasterIt.IsAtEndOfLine() && !inSlaveIt.IsAtEndOfLine() && !outIt.IsAtEndOfLine())
	      {
		if (backTopreviousColunms)
		  {
		    //// Previous index in range (for averaging) ////
		    if (inMasterIt.GetIndex()[0] >= marginRan  && marginRan != 0)
		      {
			indFirstC = -marginRan;
		      }

		    ImageSARIndexType indSARC;
		    indSARC[0] = inMasterIt.GetIndex()[0] - (2*marginRan);
		    indSARC[1] = inMasterIt.GetIndex()[1];
		
		    if ((indSARC[0] + indLastC - indFirstC) > (m_nbColSAR-1))
		      {
			indLastC = (m_nbColSAR-1) - indSARC[0] + indFirstC;
		      }

		    // Check previous index
		    if (indSARC[0] >= 0 && marginRan != 0)
		      {
			// Previous index inputs
			inMasterIt.SetIndex(indSARC);
			inSlaveIt.SetIndex(indSARC);
		
			indFirstC = -marginRan;
		      }
	
		  }

		backTopreviousColunms = true;

		for (int k = indFirstC ; k < indLastC; k++) 
		  {
		    if (!inMasterIt.IsAtEndOfLine() && !inSlaveIt.IsAtEndOfLine())
		      {
			// Complex interferogram (master * conj(slave))
			double real_interfero = (inMasterIt.Get().real()*inSlaveIt.Get().real() + 
						 inMasterIt.Get().imag()*inSlaveIt.Get().imag());

			double imag_interfero = (inMasterIt.Get().imag()*inSlaveIt.Get().real() - 
						 inMasterIt.Get().real()*inSlaveIt.Get().imag());

			///////////// Accumulations ///////////////
			complexMulConjTab_Re[colCounter] += real_interfero;

			
			complexMulConjTab_Im[colCounter] += imag_interfero;
			
			
			complexMulConjTab_Mod[colCounter] += sqrt((real_interfero*real_interfero) + 
								  (imag_interfero*imag_interfero));

			// Next colunm inputs
			++inMasterIt;
			++inSlaveIt;
		      }
		  }
	      	      
		// Estiamte and Assigne outIt
		if (i == (indLastL-1))
		  {
		    ///////////// Estimations of amplitude, phase and coherency ///////////////
		    double mod_Acc = sqrt(complexMulConjTab_Re[colCounter]*complexMulConjTab_Re[colCounter] + 
					  complexMulConjTab_Im[colCounter]*complexMulConjTab_Im[colCounter]);

		    // Amplitude
		    outPixel[0] = m_Gain * sqrt((complexMulConjTab_Mod[colCounter]/(m_MLRan*m_MLAzi)));
		    
		    // Phase
		    outPixel[1] = std::atan2(complexMulConjTab_Im[colCounter], 
					     complexMulConjTab_Re[colCounter]);

		    // Mod 2*Pi
		    outPixel[1] =  outPixel[1]-(2*M_PI)*floor(outPixel[1]/(2*M_PI));
		    
		    // Coherency
		    outPixel[2] = mod_Acc / complexMulConjTab_Mod[colCounter] ;
	  
		    // IsData always set to 1
		    outPixel[3] = 1;
		    
		    // Assigne Main output (ML geometry) 
		    outIt.Set(outPixel);
		    progress.CompletedPixel();		  
		  }

		// Next colunm output
		++outIt;
		colCounter++;
	      }
	  
	    // Next line intputs
	    inMasterIt.NextLine();
	    inSlaveIt.NextLine();
	  }
      
	// Next line output
	outIt.NextLine();
   
      }
  delete [] complexMulConjTab_Re;
  delete [] complexMulConjTab_Im;
  delete [] complexMulConjTab_Mod;
  }

  /**
   * Method ThreadedGenerateDataWithTopographicPhase
   */
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  void
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::ThreadedGenerateDataWithTopographicPhase(const ImageOutRegionType & outputRegionForThread,
					     itk::ThreadIdType threadId)
  {
    // Support progress methods/callbacks
    itk::ProgressReporter progress( this, threadId, outputRegionForThread.GetNumberOfPixels() );

    // Output Pixel (VectorImage Pixel)
    ImageOutPixelType outPixel;
    outPixel.Reserve(4);

    // Two possible outputs => Two kinds of streaming and calculation :
    // m_OutputCounter = 0 => Main output and main calculation
    // m_OutputCounter = 1 => Optionnal output (ortho geo) and copy of accumulation into ortho geo to ortho 
    // image
    if (m_OutputCounter == 1)
      {   
	// Compute corresponding input region for SAR images
	ImageSARRegionType inputRegionForThread = OutputRegionToInputRegion(outputRegionForThread, true);
  
	// Compute corresponding input region for Topographic phase image
	bool sameGeoAsMainOutput; // If true => Geo TopoPhase == main output geo else Geo TopoPhase = SAR Geo    
	ImagePhaseRegionType inputPhaseRegionForThread = OutputRegionToInputPhaseRegion(outputRegionForThread,
											sameGeoAsMainOutput, true);
	//  Define/declare an iterator that will walk the input regions for this
	// thread. 
	InputSARIterator  inMasterIt(this->GetMasterInput(), inputRegionForThread);
	InputSARIterator  inSlaveIt(this->GetSlaveInput(), inputRegionForThread);

	InputPhaseIterator inTopoPhaseIt(this->GetTopographicPhaseInput(), inputPhaseRegionForThread);

	// Define/declare an iterator that will walk the output region for this
	// thread.
	OutputIterator outIt(this->GetOutput(), outputRegionForThread);
    
	inMasterIt.GoToBegin();
	inSlaveIt.GoToBegin();
	inTopoPhaseIt.GoToBegin();
	outIt.GoToBegin();

	// Get the number of column of the region
	const ImageOutSizeType &   outputRegionForThreadSize = outputRegionForThread.GetSize();
	unsigned int nbCol = static_cast<unsigned int>(outputRegionForThreadSize[0]); 
  
	// Allocate Arrays for accumulations (size = nbCol)
	double *complexMulConjTab_Re = new double[nbCol];
	double *complexMulConjTab_Im = new double[nbCol];
	double *complexMulConjTab_Mod = new double[nbCol];
	int *isDataCounter = new int[nbCol];

	Point2DType groundPoint;
	itk::ContinuousIndex<double, 2> groundContinousIndex;

	//// Initialize the indLastL and indFirstL (for spending our averaging with a margin) ////
	int indFirstL = 0;
	int indLastL = m_MLAzi+ m_MarginAzi;
	int marginAzi = (int) m_MarginAzi;
    
	if (inMasterIt.GetIndex()[1] >= marginAzi && marginAzi != 0)
	  {
	    indFirstL = -marginAzi;
	  }
      
	if ((inMasterIt.GetIndex()[1] + (indLastL - indFirstL)) > (m_nbLinesSAR-1))
	  {
	    indLastL = (m_nbLinesSAR-1) + indFirstL - inMasterIt.GetIndex()[1];
	  }
    

	bool backTopreviousLines = false;
	bool backTopreviousColunms = false;
    
	//////// For each Line /////////
	while ( !inMasterIt.IsAtEnd() && !inSlaveIt.IsAtEnd() && !inTopoPhaseIt.IsAtEnd() && !outIt.IsAtEnd())
	  {
	    // reinitialize
	    for (unsigned int j = 0; j < nbCol; j++) 
	      {
		complexMulConjTab_Re[j] = 0;
		complexMulConjTab_Im[j] = 0;
		complexMulConjTab_Mod[j] = 0;
		isDataCounter[j] = 0;
	      }

	    if (backTopreviousLines)
	      {
		//// Previous index in azimut (for averaging) ////
		if (inMasterIt.GetIndex()[1] >= marginAzi && marginAzi != 0)
		  {
		    indFirstL = -marginAzi;
		  }

		ImageSARIndexType indSARL;
		indSARL[0] = inMasterIt.GetIndex()[0];
		indSARL[1] = inMasterIt.GetIndex()[1] - (2*marginAzi);

		ImagePhaseIndexType indPhaL;
		indPhaL[0] = inTopoPhaseIt.GetIndex()[0];
		indPhaL[1] = inTopoPhaseIt.GetIndex()[1] - (2*marginAzi);
	
	
		if ((indSARL[1] + indLastL - indFirstL) > (m_nbLinesSAR-1))
		  {
		    indLastL = (m_nbLinesSAR - 1) - indSARL[1] + indFirstL;
		  }

		// Check previous index
		if (indSARL[1] >= 0 && indPhaL[1] >= 0 && marginAzi != 0)
		  {
		    // Previous index inputs
		    inMasterIt.SetIndex(indSARL);
		    inSlaveIt.SetIndex(indSARL);
		    // If topographic phase has SAR geo
		    if (!sameGeoAsMainOutput)
		      {
			inTopoPhaseIt.SetIndex(indPhaL);
		      }

		    indFirstL = -marginAzi;
		  }
	      }

	    backTopreviousLines = true;


	    // GenerateInputRequestedRegion function requires an input region with 
	    // In_nbLine = m_Averaging * Out_nbLine (Averaing in function of ML Factors and Margins)
	    for (int i = indFirstL ; i < indLastL; i++) 
	      {
		inMasterIt.GoToBeginOfLine();
		inSlaveIt.GoToBeginOfLine();
		inTopoPhaseIt.GoToBeginOfLine();
		outIt.GoToBeginOfLine();
	  
		int colCounter = 0;
		backTopreviousColunms = false;

		//// Initialize the indLastC and indFirstC (for spending average with a margin) ////
		int indFirstC = 0;
		int indLastC = m_MLRan + m_MarginRan;
		int marginRan = (int) m_MarginRan;

		if (inMasterIt.GetIndex()[0] >= marginRan  && marginRan != 0)
		  {
		    indFirstC = -marginRan;
		  }

		if ((inMasterIt.GetIndex()[0] + (indLastC - indFirstC)) > (m_nbColSAR-1))
		  {
		    indLastC = (m_nbColSAR-1) + indFirstC - inMasterIt.GetIndex()[0];
		  }


		///////// For each column /////////
		while (!inMasterIt.IsAtEndOfLine() && !inSlaveIt.IsAtEndOfLine() && !inTopoPhaseIt.IsAtEndOfLine() 
		       && !outIt.IsAtEndOfLine())
		  {

		    if (backTopreviousColunms)
		      {
			//// Previous index in range (for averaging) ////
			if (inMasterIt.GetIndex()[0] >= marginRan  && marginRan != 0)
			  {
			    indFirstC = -marginRan;
			  }

			ImageSARIndexType indSARC;
			indSARC[0] = inMasterIt.GetIndex()[0] - (2*marginRan);
			indSARC[1] = inMasterIt.GetIndex()[1];

			ImagePhaseIndexType indPhaC;
			indPhaC[0] = inTopoPhaseIt.GetIndex()[0] - (2*marginRan);
			indPhaC[1] = inTopoPhaseIt.GetIndex()[1]; 
	
		
			if ((indSARC[0] + indLastC - indFirstC) > (m_nbColSAR-1))
			  {
			    indLastC = (m_nbColSAR-1) - indSARC[0] + indFirstC;
			  }

			// Check previous index
			if (indSARC[0] >= 0 && indPhaC[0] >= 0 && marginRan != 0)
			  {
			    // Previous index inputs
			    inMasterIt.SetIndex(indSARC);
			    inSlaveIt.SetIndex(indSARC);
			    // If topographic phase has SAR geo
			    if (!sameGeoAsMainOutput)
			      {
				inTopoPhaseIt.SetIndex(indPhaC);
			      }

			    indFirstC = -marginRan;
			  }
	
		      }

		    backTopreviousColunms = true;
	 

		    for (int k = indFirstC ; k < indLastC; k++) 
		      {
			if (!inMasterIt.IsAtEndOfLine() && !inSlaveIt.IsAtEndOfLine())
			  {
			    // Check isData (grom topographic phase)
			    if (inTopoPhaseIt.Get()[1] != 0)
			      {			
				// Complex Raw interferogram (master * conj(slave))
				double real_RawInterfero = (inMasterIt.Get().real()*inSlaveIt.Get().real() + 
							    inMasterIt.Get().imag()*inSlaveIt.Get().imag());

				double imag_RawInterfero = (inMasterIt.Get().imag()*inSlaveIt.Get().real() - 
							    inMasterIt.Get().real()*inSlaveIt.Get().imag());

				///////////// Topographoc phase as complex number ////////////////
				double topoPhase = inTopoPhaseIt.Get()[0];
				
				if (m_ApproxDiapason)
				  {
				    int topoPhase_int = static_cast<int>(std::round(inTopoPhaseIt.Get()[0]))% 
				      256;
				    topoPhase = 2*M_PI*topoPhase_int / 256; 
				  }

				double complexTopoPhase_Re = std::cos(topoPhase);
				double complexTopoPhase_Im = std::sin(topoPhase);

	
				// Multiply the conj(complexTopoPhase) with complex raw interferogram
				double real_interfero = (real_RawInterfero * complexTopoPhase_Re + 
							 imag_RawInterfero * complexTopoPhase_Im);
				double imag_interfero = (imag_RawInterfero * complexTopoPhase_Re - 
							 real_RawInterfero * complexTopoPhase_Im);

				///////////// Accumulations ///////////////
				complexMulConjTab_Re[colCounter] += real_interfero;

			
				complexMulConjTab_Im[colCounter] += imag_interfero;
			
			
				complexMulConjTab_Mod[colCounter] += sqrt((real_interfero*real_interfero) + 
									  (imag_interfero*imag_interfero));

				isDataCounter[colCounter] += 1; 

				// Accumulation for ortho geo (only if k and i betwwen 0 and ml Factors)
				if (m_UseDEMGeoAsOutput && (i >= 0 && i < (int) m_MLAzi) && (k >= 0 && k < (int) m_MLRan))
				  {
				    // Determine the index on ortho image
				    ossimEcefPoint ecefPt;
				    ecefPt.x() = inTopoPhaseIt.Get()[2];
				    ecefPt.y() = inTopoPhaseIt.Get()[3];
				    ecefPt.z() = inTopoPhaseIt.Get()[4];
				    ossimGpt gptPt(ecefPt);
				    groundPoint[0] = gptPt.lon; 
				    groundPoint[1] = gptPt.lat;
				    
				    m_DEMPtr->TransformPhysicalPointToContinuousIndex(groundPoint, 
										      groundContinousIndex);

				    int groundIndexC = static_cast<int>(std::round(groundContinousIndex[0]));
				    int groundIndexL = static_cast<int>(std::round(groundContinousIndex[1]));

				    int groundIndex = groundIndexL*m_nbColDEM + groundIndexC;

				    // Accumulate into OrthoAccByThread at threadId and at groundIndex
				    /*m_OrthoRealAccByThread[threadId][groundIndex] += real_interfero;
				      m_OrthoImagAccByThread[threadId][groundIndex] += imag_interfero;
				      m_OrthoModAccByThread[threadId][groundIndex] += sqrt((real_interfero*
				      real_interfero) + 
				      (imag_interfero*
				      imag_interfero));

				      // Increment counter
				      ++m_OrthoCounterByThread[threadId];*/

				    m_Mutex->Lock();
				    
				    m_OrthoRealAcc[groundIndex] += real_interfero;
				    m_OrthoImagAcc[groundIndex] += imag_interfero;
				    m_OrthoModAcc[groundIndex] += sqrt((real_interfero*real_interfero) + 
								       (imag_interfero*imag_interfero));

				    ++m_OrthoCounter[groundIndex];

				    m_Mutex->Unlock();

				    
				  }
			      }

			    // Next colunm inputs
			    ++inMasterIt;
			    ++inSlaveIt;
			    // If topographic phase has SAR geo
			    if (!sameGeoAsMainOutput)
			      {
				++inTopoPhaseIt;
			      }
			  }
		      } // End for k (indFirstC to indLastC)
	      	      
		    // Estiamte and Assigne outIt
		    //if (i == (m_MLAzi-1))
		    if (i == (indLastL-1))
		      {
			// Check if Data
			if (isDataCounter[colCounter] >= 1)
			  {
			    ///////////// Estimations of amplitude, phase and coherency ///////////////
			    double mod_Acc = sqrt(complexMulConjTab_Re[colCounter]*complexMulConjTab_Re[colCounter]
						  + complexMulConjTab_Im[colCounter]*
						  complexMulConjTab_Im[colCounter]);

			    int countPixel = (m_MLRan + 2*m_MarginRan) * (m_MLAzi + 2*m_MarginAzi);

			    // Amplitude
			    outPixel[0] = m_Gain * sqrt((complexMulConjTab_Mod[colCounter]/(countPixel)));
		    
			    // Phase
			    outPixel[1] = std::atan2(complexMulConjTab_Im[colCounter], 
						     complexMulConjTab_Re[colCounter]);

			    // Mod 2*Pi
			    outPixel[1] =  outPixel[1]-(2*M_PI)*floor(outPixel[1]/(2*M_PI));

			    if (m_ApproxDiapason)
			      {
				outPixel[1] = std::atan2(complexMulConjTab_Im[colCounter], 
							 complexMulConjTab_Re[colCounter])* 128 *  M_1_PI;

				outPixel[1] =  outPixel[1]-(256)*floor(outPixel[1]/(256));
				
				outPixel[1] = std::round(outPixel[1]);
			      }

			    // Coherency
			    if (complexMulConjTab_Mod[colCounter] != 0)
			      {
				outPixel[2] = mod_Acc / complexMulConjTab_Mod[colCounter];
			      }
			    else
			      {
				outPixel[2] = 0;
			      }

			    // isData set to 1 
			    outPixel[3] = 1;
			  }
			else
			  {
			    outPixel[0] = 0;
			    outPixel[1] = 0;
			    outPixel[2] = 0;
			    outPixel[3] = 0;
			  }
		    
			outIt.Set(outPixel);
			progress.CompletedPixel();		  
		      }

		    // Next colunm output
		    ++outIt;
		    // If topographic phase has Main output geo
		    if (sameGeoAsMainOutput)
		      {
			++inTopoPhaseIt;
		      }
		    colCounter++;
		  }
	  
		// Next line intputs
		inMasterIt.NextLine();
		inSlaveIt.NextLine();
		// If topographic phase has SAR geo
		if (!sameGeoAsMainOutput)
		  {
		    inTopoPhaseIt.NextLine();
		  }
	    
	      } // End for i (firstL to lastL)

	    // Next line output
	    outIt.NextLine();
	    // If topographic phase has Main output geo
	    if (sameGeoAsMainOutput)
	      {
		inTopoPhaseIt.NextLine();
	      }
   
	  }
    delete [] complexMulConjTab_Re;
    delete [] complexMulConjTab_Im;
    delete [] complexMulConjTab_Mod;
    delete [] isDataCounter;
      }

    //////////// Optionnal output ////////////////
    // Copy for optionnal output (ortho image)
    if (m_OutputCounter == 2 && m_UseDEMGeoAsOutput)
      {
	// Streming is on main output or orhto geo is very different => to guarantee Thread-safety only 
	// threadId = 0 makes the copy (the whole copy)
	if (threadId == 0)
	  {
	    int sizeDEM = m_nbColDEM * m_nbLinesDEM;
	    ImageOutIndexType outDEMIndex;
	    
	    for (int indOrtho = 0; indOrtho < sizeDEM; indOrtho++)
	      {
		// Index 
		outDEMIndex[0] = indOrtho % m_nbColDEM;
		outDEMIndex[1] = indOrtho / m_nbColDEM; // ninteger division

		// Amplitude, Phase and coherency
		if (m_OrthoCounter[indOrtho] != 0)
		  {
		    double mod_Acc = sqrt(m_OrthoRealAcc[indOrtho]*m_OrthoRealAcc[indOrtho]
					  + m_OrthoImagAcc[indOrtho]* m_OrthoImagAcc[indOrtho]);


		    // Amplitude
		    outPixel[0] = m_Gain * sqrt(m_OrthoModAcc[indOrtho]/m_OrthoCounter[indOrtho]);
		    
		    // Phase
		    outPixel[1] = std::atan2(m_OrthoImagAcc[indOrtho], 
					     m_OrthoRealAcc[indOrtho]);

		    // Mod 2*Pi
		    outPixel[1] =  outPixel[1]-(2*M_PI)*floor(outPixel[1]/(2*M_PI));

		    if (m_ApproxDiapason)
		      {
			outPixel[1] = std::atan2(m_OrthoImagAcc[indOrtho], 
						 m_OrthoRealAcc[indOrtho])* 128 *  M_1_PI;
			
			//outPixel[1] =  outPixel[1]-(256)*floor(outPixel[1]/(256));
				
			outPixel[1] = std::round(outPixel[1]);
		      }

		    // Coherency
		    if (m_OrthoModAcc[indOrtho] != 0)
		      {
			outPixel[2] = mod_Acc / m_OrthoModAcc[indOrtho];
		      }
		    else
		      {
			outPixel[2] = 0;
		      }

		    // isData set to 1 
		    outPixel[3] = 1;
		    
		  }
		else
		  {
		    outPixel[0] = 0;
		    outPixel[1] = 0;
		    outPixel[2] = 0;
		    outPixel[3] = 0;
		  }

		this->GetOutputIntoDEMGeo()->SetPixel(outDEMIndex, outPixel);
	      }
	  }
      }
  }

} /*namespace otb*/

#endif
