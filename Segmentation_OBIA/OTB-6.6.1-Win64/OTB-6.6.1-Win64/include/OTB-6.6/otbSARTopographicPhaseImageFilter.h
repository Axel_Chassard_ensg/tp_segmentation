/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARTopographicPhaseImageFilter_h
#define otbSARTopographicPhaseImageFilter_h

#include "itkImageToImageFilter.h"
#include "itkSmartPointer.h"
#include "itkPoint.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"

#include "otbImageKeywordlist.h"
#include "otbSarSensorModelAdapter.h"

namespace otb
{
/** \class SARTopographicPhaseImageFilter 
 * \brief Estimates the topographic phase between two SAR Images in function of DEM. 
 * 
 * This topographic phase can be used to create the interferogram.
 * This filter uses the Cartesian mean (per pixel and per lines) for all DEM points projection into SAR Images.
 * 
 * Five inputs are required :
 * _ Cartesian mean per pixel into Master SAR or ML geometry 
 * _ Cartesian mean per line into Master SAR or ML geometry
 * _ Cartesian mean per pixel into Slave SAR or ML geometry 
 * _ Cartesian mean per line into Slave SAR or ML geometry
 * _ A shift grid to adapt slave SAR or ML geometry to master SAR or ML geometry 
 *
 *
 * The output is a vector image that contains :
 * _ topographic phase (P = (2*Pi/ lambda) * (De - Dm) with lambda = F_carrier/c and De or Dm distance between
 * DEM point and satellite position).
 * _ A copy of Cartesian mean per pixel into Master SAR geometry (useful to change into DEM geometry).
 *
 * The output and the first input have the same dimensions.
 *
 * \ingroup DiapOTBModule
 */

  template <typename TImageIn, typename TImageOut> 
  class ITK_EXPORT SARTopographicPhaseImageFilter :
    public itk::ImageToImageFilter<TImageIn,TImageOut>
{
public:

  // Standard class typedefs
  typedef SARTopographicPhaseImageFilter                    Self;
  typedef itk::ImageToImageFilter<TImageIn,TImageOut>    Superclass;
  typedef itk::SmartPointer<Self>                        Pointer;
  typedef itk::SmartPointer<const Self>                  ConstPointer;

  // Method for creation through object factory
  itkNewMacro(Self);
  // Run-time type information
  itkTypeMacro(SARTopographicPhaseImageFilter,ImageToImageFilter);

  /** Typedef to image inputs type VectorImage   */
  typedef TImageIn                                  ImageInType;
  /** Typedef to describe the inout image pointer type. */
  typedef typename ImageInType::Pointer             ImageInPointer;
  typedef typename ImageInType::ConstPointer        ImageInConstPointer;
  /** Typedef to describe the inout image region type. */
  typedef typename ImageInType::RegionType          ImageInRegionType;
  /** Typedef to describe the type of pixel and point for inout image. */
  typedef typename ImageInType::PixelType           ImageInPixelType;
  typedef typename ImageInType::PointType           ImageInPointType;
  /** Typedef to describe the image index, size types and spacing for inout image. */
  typedef typename ImageInType::IndexType           ImageInIndexType;
  typedef typename ImageInType::IndexValueType      ImageInIndexValueType;
  typedef typename ImageInType::SizeType            ImageInSizeType;
  typedef typename ImageInType::SizeValueType       ImageInSizeValueType;
  typedef typename ImageInType::SpacingType         ImageInSpacingType;
  typedef typename ImageInType::SpacingValueType    ImageInSpacingValueType;
  
  /** Typedef to image output type : otb::VectorImage  */
  typedef TImageOut                                  ImageOutType;
  /** Typedef to describe the output image pointer type. */
  typedef typename ImageOutType::Pointer             ImageOutPointer;
  typedef typename ImageOutType::ConstPointer        ImageOutConstPointer;
  /** Typedef to describe the output image region type. */
  typedef typename ImageOutType::RegionType          ImageOutRegionType;
  /** Typedef to describe the type of pixel and point for output image. */
  typedef typename ImageOutType::PixelType           ImageOutPixelType;
  typedef typename ImageOutType::PointType           ImageOutPointType;
  /** Typedef to describe the image index, size types and spacing for output image. */
  typedef typename ImageOutType::IndexType           ImageOutIndexType;
  typedef typename ImageOutType::IndexValueType      ImageOutIndexValueType;
  typedef typename ImageOutType::SizeType            ImageOutSizeType;
  typedef typename ImageOutType::SizeValueType       ImageOutSizeValueType;
  typedef typename ImageOutType::SpacingType         ImageOutSpacingType;
  typedef typename ImageOutType::SpacingValueType    ImageOutSpacingValueType;

  // Typedef for grid
  typedef otb::VectorImage<float>                    GridType;
  typedef typename GridType::Pointer                 GridPointer;
  typedef typename GridType::RegionType              GridRegionType;
  typedef typename GridType::IndexType               GridIndexType;
  typedef typename GridType::IndexValueType          GridIndexValueType;
  typedef typename GridType::SizeType                GridSizeType;
  typedef typename GridType::SizeValueType           GridSizeValueType;

  // Define Point2DType and Point3DType
  using Point2DType = itk::Point<double,2>;
  using Point3DType = itk::Point<double,3>;

  // Typedef for iterators
  typedef itk::ImageScanlineConstIterator< ImageInType > InputIterator;
  typedef itk::ImageScanlineIterator< ImageOutType > OutputIterator;

   // Setter
  itkSetMacro(MLRan, unsigned int);
  itkSetMacro(MLAzi, unsigned int);
  itkSetMacro(MaxShiftInRange, float);
  itkSetMacro(MaxShiftInAzimut, float);
  itkSetMacro(MasterCopy, bool);
  itkSetMacro(ApproxDiapason, bool);
  // Getter
  itkGetMacro(MLRan, unsigned int);
  itkGetMacro(MLAzi, unsigned int);
  itkGetMacro(MaxShiftInRange, float);
  itkGetMacro(MaxShiftInAzimut, float);
  itkGetMacro(MasterCopy, bool);
  
  void SetGridStep(unsigned int stepRange, unsigned int stepAzimut)
    {
      m_GridStep[0] = stepRange;
      m_GridStep[1] = stepAzimut;

      int gridStepMax = std::max(stepRange, stepAzimut);

      // Adapt the tolerance for inputs
      this->SetCoordinateTolerance(gridStepMax);
      this->SetDirectionTolerance(gridStepMax);
    }
  unsigned int GetGridStepRange()
  {
    return m_GridStep[0];
  }
  unsigned int GetGridStepAzimut()
  {
    return m_GridStep[1];
  }
  
   void SetSlaveImageKeyWorList(ImageKeywordlist sarSlaveKWL);

  // Setter/Getter for inputs (Cartesian mean per pixel and per line) 
  /** Connect one of the operands for topographic phase estimation : Master Cartesian Mean */
  void SetMasterCartesianMeanInput( const ImageInType* image);

  /** Connect one of the operands for topographic phase estimation: Master Cartesian Mean Per line */
  void SetMasterCartesianMeanPerLineInput( const ImageInType* image);

  /** Connect one of the operands for topographic phase estimation : Slave Cartesian Mean */
  // void SetSlaveCartesianMeanInput( const ImageInType* image);

  /** Connect one of the operands for topographic phase estimation : Slave Cartesian Mean Per line */
  void SetSlaveCartesianMeanPerLineInput( const ImageInType* image);

  void SetGridInput( const GridType* image);

  /** Get the inputs */
  const ImageInType* GetMasterCartesianMeanInput() const;
  const ImageInType* GetMasterCartesianMeanPerLineInput() const;
  //const ImageInType* GetSlaveCartesianMeanInput() const;
  const ImageInType* GetSlaveCartesianMeanPerLineInput() const;
  const GridType * GetGridInput() const;

protected:
  // Constructor
  SARTopographicPhaseImageFilter();

  // Destructor
  virtual ~SARTopographicPhaseImageFilter() ITK_OVERRIDE;

  // Print
  void PrintSelf(std::ostream & os, itk::Indent indent) const ITK_OVERRIDE;
  
  /** SARTopographicPhaseImageFilter produces an image which are into output geometry. The differences between 
   * output and input images can be the size of images and the dimensions. 
   * As such, SARTopographicPhaseImageFilter needs to provide an implementation for 
   * GenerateOutputInformation() in order to inform the pipeline execution model. 
   */ 
  virtual void GenerateOutputInformation() ITK_OVERRIDE;

  /** SARTopographicPhaseImageFilter needs a input requested region that corresponds to the margin and shifts 
   * into the requested region of our output requested region. The output requested region needs to be modified
   * to construct as wanted tiles with input size.
   * As such, TilesAnalysesImageFilter needs to provide an implementation for 
   * GenerateInputRequestedRegion() in order to inform the pipeline execution model. 
   * \sa ProcessObject::GenerateInputRequestedRegion() */
  virtual void GenerateInputRequestedRegion() ITK_OVERRIDE;

  /**
   * OutputRegionToInputRegion returns the input Slave Cartesian Per Line Mean  
   */
  ImageInRegionType OutputRegionToSlavePerLineInputRegion(const ImageOutRegionType& outputRegion) const;

  GridRegionType OutputRegionToInputGridRegion(const ImageOutRegionType& outputRegion) const;

  /** 
   * SARTopographicPhaseImageFilterr can be implemented as a multithreaded filter.
   * Therefore, this implementation provides a ThreadedGenerateData() routine
   * which is called for each processing thread. The main output image data is
   * allocated automatically by the superclass prior to calling
   * ThreadedGenerateData().  ThreadedGenerateData can only write to the
   * portion of the output image specified by the parameter
   * "outputRegionForThread"
   *
   * \sa ImageToImageFilter::ThreadedGenerateData(),
   *     ImageToImageFilter::GenerateData() */
  virtual void ThreadedGenerateData(const ImageOutRegionType& outputRegionForThread, 
				    itk::ThreadIdType threadId) ITK_OVERRIDE;

 private:
  SARTopographicPhaseImageFilter(const Self&); // purposely not implemented
  void operator=(const Self &); // purposely not 

  // ML factor in range 
  unsigned int m_MLRan;
  
  // ML factor in azimut
  unsigned int m_MLAzi;

  // Grid Step (SLC/ SAR Geometry)
  GridSizeType m_GridStep;
  
  // Max Shift in range 
  float m_MaxShiftInRange;

  // Max Shift in azimut
  float m_MaxShiftInAzimut;

  // Factor to estimate the topographic phase (=2 if monostatic and = 1 if bistatic)
  int m_Factor;

  // Lamdba
  float m_Lambda;

  // KeyWordlist for slave image
  ImageKeywordlist m_SlaveKeyWordList;

  // bool to activate master Cart Mean Copy
  bool m_MasterCopy;

  // Instance of SarSensorModelAdapter for Slave
  SarSensorModelAdapter::Pointer m_SarSensorModelAdapterForSlave;

  // Instance of SarSensorModelAdapter for Master
  SarSensorModelAdapter::Pointer m_SarSensorModelAdapterForMaster;

  int m_OutputCounter;

  bool m_ApproxDiapason;
};

} // End namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSARTopographicPhaseImageFilter.txx"
#endif



#endif
