/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARTilesCoregistrationFunctor_h
#define otbSARTilesCoregistrationFunctor_h
#include <cassert>
#include <math.h>
#include <complex>

#include "itkVariableLengthVector.h"
#include "itkRGBPixel.h"
#include "itkRGBAPixel.h"
#include "itkObject.h"
#include "itkObjectFactory.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkImageRegionConstIterator.h"

#include "itkFFTWCommon.h"
#include <omp.h>

namespace otb
{
namespace Function
{
/** \class SARTilesCoregistrationFunctor
* \brief Coregistration functions (called into TilesAnalysis Filter).
*
* \ingroup DiapOTBModule
*
*/
  template <class TImageIn, class TImageOut, class TGridIn>
class SARTilesCoregistrationFunctor : public itk::Object
{
public:
  /** Standard class typedefs */
  typedef SARTilesCoregistrationFunctor        Self;
  typedef itk::Object                   Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Method for creation through the object factory */
  itkNewMacro(Self);

  /** Runtime information */
  itkTypeMacro(SARTilesCoregistrationFunctor, itk::Object);

  /** Typedef to image input type :  otb::Image (Complex)  */
  typedef TImageIn                                  ImageInType;
  /** Typedef to describe the inout image pointer type. */
  typedef typename ImageInType::Pointer             ImageInPointer;
  typedef typename ImageInType::ConstPointer        ImageInConstPointer;
  /** Typedef to describe the inout image region type. */
  typedef typename ImageInType::RegionType          ImageInRegionType;
  /** Typedef to describe the type of pixel and point for inout image. */
  typedef typename ImageInType::PixelType           ImageInPixelType;
  typedef typename ImageInType::PointType           ImageInPointType;
  /** Typedef to describe the image index, size types and spacing for inout image. */
  typedef typename ImageInType::IndexType           ImageInIndexType;
  typedef typename ImageInType::IndexValueType      ImageInIndexValueType;
  typedef typename ImageInType::SizeType            ImageInSizeType;
  typedef typename ImageInType::SizeValueType       ImageInSizeValueType;
  typedef typename ImageInType::SpacingType         ImageInSpacingType;
  typedef typename ImageInType::SpacingValueType    ImageInSpacingValueType;
  
  /** Typedef to image output type : otb::Image (Complex)  */
  typedef TImageOut                                  ImageOutType;
  /** Typedef to describe the output image pointer type. */
  typedef typename ImageOutType::Pointer             ImageOutPointer;
  typedef typename ImageOutType::ConstPointer        ImageOutConstPointer;
  /** Typedef to describe the output image region type. */
  typedef typename ImageOutType::RegionType          ImageOutRegionType;
  /** Typedef to describe the type of pixel and point for output image. */
  typedef typename ImageOutType::PixelType           ImageOutPixelType;
  typedef typename ImageOutType::PointType           ImageOutPointType;
  /** Typedef to describe the image index, size types and spacing for output image. */
  typedef typename ImageOutType::IndexType           ImageOutIndexType;
  typedef typename ImageOutType::IndexValueType      ImageOutIndexValueType;
  typedef typename ImageOutType::SizeType            ImageOutSizeType;
  typedef typename ImageOutType::SizeValueType       ImageOutSizeValueType;
  typedef typename ImageOutType::SpacingType         ImageOutSpacingType;
  typedef typename ImageOutType::SpacingValueType    ImageOutSpacingValueType;

  /** Typedef to input grid type :  otb::VectorImage with at least two components (range deformations, 
      azimut deformations)  */
  typedef TGridIn                                  GridInType;
  /** Typedef to describe the inout image pointer type. */
  typedef typename GridInType::Pointer             GridInPointer;
  typedef typename GridInType::ConstPointer        GridInConstPointer;
  typedef typename GridInType::RegionType          GridInRegionType;
  typedef typename GridInType::PixelType           GridInPixelType;
  typedef typename GridInType::PointType           GridInPointType;
  typedef typename GridInType::IndexType           GridInIndexType;
  typedef typename GridInType::IndexValueType      GridInIndexValueType;
  typedef typename GridInType::SizeType            GridInSizeType;
  typedef typename GridInType::SizeValueType       GridInSizeValueType;
  typedef typename GridInType::SpacingType         GridInSpacingType;
  typedef typename GridInType::SpacingValueType    GridInSpacingValueType;

  // Iterators
  typedef itk::ImageRegionConstIterator<ImageInType> InItType;
  typedef itk::ImageScanlineConstIterator< ImageInType > InputIterator;
  
  // ITK proxy to the fftw library
  typedef typename itk::fftw::Proxy<double>           FFTWProxyType;
  typedef typename FFTWProxyType::ComplexType         FFTProxyComplexType;
  typedef typename FFTWProxyType::PixelType           FFTProxyPixelType;
  typedef typename FFTWProxyType::PlanType            FFTProxyPlanType;
  
  // Setter
  itkSetMacro(SizeTiles, unsigned int);
  itkSetMacro(Margin, unsigned int);
  itkSetMacro(SizeWindow, unsigned int);
  itkSetMacro(NbRampes, unsigned int);
  itkSetMacro(DopAzi, double);
  itkSetMacro(ConvToCInt16, bool);
  void SetGridParameters(GridInPointer gridPtr, unsigned int gridStepRange, unsigned int gridStepAzimut)
  {
    m_Grid = gridPtr;
    m_GridStep[0] = gridStepRange;
    m_GridStep[1] = gridStepAzimut;
  }
  // Getter
  itkGetMacro(SizeTiles, unsigned int);
  itkGetMacro(Margin, unsigned int);
  itkGetMacro(SizeWindow, unsigned int);
  itkGetMacro(NbRampes, unsigned int);
  itkGetMacro(DopAzi, double);

  /** Constructor */
  SARTilesCoregistrationFunctor()
  {
    m_ConvToCInt16 = false;
  }

  /** Destructor */
  ~SARTilesCoregistrationFunctor() override {}
  
  /**
   * Method Initialize
   */
  void Initialize(int nbThreads=1) 
  {
    m_nbThreads = nbThreads;

    /////////////////////////////////////// Prepare FFT buffer /////////////////////////////////////
    // Allocate Tab
    m_rangeFFTInTab = new FFTProxyComplexType*[nbThreads];
    m_rangeFFTOutTab = new FFTProxyComplexType*[nbThreads];
    m_rangeFFTInvInTab = new FFTProxyComplexType*[nbThreads];
    m_rangeFFTInvOutTab = new FFTProxyComplexType*[nbThreads];
    
    m_azimutFFTInTab = new FFTProxyComplexType*[nbThreads];
    m_azimutFFTOutTab = new FFTProxyComplexType*[nbThreads];
    m_azimutFFTInvInTab = new FFTProxyComplexType*[nbThreads];
    m_azimutFFTInvOutTab = new FFTProxyComplexType*[nbThreads];
    
    m_rangeFFTPlanTab = new FFTProxyPlanType[nbThreads];
    m_rangeFFTInvPlanTab = new FFTProxyPlanType[nbThreads];
    m_azimutFFTPlanTab = new FFTProxyPlanType[nbThreads];
    m_azimutFFTInvPlanTab = new FFTProxyPlanType[nbThreads];
    
    // Memory allocation for each Thread
    for (int i = 0; i < nbThreads; i++) 
      {
	// For direct FFTs
	m_rangeFFTInTab[i] = static_cast<FFTProxyComplexType*>(fftw_malloc((m_SizeWindow) * 
									   sizeof(FFTProxyComplexType)));
	m_rangeFFTOutTab[i] = static_cast<FFTProxyComplexType*>(fftw_malloc((m_SizeWindow) * 
									   sizeof(FFTProxyComplexType)));
	
	m_azimutFFTInTab[i] = static_cast<FFTProxyComplexType*>(fftw_malloc(m_SizeWindow * 
									   sizeof(FFTProxyComplexType)));
	m_azimutFFTOutTab[i] = static_cast<FFTProxyComplexType*>(fftw_malloc(m_SizeWindow * 
									    sizeof(FFTProxyComplexType)));
	
	// For inverse FFTs
	m_rangeFFTInvInTab[i] = static_cast<FFTProxyComplexType*>(fftw_malloc(m_SizeWindow * 
									   sizeof(FFTProxyComplexType)));
	m_rangeFFTInvOutTab[i] = static_cast<FFTProxyComplexType*>(fftw_malloc(m_SizeWindow * 
									       sizeof(FFTProxyComplexType)));
	m_azimutFFTInvInTab[i] = static_cast<FFTProxyComplexType*>(fftw_malloc(m_SizeWindow * 
									   sizeof(FFTProxyComplexType)));
	m_azimutFFTInvOutTab[i] = static_cast<FFTProxyComplexType*>(fftw_malloc(m_SizeWindow * 
									       sizeof(FFTProxyComplexType)));
	// For Plans
	m_rangeFFTPlanTab[i] = FFTWProxyType::Plan_dft_1d(m_SizeWindow,
							  m_rangeFFTInTab[i],
							  m_rangeFFTOutTab[i],
							  -1, // -1 = FFTW_FORWARD 
							  FFTW_ESTIMATE);
	
	m_rangeFFTInvPlanTab[i] = FFTWProxyType::Plan_dft_1d(static_cast<int>(m_SizeWindow),
							     m_rangeFFTInvInTab[i],
							     m_rangeFFTInvOutTab[i],
							     +1,  // +1 = FFTW_BACKWARD
							     FFTW_ESTIMATE);
	
	m_azimutFFTPlanTab[i] = FFTWProxyType::Plan_dft_1d(static_cast<int>(m_SizeWindow),
							   m_azimutFFTInTab[i],
							   m_azimutFFTOutTab[i],
							   -1,  // -1 = FFTW_FORWARD
							   FFTW_ESTIMATE);
	
	m_azimutFFTInvPlanTab[i] = FFTWProxyType::Plan_dft_1d(static_cast<int>(m_SizeWindow),
							     m_azimutFFTInvInTab[i],
							     m_azimutFFTInvOutTab[i],
							     +1,   // +1 = FFTW_BACKWARD
							     FFTW_ESTIMATE);
      }

    ///////////////////////////////////// Build Ramps  //////////////////////////////////////
    this->BuildRangeRamps();
    this->BuildAzimutRamps();
  }
 
  
  /**
   * Method Terminate
   */
  void Terminate() 
  {
    /////////////////////////////////////// Free FFT buffer /////////////////////////////////////
    // Free Memory for each thread
    for (unsigned int i = 0; i < m_nbThreads; i++) 
      {
	// Destroy the FFT plans
	FFTWProxyType::DestroyPlan(m_rangeFFTPlanTab[i]);
	FFTWProxyType::DestroyPlan(m_rangeFFTInvPlanTab[i]);
	FFTWProxyType::DestroyPlan(m_azimutFFTPlanTab[i]);
	FFTWProxyType::DestroyPlan(m_azimutFFTInvPlanTab[i]);

	// Free fftw buffer
	fftw_free(m_rangeFFTInTab[i]);
	fftw_free(m_rangeFFTOutTab[i]);
	fftw_free(m_rangeFFTInvInTab[i]);
	fftw_free(m_rangeFFTInvOutTab[i]);
	fftw_free(m_azimutFFTInTab[i]);
	fftw_free(m_azimutFFTOutTab[i]);
	fftw_free(m_azimutFFTInvInTab[i]);
	fftw_free(m_azimutFFTInvOutTab[i]);
      }

    // Delete pointer arrays
    delete [] m_rangeFFTInTab;
    delete [] m_rangeFFTOutTab;
    delete [] m_rangeFFTInvInTab;
    delete [] m_rangeFFTInvOutTab;
    delete [] m_azimutFFTInTab;
    delete [] m_azimutFFTOutTab;
    delete [] m_azimutFFTInvInTab;
    delete [] m_azimutFFTInvOutTab;

    delete [] m_rangeFFTPlanTab;
    delete [] m_rangeFFTInvPlanTab;
    delete [] m_azimutFFTPlanTab;
    delete [] m_azimutFFTInvPlanTab;
    
    ////////////////////////////////////// Free Ramps buffer ////////////////////////////////////
    delete [] m_RampesRange;
    m_RampesRange = 0;
    delete [] m_RampesAzimut;
    m_RampesAzimut = 0;
  }

  
  /**
   * Method Process
   */
  void Process(ImageInPointer inputPtr, ImageOutRegionType outputRegion, ImageOutPixelType * TilesResult)
  {    
    // Get current thread id
    int threadId = omp_get_thread_num();

    // Chekc if the output Tile is cut 
    unsigned int lastInd = outputRegion.GetSize()[0]*outputRegion.GetSize()[1];
    if (lastInd < (m_SizeTiles*m_SizeTiles))
      {
	for (unsigned int i = 0; i < lastInd; i++)
	  {
	    TilesResult[i] = 0;
	  }
	return;
      }

    ///////////////////////// Transpose outputRegion (current tile) into Input Image ///////////////////////
    // Get the index of current tile into grid to retrive the shifts (the closest (round) grid point at the 
    // center of current tile). Output Geo = Master Geo = (Grid geo / GridStep) 
    int Lgrid =  std::round( ( 0.5*(m_SizeTiles+1) + outputRegion.GetIndex()[1] ) / m_GridStep[1]); 
    int Cgrid =  std::round( ( 0.5*(m_SizeTiles+1) + outputRegion.GetIndex()[0] ) / m_GridStep[0]); 
    
    Lgrid = std::min (std::max (Lgrid, 0), static_cast<int>(m_Grid->GetLargestPossibleRegion().GetSize()[1])-1); 
    Cgrid = std::min (std::max (Cgrid, 0), static_cast<int>(m_Grid->GetLargestPossibleRegion().GetSize()[0]-1)); 

    GridInIndexType gridIndex;
    gridIndex[0] = Cgrid;
    gridIndex[1] = Lgrid;


    // Pour un test les decalages de la grille en dur 
    //double gridShift_Ran = -11.65;
    //double gridShift_Azi = 20.88;
    double gridShift_Ran = m_Grid->GetPixel(gridIndex)[0];
    double gridShift_Azi = m_Grid->GetPixel(gridIndex)[1];


    // Assignate input to output
    ImageInRegionType inputRegion = outputRegion;
        
    // Apply on input, the integer shifts 
    // (0 : range shift and 1 : azimut shift)
    double CeR = outputRegion.GetIndex()[0] + gridShift_Ran + 1; 
    double LeR = outputRegion.GetIndex()[1] + gridShift_Azi + 1;

    double Ce1R = CeR - m_Margin;
    double Le1R = LeR - m_Margin;

    int Ce1f = std::round(Ce1R);
    int Le1f = std::round(Le1R);
    
    ImageInIndexType index_InRegion;
    index_InRegion[0] = static_cast<ImageInIndexValueType>(Ce1f - 1);
    index_InRegion[1] = static_cast<ImageInIndexValueType>(Le1f -1);

    // Check if input region selected is into input image and mod 64x64 : 
    // if yes do Rampes else put TilesResult to zero
    if (index_InRegion[0] < 0 || index_InRegion[1] < 0)
      {
	for (unsigned int i = 0; i < lastInd; i++)
	  {
	    TilesResult[i] = 0;
	  }
	return;
      } 
    if ((index_InRegion[0] + m_SizeWindow) >
      static_cast<ImageInIndexValueType>(inputPtr->GetLargestPossibleRegion().GetSize()[0]) ||
      (index_InRegion[1] + m_SizeWindow) >
      static_cast<ImageInIndexValueType>(inputPtr->GetLargestPossibleRegion().GetSize()[1]))
      {
	for (unsigned int i = 0; i < lastInd; i++)
	  {
	    TilesResult[i] = 0;
	  }
	return;
      }

    ImageInSizeType size_InRegion;
    size_InRegion.Fill(m_SizeWindow);
    
    inputRegion.SetSize(size_InRegion);
    inputRegion.SetIndex(index_InRegion);

    // Iterator on input
    InputIterator InIt(inputPtr, inputRegion);
    InIt.GoToBegin();

    ////////////////////////////////////////// Range Ramps ///////////////////////////////////////////////
    // Estimate the shift to select elets into m_RampesRange (selection with integer part)
    int shift_Range = std::round ((Ce1R - Ce1f) * (m_NbRampes-1)) + (m_NbRampes-1)/2; 

    // Get Pointer for FFT and FFT-1 in range
    FFTProxyComplexType * rangeFFTIn = m_rangeFFTInTab[threadId];
    FFTProxyComplexType * rangeFFTOut = m_rangeFFTOutTab[threadId];
    FFTProxyComplexType * rangeFFTInvIn = m_rangeFFTInvInTab[threadId]; ;
    FFTProxyComplexType * rangeFFTInvOut = m_rangeFFTInvOutTab[threadId];
    FFTProxyPlanType rangeFFTPlan = m_rangeFFTPlanTab[threadId]; 
    FFTProxyPlanType rangeFFTInvPlan = m_rangeFFTInvPlanTab[threadId];

    FFTProxyComplexType * bufInt = new FFTProxyComplexType[m_SizeWindow*m_SizeTiles]; 

    // For each line of input region (= extracted window)
    int compteur_C = 0;
    int compteur_L = 0;
    while (!InIt.IsAtEnd())
      {
	InIt.GoToBeginOfLine();      
	compteur_C = 0;

	// Copy into m_rangeFFTInTab the current line of extracted window
	while (!InIt.IsAtEndOfLine())
	  {
	    rangeFFTIn[compteur_C][0] = InIt.Get().real(); 
	    rangeFFTIn[compteur_C][1] = InIt.Get().imag();
	    ++InIt;

	    ++compteur_C;
	  }
	InIt.NextLine();
	
	
	// FFT with m_rangeFFTInTab as input
	FFTWProxyType::Execute(rangeFFTPlan);

	// Multiply m_rangeFFTInTab with selected elts from m_RampesRange to get m_rangeFFTInvInTab
	for (unsigned int k = 0; k < m_SizeWindow; k++)
	  {
	    // Get index for range ramps
	    unsigned int ind = shift_Range*m_SizeWindow + k;

	    // Multiply into frequency domain for filtering
	    rangeFFTInvIn[k][0] = (rangeFFTOut[k][0] * m_RampesRange[ind][0] - 
				   rangeFFTOut[k][1] * m_RampesRange[ind][1])/m_SizeWindow;   
	    
	    rangeFFTInvIn[k][1] = (rangeFFTOut[k][0] * m_RampesRange[ind][1] + 
				   rangeFFTOut[k][1] * m_RampesRange[ind][0])/m_SizeWindow;
	  }
	
	// FFT-1 to retrieve m_rangeFFTInvOutTab
	FFTWProxyType::Execute(rangeFFTInvPlan);
	
	
	// Copy into an intermediare buffer (size dC = 50 and dL = 64)
	for (unsigned int k = 0; k < m_SizeWindow; k++)
	  {
	    if (k >= m_Margin && k < (m_SizeWindow - m_Margin))
	      {
		bufInt[compteur_L*m_SizeTiles + k - m_Margin][0] = rangeFFTInvOut[k][0];
		bufInt[compteur_L*m_SizeTiles + k - m_Margin][1] = rangeFFTInvOut[k][1];
	      }

	  }

	++compteur_L;
      }

    ////////////////////////////////////////// Azimut Ramps //////////////////////////////////////////////
    // Estimate the shift to select elets into m_RampesAzimut
    int shift_Azimut = std::round ((Le1R - Le1f) * (m_NbRampes-1)) + (m_NbRampes-1)/2; 
    
    // Get Pointer for FFT and FFT-1 in range
    FFTProxyComplexType * azimutFFTIn = m_azimutFFTInTab[threadId];
    FFTProxyComplexType * azimutFFTOut = m_azimutFFTOutTab[threadId];
    FFTProxyComplexType * azimutFFTInvIn = m_azimutFFTInvInTab[threadId]; ;
    FFTProxyComplexType * azimutFFTInvOut = m_azimutFFTInvOutTab[threadId];
    FFTProxyPlanType azimuteFFTPlan = m_azimutFFTPlanTab[threadId]; 
    FFTProxyPlanType azimutFFTInvPlan = m_azimutFFTInvPlanTab[threadId];

    // For each colunm of intermediare buffer
    for (unsigned int k = 0; k < m_SizeTiles; k++)
      {
	// Copy the current colunm into m_azimutFFTInTab
	for (unsigned int l = 0; l < m_SizeWindow; l++)
	  {
	    azimutFFTIn[l][0] = bufInt[l*m_SizeTiles + k][0];
	    azimutFFTIn[l][1] = bufInt[l*m_SizeTiles + k][1];
	  }

	// FFT with colunm of inter buf as input (= m_azimutFFTInTab)
	FFTWProxyType::Execute(azimuteFFTPlan);

	// Multiply m_azimutFFTInTab with selected elts from m_RampesAzimut to get m_azimutFFTInvInTab
	for (unsigned int l = 0; l < m_SizeWindow; l++)
	  {
	    // Get index for azimut ramps
	    unsigned int ind = shift_Azimut*m_SizeWindow + l;

	    // Multiply into frequency domain for filtering
	    azimutFFTInvIn[l][0] = (azimutFFTOut[l][0] * m_RampesAzimut[ind][0] - 
				    azimutFFTOut[l][1] * m_RampesAzimut[ind][1])/m_SizeWindow;   
	    
	    azimutFFTInvIn[l][1] = (azimutFFTOut[l][0] * m_RampesAzimut[ind][1] + 
				    azimutFFTOut[l][1] * m_RampesAzimut[ind][0])/m_SizeWindow;
	  }
	
	// FFT-1 to retrieve m_azimutFFTInvOutTab
	FFTWProxyType::Execute(azimutFFTInvPlan);

	// Copy into TilesResult (size dC = 50 and dL = 50)
	for (unsigned int l = 0; l < m_SizeWindow; l++)
	  {
	    if (l >= m_Margin && l < (m_SizeWindow - m_Margin))
	      {
		// Assignate TilesResult (with conversion if m_ConvToCInt16 == true)
		if (m_ConvToCInt16)
		  {
		    TilesResult[(l-m_Margin)*m_SizeTiles + k].real(static_cast<short>(azimutFFTInvOut[l][0]+0.5));
		    TilesResult[(l-m_Margin)*m_SizeTiles + k].imag(static_cast<short>(azimutFFTInvOut[l][1]+0.5));
		  }
		else
		  {
		    TilesResult[(l-m_Margin)*m_SizeTiles + k].real(azimutFFTInvOut[l][0]);
		    TilesResult[(l-m_Margin)*m_SizeTiles + k].imag(azimutFFTInvOut[l][1]);
		  }
	      }
	  }
      }

    // Free Memory
    delete [] bufInt;
    bufInt = 0;
 }

private :
  // Input Grid
  GridInPointer m_Grid;
  // Grid Step into SAR/SLC geometry
  GridInSizeType m_GridStep; 
  
  unsigned int m_NbRampes;
  
  unsigned int m_SizeWindow;

  unsigned int m_SizeTiles;

  unsigned int m_Margin;

  // Rampe Arrays
  FFTProxyComplexType * m_RampesRange;
  FFTProxyComplexType * m_RampesAzimut;

  // Doppler frequency in Azimut
  double m_DopAzi;

  // Number of threads
  unsigned int m_nbThreads;

  // bool conversion into CINT16
  bool m_ConvToCInt16;

  // FFT parameters
  // Range
  FFTProxyComplexType ** m_rangeFFTInTab;
  FFTProxyComplexType ** m_rangeFFTOutTab;
  FFTProxyComplexType ** m_rangeFFTInvInTab;
  FFTProxyComplexType ** m_rangeFFTInvOutTab;
  
  FFTProxyPlanType * m_rangeFFTPlanTab; 
  FFTProxyPlanType * m_rangeFFTInvPlanTab;
  
  // Azimut
  FFTProxyComplexType ** m_azimutFFTInTab;
  FFTProxyComplexType ** m_azimutFFTOutTab;
  FFTProxyComplexType ** m_azimutFFTInvInTab;
  FFTProxyComplexType ** m_azimutFFTInvOutTab;
  
  FFTProxyPlanType * m_azimutFFTPlanTab; 
  FFTProxyPlanType * m_azimutFFTInvPlanTab;


  /**
   * Method BuildRangeRamps
   */
  void BuildRangeRamps()
  {
    // half of nbRamps
    int nbRamps_half = (m_NbRampes-1)/2;
    
    // CfCut
    unsigned int CfCut = m_SizeWindow/2;

    // Allocation
    m_RampesRange = new FFTProxyComplexType [m_SizeWindow*m_NbRampes];

    // Creation
    int shift;
    int i;
    for (shift=-nbRamps_half, i=0; shift <= nbRamps_half; shift ++, i++)
	{
	  for (unsigned int Cf = 0; Cf< m_SizeWindow; Cf++)                                                                                                                  
		{	
		  int aux;
		  if (Cf < CfCut)	aux = Cf;
			else aux = Cf - m_SizeWindow;

		  double phase = 2 * M_PI * (double)aux * (double)shift / (double)m_SizeWindow / (double)(m_NbRampes-1);  
		  
		  unsigned int ind = i*m_SizeWindow + Cf;
		  m_RampesRange[ind][0] = std::cos(phase); 
		  m_RampesRange[ind][1] = std::sin(phase);
		}                                                                   
	}  
  }


  /**
   * Method BuildAzimutRamps
   */
  void BuildAzimutRamps()
  {
    // half of nbRamps
    int nbRamps_half = (m_NbRampes-1)/2;
    
    // LfCut in azimut this value depends on doppler frequency
    unsigned int LfCut = (int)(std::round( (m_DopAzi + 0.5) * m_SizeWindow )) % m_SizeWindow;
    

    // Allocation
    m_RampesAzimut = new FFTProxyComplexType [m_SizeWindow*m_NbRampes];  

    // Creation
    int shift;
    int i;
    for (shift=-nbRamps_half, i=0; shift <= nbRamps_half; shift ++, i++)
	{
	  for (unsigned int Lf = 0; Lf< m_SizeWindow; Lf++)                                                                                                                  
		{	
		  int aux;
		  if (Lf < LfCut)	aux = Lf;
			else aux = Lf - m_SizeWindow;

		  double phase = 2 * M_PI * (double)aux * (double)shift / (double)m_SizeWindow / (double)(m_NbRampes-1);  
		  
		  unsigned int ind = i*m_SizeWindow + Lf;
		  m_RampesAzimut[ind][0] = std::cos(phase);
		  m_RampesAzimut[ind][1] = std::sin(phase);
		}                                                                   
	}  
  }

  };

}
}

#endif
