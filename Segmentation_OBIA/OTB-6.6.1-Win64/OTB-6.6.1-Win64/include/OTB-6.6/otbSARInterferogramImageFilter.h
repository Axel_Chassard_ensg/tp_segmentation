/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARInterferogramImageFilter_h
#define otbSARInterferogramImageFilter_h

#include "itkImageToImageFilter.h"
#include "itkSmartPointer.h"
#include "itkPoint.h"

#include "itkSimpleFastMutexLock.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"

#include "otbImageKeywordlist.h"


namespace otb
{
/** \class SARInterferogramImageFilter 
 * \brief Creates an interferogram between two images. 
 * 
 * This filter built the interferogram between a master and a slave image.
 * Two kinds of output geometries are available : ML (master) and DEM geometry.
 * A topographic phase can be used to create the interferogram.
 *
 * The output (whtaever the geometry) is a vector image with amplitude, phase and coherence.
 *
 * \ingroup DiapOTBModule
 */

  template <typename TImageSAR, typename TImageDEM, typename TImagePhase, typename TImageOut> 
  class ITK_EXPORT SARInterferogramImageFilter :
    public itk::ImageToImageFilter<TImageSAR,TImageOut>
{
public:

  // Standard class typedefs
  typedef SARInterferogramImageFilter                    Self;
  typedef itk::ImageToImageFilter<TImageSAR,TImageOut>    Superclass;
  typedef itk::SmartPointer<Self>                        Pointer;
  typedef itk::SmartPointer<const Self>                  ConstPointer;

  // Method for creation through object factory
  itkNewMacro(Self);
  // Run-time type information
  itkTypeMacro(SARInterferogramImageFilter,ImageToImageFilter);

  /** Typedef to image input type (master and slave reech images) :  otb::Image (Complex)  */
  typedef TImageSAR                                  ImageSARType;
  /** Typedef to describe the inout image pointer type. */
  typedef typename ImageSARType::Pointer             ImageSARPointer;
  typedef typename ImageSARType::ConstPointer        ImageSARConstPointer;
  /** Typedef to describe the inout image region type. */
  typedef typename ImageSARType::RegionType          ImageSARRegionType;
  /** Typedef to describe the type of pixel and point for inout image. */
  typedef typename ImageSARType::PixelType           ImageSARPixelType;
  typedef typename ImageSARType::PointType           ImageSARPointType;
  /** Typedef to describe the image index, size types and spacing for inout image. */
  typedef typename ImageSARType::IndexType           ImageSARIndexType;
  typedef typename ImageSARType::IndexValueType      ImageSARIndexValueType;
  typedef typename ImageSARType::SizeType            ImageSARSizeType;
  typedef typename ImageSARType::SizeValueType       ImageSARSizeValueType;
  typedef typename ImageSARType::SpacingType         ImageSARSpacingType;
  typedef typename ImageSARType::SpacingValueType    ImageSARSpacingValueType;
  
  /** Typedef to image output type : otb::VectorImage  */
  typedef TImageOut                                  ImageOutType;
  /** Typedef to describe the output image pointer type. */
  typedef typename ImageOutType::Pointer             ImageOutPointer;
  typedef typename ImageOutType::ConstPointer        ImageOutConstPointer;
  /** Typedef to describe the output image region type. */
  typedef typename ImageOutType::RegionType          ImageOutRegionType;
  /** Typedef to describe the type of pixel and point for output image. */
  typedef typename ImageOutType::PixelType           ImageOutPixelType;
  typedef typename ImageOutType::PointType           ImageOutPointType;
  /** Typedef to describe the image index, size types and spacing for output image. */
  typedef typename ImageOutType::IndexType           ImageOutIndexType;
  typedef typename ImageOutType::IndexValueType      ImageOutIndexValueType;
  typedef typename ImageOutType::SizeType            ImageOutSizeType;
  typedef typename ImageOutType::SizeValueType       ImageOutSizeValueType;
  typedef typename ImageOutType::SpacingType         ImageOutSpacingType;
  typedef typename ImageOutType::SpacingValueType    ImageOutSpacingValueType;

  /** Typedef to dem type : otb::Image  */
  typedef TImageDEM                                  ImageDEMType;
  /** Typedef to describe the output image pointer type. */
  typedef typename ImageDEMType::Pointer             ImageDEMPointer;
  typedef typename ImageDEMType::ConstPointer        ImageDEMConstPointer;
  /** Typedef to describe the output image region type. */
  typedef typename ImageDEMType::RegionType          ImageDEMRegionType;
  /** Typedef to describe the type of pixel and point for output image. */
  typedef typename ImageDEMType::PixelType           ImageDEMPixelType;
  typedef typename ImageDEMType::PointType           ImageDEMPointType;
  /** Typedef to describe the image index, size types and spacing for output image. */
  typedef typename ImageDEMType::IndexType           ImageDEMIndexType;
  typedef typename ImageDEMType::IndexValueType      ImageDEMIndexValueType;
  typedef typename ImageDEMType::SizeType            ImageDEMSizeType;
  typedef typename ImageDEMType::SizeValueType       ImageDEMSizeValueType;
  typedef typename ImageDEMType::SpacingType         ImageDEMSpacingType;
  typedef typename ImageDEMType::SpacingValueType    ImageDEMSpacingValueType;

  // Define Point2DType and Point3DType
  using Point2DType = itk::Point<double,2>;
  using Point3DType = itk::Point<double,3>;

  // Typedef for topographic Phase
  typedef TImagePhase                                      ImagePhaseType;
  typedef typename ImagePhaseType::Pointer                 ImagePhasePointer;
  typedef typename ImagePhaseType::ConstPointer            ImagePhaseConstPointer;
  typedef typename ImagePhaseType::RegionType              ImagePhaseRegionType;
  typedef typename ImagePhaseType::IndexType               ImagePhaseIndexType;
  typedef typename ImagePhaseType::IndexValueType          ImagePhaseIndexValueType;
  typedef typename ImagePhaseType::SizeType                ImagePhaseSizeType;
  typedef typename ImagePhaseType::SizeValueType           ImagePhaseSizeValueType;

  // Typedef for iterators
  typedef itk::ImageScanlineConstIterator< ImageSARType > InputSARIterator;
  typedef itk::ImageScanlineConstIterator< ImagePhaseType > InputPhaseIterator;
  typedef itk::ImageScanlineIterator< ImageOutType > OutputIterator;

  typedef itk::SimpleFastMutexLock         MutexType;

  // Setter
  itkSetMacro(MLRan, unsigned int);
  itkSetMacro(MLAzi, unsigned int);
  itkSetMacro(MarginRan, unsigned int);
  itkSetMacro(MarginAzi, unsigned int);
  itkSetMacro(Gain, float);
  itkSetMacro(UseDEMGeoAsOutput, bool);
  itkSetMacro(ApproxDiapason, bool);
  void SetDEMPtr(ImageDEMPointer DEMPtr);
  // Getter
  itkGetMacro(MLRan, unsigned int);
  itkGetMacro(MLAzi, unsigned int);
  itkGetMacro(MarginRan, unsigned int);
  itkGetMacro(MarginAzi, unsigned int);
  itkGetMacro(Gain, float);
  itkGetMacro(UseDEMGeoAsOutput, bool);
    
  // Setter/Getter for inputs (SAR images and potentially topographic phase) 
  /** Connect one of the operands for interferometry : Master */
  void SetMasterInput( const ImageSARType* image);

  /** Connect one of the operands for interferometry : Coregistrated Slave */
  void SetSlaveInput(const ImageSARType* image);

  /** Connect one of the operands for interferometry : Coregistrated Slave */
  void SetTopographicPhaseInput(const ImagePhaseType* imagePhaseTopo);

  /** Get the inputs */
  const ImageSARType* GetMasterInput() const;
  const ImageSARType * GetSlaveInput() const;
  const ImagePhaseType * GetTopographicPhaseInput() const;

  // Getter for outputs (Interferogram into ML geometry and potentially into DEM geometry) 
  /** Returns the const image */
  const TImageOut * GetOutput() const;
  /** Returns the image */
  TImageOut * GetOutput();
  /** Returns the optionnal output image (into DEM/Ortho geometry) */
  ImageOutType * GetOutputIntoDEMGeo();

protected:
  // Constructor
  SARInterferogramImageFilter();

  // Destructor
  virtual ~SARInterferogramImageFilter() ITK_OVERRIDE;

  // Print
  void PrintSelf(std::ostream & os, itk::Indent indent) const ITK_OVERRIDE;
  
  /** SARInterferogramImageFilter produces an image which are into output geometry. The differences between 
   * output and input images can be the size of images and the dimensions. 
   * As such, SARInterferogramImageFilter needs to provide an implementation for 
   * GenerateOutputInformation() in order to inform the pipeline execution model. 
   */ 
  virtual void GenerateOutputInformation() ITK_OVERRIDE;

  /** SARInterferogramImageFilter needs a input requested region that corresponds to the margin and shifts 
   * into the requested region of our output requested region. The output requested region needs to be modified
   * to construct as wanted tiles with input size.
   * As such, TilesAnalysesImageFilter needs to provide an implementation for 
   * GenerateInputRequestedRegion() in order to inform the pipeline execution model. 
   * \sa ProcessObject::GenerateInputRequestedRegion() */
  virtual void GenerateInputRequestedRegion() ITK_OVERRIDE;

  /**
   * OutputRegionToInputRegion returns the input SAR region 
   */
  ImageSARRegionType OutputRegionToInputRegion(const ImageOutRegionType& outputRegion, bool withMargin) const;

   /**
   * OutputRegionToInputPhaseRegion returns the input Phase region 
   */
  ImagePhaseRegionType OutputRegionToInputPhaseRegion(const ImageOutRegionType& outputRegion,
						      bool & sameGeoAsMainOutput, bool withMargin) const;

  /**
   * SARInterferogramImageFilter can produce an optionnal image if UseDEMGeoAsOutput = true. The requested 
   * region for this optionnal output is set to the largest possible region.
   */
  void EnlargeOutputRequestedRegion( itk::DataObject *output ) ITK_OVERRIDE;

  /**
   * SARInterferogramImageFilter can produce an optionnal image according to the functor. An allocation of
   * this optionnal output is made into BeforeThreadedGenerateData and free memory into 
   * AfterThreadedGenerateData.
   */
  void BeforeThreadedGenerateData() ITK_OVERRIDE;

  void AfterThreadedGenerateData() ITK_OVERRIDE;

  /** 
   * SARInterferogramImageFilterr can be implemented as a multithreaded filter.
   * Therefore, this implementation provides a ThreadedGenerateData() routine
   * which is called for each processing thread. The main output image data is
   * allocated automatically by the superclass prior to calling
   * ThreadedGenerateData().  ThreadedGenerateData can only write to the
   * portion of the output image specified by the parameter
   * "outputRegionForThread"
   *
   * \sa ImageToImageFilter::ThreadedGenerateData(),
   *     ImageToImageFilter::GenerateData() */
  virtual void ThreadedGenerateData(const ImageOutRegionType& outputRegionForThread, 
				    itk::ThreadIdType threadId) ITK_OVERRIDE;
  
  virtual void ThreadedGenerateDataWithoutTopographicPhase(const ImageOutRegionType& outputRegionForThread, 
							   itk::ThreadIdType threadId);
  virtual void ThreadedGenerateDataWithTopographicPhase(const ImageOutRegionType& outputRegionForThread, 
							itk::ThreadIdType threadId);


 private:
  SARInterferogramImageFilter(const Self&); // purposely not implemented
  void operator=(const Self &); // purposely not 
  
  // ML factor in range 
  unsigned int m_MLRan;
  
  // ML factor in azimut
  unsigned int m_MLAzi;
  
  // gain for amplitude estimation
  float m_Gain;

  // DEM Pointer (metadata only)
  ImageDEMPointer m_DEMPtr;

  // Use DEM Geometry as optionnal output
  bool m_UseDEMGeoAsOutput;

  // SAR Dimensions
  int m_nbLinesSAR;
  int m_nbColSAR;

  // Margin in order to spend our averaging
  unsigned int m_MarginRan;
  unsigned int m_MarginAzi;
  
  // Counter to synchronize our outputs with calculation
  int m_OutputCounter;

  // Mutex for optionnalImage (geo ortho)
  MutexType * m_Mutex; 
  
  // Arrays to store ortho accumlations and counters
  double * m_OrthoRealAcc;
  double * m_OrthoImagAcc;
  double * m_OrthoModAcc;
  double * m_OrthoCounter;
  // One arrays by thread to guarantee Thread-Safety
  float ** m_OrthoRealAccByThread;
  float ** m_OrthoImagAccByThread;
  float ** m_OrthoModAccByThread;
  float ** m_OrthoCounterByThread;

  // Dimensions of DEM
  int m_nbLinesDEM;
  int m_nbColDEM;

  bool m_ApproxDiapason;
};

} // End namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSARInterferogramImageFilter.txx"
#endif



#endif
