/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARCorrectionGridImageFilter_h
#define otbSARCorrectionGridImageFilter_h

#include "itkImageToImageFilter.h"
#include "itkSmartPointer.h"
#include "itkPoint.h"

#include "otbImageKeywordlist.h"
#include "otbGenericRSTransform.h"
#
namespace otb
{
  /** \class SARCorrectionGridImageFilter 
   * \brief Estimates a deformation grid between two images (master and slave) related to the correction of DEM 
   * Gird with a correlation Grid.
   *    
   * The inputs are the two grids : DEM Grid and Correlation Grid. The grid are vector images with three 
   * components : Deformation in range, Deformations in azimut, Number of occurences or correlation rate. 
   * 
   * \ingroup DiapOTBModule
   */

  template <typename TImageIn, typename TImageOut> class ITK_EXPORT SARCorrectionGridImageFilter :
    public itk::ImageToImageFilter<TImageIn,TImageOut>
  {
  public:

    // Standard class typedefs
    typedef SARCorrectionGridImageFilter                   Self;
    typedef itk::ImageToImageFilter<TImageIn,TImageOut>    Superclass;
    typedef itk::SmartPointer<Self>                        Pointer;
    typedef itk::SmartPointer<const Self>                  ConstPointer;

    // Method for creation through object factory
    itkNewMacro(Self);
    // Run-time type information
    itkTypeMacro(SARCorrectionGridFilter,ImageToImageFilter);

    /** Typedef to image input type : otb::VectorImage for DEM Grid and Correlation Grid */
    typedef TImageIn                                  ImageInType;
    /** Typedef to describe the inout image pointer type. */
    typedef typename ImageInType::Pointer             ImageInPointer;
    typedef typename ImageInType::ConstPointer        ImageInConstPointer;
    /** Typedef to describe the inout image region type. */
    typedef typename ImageInType::RegionType          ImageInRegionType;
    /** Typedef to describe the type of pixel and point for inout image. */
    typedef typename ImageInType::PixelType           ImageInPixelType;
    typedef typename ImageInType::PointType           ImageInPointType;
    /** Typedef to describe the image index, size types and spacing for inout image. */
    typedef typename ImageInType::IndexType           ImageInIndexType;
    typedef typename ImageInType::IndexValueType      ImageInIndexValueType;
    typedef typename ImageInType::SizeType            ImageInSizeType;
    typedef typename ImageInType::SizeValueType       ImageInSizeValueType;
    typedef typename ImageInType::SpacingType         ImageInSpacingType;
    typedef typename ImageInType::SpacingValueType    ImageInSpacingValueType;
  
    /** Typedef to image output type : otb::VectorImage with two components (range deformations and 
	azimut deformations) */
    typedef TImageOut                                  ImageOutType;
    /** Typedef to describe the output image pointer type. */
    typedef typename ImageOutType::Pointer             ImageOutPointer;
    typedef typename ImageOutType::ConstPointer        ImageOutConstPointer;
    /** Typedef to describe the output image region type. */
    typedef typename ImageOutType::RegionType          ImageOutRegionType;
    /** Typedef to describe the type of pixel and point for output image. */
    typedef typename ImageOutType::PixelType           ImageOutPixelType;
    typedef typename ImageOutType::PointType           ImageOutPointType;
    /** Typedef to describe the image index, size types and spacing for output image. */
    typedef typename ImageOutType::IndexType           ImageOutIndexType;
    typedef typename ImageOutType::IndexValueType      ImageOutIndexValueType;
    typedef typename ImageOutType::SizeType            ImageOutSizeType;
    typedef typename ImageOutType::SizeValueType       ImageOutSizeValueType;
    typedef typename ImageOutType::SpacingType         ImageOutSpacingType;
    typedef typename ImageOutType::SpacingValueType    ImageOutSpacingValueType;

    // Define Point2DType and Point3DType
    using Point2DType = itk::Point<double,2>;
    using Point3DType = itk::Point<double,3>;
    
    // Setter/Getter
    /** Set/Get Threshold, Gap and mean values */
    itkSetMacro(Threshold, double);
    itkGetMacro(Threshold, double);
    itkSetMacro(Gap, double);
    itkGetMacro(Gap, double);
    itkSetMacro(MeanRange, double);
    itkGetMacro(MeanRange, double);
    itkSetMacro(MeanAzimut, double);
    itkGetMacro(MeanAzimut, double);

    // Setter/Getter for input grids 
    /** Connect one of the operands for registration */
    void SetDEMGridInput( const ImageInType* image);

    /** Connect one of the operands for registration */
    void SetCorGridInput(const ImageInType* image);

    /** Get the inputs */
    const ImageInType * GetDEMGridInput() const;
    const ImageInType * GetCorGridInput() const;

  protected:
    // Constructor
    SARCorrectionGridImageFilter();

    // Destructor
    virtual ~SARCorrectionGridImageFilter() ITK_OVERRIDE {};

    // Print
    void PrintSelf(std::ostream & os, itk::Indent indent) const ITK_OVERRIDE;
  
    /** SARCorrectionGridImageFilter produces an vector image to indicate the deformations into the two 
     * dimensions (range and azimut). The differences between is the number of components. 
     * As such, SARCorrectionGridImageFilter needs to provide an implementation for 
     * GenerateOutputInformation() in order to inform the pipeline execution model. 
     */ 
    virtual void GenerateOutputInformation() ITK_OVERRIDE;

    /** SARCorrectionGridImageFilter needs input requested regions (same region than input grids).
     * As such, SARQuadraticAveragingImageFilter needs to provide an implementation for 
     * GenerateInputRequestedRegion() in order to inform the pipeline execution model. 
     * \sa ProcessObject::GenerateInputRequestedRegion() */
    virtual void GenerateInputRequestedRegion() ITK_OVERRIDE;

    /** SARCorrectionGridImageFilter can be implemented as a multithreaded filter.
     * Therefore, this implementation provides a ThreadedGenerateData() routine
     * which is called for each processing thread. The output image data is
     * allocated automatically by the superclass prior to calling
     * ThreadedGenerateData().  ThreadedGenerateData can only write to the
     * portion of the output image specified by the parameter
     * "outputRegionForThread"
     *
     * \sa ImageToImageFilter::ThreadedGenerateData(),
     *     ImageToImageFilter::GenerateData() */
    void ThreadedGenerateData(const ImageOutRegionType& outputRegionForThread,
			      itk::ThreadIdType threadId ) override;

  
  private:
    SARCorrectionGridImageFilter(const Self&); // purposely not implemented
    void operator=(const Self &); // purposely not 
    
    /** Threshold */
    double m_Threshold;
    
    /** Mean values in range and azimut for DEM grid */
    double m_MeanRange;
    double m_MeanAzimut;

    /** Gap between DEM grid value and mean */
    double m_Gap;
  };

} // End namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSARCorrectionGridImageFilter.txx"
#endif



#endif
