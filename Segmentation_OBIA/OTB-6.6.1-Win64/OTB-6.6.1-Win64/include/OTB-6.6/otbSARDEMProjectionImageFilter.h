/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARDEMProjectionImageFilter_h
#define otbSARDEMProjectionImageFilter_h

#include "itkImageToImageFilter.h"
#include "itkSmartPointer.h"
#include "itkPoint.h"

#include "otbSarSensorModelAdapter.h"
#include "otbImageKeywordlist.h"

namespace otb
{
/** \class SARDEMProjectionImageFilter 
 * \brief Performs the projection of the DEM into SAR geometry 
 * 
 * This filter performs the projection of the DEM into SAR geometry.
 * 
 * \ingroup DiapOTBModule
 */

  template <typename TImageIn,  typename TImageOut> class ITK_EXPORT SARDEMProjectionImageFilter :
    public itk::ImageToImageFilter<TImageIn,TImageOut>
{
public:

  // Standard class typedefs
  typedef SARDEMProjectionImageFilter                    Self;
  typedef itk::ImageToImageFilter<TImageIn,TImageOut>    Superclass;
  typedef itk::SmartPointer<Self>                        Pointer;
  typedef itk::SmartPointer<const Self>                  ConstPointer;

  // Method for creation through object factory
  itkNewMacro(Self);
  // Run-time type information
  itkTypeMacro(SARDEMProjectionFilter,ImageToImageFilter);

  /** Typedef to image type */
  typedef TImageIn                                  ImageInType;
  /** Typedef to describe the inout image pointer type. */
  typedef typename ImageInType::Pointer             ImageInPointer;
  typedef typename ImageInType::ConstPointer        ImageInConstPointer;
  /** Typedef to describe the inout image region type. */
  typedef typename ImageInType::RegionType          ImageInRegionType;
  /** Typedef to describe the type of pixel and point for inout image. */
  typedef typename ImageInType::PixelType           ImageInPixelType;
  typedef typename ImageInType::PointType           ImageInPointType;
  /** Typedef to describe the image index, size types and spacing for inout image. */
  typedef typename ImageInType::IndexType           ImageInIndexType;
  typedef typename ImageInType::IndexValueType      ImageInIndexValueType;
  typedef typename ImageInType::SizeType            ImageInSizeType;
  typedef typename ImageInType::SizeValueType       ImageInSizeValueType;
  typedef typename ImageInType::SpacingType         ImageInSpacingType;
  typedef typename ImageInType::SpacingValueType    ImageInSpacingValueType;
  

  typedef TImageOut                                  ImageOutType;
  /** Typedef to describe the output image pointer type. */
  typedef typename ImageOutType::Pointer             ImageOutPointer;
  typedef typename ImageOutType::ConstPointer        ImageOutConstPointer;
  /** Typedef to describe the output image region type. */
  typedef typename ImageOutType::RegionType          ImageOutRegionType;
  /** Typedef to describe the type of pixel and point for output image. */
  typedef typename ImageOutType::PixelType           ImageOutPixelType;
  typedef typename ImageOutType::PointType           ImageOutPointType;
  /** Typedef to describe the image index, size types and spacing for output image. */
  typedef typename ImageOutType::IndexType           ImageOutIndexType;
  typedef typename ImageOutType::IndexValueType      ImageOutIndexValueType;
  typedef typename ImageOutType::SizeType            ImageOutSizeType;
  typedef typename ImageOutType::SizeValueType       ImageOutSizeValueType;
  typedef typename ImageOutType::SpacingType         ImageOutSpacingType;
  typedef typename ImageOutType::SpacingValueType    ImageOutSpacingValueType;

  // Define Point2DType and Point3DType
  using Point2DType = itk::Point<double,2>;
  using Point3DType = itk::Point<double,3>;

  // DEMHandler
  typedef otb::DEMHandler DEMHandlerType;
  typedef typename DEMHandlerType::Pointer DEMHandlerPointerType;

  // Setter
  void SetSARImageKeyWorList(ImageKeywordlist sarImageKWL);
  itkSetMacro(NoData, int);
  itkSetMacro(withXYZ, bool);
  itkSetMacro(withH, bool);
  itkSetMacro(withSatPos, bool);

  // Getter
  itkGetMacro(NoData, int);
  itkGetMacro(withXYZ, bool);
  itkGetMacro(withH, bool);
  itkGetMacro(withSatPos, bool);

protected:
  // Constructor
  SARDEMProjectionImageFilter();

  // Destructor
  virtual ~SARDEMProjectionImageFilter() ITK_OVERRIDE {};

  // Print
  void PrintSelf(std::ostream & os, itk::Indent indent) const ITK_OVERRIDE;
  
  /** SARDEMProjectionImageFilter produces an image which the same size
   * its input image. The different between output and input images are
   * the dimensions. The output image are a otb::VectorImage with four components.
   * As such, SARDEMProjectionImageFilter needs to provide
   * an implementation for GenerateOutputInformation() in order to
   * inform the pipeline execution model. 
   */ 
  virtual void GenerateOutputInformation() ITK_OVERRIDE;

  /** SARDEMProjectionImageFilter needs a input requested region with the same size of
      the output requested region. 
   * \sa ProcessObject::GenerateInputRequestedRegion() */
  virtual void GenerateInputRequestedRegion() ITK_OVERRIDE;


  /** SARDEMProjectionImageFilter can be implemented as a multithreaded filter.
   * Therefore, this implementation provides a ThreadedGenerateData() routine
   * which is called for each processing thread. The output image data is
   * allocated automatically by the superclass prior to calling
   * ThreadedGenerateData().  ThreadedGenerateData can only write to the
   * portion of the output image specified by the parameter
   * "outputRegionForThread"
   *
   * \sa ImageToImageFilter::ThreadedGenerateData(),
   *     ImageToImageFilter::GenerateData() */
  virtual void ThreadedGenerateData(const ImageOutRegionType& outputRegionForThread, itk::ThreadIdType threadId) ITK_OVERRIDE;

 
 private:
  SARDEMProjectionImageFilter(const Self&); // purposely not implemented
  void operator=(const Self &); // purposely not 
  
  // Instance of SarSensorModelAdapter
  SarSensorModelAdapter::Pointer m_SarSensorModelAdapter; 

  // SAR Image KeyWorldList
  ImageKeywordlist m_SarImageKwl;

  // Value for NoData into DEM
  int m_NoData;

  // Boleans for more components into DEM projection
  bool m_withXYZ; // Cartesian coordonates
  bool m_withH; // High
  bool m_withSatPos; // Topographic phase

  int m_NbComponents;
  int m_indH; // indice into output to set the H component
  int m_indSatPos; // indice  into output to set the SatPos components
};

} // End namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSARDEMProjectionImageFilter.txx"
#endif



#endif
