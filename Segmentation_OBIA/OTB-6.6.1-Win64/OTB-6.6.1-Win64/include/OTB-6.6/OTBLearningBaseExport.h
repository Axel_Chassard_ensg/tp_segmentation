
#ifndef OTBLearningBase_EXPORT_H
#define OTBLearningBase_EXPORT_H

#ifdef OTB_STATIC
#  define OTBLearningBase_EXPORT
#  define OTBLearningBase_HIDDEN
#else
#  ifndef OTBLearningBase_EXPORT
#    ifdef OTBLearningBase_EXPORTS
        /* We are building this library */
#      define OTBLearningBase_EXPORT __declspec(dllexport)
#    else
        /* We are using this library */
#      define OTBLearningBase_EXPORT __declspec(dllimport)
#    endif
#  endif

#  ifndef OTBLearningBase_HIDDEN
#    define OTBLearningBase_HIDDEN 
#  endif
#endif

#ifndef OTBLEARNINGBASE_DEPRECATED
#  define OTBLEARNINGBASE_DEPRECATED __declspec(deprecated)
#endif

#ifndef OTBLEARNINGBASE_DEPRECATED_EXPORT
#  define OTBLEARNINGBASE_DEPRECATED_EXPORT OTBLearningBase_EXPORT OTBLEARNINGBASE_DEPRECATED
#endif

#ifndef OTBLEARNINGBASE_DEPRECATED_NO_EXPORT
#  define OTBLEARNINGBASE_DEPRECATED_NO_EXPORT OTBLearningBase_HIDDEN OTBLEARNINGBASE_DEPRECATED
#endif

#define DEFINE_NO_DEPRECATED 0
#if DEFINE_NO_DEPRECATED
# define OTBLEARNINGBASE_NO_DEPRECATED
#endif

#endif
