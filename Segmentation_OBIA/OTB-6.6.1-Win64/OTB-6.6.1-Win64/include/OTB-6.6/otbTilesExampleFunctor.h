/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbTilesExampleFunctor_h
#define otbTilesExampleFunctor_h
#include <cassert>

#include "itkVariableLengthVector.h"
#include "itkRGBPixel.h"
#include "itkRGBAPixel.h"
#include "itkObject.h"
#include "itkObjectFactory.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageRegionConstIterator.h"
#include "itkImageScanlineIterator.h"

namespace otb
{
namespace Function
{
/** \class TilesExampleFunctor
* \brief Example functions (called into TilesAnalysis Filter).
*
* \ingroup DiapOTBModule
*
*/
  template <class TImageIn, class TImageOut>
class TilesExampleFunctor : public itk::Object
{
public:
  /** Standard class typedefs */
  typedef TilesExampleFunctor        Self;
  typedef itk::Object                   Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Method for creation through the object factory */
  itkNewMacro(Self);

  /** Runtime information */
  itkTypeMacro(TilesExampleFunctor, itk::Object);

  /** Typedef to image input type :  otb::Image (Complex or real)  */
  typedef TImageIn                                  ImageInType;
  /** Typedef to describe the inout image pointer type. */
  typedef typename ImageInType::Pointer             ImageInPointer;
  typedef typename ImageInType::ConstPointer        ImageInConstPointer;
  /** Typedef to describe the inout image region type. */
  typedef typename ImageInType::RegionType          ImageInRegionType;
  /** Typedef to describe the type of pixel and point for inout image. */
  typedef typename ImageInType::PixelType           ImageInPixelType;
  typedef typename ImageInType::PointType           ImageInPointType;
  /** Typedef to describe the image index, size types and spacing for inout image. */
  typedef typename ImageInType::IndexType           ImageInIndexType;
  typedef typename ImageInType::IndexValueType      ImageInIndexValueType;
  typedef typename ImageInType::SizeType            ImageInSizeType;
  typedef typename ImageInType::SizeValueType       ImageInSizeValueType;
  typedef typename ImageInType::SpacingType         ImageInSpacingType;
  typedef typename ImageInType::SpacingValueType    ImageInSpacingValueType;
  
  /** Typedef to image output type : otb::Image (Complex or real)  */
  typedef TImageOut                                  ImageOutType;
  /** Typedef to describe the output image pointer type. */
  typedef typename ImageOutType::Pointer             ImageOutPointer;
  typedef typename ImageOutType::ConstPointer        ImageOutConstPointer;
  /** Typedef to describe the output image region type. */
  typedef typename ImageOutType::RegionType          ImageOutRegionType;
  /** Typedef to describe the type of pixel and point for output image. */
  typedef typename ImageOutType::PixelType           ImageOutPixelType;
  typedef typename ImageOutType::PointType           ImageOutPointType;
  /** Typedef to describe the image index, size types and spacing for output image. */
  typedef typename ImageOutType::IndexType           ImageOutIndexType;
  typedef typename ImageOutType::IndexValueType      ImageOutIndexValueType;
  typedef typename ImageOutType::SizeType            ImageOutSizeType;
  typedef typename ImageOutType::SizeValueType       ImageOutSizeValueType;
  typedef typename ImageOutType::SpacingType         ImageOutSpacingType;
  typedef typename ImageOutType::SpacingValueType    ImageOutSpacingValueType;

  // Iterators
  typedef itk::ImageRegionConstIterator<ImageInType> InItType;
  
  /** Constructor */
  TilesExampleFunctor(){}

  /** Destructor */
  ~TilesExampleFunctor() override {}
  
  // Initialize
  void Initialize(int /*nbThreads=1*/) {}
  
  // Terminate
  void Terminate() {}

  // Process
  void Process(ImageInPointer inputPtr, ImageOutRegionType outputRegion, ImageOutPixelType * TilesResult)
  {
    // Transpose outputRegion (current tile) into input image
    // Input (= output now)
    ImageInRegionType inputRegion = outputRegion;
    
    // Crop to be sure
    inputRegion.Crop(inputPtr->GetLargestPossibleRegion());

    InItType InIt(inputPtr, inputRegion);
    InIt.GoToBegin();
    
    int compteur = 0;

    // Just a copy into TilesResult
    while (!InIt.IsAtEnd())
      {
	TilesResult[compteur] = InIt.Get();

	++compteur; 
	++InIt;
      }
  }

private :
  
  };

}
}

#endif
