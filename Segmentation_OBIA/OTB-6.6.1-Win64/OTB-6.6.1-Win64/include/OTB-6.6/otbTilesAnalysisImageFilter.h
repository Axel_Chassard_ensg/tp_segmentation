/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbTilesAnalysisImageFilter_h
#define otbTilesAnalysisImageFilter_h

#include "itkImageToImageFilter.h"
#include "itkSmartPointer.h"
#include "itkPoint.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"

#include "otbImageKeywordlist.h"
#include "otbVectorImage.h"

namespace otb
{
/** \class TilesAnalysisImageFilter 
 * \brief Creates/Analyses tiles fixed by a given size. 
 * 
 * This filter :
 * _ creates output tiles with given size 
 * _ select input requested region with built tiles, a given margin and possibly an input grid of deformations.
 * _ scan all tiles to apply a choosen fonction
 * 
 * This filter can be used for SAR coregistration.
 * \ingroup DiapOTBModule
 */

  template <typename TImageIn,  typename TImageOut, typename TFunction> 
class ITK_EXPORT TilesAnalysisImageFilter :
    public itk::ImageToImageFilter<TImageIn,TImageOut>
{
public:

  // Standard class typedefs
  typedef TilesAnalysisImageFilter                    Self;
  typedef itk::ImageToImageFilter<TImageIn,TImageOut>    Superclass;
  typedef itk::SmartPointer<Self>                        Pointer;
  typedef itk::SmartPointer<const Self>                  ConstPointer;

  // Method for creation through object factory
  itkNewMacro(Self);
  // Run-time type information
  itkTypeMacro(TilesAnalysisImageFilter,ImageToImageFilter);

  /** Typedef to image input type :  otb::Image (Complex or real)  */
  typedef TImageIn                                  ImageInType;
  /** Typedef to describe the inout image pointer type. */
  typedef typename ImageInType::Pointer             ImageInPointer;
  typedef typename ImageInType::ConstPointer        ImageInConstPointer;
  /** Typedef to describe the inout image region type. */
  typedef typename ImageInType::RegionType          ImageInRegionType;
  /** Typedef to describe the type of pixel and point for inout image. */
  typedef typename ImageInType::PixelType           ImageInPixelType;
  typedef typename ImageInType::PointType           ImageInPointType;
  /** Typedef to describe the image index, size types and spacing for inout image. */
  typedef typename ImageInType::IndexType           ImageInIndexType;
  typedef typename ImageInType::IndexValueType      ImageInIndexValueType;
  typedef typename ImageInType::SizeType            ImageInSizeType;
  typedef typename ImageInType::SizeValueType       ImageInSizeValueType;
  typedef typename ImageInType::SpacingType         ImageInSpacingType;
  typedef typename ImageInType::SpacingValueType    ImageInSpacingValueType;
  
  /** Typedef to image output type : otb::Image (Complex or real)  */
  typedef TImageOut                                  ImageOutType;
  /** Typedef to describe the output image pointer type. */
  typedef typename ImageOutType::Pointer             ImageOutPointer;
  typedef typename ImageOutType::ConstPointer        ImageOutConstPointer;
  /** Typedef to describe the output image region type. */
  typedef typename ImageOutType::RegionType          ImageOutRegionType;
  /** Typedef to describe the type of pixel and point for output image. */
  typedef typename ImageOutType::PixelType           ImageOutPixelType;
  typedef typename ImageOutType::PointType           ImageOutPointType;
  /** Typedef to describe the image index, size types and spacing for output image. */
  typedef typename ImageOutType::IndexType           ImageOutIndexType;
  typedef typename ImageOutType::IndexValueType      ImageOutIndexValueType;
  typedef typename ImageOutType::SizeType            ImageOutSizeType;
  typedef typename ImageOutType::SizeValueType       ImageOutSizeValueType;
  typedef typename ImageOutType::SpacingType         ImageOutSpacingType;
  typedef typename ImageOutType::SpacingValueType    ImageOutSpacingValueType;

  // Define Point2DType and Point3DType
  using Point2DType = itk::Point<double,2>;
  using Point3DType = itk::Point<double,3>;

  // Typedef for iterators
  typedef itk::ImageScanlineConstIterator< ImageInType > InputIterator;
  typedef itk::ImageScanlineIterator< ImageOutType > OutputIterator;

  // Typedef for Functor
  typedef TFunction                                  FunctionType;
  typedef typename FunctionType::Pointer             FunctionPointer;

  // Typedef for grid
  typedef otb::VectorImage<float>                    GridType;
  typedef typename GridType::Pointer                 GridPointer;
  typedef typename GridType::RegionType              GridRegionType;
  typedef typename GridType::IndexType               GridIndexType;
  typedef typename GridType::IndexValueType          GridIndexValueType;
  typedef typename GridType::SizeType                GridSizeType;
  typedef typename GridType::SizeValueType           GridSizeValueType;

  // Setter
  itkSetMacro(SizeTiles, unsigned int);
  itkSetMacro(Margin, unsigned int);
  itkSetMacro(UseMasterGeo, bool);
  void SetMasterImagePtr(ImageInPointer masterImagePtr);
  void SetFunctorPtr(FunctionPointer functorPtr);
  // Getter
  itkGetMacro(SizeTiles, unsigned int);
  itkGetMacro(Margin, unsigned int);
  itkGetMacro(UseMasterGeo, bool);
    
  // Setter/Getter for inputs (image and potentially grid) 
  /** Connect one of the operands for registration */
  void SetGridInput( const GridType* image);

  /** Connect one of the operands for registration */
  void SetImageInput(const ImageInType* image);

  /** Get the inputs */
  const GridType * GetGridInput() const;
  const ImageInType * GetImageInput() const;

  // Configuration for our cache
  void SetConfigForCache(int maxMemoryInMB);

  // Set Grid Parameters for coregistion processing
  void SetGridParameters(float maxShiftInRange, float maxShiftInAzimut, unsigned int gridStepRange , 
			 unsigned int gridStepAzimut);

protected:
  // Constructor
  TilesAnalysisImageFilter();

  // Destructor
  virtual ~TilesAnalysisImageFilter() ITK_OVERRIDE;

  // Print
  void PrintSelf(std::ostream & os, itk::Indent indent) const ITK_OVERRIDE;
  
  /** TilesAnalysisImageFilter produces an image which are into output geometry. The differences between 
   * output and input images can be the size of images and the dimensions. 
   * As such, TilesAnalysisImageFilter needs to provide an implementation for 
   * GenerateOutputInformation() in order to inform the pipeline execution model. 
   */ 
  virtual void GenerateOutputInformation() ITK_OVERRIDE;

  /** TilesAnalysisImageFilter needs a input requested region that corresponds to the margin and shifts 
   * into the requested region of our output requested region. The output requested region needs to be modified
   * to construct as wanted tiles with input size.
   * As such, TilesAnalysesImageFilter needs to provide an implementation for 
   * GenerateInputRequestedRegion() in order to inform the pipeline execution model. 
   * \sa ProcessObject::GenerateInputRequestedRegion() */
  virtual void GenerateInputRequestedRegion() ITK_OVERRIDE;

  /**
   * OutputRegionToInputRegion returns the input region 
   */
  ImageInRegionType OutputRegionToInputRegion(const ImageOutRegionType& outputRegion) const;

  /**
   * OutputRegionToInpuGridtRegion returns the grid region 
   */
  GridRegionType OutputRegionToInputGridRegion(const ImageOutRegionType& outputRegion) const;


  /** 
   * \sa ImageToImageFilter::ThreadedGenerateData(),
   *     ImageToImageFilter::GenerateData() */
  virtual void GenerateData() ITK_OVERRIDE;
  
 private:
  TilesAnalysisImageFilter(const Self&); // purposely not implemented
  void operator=(const Self &); // purposely not 
  
  // Vector for processed tiles (with tiles index)
  std::vector<ImageOutIndexType> m_ProcessedTilesIndex;
    
  // Map to store Tiles in function of Index
  std::map<int, ImageOutPixelType*> m_ProcessedTiles;

  // Size of Tiles
  unsigned int m_SizeTiles;
  
  // Margin for input
  unsigned int m_Margin;
  
  // Use Cache for tiles
  bool m_UseCache;

  // Max Memory for our cache
  int m_MaxMemoryCacheInMB;

  // Function to apply on Output Tiles
  FunctionPointer m_FunctionOnTiles;

  ///////// Parameters used for coregistration /////////
  // Max Shift in range 
  float m_MaxShiftInRange;

  // Max Shift in azimut
  float m_MaxShiftInAzimut;

  // Use Master geometry 
  bool m_UseMasterGeo;
  
  // Master Image (same type as input Image which is the Slave)
  ImageInPointer m_MasterImagePtr;

  // Step (SLC or SAR geo) for input deformation grid
  GridSizeType m_GridStep;
};

} // End namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbTilesAnalysisImageFilter.txx"
#endif



#endif
