/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARCartesianMeanWithMeanPerLineFunctor_h
#define otbSARCartesianMeanWithMeanPerLineFunctor_h
#include <cassert>

#include "itkVariableLengthVector.h"
#include "itkRGBPixel.h"
#include "itkRGBAPixel.h"
#include "itkObject.h"
#include "itkObjectFactory.h"

#include "otbSARPolygonsFunctor.h"
#include "otbSARCartesianMeanFunctor.h" 

namespace otb
{
namespace Function
{
/** \class SARCartesianMeanWithMeanPerLineFunctor
* \brief Catesian mean estimation for each output pixel and output line.
*
* This functor spreads SARCartesianMeanFunctor. It estimates the Cartesian coordonates mean image and
* an optionnal image (Cartesian coordonates mean for each output line).
*
* The characteristics are the following :
* _ Output Geometry : ML (defined by ML factors)
* _ Seven required components : L, C, Y, Z, XCart, YCart and ZCart
* _ Three estimated component : Mean of XCart, YCart and ZCart
* _ Optionnal image : Yes (dimension 1 x nbLineOfMainOutput)
*
* \ingroup DiapOTBModule
*/
  template <class TInputPixel, class TOutputPixel>
  class SARCartesianMeanWithMeanPerLineFunctor : public SARCartesianMeanFunctor<TInputPixel, TOutputPixel>
{
public:
  /** Standard class typedefs */
  typedef SARCartesianMeanWithMeanPerLineFunctor                     Self;
  typedef SARCartesianMeanFunctor<TInputPixel, TOutputPixel>         Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;
 
  /** Runtime information */
  itkTypeMacro(SARCartesianMeanWithMeanPerLineFunctor, itk::Object);

  /**
   * Method synthetize (override)
   */
  // Synthetize to apply on XCart, YCart and Zcart the counter of polygons
  void synthetize(int nbElt, TOutputPixel * outValue, int threadId=1) override
  {
    // ReInitialize m_OutMeanByLine and m_CountPolygonsPerLine
    m_OutMeanByLine[threadId][0] = 0;
    m_OutMeanByLine[threadId][1] = 0;
    m_OutMeanByLine[threadId][2] = 0;
    m_CountPolygonsPerLine[threadId] = 0;

    //////// Mean by line ////////
    for(int ind = 0; ind < nbElt; ind++)
      {
	m_OutMeanByLine[threadId][0] += outValue[ind][0];
	m_OutMeanByLine[threadId][1] += outValue[ind][1];
	m_OutMeanByLine[threadId][2] += outValue[ind][2];

	m_CountPolygonsPerLine[threadId] += this->m_CountPolygons[threadId][ind];
      }

    if (m_CountPolygonsPerLine[threadId] != 0)
      {
	m_OutMeanByLine[threadId][0] /= m_CountPolygonsPerLine[threadId];
	m_OutMeanByLine[threadId][1] /= m_CountPolygonsPerLine[threadId];
	m_OutMeanByLine[threadId][2] /= m_CountPolygonsPerLine[threadId];
      }

    //////// Mean by pixel ////////
    for(int ind = 0; ind < nbElt; ind++)
      {
	if (this->m_CountPolygons[threadId][ind] !=0)
	  {
	    outValue[ind][0] /= this->m_CountPolygons[threadId][ind];
	    outValue[ind][1] /= this->m_CountPolygons[threadId][ind];
	    outValue[ind][2] /= this->m_CountPolygons[threadId][ind];
	  }
      }
  }
  

  /**
   * Method estimateOptionnalImage (override)
   */
  void estimateOptionnalImage(TOutputPixel * outValue, int threadId=1) override
  {
    // Allocation of Components
    outValue[0].Reserve(4);

    // Assignate (only one element per line for this functor)
    outValue[0][0] = m_OutMeanByLine[threadId][0];
    outValue[0][1] = m_OutMeanByLine[threadId][1];
    outValue[0][2] = m_OutMeanByLine[threadId][2];
    
    // IsData always to 1
    outValue[0][3] = 1;    
  }
  

  /** Constructor */ // 
  SARCartesianMeanWithMeanPerLineFunctor(int nbThreads, unsigned int mlran = 1, unsigned int mlazi = 1) : Superclass(nbThreads, mlran, mlazi)
  {    
    // Specific argument
     m_CountPolygonsPerLine = new int[this->m_NbThreads];

    // Allocate the buffer of polygons for each thread 
    for (int i = 0; i < this->m_NbThreads; i++)
      {
	m_CountPolygonsPerLine[i] = 0;
      }
  
    // Set optionnal output (MeanPerline Image) to true
    this->SetWithOptionnalOutput(true);    

    // Allocate the MeanPerline Buffer
    m_OutMeanByLine = new TOutputPixel[this->m_NbThreads];
   
    for (int i = 0; i < this->m_NbThreads; i++)
      {
	m_OutMeanByLine[i].Reserve(4);
	m_OutMeanByLine[i][0] = 0;
	m_OutMeanByLine[i][1] = 0;
	m_OutMeanByLine[i][2] = 0;
	m_OutMeanByLine[i][3] = 0;
      }
  }


  // Redefinition to use construction with NumberOfThreads (Functor used in Multi-Threading)
  static Pointer New(int nbThreads, unsigned int mlran = 1, unsigned int mlazi = 1) 
  {
  Pointer smartPtr = ::itk::ObjectFactory< Self >::Create();    
    if ( smartPtr == nullptr )				     
       {                                                
         smartPtr = new Self(nbThreads, mlran, mlazi);                                
        }                                                
       smartPtr->UnRegister();                            
       return smartPtr;
  }

  /** Destructor */
  ~SARCartesianMeanWithMeanPerLineFunctor() override 
  {
    // free Memory
    delete m_CountPolygonsPerLine;

    for (int i = 0; i < this->m_NbThreads; i++)
      {
	m_OutMeanByLine[i].DestroyExistingData(); // TOutputPixel a itk::VariableLengthVector
      }
  }
  
  
private :
  int * m_CountPolygonsPerLine;
  TOutputPixel * m_OutMeanByLine;
  int m_CountLine;
  };

}
}

#endif
