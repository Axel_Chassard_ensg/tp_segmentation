/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARDEMProjectionImageFilter_txx
#define otbSARDEMProjectionImageFilter_txx

#include "otbSARDEMProjectionImageFilter.h"
#include "otbConfigurationManager.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkProgressReporter.h"
#include "itkNumericTraitsPointPixel.h"

#include <cmath>

namespace otb
{
/** 
 * Constructor with default initialization
 */
  template <class TImageIn, class TImageOut> 
  SARDEMProjectionImageFilter<TImageIn ,TImageOut>::SARDEMProjectionImageFilter()
    : m_SarSensorModelAdapter(ITK_NULLPTR), m_NoData(-32768), m_withXYZ(false), m_withH(false), 
      m_withSatPos(false), m_NbComponents(4), m_indH(4), m_indSatPos(4)
  {
    std::cout << "ConfigurationManager::GetGeoidFile() = " << ConfigurationManager::GetGeoidFile() << std::endl;
   
    DEMHandlerPointerType DEMHandler = DEMHandler::Instance();

    if (!(ConfigurationManager::GetGeoidFile().empty()))
      {
	// DEMHandler instance to specify the geoid file
	DEMHandler->OpenGeoidFile(ConfigurationManager::GetGeoidFile()); 
      }
  }

/**
 * Set Sar Image keyWordList
 */ 
template<class TImageIn, class TImageOut>
void
SARDEMProjectionImageFilter< TImageIn ,TImageOut >
::SetSARImageKeyWorList(ImageKeywordlist sarImageKWL)
{
  // Check if sarImageKWL not NULL
  assert(&sarImageKWL && "SAR Image Metadata don't exist.");
  m_SarImageKwl = sarImageKWL;
}

/**
 * Print
 */
template<class TImageIn, class TImageOut>
void
SARDEMProjectionImageFilter< TImageIn, TImageOut >
::PrintSelf(std::ostream & os, itk::Indent indent) const
{
  Superclass::PrintSelf(os, indent);
}

/**
 * Method GenerateOutputInformaton()
 **/
template<class TImageIn, class TImageOut>
void
SARDEMProjectionImageFilter< TImageIn, TImageOut >
::GenerateOutputInformation()
{
  // Call superclass implementation
  Superclass :: GenerateOutputInformation();

  // Get pointers to the input and output
  ImageInConstPointer inputPtr = this->GetInput();
  ImageOutPointer     outputPtr = this->GetOutput();

  // Image KeyWordList
  ImageKeywordlist inputKwl = inputPtr->GetImageKeywordlist();

  // Set the number of Components for the Output VectorImage
  // At Least 4 Components :  
  //                _ C : Colunm of DEM into SAR Image
  //                _ L : Line of DEM into SAR Image
  //                _ Z : Coordonates Z  
  //                _ Y : Coordonates Y 
  //  (Z,Y)  coordinates, defined into ossimSarSensorMode as follows:
  //  Let n = |sensorPos|,
  //  ps2 = scalar_product(sensorPos,worldPoint)
  //  d = distance(sensorPos,worldPoint)
  // 
  //  Z = n - ps2/n
  //  Y = sqrt(d*d - Z*Z)
  // 
  // m_withH = true => 1 component added H : high
  // m_withXYZ = true => 3 Three components added Xcart Ycart Zcart : cartesien coordonates 
  // m_withSatPos = true => 3 Three components added : Satellite Position into cartesian
  
  // Fill KeyWordList
  inputKwl.AddKey("support_data.band.L", std::to_string(1)); 
  inputKwl.AddKey("support_data.band.C", std::to_string(0));
  inputKwl.AddKey("support_data.band.Z", std::to_string(2));
  inputKwl.AddKey("support_data.band.Y", std::to_string(3));
 
  m_NbComponents = 4;
  if (m_withXYZ)
    {
      m_NbComponents += 3;
      m_indH += 3;
      m_indSatPos += 3;

      inputKwl.AddKey("support_data.band.XCart", std::to_string(4));
      inputKwl.AddKey("support_data.band.YCart", std::to_string(5));
      inputKwl.AddKey("support_data.band.ZCart", std::to_string(6));
    }
    if (m_withH)
    {
      m_NbComponents += 1;
      m_indSatPos += 1;
      
      inputKwl.AddKey("support_data.band.H", std::to_string(m_indH));
    }
  if (m_withSatPos)
    {
      m_NbComponents += 3;

      inputKwl.AddKey("support_data.band.XSatPos", std::to_string(m_indSatPos));
      inputKwl.AddKey("support_data.band.YSatPos", std::to_string(m_indSatPos+1));
      inputKwl.AddKey("support_data.band.ZSatPos", std::to_string(m_indSatPos+2));
    }
  
  
  outputPtr->SetNumberOfComponentsPerPixel(m_NbComponents);
  
  // Create and Initilaze the SarSensorModelAdapter
  m_SarSensorModelAdapter = SarSensorModelAdapter::New();
  bool loadOk = m_SarSensorModelAdapter->LoadState(m_SarImageKwl);
  
  if(!loadOk || !m_SarSensorModelAdapter->IsValidSensorModel())
    {
      itkExceptionMacro(<<"SAR image does not contain a valid SAR sensor model.");
    }

  // Compute the output image size, spacing and origin (same as the input)
  const ImageInSizeType &   inputSize = inputPtr->GetLargestPossibleRegion().GetSize();
  ImageInPointType origin =  inputPtr->GetOrigin();
  const ImageInSpacingType & inSP = inputPtr->GetSpacing();

  ImageOutRegionType outputLargestPossibleRegion = 
    static_cast<ImageOutRegionType>(inputPtr->GetLargestPossibleRegion());
  outputLargestPossibleRegion.SetSize(static_cast<ImageOutSizeType>(inputSize));
  outputPtr->SetLargestPossibleRegion(outputLargestPossibleRegion);
  outputPtr->SetOrigin(static_cast<ImageOutPointType>(origin));
  outputPtr->SetSpacing(static_cast<ImageOutSpacingType>(inSP));

  // Set Image KeyWordList
  outputPtr->SetImageKeywordList(inputKwl);
}

/** 
 * Method GenerateInputRequestedRegion
 */
template<class TImageIn, class TImageOut>
void
SARDEMProjectionImageFilter< TImageIn, TImageOut >
::GenerateInputRequestedRegion()
{
  ImageOutRegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();
  //ImageRegionType inputRequestedRegion = OutputRegionToInputRegion(outputRequestedRegion);

  ImageInRegionType inputRequestedRegion = static_cast<ImageInRegionType>(outputRequestedRegion);
  ImageInPointer  inputPtr = const_cast< ImageInType * >( this->GetInput() );
  
  inputPtr->SetRequestedRegion(inputRequestedRegion);
}


/**
 * Method ThreadedGenerateData
 */
template<class TImageIn, class TImageOut >
void
SARDEMProjectionImageFilter< TImageIn, TImageOut >
::ThreadedGenerateData(const ImageOutRegionType & outputRegionForThread,
                       itk::ThreadIdType threadId)
{
  // Compute corresponding input region (same as output here)
  ImageInRegionType inputRegionForThread = static_cast<ImageInRegionType>(outputRegionForThread);

  //  Define/declare an iterator that will walk the input region for this
  // thread. 
  typedef itk::ImageScanlineConstIterator< ImageInType > InputIterator;
  InputIterator  InIt(this->GetInput(), inputRegionForThread);

  // Define/declare an iterator that will walk the output region for this
  // thread. NO VECTOR IMAGE INTO ITERATOR TEMPLATE
  typedef itk::ImageScanlineIterator< ImageOutType > OutputIterator;
  OutputIterator OutIt(this->GetOutput(), outputRegionForThread);
  
  // Support progress methods/callbacks
  itk::ProgressReporter progress( this, threadId, outputRegionForThread.GetNumberOfPixels() );

  // Transform all points
  InIt.GoToBegin();
  OutIt.GoToBegin();

  Point3DType demGeoPoint;
  Point2DType demLatLonPoint;
  Point2DType col_row(0);
  Point2DType y_z(0);
  Point3DType xyzCart;
  Point3DType satPos;
  Point3DType satVel;

  ImageOutPixelType pixelOutput;
  pixelOutput.Reserve(m_NbComponents);
  
  // Elevation from earth geoid
  double h = 0;

  // For each Line in MNT
  while ( !InIt.IsAtEnd())
    {
      InIt.GoToBeginOfLine();
      OutIt.GoToBeginOfLine();
   
      // For each column in MNT
      while (!InIt.IsAtEndOfLine())
	{
	  // Check if NODATA
	  if (InIt.Get() != m_NoData)
	    {
	      // Trnasform index to Lat/Lon Point
	      this->GetInput()->TransformIndexToPhysicalPoint(InIt.GetIndex(), demLatLonPoint);
	      demGeoPoint[0] = demLatLonPoint[0];
	      demGeoPoint[1] = demLatLonPoint[1];

	      // Get elevation from earth geoid thanks to DEMHandler
	      h = DEMHandler::Instance()->GetHeightAboveEllipsoid(demGeoPoint[0],demGeoPoint[1]);

	      // Correct the height with earth geoid
	      demGeoPoint[2] = InIt.Get() + h;

	      // Call the method of sarSensorModelAdapter
	      m_SarSensorModelAdapter->WorldToLineSampleYZ(demGeoPoint, col_row, y_z);

	      // Fill the four channels for output
	      pixelOutput[0] = col_row[0];
	      pixelOutput[1] = col_row[1];
	      pixelOutput[2] = y_z[1];
	      pixelOutput[3] = y_z[0];

	      // Add channels if required
	      if (m_withXYZ)
		{
		  SarSensorModelAdapter::WorldToCartesian(demGeoPoint, xyzCart);
		  pixelOutput[4] = xyzCart[0];
		  pixelOutput[5] = xyzCart[1];
		  pixelOutput[6] = xyzCart[2];
		}
	      if (m_withH)
		{
		  pixelOutput[m_indH] = demGeoPoint[2];
		}
	      if (m_withSatPos)
		{
		  m_SarSensorModelAdapter->WorldToSatPositionAndVelocity(demGeoPoint, satPos, satVel); 
		  pixelOutput[m_indSatPos] = satPos[0];
		  pixelOutput[m_indSatPos+1] = satPos[1];
		  pixelOutput[m_indSatPos+2] = satPos[2];
		}

	      
	      // Set the output (without itertor because no VectorImage into iterator template)
	      OutIt.Set(pixelOutput);
	      progress.CompletedPixel();
	    }
	  else
	    {
	      // Fill all channels with NoData
	      for (int id = 0; id < m_NbComponents; id++)
		{
		  pixelOutput[id] = m_NoData;
		}
	      
	      // Set the output (without itertor because no VectorImage into iterator template)
	      OutIt.Set(pixelOutput);
	      progress.CompletedPixel();
	    }

	  // Next Col
	  ++InIt;
	  ++OutIt;
	}

      // Next Line
      InIt.NextLine();
      OutIt.NextLine();  
    }

}

} /*namespace otb*/

#endif
