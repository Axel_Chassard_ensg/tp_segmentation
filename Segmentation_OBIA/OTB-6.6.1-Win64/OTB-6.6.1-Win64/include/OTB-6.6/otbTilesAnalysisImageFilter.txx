/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbTilesAnalysisImageFilter_txx
#define otbTilesAnalysisImageFilter_txx

#include "otbTilesAnalysisImageFilter.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkImageRegionIterator.h"
#include "itkProgressReporter.h"
#include "itkNumericTraitsPointPixel.h"

#include <cmath>
#include <algorithm>
#include <omp.h>

namespace otb
{
  /** 
   * Constructor with default initialization
   */
  template <class TImageIn, class TImageOut, class TFunction> 
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >::TilesAnalysisImageFilter()
    :  m_SizeTiles(50), m_Margin(7), m_UseCache(false), m_MaxMemoryCacheInMB(0), m_MaxShiftInRange(0.), 
       m_MaxShiftInAzimut(0.), m_UseMasterGeo(false)
  {
    m_ProcessedTilesIndex.clear();
    m_ProcessedTiles.clear();

    m_GridStep.Fill(1);

    // Inputs required and/or needed
    this->SetNumberOfRequiredInputs(1);
    this->SetNumberOfIndexedInputs(2);
  }
    
  /** 
   * Destructor
   */
  template <class TImageIn, class TImageOut, class TFunction> 
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >::~TilesAnalysisImageFilter()
  {
    // Clear our caches
    typedef typename std::map<int, ImageOutPixelType *>::iterator mapIt;

    if (!m_ProcessedTiles.empty())
      {
	// Free Memory into m_ProcessedTiles
	for (mapIt mapPT = m_ProcessedTiles.begin(); mapPT != m_ProcessedTiles.end(); ++mapPT)
	  {
	      delete mapPT->second;
	      mapPT->second = 0;    
	  }	

	m_ProcessedTiles.clear();
	m_ProcessedTilesIndex.clear();
      }
  }

  /**
   * Print
   */
  template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::PrintSelf(std::ostream & os, itk::Indent indent) const
  {
    Superclass::PrintSelf(os, indent);

    os << indent << "Size of Tiles : " << m_SizeTiles << std::endl;
    os << indent << "Margin for input requested : " << m_Margin << std::endl;
  }

    /**
   * SetFunctorPtr
   */
  template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::SetFunctorPtr(FunctionPointer functorPtr)
  {
    m_FunctionOnTiles = functorPtr;
  }

    /**
   * Set (Slave) Image
   */ 
  template<class TImageIn, class TImageOut, class TFunction>
  void
 TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::SetImageInput(const ImageInType* image )
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(0, const_cast<ImageInType *>(image));
  }

  /**
   * Set Grid
   */ 
  template<class TImageIn, class TImageOut, class TFunction>
  void
 TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::SetGridInput(const GridType* grid)
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(1, const_cast<GridType *>(grid));
  }

  /**
   * Get (Slave) Image
   */ 
  template<class TImageIn, class TImageOut, class TFunction>
  const typename TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >::ImageInType *
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::GetImageInput() const
  {
    if (this->GetNumberOfInputs()<1)
      {
	return 0;
      }
    return static_cast<const ImageInType *>(this->itk::ProcessObject::GetInput(0));
  }

  /**
   * Get Grid
   */ 
  template<class TImageIn, class TImageOut, class TFunction>
  const typename TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >::GridType *
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::GetGridInput() const
  {
    if (this->GetNumberOfInputs()<2)
      {
	return 0;
      }
    return static_cast<const GridType *>(this->itk::ProcessObject::GetInput(1));
  }


  /**
   * SetConfigForCache
   */
  template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::SetConfigForCache(int maxMemoryInMB)
  {
    m_UseCache = true;
    m_MaxMemoryCacheInMB = maxMemoryInMB;
  }

    /**
   * Set Sar Image keyWordList
   */ 
  template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::SetMasterImagePtr(ImageInPointer masterImagePtr)
  {
    // Check if masterImageKWL not NULL
    assert(masterImagePtr && "Master Image Metadata don't exist.");
    m_MasterImagePtr = masterImagePtr;
    m_UseMasterGeo = true;
  }
  
/**
 * SetGridParameters
 */
  template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::SetGridParameters(float maxShiftInRange, float maxShiftInAzimut, unsigned int gridStepRange, 
		      unsigned int gridStepAzimut)
  {
    m_MaxShiftInRange = maxShiftInRange;
    m_MaxShiftInAzimut = maxShiftInAzimut;
    m_GridStep[0] = gridStepRange;
    m_GridStep[1] = gridStepAzimut;

    int gridStep = std::max(gridStepRange, gridStepAzimut);

    // Adapt the tolerance for the two inputs
    this->SetCoordinateTolerance(gridStep);
  }

  /**
   * Method GenerateOutputInformaton()
   **/
  template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::GenerateOutputInformation()
  {
    // Call superclass implementation
    Superclass::GenerateOutputInformation();

    // Get pointers to the input and output
    ImageInConstPointer inputPtr = this->GetImageInput();
    ImageOutPointer     outputPtr = this->GetOutput();

    ImageOutRegionType outputLargestPossibleRegion = inputPtr->GetLargestPossibleRegion();
    ImageOutPointType outOrigin = inputPtr->GetOrigin();
    ImageOutSpacingType outSP = inputPtr->GetSpacing();
    // The output can be defined with the m_MasterImageKwl
    // Origin, Spacing and Size 
    if (m_UseMasterGeo)
      {
	ImageOutSizeType outputSize;
	outputSize[0] = m_MasterImagePtr->GetLargestPossibleRegion().GetSize()[0];
	outputSize[1] = m_MasterImagePtr->GetLargestPossibleRegion().GetSize()[1];
	
	outOrigin = m_MasterImagePtr->GetOrigin();
	
	outSP = m_MasterImagePtr->GetSpacing();

	// Define Output Largest Region
	outputLargestPossibleRegion = m_MasterImagePtr->GetLargestPossibleRegion();
	outputLargestPossibleRegion.SetSize(outputSize);

	// KeyWordList
	ImageKeywordlist outputKWL = m_MasterImagePtr->GetImageKeywordlist();
	outputPtr->SetImageKeywordList(outputKWL);
      }
    
    // Define output Largest Region
    outputPtr->SetLargestPossibleRegion(outputLargestPossibleRegion);
    outputPtr->SetOrigin(outOrigin);
    outputPtr->SetSpacing(outSP);
    

    // Check gridSteps
    if (this->GetGridInput() != nullptr)
      {
	ImageKeywordlist gridKWL = this->GetGridInput()->GetImageKeywordlist();
	if (gridKWL.HasKey("support_data.gridstep.range") && gridKWL.HasKey("support_data.gridstep.azimut"))
	  {
	    unsigned int gridStepRange = atoi(gridKWL.GetMetadataByKey("support_data.gridstep.range").c_str());
	    unsigned int gridStepAzimut = atoi(gridKWL.GetMetadataByKey("support_data.gridstep.azimut").c_str());

	    if (gridStepRange != m_GridStep[0] || gridStepAzimut != m_GridStep[1])
	      {
		itkExceptionMacro(<<"Provided GridSteps are not consistent with grid keywordlist.");
	      }
	  }
      }
  }

  /** 
   * Method OutputRegionToInputRegion for GenerateInputRequestedRegion
   */
  template<class TImageIn, class TImageOut, class TFunction >
  typename TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >::ImageInRegionType 
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::OutputRegionToInputRegion(const ImageOutRegionType& outputRegion) const
  {
    // Compute the input requested region (size and start index)
    // Use the image transformations to insure an input requested region
    // that will provide the proper range
    const ImageOutSizeType & outputRequestedRegionSize = outputRegion.GetSize();
    ImageOutIndexType outputRequestedRegionIndex = outputRegion.GetIndex();

    // Define the margin for each dimension to get the correct input requested region
    int margin_range = m_Margin + static_cast<int>(std::abs(m_MaxShiftInRange)) + 1;
    int margin_azimut = m_Margin + static_cast<int>(std::abs(m_MaxShiftInAzimut)) + 1;

    // Add to output region Margin to get input region
    ImageInIndexType inputRequestedRegionIndex; 
    inputRequestedRegionIndex[0] = static_cast<ImageInIndexValueType>(outputRequestedRegionIndex[0] - 
								      margin_range);
    inputRequestedRegionIndex[1] = static_cast<ImageInIndexValueType>(outputRequestedRegionIndex[1] - 
								      margin_azimut);
    ImageInSizeType inputRequestedRegionSize;
    inputRequestedRegionSize[0] = static_cast<ImageInSizeValueType>(outputRequestedRegionSize[0] + 
								    2*margin_range);
    inputRequestedRegionSize[1] = static_cast<ImageInSizeValueType>(outputRequestedRegionSize[1] + 
								    2*margin_azimut);  
   
    // Check Index and Size
    if (inputRequestedRegionIndex[0] < this->GetImageInput()->GetLargestPossibleRegion().GetIndex()[0])
      {
	inputRequestedRegionIndex[0] = this->GetImageInput()->GetLargestPossibleRegion().GetIndex()[0];
      }
    if (inputRequestedRegionIndex[1] < this->GetImageInput()->GetLargestPossibleRegion().GetIndex()[1])
      {
	inputRequestedRegionIndex[1] = this->GetImageInput()->GetLargestPossibleRegion().GetIndex()[1];
      }
    if ((inputRequestedRegionSize[0] + inputRequestedRegionIndex[0]) > 
	this->GetImageInput()->GetLargestPossibleRegion().GetSize()[0])
      {
	inputRequestedRegionSize[0] = this->GetImageInput()->GetLargestPossibleRegion().GetSize()[0] - 
	  inputRequestedRegionIndex[0];
      }
    if ((inputRequestedRegionSize[1] + inputRequestedRegionIndex[1]) > 
	this->GetImageInput()->GetLargestPossibleRegion().GetSize()[1])
      {
	inputRequestedRegionSize[1] = this->GetImageInput()->GetLargestPossibleRegion().GetSize()[1] - 
	  inputRequestedRegionIndex[1];
      }


    // Transform into a region
    ImageInRegionType inputRequestedRegion = outputRegion;
    inputRequestedRegion.SetIndex(inputRequestedRegionIndex);
    inputRequestedRegion.SetSize(inputRequestedRegionSize);
    
    //inputRequestedRegion.Crop(this->GetImageInput()->GetLargestPossibleRegion());
    
    return inputRequestedRegion;  
  }

/** 
   * Method OutputRegionToInputRegion for GenerateInputRequestedRegion
   */
  template<class TImageIn, class TImageOut, class TFunction >
  typename TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >::GridRegionType 
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::OutputRegionToInputGridRegion(const ImageOutRegionType& outputRegion) const
  {
    // Compute the input requested region (size and start index)
    // Use the image transformations to insure an input grid requested region
    // that will provide the proper range
    const ImageOutSizeType & outputRequestedRegionSize = outputRegion.GetSize();
    ImageOutIndexType outputRequestedRegionIndex = outputRegion.GetIndex();
        
    // Define the index and size for grid requested region. The input grid is on output/master geometry with
    // m_GridStep as factor
    GridIndexType indexGrid;
    indexGrid[0] = static_cast<GridIndexValueType>(outputRequestedRegionIndex[0]/m_GridStep[0]) - 1; 
    indexGrid[1] = static_cast<GridIndexValueType>(outputRequestedRegionIndex[1]/m_GridStep[1]) - 1;

    GridSizeType sizeGrid;
    sizeGrid[0] = static_cast<GridSizeValueType>(outputRequestedRegionSize[0]/m_GridStep[0]) + 3; 
    sizeGrid[1] = static_cast<GridSizeValueType>(outputRequestedRegionSize[1]/m_GridStep[1]) + 3;

    // Check Index and Size
    if (indexGrid[0] < this->GetGridInput()->GetLargestPossibleRegion().GetIndex()[0])
      {
	indexGrid[0] = this->GetGridInput()->GetLargestPossibleRegion().GetIndex()[0];
      }
    if (indexGrid[1] < this->GetGridInput()->GetLargestPossibleRegion().GetIndex()[1])
      {
	indexGrid[1] = this->GetGridInput()->GetLargestPossibleRegion().GetIndex()[1];
      }
    if ((sizeGrid[0] + indexGrid[0]) > 
	this->GetGridInput()->GetLargestPossibleRegion().GetSize()[0])
      {
	sizeGrid[0] = this->GetGridInput()->GetLargestPossibleRegion().GetSize()[0] - 
	  indexGrid[0];
      }
    if ((sizeGrid[1] + indexGrid[1]) > 
	this->GetGridInput()->GetLargestPossibleRegion().GetSize()[1])
      {
	sizeGrid[1] = this->GetGridInput()->GetLargestPossibleRegion().GetSize()[1] - 
	  indexGrid[1];
      }
    
    // Transform into a region1
    GridRegionType gridRequestedRegion = outputRegion;
    gridRequestedRegion.SetIndex(indexGrid);
    gridRequestedRegion.SetSize(sizeGrid);

    return gridRequestedRegion;
  }

  /** 
   * Method GenerateInputRequestedRegion
   */
  template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::GenerateInputRequestedRegion()
  {
    ///////////// Set Output Requestion Region (Contruct Tiles with given size for output) ///////////////
    ImageOutRegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();
    
    // New Index : closest inf multiple of SizeTiles
    ImageOutIndexType outputIndex;
    outputIndex[0] = (static_cast<int>(outputRequestedRegion.GetIndex()[0]/m_SizeTiles))*m_SizeTiles;;
    outputIndex[1] = (static_cast<int>(outputRequestedRegion.GetIndex()[1]/m_SizeTiles))*m_SizeTiles;

    // New Size : closest sup multiple of SizeTiles with addtional when difference between new and old index is 
    // too important
    ImageOutSizeType outputSize;
    outputSize[0] = (static_cast<int>((outputRequestedRegion.GetIndex()[0] - outputIndex[0] + 
				       outputRequestedRegion.GetSize()[0])/m_SizeTiles) + 1)*m_SizeTiles;
    outputSize[1] = (static_cast<int>((outputRequestedRegion.GetIndex()[1] - outputIndex[1] + 
				       outputRequestedRegion.GetSize()[1])/m_SizeTiles) + 1)*m_SizeTiles;
    
    
    // Region affectation (with Crop to respect image dimension)
    outputRequestedRegion.SetSize(outputSize);
    outputRequestedRegion.SetIndex(outputIndex);
    outputRequestedRegion.Crop(this->GetOutput()->GetLargestPossibleRegion());
    this->GetOutput()->SetRequestedRegion(outputRequestedRegion);
    
    ///////////// With the new output requested region, find the region into input image /////////////
    ImageInRegionType inputRequestedRegion = OutputRegionToInputRegion(outputRequestedRegion);
    ImageInPointer  inputPtr = const_cast< ImageInType * >( this->GetImageInput() );
    inputPtr->SetRequestedRegion(inputRequestedRegion);

    ///////////// With the new output requested region, find the region into grid (if needed) /////////////
    if (this->GetGridInput() != nullptr)
      {
	GridRegionType gridRequestedRegion = OutputRegionToInputGridRegion(outputRequestedRegion);
	GridPointer  gridPtr = const_cast< GridType * >( this->GetGridInput() );
	gridPtr->SetRequestedRegion(gridRequestedRegion);
      }
  }


  /**
   * Method ThreadedGenerateData
   */
  template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::GenerateData()
  {
    // Allocate outputs
    this->AllocateOutputs();

    int nbThreads = this->GetNumberOfThreads();
    omp_set_num_threads(nbThreads);

    ImageInPointer  inputPtr = const_cast< ImageInType * >( this->GetImageInput() );
	
    // Get Output and Input Requested region
    ImageInRegionType inputRegion = this->GetImageInput()->GetRequestedRegion();
    ImageOutRegionType outputRegion = this->GetOutput()->GetRequestedRegion();

    // Typedef for Iterators on selected region
    typedef itk::ImageRegionIterator<ImageOutType> OutItType;
        
    // Size of the whole output image
    int outImage_SizeL = this->GetOutput()->GetLargestPossibleRegion().GetSize()[1];
    
    // Define the number of tiles for processing
    int nbTiles_Lines = outputRegion.GetSize()[1]/m_SizeTiles; 
    int nbTiles_Col = outputRegion.GetSize()[0]/m_SizeTiles;
    
    // Check if additionnal tiles are required
    if (outputRegion.GetSize()[1] % m_SizeTiles != 0)  
      {
	++nbTiles_Lines;
      }
    if (outputRegion.GetSize()[0] % m_SizeTiles != 0)
      {
	++nbTiles_Col;
      }
    
    // First Tile = index of output requested region
    int IndL_FirstTiles = outputRegion.GetIndex()[1];
    int IndC_FirstTiles = outputRegion.GetIndex()[0];

    
    ///////////////////////// Pre Processing ///////////////////////
    bool *TilesIsIntoCache = new bool[nbTiles_Lines*nbTiles_Col];
    if (m_UseCache)
      {
	///////////// Clear the map of Processed Tiles if cache used and maxMemory reached //////////////
	// Transform maxMemory into elts
	long unsigned int nbTilesMax = (1024*1024*m_MaxMemoryCacheInMB)/(sizeof(ImageOutPixelType)*m_SizeTiles*m_SizeTiles);
	typedef typename std::map<int, ImageOutPixelType *>::iterator mapIt ;
	// Erase all elts
	if (m_ProcessedTiles.size() > nbTilesMax)
	  {
	    mapIt mapPT = m_ProcessedTiles.begin();
	
	    // Free Memory into m_ProcessedTiles 
	    for (long unsigned int k = 0; k <m_ProcessedTiles.size(); k++)
	      {
		delete mapPT->second;
		mapPT->second = 0;
		++mapPT;
	      }
	    // Erase elts
	    m_ProcessedTiles.clear();
	    m_ProcessedTilesIndex.clear();
	  }

	///////////// Determine if some tiles are already into our cache //////////////
	// For each Tiles
	for (int k = 0; k < (nbTiles_Lines*nbTiles_Col); k++) 
	  {
	    ImageOutIndexType index_current;
	    int id_L = k/nbTiles_Col;        // quotient of Euclidean division (integer division)
	    int id_C = k - id_L*nbTiles_Col; // remainder of Euclidean division
	    index_current[1] = IndL_FirstTiles + id_L*m_SizeTiles;
	    index_current[0] = IndC_FirstTiles + id_C*m_SizeTiles;

	    TilesIsIntoCache[k] = (std::find(m_ProcessedTilesIndex.begin(), m_ProcessedTilesIndex.end(), 
					 index_current) != m_ProcessedTilesIndex.end());
	  }
      }

    // Call Function initialization
    m_FunctionOnTiles->Initialize(nbThreads);

    ///////////////////////// Processing ///////////////////////
#pragma omp parallel for schedule(dynamic)
    // For each Tiles
    for (int k = 0; k < (nbTiles_Lines*nbTiles_Col); k++) 
      {
	// Transform Tiles id into index
	ImageOutIndexType index_current;
	int id_L = k/nbTiles_Col;        // quotient of Euclidean division (integer division)
	int id_C = k - id_L*nbTiles_Col; // remainder of Euclidean division
	index_current[1] = IndL_FirstTiles + id_L*m_SizeTiles;
	index_current[0] = IndC_FirstTiles + id_C*m_SizeTiles;
	    
	// Construct the current tile
	ImageOutIndexType currentOutTilesIndex;
	ImageOutSizeType currentOutTilesSize;
	    
	// Create regions with index and the given size
	// Output tiles
	currentOutTilesIndex[0] = index_current[0];
	currentOutTilesIndex[1] = index_current[1];
	currentOutTilesSize[0] = m_SizeTiles;
	currentOutTilesSize[1] = m_SizeTiles;
	ImageOutRegionType outputCurrentRegion;
	outputCurrentRegion.SetIndex(currentOutTilesIndex);
	outputCurrentRegion.SetSize(currentOutTilesSize);
	
	outputCurrentRegion.Crop(this->GetOutput()->GetLargestPossibleRegion());		
	
	// If Tile is not into cache or m_UseCache = false : process the current tile
	if (!TilesIsIntoCache[k] || !m_UseCache)
	  {
	    ImageOutPixelType * TilesCache = new ImageOutPixelType[outputCurrentRegion.GetSize()[1]*
								   outputCurrentRegion.GetSize()[0]];

	    // Call Function Process : 
	    // Inputs : Ptr Image In and the output tile
	    // Output : Ptr on result array
	    m_FunctionOnTiles->Process(inputPtr, outputCurrentRegion, TilesCache);

	    int compteur = 0;

	    // Assignate Output with function result
	    // Iterators on output
	    OutItType OutIt(this->GetOutput(), outputCurrentRegion);
	    OutIt.GoToBegin();
	    while (!OutIt.IsAtEnd())
	      {
		OutIt.Set(TilesCache[compteur]);
		++compteur; 
		++OutIt;
	      }

	    if (m_UseCache)
	      {
		// Store elts into our two caches : m_ProcessedTiles and m_ProcessedTilesIndex (monothread)
#pragma omp critical
		{
		  int ind = index_current[1] * outImage_SizeL + index_current[0];
		  m_ProcessedTiles[ind] = TilesCache;
		  m_ProcessedTilesIndex.push_back(index_current);
		}
	      }
	    else
	      {
		delete [] TilesCache;
		TilesCache = 0;
	      }
	  }
	// Else use the cache
	else
	  {
	    OutItType OutIt(this->GetOutput(), outputCurrentRegion);
	    OutIt.GoToBegin();
		
	    int ind = index_current[1] * outImage_SizeL + index_current[0];
	    ImageOutPixelType * TilesCache = m_ProcessedTiles[ind];

	    int compteur = 0;

	    while (!OutIt.IsAtEnd())
	      {
		OutIt.Set(TilesCache[compteur]);
		   		   
		++compteur; 
		++OutIt;
	      }
	  }
      }

    delete [] TilesIsIntoCache;

    ///////////// Post Processing ////////////
    // Call Function terminate
    m_FunctionOnTiles->Terminate();
  }



} /*namespace otb*/

#endif
