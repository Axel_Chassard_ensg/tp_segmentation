/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARStreamingCorrelationImageFilter_txx
#define otbSARStreamingCorrelationImageFilter_txx
#include "otbSARStreamingCorrelationImageFilter.h"

#include "itkImageRegionIterator.h"
#include "itkProgressReporter.h"
#include "otbMacro.h"

#include <complex>

namespace otb
{

template<class TInputImage>
PersistentCorrelationImageFilter<TInputImage>
::PersistentCorrelationImageFilter()
 : m_ThreadCor(1),
   m_ThreadSum(1),
   m_ThreadCount(1),
   m_IgnoreInfiniteValues(true),
   m_IgnoreUserDefinedValue(false)
{
  // first output is a copy of the image, DataObject created by
  // superclass
  //
  // allocate the data objects for the outputs which are
  // just decorators around pixel types
  for (int i = 1; i < 3; ++i)
    {
    typename PixelObjectType::Pointer output
      = static_cast<PixelObjectType*>(this->MakeOutput(i).GetPointer());
    this->itk::ProcessObject::SetNthOutput(i, output.GetPointer());
    }
  // allocate the data objects for the outputs which are
  // just decorators around long types
  typename RealObjectType::Pointer output
    = static_cast<RealObjectType*>(this->MakeOutput(3).GetPointer());
  this->itk::ProcessObject::SetNthOutput(3, output.GetPointer());
    

  this->GetCountOutput()->Set(static_cast<long>(0));
  this->GetCorrelationOutput()->Set(itk::NumericTraits<PixelType>::Zero);
  this->GetSumOutput()->Set(itk::NumericTraits<PixelType>::Zero);


  // Initiate the infinite ignored pixel counters
  m_IgnoredInfinitePixelCount= std::vector<unsigned long>(this->GetNumberOfThreads(), 0);
  m_IgnoredUserPixelCount= std::vector<unsigned int>(this->GetNumberOfThreads(), 0);

  this->Reset();
}

template<class TInputImage>
typename itk::DataObject::Pointer
PersistentCorrelationImageFilter<TInputImage>
::MakeOutput(DataObjectPointerArraySizeType output)
{
  switch (output)
    {
    case 0:
      return static_cast<itk::DataObject*>(TInputImage::New().GetPointer());
      break;
    case 1:
      return static_cast<itk::DataObject*>(PixelObjectType::New().GetPointer());
      break;
    case 2:
      return static_cast<itk::DataObject*>(PixelObjectType::New().GetPointer());
      break;
    case 3:
      return static_cast<itk::DataObject*>(LongObjectType::New().GetPointer());
      break;
    default:
      // might as well make an image
      return static_cast<itk::DataObject*>(TInputImage::New().GetPointer());
      break;
    }
}

template<class TInputImage>
typename PersistentCorrelationImageFilter<TInputImage>::PixelObjectType*
PersistentCorrelationImageFilter<TInputImage>
::GetCorrelationOutput()
{
  return static_cast<PixelObjectType*>(this->itk::ProcessObject::GetOutput(1));
}

template<class TInputImage>
const typename PersistentCorrelationImageFilter<TInputImage>::PixelObjectType*
PersistentCorrelationImageFilter<TInputImage>
::GetCorrelationOutput() const
{
  return static_cast<const PixelObjectType*>(this->itk::ProcessObject::GetOutput(1));
}

template<class TInputImage>
typename PersistentCorrelationImageFilter<TInputImage>::PixelObjectType*
PersistentCorrelationImageFilter<TInputImage>
::GetSumOutput()
{
  return static_cast<PixelObjectType*>(this->itk::ProcessObject::GetOutput(2));
}

template<class TInputImage>
const typename PersistentCorrelationImageFilter<TInputImage>::PixelObjectType*
PersistentCorrelationImageFilter<TInputImage>
::GetSumOutput() const
{
  return static_cast<const PixelObjectType*>(this->itk::ProcessObject::GetOutput(2));
}

template<class TInputImage>
typename PersistentCorrelationImageFilter<TInputImage>::LongObjectType*
PersistentCorrelationImageFilter<TInputImage>
::GetCountOutput()
{
  return static_cast<LongObjectType*>(this->itk::ProcessObject::GetOutput(3));
}

template<class TInputImage>
const typename PersistentCorrelationImageFilter<TInputImage>::LongObjectType*
PersistentCorrelationImageFilter<TInputImage>
::GetCountOutput() const
{
  return static_cast<const LongObjectType*>(this->itk::ProcessObject::GetOutput(3));
}

template<class TInputImage>
void
PersistentCorrelationImageFilter<TInputImage>
::GenerateOutputInformation()
{
  Superclass::GenerateOutputInformation();
  if (this->GetInput())
    {
    this->GetOutput()->CopyInformation(this->GetInput());
    this->GetOutput()->SetLargestPossibleRegion(this->GetInput()->GetLargestPossibleRegion());

    if (this->GetOutput()->GetRequestedRegion().GetNumberOfPixels() == 0)
      {
      this->GetOutput()->SetRequestedRegion(this->GetOutput()->GetLargestPossibleRegion());
      }
    }
}
template<class TInputImage>
void
PersistentCorrelationImageFilter<TInputImage>
::AllocateOutputs()
{
  // This is commented to prevent the streaming of the whole image for the first stream strip
  // It shall not cause any problem because the output image of this filter is not intended to be used.
  //InputImagePointer image = const_cast< TInputImage * >( this->GetInput() );
  //this->GraftOutput( image );
  // Nothing that needs to be allocated for the remaining outputs
}

template<class TInputImage>
void
PersistentCorrelationImageFilter<TInputImage>
::Synthetize()
{
  
  int numberOfThreads = this->GetNumberOfThreads();
  
  long     count;
  PixelType correlation;
  PixelType mean;
  
  mean = correlation = itk::NumericTraits<PixelType>::Zero;
  count = 0;

  for (int i = 0; i < numberOfThreads; ++i)
    {
    count += m_ThreadCount[i];
    correlation += m_ThreadCor[i];
    mean += m_ThreadSum[i];
    }
 

  // Set the outputs
  this->GetCorrelationOutput()->Set(correlation);
  this->GetSumOutput()->Set(mean);
  this->GetCountOutput()->Set(count);
}

template<class TInputImage>
void
PersistentCorrelationImageFilter<TInputImage>
::Reset()
{
  int numberOfThreads = this->GetNumberOfThreads();

  // Resize the thread temporaries
  m_ThreadCount.SetSize(numberOfThreads);
  m_ThreadCor.SetSize(numberOfThreads);
  m_ThreadSum.SetSize(numberOfThreads);
  // Initialize the temporaries
  m_ThreadCount.Fill(itk::NumericTraits<long>::Zero);
  m_ThreadCor.Fill(itk::NumericTraits<PixelType>::Zero);
  m_ThreadSum.Fill(itk::NumericTraits<PixelType>::Zero);
  if (m_IgnoreInfiniteValues)
    {
      m_IgnoredInfinitePixelCount= std::vector<unsigned long>(numberOfThreads, 0);
    }

  if (m_IgnoreUserDefinedValue)
    {
      m_IgnoredUserPixelCount= std::vector<unsigned int>(this->GetNumberOfThreads(), 0);
    }
}



/** 
 * Method OutputRegionToInputRegion for GenerateInputRequestedRegion
 */
template<class TInputImage >
typename PersistentCorrelationImageFilter< TInputImage >::RegionType 
PersistentCorrelationImageFilter< TInputImage >
::OutputRegionToInputRegion(const RegionType& outputRegion) const
{
  // Compute the input requested region (size and start index)
  // Use the image transformations to insure an input requested region
  // that will provide the proper range
  const SizeType & outputRequestedRegionSize = outputRegion.GetSize();
  const IndexType & outputRequestedRegionIndex = outputRegion.GetIndex();

  // Compute axis
  unsigned int transformedAxis =  1; // Requirement on line (need one line in more if not the first line)
  unsigned int untransformedAxis = 0; // nb colunm stays the same between input and output

  // Compute input index 
  IndexType  inputRequestedRegionIndex;
  inputRequestedRegionIndex[untransformedAxis] = outputRequestedRegionIndex[untransformedAxis];
  if (outputRequestedRegionIndex[transformedAxis] == 0)
    {
      inputRequestedRegionIndex[transformedAxis] = outputRequestedRegionIndex[transformedAxis];
    }
  else
    {
      inputRequestedRegionIndex[transformedAxis] = static_cast<IndexValueType>(
      outputRequestedRegionIndex[transformedAxis] - 1 );
    }

  // Compute input size
  SizeType inputRequestedRegionSize;
  inputRequestedRegionSize[untransformedAxis] = outputRequestedRegionSize[untransformedAxis];
  if (outputRequestedRegionIndex[transformedAxis] == 0)
    {
      inputRequestedRegionSize[transformedAxis] = outputRequestedRegionSize[transformedAxis];
    }
  else
    {
      inputRequestedRegionSize[transformedAxis] = static_cast<SizeValueType>(
      outputRequestedRegionSize[transformedAxis] + 1 ) ;
    }
 
 
  // Input region
  RegionType inputRequestedRegion = outputRegion;
  inputRequestedRegion.SetIndex(inputRequestedRegionIndex);
  inputRequestedRegion.SetSize(inputRequestedRegionSize);

  return inputRequestedRegion;  
}
/** 
 * Method OutputRegionToInputRegion
 */
template<class TInputImage>
void
PersistentCorrelationImageFilter< TInputImage >
::GenerateInputRequestedRegion()
{
  RegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();
  RegionType inputRequestedRegion = OutputRegionToInputRegion(outputRequestedRegion);

  InputImagePointer  inputPtr = const_cast< ImageType * >( this->GetInput() );
  
  inputPtr->SetRequestedRegion(inputRequestedRegion);
}



template<class TInputImage>
void
PersistentCorrelationImageFilter<TInputImage>
::ThreadedGenerateData(const RegionType& outputRegionForThread,
                       itk::ThreadIdType threadId)
{
  /**
   * Grab the input
   */
  InputImagePointer inputPtr =  const_cast<TInputImage *>(this->GetInput(0));
  // Compute corresponding input region
  RegionType inputRegionForThread = OutputRegionToInputRegion(outputRegionForThread);

  // support progress methods/callbacks
  itk::ProgressReporter progress(this, threadId, outputRegionForThread.GetNumberOfPixels());

  //  Define/declare an iterator that will walk the input region for this
  // thread. 
  typedef itk::ImageScanlineConstIterator< ImageType > InputIterator;
  InputIterator  inCurrentLineIt(this->GetInput(), inputRegionForThread);
  InputIterator  inPreviousLineIt(this->GetInput(), inputRegionForThread);
  
  inCurrentLineIt.GoToBegin();
  inPreviousLineIt.GoToBegin();
  
  // Current line = previous line + 1 
  inCurrentLineIt.NextLine();

  PixelType valueCurrentLine;
  PixelType valuePreviousLine;

  RealType  realValueCurrentLine;
  RealType  imgValueCurrentLine;
  RealType  realValuePreviousLine;
  RealType  imgValuePreviousLine;

  // For each line 
  while ( !inCurrentLineIt.IsAtEnd())
  {
    inCurrentLineIt.GoToBeginOfLine();
    inPreviousLineIt.GoToBeginOfLine();

    // For each column
    while (!inCurrentLineIt.IsAtEndOfLine() && !inPreviousLineIt.IsAtEndOfLine())
    {
      valueCurrentLine = inCurrentLineIt.Get();
      valuePreviousLine = inPreviousLineIt.Get();
      
      realValueCurrentLine = std::real(valueCurrentLine);
      imgValueCurrentLine = std::imag(valueCurrentLine);
      
      realValuePreviousLine = std::real(valuePreviousLine);
      imgValuePreviousLine = std::imag(valuePreviousLine);				     
      
      bool valueCurrentIsFinite = (vnl_math_isfinite(realValueCurrentLine) && 
				 vnl_math_isfinite(imgValueCurrentLine));
      
      bool valuePreviousIsFinite = (vnl_math_isfinite(realValuePreviousLine) && 
				 vnl_math_isfinite(imgValuePreviousLine));
      
      
      // Assigne m_IgnoredInfinitePixelCount if some values are infinite
       if (m_IgnoreInfiniteValues && !(valueCurrentIsFinite || valuePreviousIsFinite))
	 {
	   ++m_IgnoredInfinitePixelCount[threadId];
	 }
       // else finite
       else
	 {
	   if (m_IgnoreUserDefinedValue && 
	       (realValueCurrentLine == m_UserIgnoredValue || realValuePreviousLine == m_UserIgnoredValue))
	     {
	       ++m_IgnoredUserPixelCount[threadId];
	     }
	   else 
	     {
	       m_ThreadCor[threadId] += valueCurrentLine*std::conj(valuePreviousLine);
	       m_ThreadSum[threadId] += valueCurrentLine;
	       ++m_ThreadCount[threadId];
	     }
	 }
       
       // Next colunm
       ++inCurrentLineIt;
       ++inPreviousLineIt;
       
    }

    // Next Line
    inCurrentLineIt.NextLine();
    inPreviousLineIt.NextLine();
  }
}

template <class TImage>
void
PersistentCorrelationImageFilter<TImage>
::PrintSelf(std::ostream& os, itk::Indent indent) const
{
  Superclass::PrintSelf(os, indent);

  os << indent << "Correlation: "
     << static_cast<typename itk::NumericTraits<PixelType>::PrintType>(this->GetCorrelation()) << std::endl;
  os << indent << "Sum: "
     << static_cast<typename itk::NumericTraits<PixelType>::PrintType>(this->GetSum()) << std::endl;
  os << indent << "Count: "      << this->GetCount() << std::endl;
}
} // end namespace otb
#endif
