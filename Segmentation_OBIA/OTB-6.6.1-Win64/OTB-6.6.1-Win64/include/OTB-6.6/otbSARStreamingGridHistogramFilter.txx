/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARStreamingGridHistogramFilter_txx
#define otbSARStreamingGridHistogramFilter_txx
#include "otbSARStreamingGridHistogramFilter.h"

#include "itkImageRegionIterator.h"
#include "itkImageScanlineConstIterator.h"
#include "itkProgressReporter.h"
#include "otbMacro.h"

#include <complex>

namespace otb
{

template<class TInputImage>
PersistentGridHistogramFilter<TInputImage>
::PersistentGridHistogramFilter()
  : m_StepHist(-1.),
    m_MeanShift(-1.),
    m_Dimension(0),
    m_Threshold(0.3),
    m_NumberHist(256)
{

  // allocate the data objects for the outputs which are
  // just decorators around vector of real values. These values
  // represent the selected shifts from the input grid into the the choosen dimension.
  typename RealVectorObjectType::Pointer output
      = static_cast<RealVectorObjectType*>(this->MakeOutput(4).GetPointer());
  this->itk::ProcessObject::SetNthOutput(1, output.GetPointer());
    
  int numberOfThreads = this->GetNumberOfThreads();
  
  // Fill the Thread Vector with new pointers 
  for (int i = 0; i < numberOfThreads; ++i)
    {
      m_Thread_Shifts.push_back(new std::vector<RealType>());
    }
    
  this->GetVectorShifts_Output()->Set(new std::vector<RealType>());

  this->Reset();
}

template<class TInputImage>
typename itk::DataObject::Pointer
PersistentGridHistogramFilter<TInputImage>
::MakeOutput(DataObjectPointerArraySizeType output)
{
  switch (output)
    {
    case 0:
      return static_cast<itk::DataObject*>(TInputImage::New().GetPointer());
      break;
    case 1:
      return static_cast<itk::DataObject*>(PixelObjectType::New().GetPointer());
      break;
    case 2:
      return static_cast<itk::DataObject*>(PixelObjectType::New().GetPointer());
      break;
    case 3:
      return static_cast<itk::DataObject*>(LongObjectType::New().GetPointer());
      break;
    case 4:
      return static_cast<itk::DataObject*>(RealVectorObjectType::New().GetPointer());
      break;
    default:
      // might as well make an image
      return static_cast<itk::DataObject*>(TInputImage::New().GetPointer());
      break;
    }
}

template<class TInputImage>
typename PersistentGridHistogramFilter<TInputImage>::RealVectorObjectType*
PersistentGridHistogramFilter<TInputImage>
::GetVectorShifts_Output()
{
  return static_cast<RealVectorObjectType*>(this->itk::ProcessObject::GetOutput(1));
}

template<class TInputImage>
const typename PersistentGridHistogramFilter<TInputImage>::RealVectorObjectType*
PersistentGridHistogramFilter<TInputImage>
::GetVectorShifts_Output() const
{
  return static_cast<const RealVectorObjectType*>(this->itk::ProcessObject::GetOutput(1));
}


template<class TInputImage>
void
PersistentGridHistogramFilter<TInputImage>
::GenerateOutputInformation()
{
  Superclass::GenerateOutputInformation();
  if (this->GetInput())
    {
      this->GetOutput()->CopyInformation(this->GetInput());
      this->GetOutput()->SetLargestPossibleRegion(this->GetInput()->GetLargestPossibleRegion());

      if (this->GetOutput()->GetRequestedRegion().GetNumberOfPixels() == 0)
	{
	  this->GetOutput()->SetRequestedRegion(this->GetOutput()->GetLargestPossibleRegion());
	}
    }
}
template<class TInputImage>
void
PersistentGridHistogramFilter<TInputImage>
::AllocateOutputs()
{
  // This is commented to prevent the streaming of the whole image for the first stream strip
  // It shall not cause any problem because the output image of this filter is not intended to be used.
  //InputImagePointer image = const_cast< TInputImage * >( this->GetInput() );
  //this->GraftOutput( image );
  // Nothing that needs to be allocated for the remaining outputs
}

template<class TInputImage>
void
PersistentGridHistogramFilter<TInputImage>
::Synthetize()
{
  int numberOfThreads = this->GetNumberOfThreads();
  
  // Fill the Output Vector with selected shifts 
  for (int i = 0; i < numberOfThreads; ++i)
    {
      if (!m_Thread_Shifts[i]->empty())
	{
	  for (long unsigned int j = 0; j < m_Thread_Shifts[i]->size(); j++)
	    {
	      //this->GetVectorShifts_Output()->Set(this->GetVectorShifts().push_back(m_Thread_Shifts[i]->at(j)); 
	      this->GetVectorShifts()->push_back(m_Thread_Shifts[i]->at(j)); // Ref ??
	    }
	}
    }
}

template<class TInputImage>
void
PersistentGridHistogramFilter<TInputImage>
::Reset()
{
  int numberOfThreads = this->GetNumberOfThreads();

  // Resize the thread temporaries
  //m_Thread_Shifts.SetSize(numberOfThreads);
  
  // Initialize the temporaries
  for (int i = 0; i < numberOfThreads; i++) 
    {
      m_Thread_Shifts[i]->clear();
    }
  
}


/** 
 * Method GenerateInputRequestedRegion
 */
template<class TInputImage>
void
PersistentGridHistogramFilter< TInputImage >
::GenerateInputRequestedRegion()
{
  RegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();
  RegionType inputRequestedRegion = outputRequestedRegion;

  InputImagePointer  inputPtr = const_cast< ImageType * >( this->GetInput() );
  
  inputPtr->SetRequestedRegion(inputRequestedRegion);
}



template<class TInputImage>
void
PersistentGridHistogramFilter<TInputImage>
::ThreadedGenerateData(const RegionType& outputRegionForThread,
                       itk::ThreadIdType threadId)
{
  /**
   * Grab the input
   */
  //InputImagePointer inputPtr =  const_cast<TInputImage *>(this->GetInput(0));
  // Compute corresponding input region (same as output here)
  RegionType inputRegionForThread = outputRegionForThread;

  // support progress methods/callbacks
  itk::ProgressReporter progress(this, threadId, outputRegionForThread.GetNumberOfPixels());

  //  Define/declare an iterator that will walk the input region (input grid) for this
  // thread. 
  typedef itk::ImageScanlineConstIterator< ImageType > InputIterator;
  InputIterator  inIt(this->GetInput(), inputRegionForThread);
  
  inIt.GoToBegin();

  double minShift = 0.;
  double maxShift = 0.;

  if (m_StepHist != -1.)
    {
      // Narrow selection
      minShift = m_MeanShift - m_NumberHist * m_StepHist / 3.;
      maxShift = m_MeanShift + m_NumberHist * m_StepHist / 3.;
    }

  
  // For each line 
  while ( !inIt.IsAtEnd())
  {
    inIt.GoToBeginOfLine();
   
    // For each column
    while (!inIt.IsAtEndOfLine())
    {
      // Check Correlation rate
      if (inIt.Get()[2] > m_Threshold)
	{
	  // Check min and max
	  if (m_StepHist != -1.)
	    {
	      if (inIt.Get()[m_Dimension] > minShift && inIt.Get()[m_Dimension] < maxShift )
		{
		  m_Thread_Shifts[threadId]->push_back(inIt.Get()[m_Dimension]);
		}
	    }
	  else
	    {
	      m_Thread_Shifts[threadId]->push_back(inIt.Get()[m_Dimension]);
	    }
	}
            
       // Next colunm
       ++inIt;
    }

    // Next Line
    inIt.NextLine();
  }
}

template <class TImage>
void
PersistentGridHistogramFilter<TImage>
::PrintSelf(std::ostream& os, itk::Indent indent) const
{
  Superclass::PrintSelf(os, indent);

}
} // end namespace otb
#endif
