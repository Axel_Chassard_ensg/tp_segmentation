/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARPolygonsFunctor_h
#define otbSARPolygonsFunctor_h
#include <cassert>

#include "itkVariableLengthVector.h"
#include "itkRGBPixel.h"
#include "itkRGBAPixel.h"
#include "itkObject.h"
#include "itkObjectFactory.h"

#include <map>
#include <string>

#define sign(a,b) (((b)>=0)?fabs((double)(a)):-fabs((double)(a)))

namespace otb
{
namespace Function
{
/** \class SARPolygonsFunctor
* \brief Base class for functor used on polygons into SARDEMPolygonsAnalysis 
*
* This base class contains four main functions :
* _ initialize : to initialize the outValue array (one for each output line). Initialize a simple array by default.
* _ estimate : to calculate the outValue array. Has to be implemented. 
* _ synthetize : to synthetize the estimation made into estimate function. 
* Useful to calculate mean, for example. Empty By default.
* _ estimateOptionnalImage : to estimate the optionnal image. Empty By default.
*
* Several setter/getter are available to specify each functor.
* For example each functor can choose :
* _ The main output geometry (SAR or ML) 
* _ The required components into projeted DEM
* _ The estimated components
* _ The optionnal image estimation
*
* \sa PolygonsFunctor
*
* \ingroup DiapOTBModule
*/
template <class TInputPixel, class TOutputPixel>
class SARPolygonsFunctor : public itk::Object
{
public:
  /** Standard class typedefs */
  typedef SARPolygonsFunctor        Self;
  typedef itk::Object                   Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;
 
  /** Runtime information */
  itkTypeMacro(SARPolygonsFunctor, itk::Object);

  // Initialize method
  virtual void initalize(int nbElt, TOutputPixel * outValue, int /*threadId=1*/)
  {
    // Re Initialize outValue
    for(int ind = 0; ind < nbElt; ind++)
      {
	outValue[ind] = 0;
      }
  }
  
  // Estimate method
  virtual void estimate(const int ind_LineSAR, TInputPixel * CLZY_InSideUpPtr, 
			TInputPixel * CLZY_InSideDownPtr, 
			TInputPixel * CLZY_OutSideUpPtr, 
			TInputPixel * CLZY_OutSideDownPtr, 
			int firstCol_into_outputRegion, 
			int nbCol_into_outputRegion, TOutputPixel * outValue,
			bool isFirstMaille, double & tgElevationMaxForCurrentLine, int threadId=1) = 0;

  // Synthetize method
  virtual void synthetize(int /*nbElt*/, TOutputPixel * /*outValue*/, int /*threadId=1*/)
  {    
  }
  
  // estimateOptionnalImage method
  virtual void estimateOptionnalImage(TOutputPixel * /*outValue*/, int /*threadId=1*/)
  {
  }

  // Getter/Setter
  itkSetMacro(NumberOfExpectedComponents, unsigned int);
  itkSetMacro(NumberOfEstimatedComponents, unsigned int);
  itkSetMacro(OutputGeometry, std::string);
  itkSetMacro(WithOptionnalOutput, bool);
 
  void SetEstimatedComponents(std::vector<std::string> vecEstimated)
  {
    m_EstimatedComponents = vecEstimated;
  }

  void SetRequiredComponents(std::vector<std::string> vecRequired)
  {
    m_RequiredComponents = vecRequired;
  }

  void SetSARDimensions(int nbColSAR, int nbLinesSAR)
  {
    m_NbColSAR = nbColSAR;
    m_NbLinesSAR = nbLinesSAR; 
  }

  virtual void SetRequiredComponentsToInd(std::map<std::string, int> mapForInd)
  {
    std::map<std::string, int>::const_iterator it = mapForInd.begin();
    while(it!=mapForInd.end())
      {
	m_RequiredComponentsToInd[it->first] = it->second;
	++it;
      }
  }

  itkGetMacro(NumberOfExpectedComponents, unsigned int);
  itkGetMacro(NumberOfEstimatedComponents, unsigned int);
  itkGetMacro(EstimatedComponents, std::vector<std::string>);
  itkGetMacro(RequiredComponents, std::vector<std::string>);
  itkGetMacro(OutputGeometry, std::string);
  itkGetMacro(NbColSAR, int);
  itkGetMacro(NbLinesSAR, int);
  itkGetMacro(WithOptionnalOutput, bool);
  virtual void GetOutputGeometry(std::string & outputGeo, unsigned int & mlran, unsigned int & mlazi)
  {
    outputGeo = m_OutputGeometry;
    mlran = 1;
    mlazi = 1;
  }
  std::map<std::string, int> & GetRequiredComponentsToInd()
  {
    return m_RequiredComponentsToInd;    
  }

  /** Constructor */
  SARPolygonsFunctor()
  {
    m_NumberOfExpectedComponents = 1;
    m_NumberOfEstimatedComponents = 1;
    m_OutputGeometry = "SAR";
    m_WithOptionnalOutput = false;
  }

  /** Destructor */
  ~SARPolygonsFunctor() override {}

private :

  // Number of expected Components for input 
  unsigned m_NumberOfExpectedComponents;
  // Number of estimated Components for output 
  unsigned m_NumberOfEstimatedComponents;
  // Geometry of output image into PolygonsAnalysis Filter (SAR or Polygons)
  std::string m_OutputGeometry;
  // Vector of required Components
  std::vector<std::string> m_RequiredComponents;
  // Map of required Components
  std::map<std::string, int> m_RequiredComponentsToInd;
  // Vector of estimated Components
  std::vector<std::string> m_EstimatedComponents;
  // SAR Dimensions
  int m_NbColSAR;
  int m_NbLinesSAR;
  // Boolean for optionnal output
  bool m_WithOptionnalOutput; // Optionnal output must be a complement of the main output (small image)

};
    
  }
}

#endif
