/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARStreamingGridHistogramFilter_h
#define otbSARStreamingGridHistogramFilter_h

#include "otbPersistentImageFilter.h"
#include "itkNumericTraits.h"
#include "itkArray.h"
#include "itkSimpleDataObjectDecorator.h"
#include "itkPoint.h"

#include "otbPersistentFilterStreamingDecorator.h"

#include "otbSarSensorModelAdapter.h"
#include "otbImageKeywordlist.h"

#include <complex>
#include <cmath>

namespace otb
{

/** \class PersistentGridHistogramFilter
 * \brief Retrive some shifts from the input grid. The grid is a vector image of three components :
 * shift_range, shift_azimut and correlation_rate. The selection on shifts depends on the correlation rate, 
 * the mean shift into the choosen dimension and the step of current histogram.
 *
 * This filter persists its temporary data. It means that if you Update it n times on n different
 * requested regions, the output statistics will be the statitics of the whole set of n regions.
 *
 * To reset the temporary data, one should call the Reset() function.
 *
 * To get the wanted values once the regions have been processed via the pipeline, use the Synthetize() method.
 *
 * \ingroup DiapOTBModule
 */
template<class TInputImage>
class ITK_EXPORT PersistentGridHistogramFilter :
  public PersistentImageFilter<TInputImage, TInputImage>
{
public:
  /** Standard Self typedef */
  typedef PersistentGridHistogramFilter                 Self;
  typedef PersistentImageFilter<TInputImage, TInputImage> Superclass;
  typedef itk::SmartPointer<Self>                         Pointer;
  typedef itk::SmartPointer<const Self>                   ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Runtime information support. */
  itkTypeMacro(PersistentGridHistogramFilter, PersistentImageFilter);

  /** Image related typedefs. */
  typedef TInputImage                   ImageType;
  typedef typename TInputImage::Pointer InputImagePointer;

  typedef typename TInputImage::RegionType RegionType;
  typedef typename TInputImage::SizeType   SizeType;
  typedef typename TInputImage::IndexType  IndexType;
  typedef typename TInputImage::PixelType  PixelType;
  typedef typename ImageType::IndexValueType IndexValueType;
  typedef typename ImageType::SizeValueType  SizeValueType;

  itkStaticConstMacro(InputImageDimension, unsigned int,
                      TInputImage::ImageDimension);

  /** Image related typedefs. */
  itkStaticConstMacro(ImageDimension, unsigned int,
                      TInputImage::ImageDimension);

  /** Type to use for computations. */
  //  typedef typename itk::NumericTraits<PixelType>::RealType RealType;
  typedef typename itk::NumericTraits<double>::RealType RealType;
 
  /** Smart Pointer type to a DataObject. */
  typedef typename itk::DataObject::Pointer DataObjectPointer;
  typedef itk::ProcessObject::DataObjectPointerArraySizeType DataObjectPointerArraySizeType;

  /** Type of DataObjects used for scalar outputs */
  typedef itk::SimpleDataObjectDecorator<RealType>  RealObjectType;
  typedef itk::SimpleDataObjectDecorator<std::vector<RealType> *>  RealVectorObjectType;
  typedef itk::SimpleDataObjectDecorator<long>  LongObjectType;
  typedef itk::SimpleDataObjectDecorator<PixelType> PixelObjectType;

  /** Return the vectors of selected shifts for the two dimensions. */
  std::vector<RealType> * GetVectorShifts() const
  {
    return this->GetVectorShifts_Output()->Get();
  }
  RealVectorObjectType* GetVectorShifts_Output();
  const RealVectorObjectType* GetVectorShifts_Output() const;


  /** Make a DataObject of the correct type to be used as the specified
   * output. */
  DataObjectPointer MakeOutput(DataObjectPointerArraySizeType idx) ITK_OVERRIDE;
  using Superclass::MakeOutput;

  /**
   * Synthetize and Reset function called by our PersistentFilterStreamingDecorator
   */
  void Synthetize(void) ITK_OVERRIDE;
  void Reset(void) ITK_OVERRIDE;

  // Getter/Setter
  itkSetMacro(StepHist, RealType);
  itkGetMacro(StepHist, RealType);
  itkSetMacro(MeanShift, RealType);
  itkGetMacro(MeanShift, RealType);
  itkSetMacro(Dimension, int);
  itkGetMacro(Dimension, int);
  itkSetMacro(Threshold, float);
  itkGetMacro(Threshold, float);
  itkSetMacro(NumberHist, int);
  itkGetMacro(NumberHist, int);

protected:
  PersistentGridHistogramFilter();
  ~PersistentGridHistogramFilter() ITK_OVERRIDE {}
  void PrintSelf(std::ostream& os, itk::Indent indent) const ITK_OVERRIDE;

  
  /** PersistentGridHistogramFilter needs a input requested region with the same size of
      the output requested region.
   * \sa ProcessObject::GenerateInputRequestedRegion() */
  virtual void GenerateInputRequestedRegion() ITK_OVERRIDE;

  /** Multi-thread version GenerateData. */
  void  ThreadedGenerateData(const RegionType&
                             outputRegionForThread,
                             itk::ThreadIdType threadId) ITK_OVERRIDE;

  /** Pass the input through unmodified. Do this by Grafting in the
   *  AllocateOutputs method.
   */
  void AllocateOutputs() ITK_OVERRIDE;
  void GenerateOutputInformation() ITK_OVERRIDE;

private:
  PersistentGridHistogramFilter(const Self &); //purposely not implemented
  void operator =(const Self&); //purposely not implemented

  std::vector<std::vector<RealType> * >       m_Thread_Shifts;

  // Step for the histogram
  RealType m_StepHist;
  RealType m_MeanShift;
  // Dimension for the selection : 0 = range, 1 = azimut 
  int      m_Dimension; 
  // Threshold on correlation rate
  float    m_Threshold;
  // Number of values range
  int      m_NumberHist;


}; // end of class PersistentGridHistogramFilter

/*===========================================================================*/

/** \class SARStreamingGridHistogramFilter
 * \brief This class streams the whole input image through the PersistentGridHistogramFilter.
 *
 * This way, it allows computing the first order global statistics of this image. It calls the
 * Reset() method of the PersistentGridHistogramFilter before streaming the image and the
 * Synthetize() method of the PersistentGridHistogramFilter after having streamed the image
 * to compute the statistics. The accessor on the results are wrapping the accessors of the
 * internal PersistentGridHistogramFilter.
 *
 * This filter can be used as:
 * \code
 * typedef otb::SARStreamingGridHistogramFilter<ImageType> StatisticsType;
 * StatisticsType::Pointer statistics = StatisticsType::New();
 * statistics->SetInput(reader->GetOutput());
 * statistics->SetDimension(0);
 * statistics->SetStepHist(0.2);
 * statistics->SetThreshold(0.3);
 * statistics->SetMeanShift(-11.5);
 * statistics->SetNumberHist(256);
 * statistics->Update();
 * double step, meanShift;
 * statistics->GetGridHistogramInformation(step, meanShift);
 * std::cout << step << ", " <<  meanShift << std::endl;
 * \endcode
 *
 * \sa PersistentGridHistogramFilter
 * \sa PersistentImageFilter
 * \sa PersistentFilterStreamingDecorator
 * \sa StreamingImageVirtualWriter
 * \ingroup Streamed
 * \ingroup Multithreaded
 * \ingroup MathematicalStatisticsImageFilters
 *
 * \ingroup DiapOTBModule
 */

template<class TInputImage>
class ITK_EXPORT SARStreamingGridHistogramFilter :
  public PersistentFilterStreamingDecorator<PersistentGridHistogramFilter<TInputImage> >
{
public:
  /** Standard Self typedef */
  typedef SARStreamingGridHistogramFilter Self;
  typedef PersistentFilterStreamingDecorator
  <PersistentGridHistogramFilter<TInputImage> > Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Type macro */
  itkNewMacro(Self);

  /** Creation through object factory macro */
  itkTypeMacro(StreamingGridHistogramFilter, PersistentFilterStreamingDecorator);

  typedef typename Superclass::FilterType    StatFilterType;
  typedef typename StatFilterType::PixelType PixelType;
  typedef typename StatFilterType::RealType  RealType;
  typedef TInputImage                        InputImageType;

  /** Type of DataObjects used for scalar outputs */
  typedef itk::SimpleDataObjectDecorator<RealType>  RealObjectType;
  typedef itk::SimpleDataObjectDecorator<long>  LongObjectType;
  typedef itk::SimpleDataObjectDecorator<PixelType> PixelObjectType;
  typedef itk::SimpleDataObjectDecorator<std::vector<RealType> *>  RealVectorObjectType;

  /** Image related typedefs. */
  typedef TInputImage                   ImageType;
  typedef typename TInputImage::Pointer InputImagePointer;

  typedef typename TInputImage::RegionType RegionType;
  typedef typename TInputImage::SizeType   SizeType;
  typedef typename TInputImage::IndexType  IndexType;

   // Define Point2DType and Point3DType
  using Point2DType = itk::Point<double,2>;
  using Point3DType = itk::Point<double,3>;

  using Superclass::SetInput;
  void SetInput(InputImageType * input)
  {
    this->GetFilter()->SetInput(input);
  }
  const InputImageType * GetInput()
  {
    return this->GetFilter()->GetInput();
  }

  // Setter for Streaming and Persistent Filter
  void SetStepHist(RealType stepHist)
  {
    this->GetFilter()->SetStepHist(stepHist);
  }
  void SetMeanShift(RealType meanShift)
  {
    this->GetFilter()->SetMeanShift(meanShift);
  }
  void SetThreshold(float threshold)
  {
    this->GetFilter()->SetThreshold(threshold);
  }
  void SetNumberHist(int numberHist)
  {
    this->GetFilter()->SetNumberHist(numberHist);
    m_NumberHist = numberHist;
  }
  void SetDimension(int dimension)
  {
    if (dimension == 0 || dimension == 1)
      {
	this->GetFilter()->SetDimension(dimension);
      }
    else
      {
	std::cout << "Wrong dimension : Only 0 for range and 1 for azimut" << std::endl;
	this->GetFilter()->SetDimension(0);
      }
  }


  /** Return vectors. */
  std::vector<RealType> * GetVectorShifts() const
  {
    return this->GetFilter()->GetVectorShifts_Output()->Get();
  }
  RealVectorObjectType* GetVectorShifts_Output()
  {
    return this->GetFilter()->GetVectorShifts_Output();
  }
  const RealVectorObjectType* GetVectorShifts_Output() const
  {
    return this->GetFilter()->GetVectorShifts_Output();
  }

  
  // Get step and max occurence for the current histogram
  void GetGridHistogramInformation(double & step, double & meanShift)
  {
    // Retrieve selected shifts
    std::vector<RealType> * shifts = this->GetVectorShifts();

    // Estimate min, max and mean for each dimension
    double minShift = 1E20;
    double maxShift = -1E20;
    
    double sumShift = 0.;

    for (long unsigned int i = 0; i < shifts->size(); i++) 
      {
	sumShift += shifts->at(i);
	minShift = std::min(minShift, shifts->at(i));
	maxShift = std::max(maxShift, shifts->at(i));
      }

    meanShift = sumShift/shifts->size();
    
    // Assignate the step
    step = (maxShift - minShift) / (float)(m_NumberHist - 1.);

    // Initialize the histogram
    float *histo = new float[m_NumberHist];
    float *plages = new float[m_NumberHist];
    for (int index = 0; index < m_NumberHist; index++)
	{
	  histo[index] = 0;
	  plages[index] = minShift + index * step;
	}

    // Estimate the histogram
    for (long unsigned int i = 0; i < shifts->size(); i++) 
      {
	int index = int((shifts->at(i) - minShift) / step);
	if ((index >= 0) && (index < m_NumberHist))
	  {
	    ++histo[index];
	  }
      }

    // Determinate the maximum
    int index_top = 0;
	
    for (int index = 1; index < m_NumberHist; index++)
      {
	if (histo[index] > histo[index_top])
	  {
	    index_top = index;
	  }
      }
    
    // Assignate meanShift
    meanShift = plages[index_top];

    delete [] histo;
    delete [] plages;
  }


protected:
  /** Constructor */
  SARStreamingGridHistogramFilter() {};
  /** Destructor */
  ~SARStreamingGridHistogramFilter() ITK_OVERRIDE {}

private:
  SARStreamingGridHistogramFilter(const Self &); //purposely not implemented
  void operator =(const Self&); //purposely not implemented
  
  int      m_NumberHist;
  
};

} // end namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSARStreamingGridHistogramFilter.txx"
#endif

#endif
