/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARDEMGridImageFilter_txx
#define otbSARDEMGridImageFilter_txx

#include "otbSARDEMGridImageFilter.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkProgressReporter.h"
#include "itkNumericTraitsPointPixel.h"
#include "itkContinuousIndex.h"

#include <cmath>


namespace otb
{
  /** 
   * Constructor with default initialization
   */
  template <class TImageIn, class TImageSAR, class TImageOut> 
  SARDEMGridImageFilter< TImageIn ,TImageSAR, TImageOut >::SARDEMGridImageFilter()
  {
    this->SetNumberOfRequiredInputs(2);

    // Grid Step
    m_GridStep.Fill(1);

    m_MLran = 3;
    m_MLazi = 3;
  }

  /**
   * Set Master Image
   */ 
  template <class TImageIn, class TImageSAR, class TImageOut> 
  void
  SARDEMGridImageFilter< TImageIn ,TImageSAR, TImageOut >
  ::SetDEMProjOnMasterInput(const ImageInType* image )
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(0, const_cast<ImageInType *>(image));
  }

  /**
   * Set Slave Image
   */ 
  template <class TImageIn, class TImageSAR, class TImageOut> 
  void
  SARDEMGridImageFilter< TImageIn ,TImageSAR, TImageOut >
  ::SetDEMProjOnSlaveInput(const ImageInType* image)
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(1, const_cast<ImageInType *>(image));
  }

  /**
   * Get Master Image
   */ 
  template <class TImageIn, class TImageSAR, class TImageOut> 
  const typename SARDEMGridImageFilter< TImageIn ,TImageSAR, TImageOut >::ImageInType *
  SARDEMGridImageFilter< TImageIn ,TImageSAR, TImageOut >
  ::GetDEMProjOnMasterInput() const
  {
    if (this->GetNumberOfInputs()<1)
      {
	return 0;
      }
    return static_cast<const ImageInType *>(this->itk::ProcessObject::GetInput(0));
  }

  /**
   * Get Slave Image
   */ 
  template <class TImageIn, class TImageSAR, class TImageOut> 
  const typename SARDEMGridImageFilter< TImageIn ,TImageSAR, TImageOut >::ImageInType *
  SARDEMGridImageFilter< TImageIn ,TImageSAR, TImageOut >
  ::GetDEMProjOnSlaveInput() const
  {
    if (this->GetNumberOfInputs()<2)
      {
	return 0;
      }
    return static_cast<const ImageInType *>(this->itk::ProcessObject::GetInput(1));
  }

    /**
   * Set Sar Image Ptr (Master Ptr)
   */ 
  template <class TImageIn, class TImageSAR, class TImageOut>
  void
  SARDEMGridImageFilter< TImageIn ,TImageSAR, TImageOut >
  ::SetSARImagePtr(ImageSARPointer sarPtr)
  {
    // Check if sarImageKWL not NULL
    assert(sarPtr && "SAR Image doesn't exist.");
    m_SarImagePtr = sarPtr;
  }

  /**
   * Print
   */
  template<class TImageIn, class TImageSAR, class TImageOut>
  void
  SARDEMGridImageFilter< TImageIn, TImageSAR, TImageOut >
  ::PrintSelf(std::ostream & os, itk::Indent indent) const
  {
    Superclass::PrintSelf(os, indent);
    
    os << indent << "ML range factor : " << m_MLran << std::endl;
    os << indent << "ML azimut factor : " << m_MLazi << std::endl;
    os << indent << "Step for the grid : " << m_GridStep[0] << ", " << m_GridStep[1] << std::endl;
  }

  /**
   * Method GenerateOutputInformaton()
   **/
  template<class TImageIn, class TImageSAR, class TImageOut>
  void
  SARDEMGridImageFilter< TImageIn, TImageSAR, TImageOut >
  ::GenerateOutputInformation()
  {    
    // Call superclass implementation
    Superclass::GenerateOutputInformation();

    // Get pointers to the input and output
    ImageInType * DEMProjOnMasterPtr  = const_cast<ImageInType * >(this->GetDEMProjOnMasterInput());
    ImageOutPointer outputPtr = this->GetOutput();
    
    outputPtr->CopyInformation(m_SarImagePtr);
  
    // Set the number of Components for the Output VectorImage
    // 3 Components :  
    //                _ range shift between Master and Slave
    //                _ azimut shift between Master and Slave
    //                _ nb DEM points for contribution
    outputPtr->SetNumberOfComponentsPerPixel(3);

    // Update size and spacing according to grid step
    ImageOutRegionType largestRegion = static_cast<ImageOutRegionType>(m_SarImagePtr->GetLargestPossibleRegion());
    ImageOutSizeType outputSize = static_cast<ImageOutSizeType>(largestRegion.GetSize());
    ImageOutSpacingType outputSpacing = static_cast<ImageOutSpacingType>(m_SarImagePtr->GetSpacing());
    ImageOutPointType outputOrigin = static_cast<ImageOutPointType>(m_SarImagePtr->GetOrigin());

    ImageInSizeType gridStep_SLC;
    gridStep_SLC[0] = m_GridStep[0]*m_MLran;
    gridStep_SLC[1] = m_GridStep[1]*m_MLazi;
    
    for(unsigned int dim = 0; dim < ImageOutType::ImageDimension; ++dim)
    {
      outputSize[dim] = (outputSize[dim] - outputSize[dim]%m_GridStep[dim]) / m_GridStep[dim];
      outputSpacing[dim] *= m_GridStep[dim];
    }

    // Set largest region size
    largestRegion.SetSize(outputSize);
    outputPtr->SetLargestPossibleRegion(largestRegion);

    // Set origin
    outputPtr->SetOrigin(outputOrigin);

    // Set spacing
    outputPtr->SetSpacing(outputSpacing);
    
    // RSTransform
    m_rsTransform = RSTransformType2D::New();
    m_rsTransform->SetInputKeywordList(m_SarImagePtr->GetImageKeywordlist() );
    m_rsTransform->SetOutputKeywordList(DEMProjOnMasterPtr->GetImageKeywordlist());
    m_rsTransform->InstantiateTransform();
    
    // Add GridSteps into KeyWordList
    ImageKeywordlist outputKWL;
    outputKWL.AddKey("support_data.gridstep.range", std::to_string(m_GridStep[0]));
    outputKWL.AddKey("support_data.gridstep.azimut", std::to_string(m_GridStep[1]));
    
    outputPtr->SetImageKeywordList(outputKWL); 
  }

  /** 
   * Method OutputRegionToInputRegion for GenerateInputRequestedRegion
   */
  template<class TImageIn, class TImageSAR, class TImageOut>
  typename SARDEMGridImageFilter< TImageIn, TImageSAR, TImageOut>::ImageInRegionType 
  SARDEMGridImageFilter< TImageIn, TImageSAR, TImageOut >
  ::OutputRegionToInputRegion(const ImageOutRegionType& outputRegion) const
  {
    // Get pointers to the input
    ImageInType * DEMProjOnMasterPtr  = const_cast<ImageInType * >(this->GetDEMProjOnMasterInput());

    // Compute the input requested region (size and start index)
    // Use the image transformations to insure an input requested region
    // that will provide the proper range
    const ImageOutSizeType & outputRequestedRegionSize = outputRegion.GetSize();
    ImageOutIndexType outputRequestedRegionIndex = outputRegion.GetIndex();

    // Margin to apply on inverse localisation for the four sides.
    int margin = 100;
    ImageInSizeType DEMSize = DEMProjOnMasterPtr->GetLargestPossibleRegion().GetSize();
    if (std::max(DEMSize[0], DEMSize[1]) > 2000)
      {
	margin = std::max(DEMSize[0], DEMSize[1])*0.01; // %1 of the largest dimension
      }

    ImageInSizeType gridStep_SLC;
    gridStep_SLC[0] = m_GridStep[0]*m_MLran;
    gridStep_SLC[1] = m_GridStep[1]*m_MLazi;

    // Index for the SAR bloc (determined by the Pipeline)
    ImageOutIndexType id[4] ;
    id[0][0] = outputRequestedRegionIndex[0]*m_GridStep[0];
    id[0][1] = outputRequestedRegionIndex[1]*m_GridStep[1];
    id[1][0] = outputRequestedRegionIndex[0]*m_GridStep[0];
    id[1][1] = (outputRequestedRegionIndex[1] + outputRequestedRegionSize[1])*m_GridStep[1];
    id[2][0] = (outputRequestedRegionIndex[0] + outputRequestedRegionSize[0])*m_GridStep[0];
    id[2][1] = outputRequestedRegionIndex[1]*m_GridStep[1];
    id[3][0] = (outputRequestedRegionIndex[0] + outputRequestedRegionSize[0])*m_GridStep[0];
    id[3][1] = (outputRequestedRegionIndex[1] + outputRequestedRegionSize[1])*m_GridStep[1];
    
    Point2DType pixelSAR_Into_DEM_LatLon(0);
    Point2DType pixelSAR(0);
    
    itk::ContinuousIndex<double,2> pixelSAR_Into_DEM;

    // Initialize the first and last DEM index
    int firstL, firstC, lastL, lastC; 
  
    firstL = DEMSize[1];
    lastL = 0;
    firstC = DEMSize[0];
    lastC = 0;


    // For each side of the Master SAR region (projection from Master SAR image to DEM)
    for (int i = 0; i < 4; i++) 
      {
	// Index to physical 
	m_SarImagePtr->TransformIndexToPhysicalPoint(id[i], pixelSAR);
	// Call TransformPoint with physical point
	pixelSAR_Into_DEM_LatLon = m_rsTransform->TransformPoint(pixelSAR);
	// Transform Lat/Lon to index 
	DEMProjOnMasterPtr->TransformPhysicalPointToContinuousIndex(pixelSAR_Into_DEM_LatLon, pixelSAR_Into_DEM);
      
	// Assigne the limits
	if (firstL > pixelSAR_Into_DEM[1])
	  {
	    firstL = pixelSAR_Into_DEM[1];
	  }
      
	if (lastL < pixelSAR_Into_DEM[1])
	  {
	    lastL = pixelSAR_Into_DEM[1];
	  }

	if (firstC > pixelSAR_Into_DEM[0])
	  {
	    firstC = pixelSAR_Into_DEM[0];
	  }
      
	if (lastC < pixelSAR_Into_DEM[0])
	  {
	    lastC = pixelSAR_Into_DEM[0];
	  }

      }

    // Apply the margin
    firstL -= margin;
    firstC -= margin;
    lastL +=  margin;
    lastC +=  margin;

    // Check the limits
    if (firstC < 0)
      {
	firstC = 0;
      } 
    if (firstL < 0)
      {
	firstL = 0;
      } 
    if (lastC > static_cast<int>(DEMSize[0])-1)
      {
	lastC = static_cast<int>(DEMSize[0])-1;
      } 
    if (lastL > static_cast<int>(DEMSize[1])-1)
      {
	lastL = static_cast<int>(DEMSize[1])-1;
      } 

    // Transform sides to region
    ImageInIndexType inputRequestedRegionIndex; 
    inputRequestedRegionIndex[0] = static_cast<ImageInIndexValueType>(firstC);
    inputRequestedRegionIndex[1] = static_cast<ImageInIndexValueType>(firstL);
    ImageInSizeType inputRequestedRegionSize;
    inputRequestedRegionSize[0] = static_cast<ImageInIndexValueType>(lastC - firstC)+1;
    inputRequestedRegionSize[1] = static_cast<ImageInIndexValueType>(lastL - firstL)+1;

    // input Region requested for our two inputs (same requested region for projected DEM into master and slave)
    ImageInRegionType inputRequestedRegion = outputRegion;
    inputRequestedRegion.SetIndex(inputRequestedRegionIndex);
    inputRequestedRegion.SetSize(inputRequestedRegionSize);

    return inputRequestedRegion;
  }


  /** 
   * Method GenerateInputRequestedRegion
   */
  template<class TImageIn, class TImageSAR, class TImageOut>
  void
  SARDEMGridImageFilter< TImageIn, TImageSAR, TImageOut >
  ::GenerateInputRequestedRegion()
  {
    // Call the superclass' implementation of this method
    Superclass::GenerateInputRequestedRegion();
 
    ImageOutRegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();
    ImageInRegionType inputRequestedRegion = this->OutputRegionToInputRegion(outputRequestedRegion);
   
    // Get pointers to inputs
    ImageInType * DEMProjOnMasterPtr  = const_cast<ImageInType * >(this->GetDEMProjOnMasterInput());
    ImageInType * DEMProjOnSlavePtr  = const_cast<ImageInType * >(this->GetDEMProjOnSlaveInput());
    
    DEMProjOnMasterPtr->SetRequestedRegion(inputRequestedRegion);
    DEMProjOnSlavePtr->SetRequestedRegion(inputRequestedRegion);
  }
 

 /**
   * Method BeforeThreadedGenerateData
   */
  template<class TImageIn, class TImageSAR, class TImageOut>
  void
  SARDEMGridImageFilter< TImageIn, TImageSAR, TImageOut >
  ::ThreadedGenerateData(const ImageOutRegionType& outputRegionForThread,
			 itk::ThreadIdType threadId)
  {
    // Get pointers to inputs
    const ImageInType * DEMProjOnMasterPtr = this->GetDEMProjOnMasterInput();
    const ImageInType * DEMProjOnSlavePtr = this->GetDEMProjOnSlaveInput();

    // Compute corresponding input region (same region forDEMProjOnMasterPtr and DEMProjOnSlavePtr)
    ImageInRegionType inputRegionForThread = OutputRegionToInputRegion(outputRegionForThread);

    // Typedef for iterators
    typedef itk::ImageScanlineConstIterator< ImageInType > InputIterator;
    typedef itk::ImageScanlineIterator< ImageOutType > OutputIterator;
        
    // Iterator on output (Grid geometry)
    OutputIterator OutIt(this->GetOutput(), outputRegionForThread);
    OutIt.GoToBegin();

    // Allocate outputs Tabs
    const ImageOutSizeType &  outputRegionForThreadSize = outputRegionForThread.GetSize();
    unsigned int nbCol = static_cast<unsigned int>(outputRegionForThreadSize[0]); 
    unsigned int firstCol = static_cast<unsigned int>(outputRegionForThread.GetIndex()[0]+this->GetOutput()->GetOrigin()[0]);
    unsigned int nbLine = static_cast<unsigned int>(outputRegionForThreadSize[1]); 
    unsigned int firstLine = static_cast<unsigned int>(outputRegionForThread.GetIndex()[1]+this->GetOutput()->GetOrigin()[1]);

    double *outRange = new double[nbCol*nbLine];
    double *outAzi = new double[nbCol*nbLine];
    int *outNbContribution = new int[nbCol*nbLine];

    // Initialize arrays
    memset(outRange, 0., nbCol*nbLine*sizeof(double));
    memset(outAzi, 0., nbCol*nbLine*sizeof(double));
    memset(outNbContribution, 0, nbCol*nbLine*sizeof(int));

    // Iterators on inputs (DEM geometry)
    InputIterator  InIt_DEMProjMaster(DEMProjOnMasterPtr, inputRegionForThread); 
    InputIterator  InIt_DEMProjSlave(DEMProjOnSlavePtr, inputRegionForThread);

    InIt_DEMProjMaster.GoToBegin();
    InIt_DEMProjSlave.GoToBegin();

    // Support progress methods/callbacks
    itk::ProgressReporter progress(this, threadId, this->GetOutput()->GetRequestedRegion().GetNumberOfPixels());

    ImageInSizeType gridStep_SLC;
    gridStep_SLC[0] = m_GridStep[0]*m_MLran;
    gridStep_SLC[1] = m_GridStep[1]*m_MLazi;

    ////////////////////////////// Scan Input Projected DEM //////////////////////////////
    // For each Line of input 
    while ( !InIt_DEMProjMaster.IsAtEnd() && !InIt_DEMProjSlave.IsAtEnd())
      {
	InIt_DEMProjMaster.GoToBeginOfLine();
	InIt_DEMProjSlave.GoToBeginOfLine();

	// For each column
	while (!InIt_DEMProjMaster.IsAtEndOfLine() && !InIt_DEMProjSlave.IsAtEndOfLine())
	  {
	    // Grid geometry for DEM current point
	    unsigned int indGrid_L = (std::round(InIt_DEMProjMaster.Get()[1]))/(m_GridStep[1]);
	    unsigned int indGrid_C = (std::round(InIt_DEMProjMaster.Get()[0]))/(m_GridStep[0]);

	    // Check if the DEM point is into outputThreadRegion
	    if ((indGrid_L >= firstLine && indGrid_L < firstLine + nbLine) &&   
		(indGrid_C >= firstCol && indGrid_C < firstCol + nbCol))
	      {
		// Array Index
		int indArray_L = indGrid_L -firstLine;
		int indArray_C = indGrid_C -firstCol;

		// Estimate its contribution
		double a = outNbContribution[indArray_L*nbCol + indArray_C];
		outNbContribution[indArray_L*nbCol + indArray_C] +=1;
		double b = 1./outNbContribution[indArray_L*nbCol + indArray_C];
		a *= b;

		outRange[indArray_L*nbCol + indArray_C] = a * outRange[indArray_L*nbCol + indArray_C] + 
		  b * (InIt_DEMProjSlave.Get()[0] - InIt_DEMProjMaster.Get()[0]);

		outAzi[indArray_L*nbCol + indArray_C] = a * outAzi[indArray_L*nbCol + indArray_C] + 
		  b * (InIt_DEMProjSlave.Get()[1] - InIt_DEMProjMaster.Get()[1]);
	      }

	    // Next colunm
	    ++InIt_DEMProjMaster;
	    ++InIt_DEMProjSlave; 
	  }
	
	// Next line
	InIt_DEMProjMaster.NextLine();
	InIt_DEMProjSlave.NextLine();
      }

    ////////////////////////////// Output Assignation //////////////////////////////
    int counterLine = 0;
    int counterCol = 0;
    
    ImageOutPixelType pixelDEMGrid;
    pixelDEMGrid.Reserve(3);

    while ( !OutIt.IsAtEnd())
      {
	OutIt.GoToBeginOfLine();
	counterCol = 0;
	
	while (!OutIt.IsAtEndOfLine()) 
	  {
	    pixelDEMGrid[0] = outRange[counterLine*nbCol + counterCol];
	    pixelDEMGrid[1] = outAzi[counterLine*nbCol + counterCol];
	    pixelDEMGrid[2] = outNbContribution[counterLine*nbCol + counterCol];

	    // Assign Output
	    OutIt.Set(pixelDEMGrid);
	    progress.CompletedPixel();
	      
	    // Next colunm
	    ++OutIt;
	    ++counterCol;
	  }
	
	// Next Line
	OutIt.NextLine();
	++counterLine;
      }
   delete [] outRange;
   delete [] outAzi;
   delete [] outNbContribution;
  }

} /*namespace otb*/

#endif
