/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARStreamingGridInformationImageFilter_h
#define otbSARStreamingGridInformationImageFilter_h

#include "otbPersistentImageFilter.h"
#include "itkNumericTraits.h"
#include "itkArray.h"
#include "itkSimpleDataObjectDecorator.h"
#include "otbPersistentFilterStreamingDecorator.h"

#include <complex>
#include <cmath>

namespace otb
{

/** \class PersistentGridInformationImageFilter
 * \brief Estimate some values for an input grid (the mean value for valid points only or the min/max shifts
 * for each dimension). A point is defined as valid if its correlation is superior to the input threshold. 
 * The input grid is a vector image composed of three values :
 * range deformations, azimut deformations and correlation rate or nb DEM points with contribution.
 *
 * This filter persists its temporary data. It means that if you Update it n times on n different
 * requested regions, the output statistics will be the statitics of the whole set of n regions.
 *
 * To reset the temporary data, one should call the Reset() function.
 *
 * To get the wanted values once the regions have been processed via the pipeline, use the Synthetize() method.
 *
 * \ingroup DiapOTBModule
 */
template<class TInputImage>
class ITK_EXPORT PersistentGridInformationImageFilter :
  public PersistentImageFilter<TInputImage, TInputImage>
{
public:
  /** Standard Self typedef */
  typedef PersistentGridInformationImageFilter                 Self;
  typedef PersistentImageFilter<TInputImage, TInputImage> Superclass;
  typedef itk::SmartPointer<Self>                         Pointer;
  typedef itk::SmartPointer<const Self>                   ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Runtime information support. */
  itkTypeMacro(PersistentGridInformationImageFilter, PersistentImageFilter);

  /** Image related typedefs. For Grid Input (VectorImage) */
  typedef TInputImage                   ImageType;
  typedef typename TInputImage::Pointer InputImagePointer;

  typedef typename TInputImage::RegionType   RegionType;
  typedef typename TInputImage::SizeType     SizeType;
  typedef typename TInputImage::IndexType    IndexType;
  typedef typename TInputImage::PixelType    PixelType;
  typedef double                             ValueType;
  typedef typename ImageType::IndexValueType IndexValueType;
  typedef typename ImageType::SizeValueType  SizeValueType;

  itkStaticConstMacro(InputImageDimension, unsigned int,
                      TInputImage::ImageDimension);

  /** Image related typedefs. */
  itkStaticConstMacro(ImageDimension, unsigned int,
                      TInputImage::ImageDimension);

  /** Type to use for computations. */
  //  typedef typename itk::NumericTraits<PixelType>::RealType RealType;
  typedef typename itk::NumericTraits<double>::RealType RealType;
 
  /** Smart Pointer type to a DataObject. */
  typedef typename itk::DataObject::Pointer DataObjectPointer;
  typedef itk::ProcessObject::DataObjectPointerArraySizeType DataObjectPointerArraySizeType;

  /** Type of DataObjects used for scalar outputs */
  typedef itk::SimpleDataObjectDecorator<RealType>  RealObjectType;
  typedef itk::SimpleDataObjectDecorator<long>      LongObjectType;
  typedef itk::SimpleDataObjectDecorator<PixelType> PixelObjectType;
  typedef itk::SimpleDataObjectDecorator<ValueType> ValueObjectType;

    /** Return the number of valid points contained into the input grid. */
  long GetCount() const
  {
    return this->GetCountOutput()->Get();
  }
  LongObjectType* GetCountOutput();
  const LongObjectType* GetCountOutput() const;

  /** Return the computed Sum. */
  ValueType GetSumAzimut() const
  {
    return this->GetSumAzimutOutput()->Get();
  }
  ValueObjectType* GetSumAzimutOutput();
  const ValueObjectType* GetSumAzimutOutput() const;

  /** Return the computed Sum. */
  ValueType GetSumRange() const
  {
    return this->GetSumRangeOutput()->Get();
  }
  ValueObjectType* GetSumRangeOutput();
  const ValueObjectType* GetSumRangeOutput() const;

  /** Return the Minimum. */
  ValueType GetMinRange() const
  {
    return this->GetMinRangeOutput()->Get();
  }
  ValueObjectType* GetMinRangeOutput();
  const ValueObjectType* GetMinRangeOutput() const;

  /** Return the Minimum. */
  ValueType GetMinAzimut() const
  {
    return this->GetMinAzimutOutput()->Get();
  }
  ValueObjectType* GetMinAzimutOutput();
  const ValueObjectType* GetMinAzimutOutput() const;

  /** Return the Maximum. */
  ValueType GetMaxRange() const
  {
    return this->GetMaxRangeOutput()->Get();
  }
  ValueObjectType* GetMaxRangeOutput();
  const ValueObjectType* GetMaxRangeOutput() const;

  /** Return the Maximum. */
  ValueType GetMaxAzimut() const
  {
    return this->GetMaxAzimutOutput()->Get();
  }
  ValueObjectType* GetMaxAzimutOutput();
  const ValueObjectType* GetMaxAzimutOutput() const;

  /** Make a DataObject of the correct type to be used as the specified
   * output. */
  DataObjectPointer MakeOutput(DataObjectPointerArraySizeType idx) ITK_OVERRIDE;
  using Superclass::MakeOutput;

  /**
   * Synthetize and Reset function called by our PersistentFilterStreamingDecorator
   */
  void Synthetize(void) ITK_OVERRIDE;
  void Reset(void) ITK_OVERRIDE;

  itkSetMacro(IgnoreInfiniteValues, bool);
  itkGetMacro(IgnoreInfiniteValues, bool);

  itkSetMacro(IgnoreUserDefinedValue, bool);
  itkGetMacro(IgnoreUserDefinedValue, bool);

  itkSetMacro(Threshold, double);
  itkGetMacro(Threshold, double);

  itkSetMacro(EstimateMean, bool);
  itkGetMacro(EstimateMean, bool);

  itkSetMacro(UserIgnoredValue, RealType);
  itkGetMacro(UserIgnoredValue, RealType);

protected:
  PersistentGridInformationImageFilter();
  ~PersistentGridInformationImageFilter() ITK_OVERRIDE {}
  void PrintSelf(std::ostream& os, itk::Indent indent) const ITK_OVERRIDE;

  
  /** PersistentGridInformationImageFilter needs a larger input requested region than the output
   * requested region.  As such, SARQuadraticAveragingImageFilter needs to provide an
   * implementation for GenerateInputRequestedRegion() in order to inform the
   * pipeline execution model.
   * \sa ProcessObject::GenerateInputRequestedRegion() */
  virtual void GenerateInputRequestedRegion() ITK_OVERRIDE;

  /**
   * OutputRegionToInputRegion returns the input region 
   */
  RegionType OutputRegionToInputRegion(const RegionType& outputRegion) const;

  /** Multi-thread version GenerateData. */
  void  ThreadedGenerateData(const RegionType&
                             outputRegionForThread,
                             itk::ThreadIdType threadId) ITK_OVERRIDE;

  /** Pass the input through unmodified. Do this by Grafting in the
   *  AllocateOutputs method.
   */
  void AllocateOutputs() ITK_OVERRIDE;
  void GenerateOutputInformation() ITK_OVERRIDE;

private:
  PersistentGridInformationImageFilter(const Self &); //purposely not implemented
  void operator =(const Self&); //purposely not implemented

  itk::Array<ValueType>       m_ThreadSumRange;
  itk::Array<ValueType>       m_ThreadSumAzimut;
  itk::Array<ValueType>       m_ThreadMinRange;
  itk::Array<ValueType>       m_ThreadMinAzimut;
  itk::Array<ValueType>       m_ThreadMaxRange;
  itk::Array<ValueType>       m_ThreadMaxAzimut;
  itk::Array<long>            m_ThreadCount;

  /* Ignored values */
  bool                        m_IgnoreInfiniteValues;
  bool                        m_IgnoreUserDefinedValue;
  RealType                    m_UserIgnoredValue;
  std::vector<unsigned long>  m_IgnoredInfinitePixelCount;
  std::vector<unsigned int>   m_IgnoredUserPixelCount;
  double                      m_Threshold;

  int                        m_NbComponentForInput;
  
  // Boolean to indicate if the mean value has to be estimated
  bool                        m_EstimateMean;


}; // end of class PersistentGridInformationImageFilter

/*===========================================================================*/

/** \class SARStreamingGridInformationImageFilter
 * \brief This class streams the whole input image through the PersistentGridInformationImageFilter.
 *
 * This way, it allows computing the first order global statistics of this image. It calls the
 * Reset() method of the PersistentGridInformationImageFilter before streaming the image and the
 * Synthetize() method of the PersistentGridInformationImageFilter after having streamed the image
 * to compute the statistics. The accessor on the results are wrapping the accessors of the
 * internal PersistentGridInformationImageFilter.
 * By default infinite values are ignored, use IgnoreInfiniteValues accessor to consider
 * infinite values in the computation.
 *
 * This filter can be used as:
 * \code
 * typedef otb::StreamingGridInformationImageFilter<ImageType> StatisticsType;
 * StatisticsType::Pointer statistics = StatisticsType::New();
 * statistics->SetInput(reader->GetOutput());
 * statistics->Update();
 * std::cout << statistics-> GetSumRange() << std::endl;
 * std::cout << statistics-> GetSumAzimut() << std::endl;
 * std::cout << statistics-> GetMeanGrid(meanRange, meanAzimut) << std::endl;
 * \endcode
 *
 * \sa PersistentGridInformationImageFilter
 * \sa PersistentImageFilter
 * \sa PersistentFilterStreamingDecorator
 * \sa StreamingImageVirtualWriter
 * \ingroup Streamed
 * \ingroup Multithreaded
 * \ingroup MathematicalStatisticsImageFilters
 *
 * \ingroup DiapOTBModule
 */

template<class TInputImage>
class ITK_EXPORT SARStreamingGridInformationImageFilter :
  public PersistentFilterStreamingDecorator<PersistentGridInformationImageFilter<TInputImage> >
{
public:
  /** Standard Self typedef */
  typedef SARStreamingGridInformationImageFilter Self;
  typedef PersistentFilterStreamingDecorator
  <PersistentGridInformationImageFilter<TInputImage> > Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Type macro */
  itkNewMacro(Self);

  /** Creation through object factory macro */
  itkTypeMacro(StreamingGridInformationImageFilter, PersistentFilterStreamingDecorator);

  typedef typename Superclass::FilterType    StatFilterType;
  typedef typename StatFilterType::PixelType PixelType;
  typedef double ValueType;
  typedef typename StatFilterType::RealType  RealType;
  typedef TInputImage                        InputImageType;

  /** Type of DataObjects used for scalar outputs */
  typedef itk::SimpleDataObjectDecorator<RealType>  RealObjectType;
  typedef itk::SimpleDataObjectDecorator<long>      LongObjectType;
  typedef itk::SimpleDataObjectDecorator<PixelType> PixelObjectType;
  typedef itk::SimpleDataObjectDecorator<ValueType> ValueObjectType;

  using Superclass::SetInput;
  void SetInput(InputImageType * input)
  {
    this->GetFilter()->SetInput(input);
  }
  const InputImageType * GetInput()
  {
    return this->GetFilter()->GetInput();
  }

  void SetEstimateMean(bool estimateMean)
  {
    this->GetFilter()->SetEstimateMean(estimateMean);
  }

  /** Return the computed number of finite pixel. */
  long GetCount() const
  {
    return this->GetFilter()->GetCountOutput()->Get();
  }
  LongObjectType* GetCountOutput()
  {
    return this->GetFilter()->GetCountOutput();
  }
  const LongObjectType* GetCountOutput() const
  {
    return this->GetFilter()->GetCountOutput();
  }

  // Estimate Mean for valid points only
  void GetMean(ValueType & meanRange, ValueType & meanAzimut)
  {
    // Get values estimated into our PersistentFilter
    ValueType sumRange = this->GetFilter()->GetSumRangeOutput()->Get();
    ValueType sumAzimut = this->GetFilter()->GetSumAzimutOutput()->Get();
    long count_ValidPts = this->GetFilter()->GetCountOutput()->Get();

    // Estimate Mean
    ValueType countConvert = count_ValidPts; // In order to match with float or double for the next
                                                         // division
    meanRange= sumRange/countConvert;
    meanAzimut= sumAzimut/countConvert;
  }

  // Estimate Min/Max for each dimension
  void GetMinMax(ValueType & minRange, ValueType & minAzimut, ValueType & maxRange, ValueType & maxAzimut)
  {
    // Get values estimated into our PersistentFilter
    minRange = this->GetFilter()->GetMinRangeOutput()->Get();
    minAzimut = this->GetFilter()->GetMinAzimutOutput()->Get();
    maxRange = this->GetFilter()->GetMaxRangeOutput()->Get();
    maxAzimut = this->GetFilter()->GetMaxAzimutOutput()->Get();
  }

  // Set Threshold for valid point
  void SetThreshold(double threshold)
  {
    this->GetFilter()->SetThreshold(threshold);
  }

  otbSetObjectMemberMacro(Filter, IgnoreInfiniteValues, bool);
  otbGetObjectMemberMacro(Filter, IgnoreInfiniteValues, bool);

  otbSetObjectMemberMacro(Filter, IgnoreUserDefinedValue, bool);
  otbGetObjectMemberMacro(Filter, IgnoreUserDefinedValue, bool);

  otbSetObjectMemberMacro(Filter, UserIgnoredValue, RealType);
  otbGetObjectMemberMacro(Filter, UserIgnoredValue, RealType);

protected:
  /** Constructor */
  SARStreamingGridInformationImageFilter() {};
  /** Destructor */
  ~SARStreamingGridInformationImageFilter() ITK_OVERRIDE {}

private:
  SARStreamingGridInformationImageFilter(const Self &); //purposely not implemented
  void operator =(const Self&); //purposely not implemented
};

} // end namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSARStreamingGridInformationImageFilter.txx"
#endif

#endif
