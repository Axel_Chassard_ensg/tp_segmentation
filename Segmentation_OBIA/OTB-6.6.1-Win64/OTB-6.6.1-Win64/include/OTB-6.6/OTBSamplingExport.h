
#ifndef OTBSampling_EXPORT_H
#define OTBSampling_EXPORT_H

#ifdef OTB_STATIC
#  define OTBSampling_EXPORT
#  define OTBSampling_HIDDEN
#else
#  ifndef OTBSampling_EXPORT
#    ifdef OTBSampling_EXPORTS
        /* We are building this library */
#      define OTBSampling_EXPORT __declspec(dllexport)
#    else
        /* We are using this library */
#      define OTBSampling_EXPORT __declspec(dllimport)
#    endif
#  endif

#  ifndef OTBSampling_HIDDEN
#    define OTBSampling_HIDDEN 
#  endif
#endif

#ifndef OTBSAMPLING_DEPRECATED
#  define OTBSAMPLING_DEPRECATED __declspec(deprecated)
#endif

#ifndef OTBSAMPLING_DEPRECATED_EXPORT
#  define OTBSAMPLING_DEPRECATED_EXPORT OTBSampling_EXPORT OTBSAMPLING_DEPRECATED
#endif

#ifndef OTBSAMPLING_DEPRECATED_NO_EXPORT
#  define OTBSAMPLING_DEPRECATED_NO_EXPORT OTBSampling_HIDDEN OTBSAMPLING_DEPRECATED
#endif

#define DEFINE_NO_DEPRECATED 0
#if DEFINE_NO_DEPRECATED
# define OTBSAMPLING_NO_DEPRECATED
#endif

#endif
