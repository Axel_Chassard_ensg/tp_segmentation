/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARStreamingCorrelationImageFilter_h
#define otbSARStreamingCorrelationImageFilter_h

#include "otbPersistentImageFilter.h"
#include "itkNumericTraits.h"
#include "itkArray.h"
#include "itkSimpleDataObjectDecorator.h"
#include "otbPersistentFilterStreamingDecorator.h"

#include <complex>
#include <cmath>

namespace otb
{

/** \class PersistentCorrelationImageFilter
 * \brief Compute the correlation between lines of an image using the output requested region. 
 *  A mean and a counter on pixel are also calculated.
 *
 *  This filter persists its temporary data. It means that if you Update it n times on n different
 * requested regions, the output statistics will be the statitics of the whole set of n regions.
 *
 * To reset the temporary data, one should call the Reset() function.
 *
 * To get the wanted values once the regions have been processed via the pipeline, use the Synthetize() method.
 *
 * \ingroup DiapOTBModule
 */
template<class TInputImage>
class ITK_EXPORT PersistentCorrelationImageFilter :
  public PersistentImageFilter<TInputImage, TInputImage>
{
public:
  /** Standard Self typedef */
  typedef PersistentCorrelationImageFilter                 Self;
  typedef PersistentImageFilter<TInputImage, TInputImage> Superclass;
  typedef itk::SmartPointer<Self>                         Pointer;
  typedef itk::SmartPointer<const Self>                   ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Runtime information support. */
  itkTypeMacro(PersistentCorrelationImageFilter, PersistentImageFilter);

  /** Image related typedefs. */
  typedef TInputImage                   ImageType;
  typedef typename TInputImage::Pointer InputImagePointer;

  typedef typename TInputImage::RegionType RegionType;
  typedef typename TInputImage::SizeType   SizeType;
  typedef typename TInputImage::IndexType  IndexType;
  typedef typename TInputImage::PixelType  PixelType;
  typedef typename ImageType::IndexValueType IndexValueType;
  typedef typename ImageType::SizeValueType  SizeValueType;

  itkStaticConstMacro(InputImageDimension, unsigned int,
                      TInputImage::ImageDimension);

  /** Image related typedefs. */
  itkStaticConstMacro(ImageDimension, unsigned int,
                      TInputImage::ImageDimension);

  /** Type to use for computations. */
  //  typedef typename itk::NumericTraits<PixelType>::RealType RealType;
  typedef typename itk::NumericTraits<double>::RealType RealType;
 
  /** Smart Pointer type to a DataObject. */
  typedef typename itk::DataObject::Pointer DataObjectPointer;
  typedef itk::ProcessObject::DataObjectPointerArraySizeType DataObjectPointerArraySizeType;

  /** Type of DataObjects used for scalar outputs */
  typedef itk::SimpleDataObjectDecorator<RealType>  RealObjectType;
  typedef itk::SimpleDataObjectDecorator<long>  LongObjectType;
  typedef itk::SimpleDataObjectDecorator<PixelType> PixelObjectType;

  /** Return the computed Correlation coefficient between lines. */
  PixelType GetCorrelation() const
  {
    return this->GetCorrelationOutput()->Get();
  }
  PixelObjectType* GetCorrelationOutput();
  const PixelObjectType* GetCorrelationOutput() const;

  /** Return the number of finite pixel. */
  long GetCount() const
  {
    return this->GetCountOutput()->Get();
  }
  LongObjectType* GetCountOutput();
  const LongObjectType* GetCountOutput() const;

  /** Return the computed Sum. */
  PixelType GetSum() const
  {
    return this->GetSumOutput()->Get();
  }
  PixelObjectType* GetSumOutput();
  const PixelObjectType* GetSumOutput() const;

  /** Make a DataObject of the correct type to be used as the specified
   * output. */
  DataObjectPointer MakeOutput(DataObjectPointerArraySizeType idx) ITK_OVERRIDE;
  using Superclass::MakeOutput;

  /**
   * Synthetize and Reset function called by our PersistentFilterStreamingDecorator
   */
  void Synthetize(void) ITK_OVERRIDE;
  void Reset(void) ITK_OVERRIDE;

  itkSetMacro(IgnoreInfiniteValues, bool);
  itkGetMacro(IgnoreInfiniteValues, bool);

  itkSetMacro(IgnoreUserDefinedValue, bool);
  itkGetMacro(IgnoreUserDefinedValue, bool);

  itkSetMacro(UserIgnoredValue, RealType);
  itkGetMacro(UserIgnoredValue, RealType);

protected:
  PersistentCorrelationImageFilter();
  ~PersistentCorrelationImageFilter() ITK_OVERRIDE {}
  void PrintSelf(std::ostream& os, itk::Indent indent) const ITK_OVERRIDE;

  
  /** PersistentCorrelationImageFilter needs a larger input requested region than the output
   * requested region.  As such, SARQuadraticAveragingImageFilter needs to provide an
   * implementation for GenerateInputRequestedRegion() in order to inform the
   * pipeline execution model.
   * \sa ProcessObject::GenerateInputRequestedRegion() */
  virtual void GenerateInputRequestedRegion() ITK_OVERRIDE;

  /**
   * OutputRegionToInputRegion returns the input region 
   */
  RegionType OutputRegionToInputRegion(const RegionType& outputRegion) const;

  /** Multi-thread version GenerateData. */
  void  ThreadedGenerateData(const RegionType&
                             outputRegionForThread,
                             itk::ThreadIdType threadId) ITK_OVERRIDE;

  /** Pass the input through unmodified. Do this by Grafting in the
   *  AllocateOutputs method.
   */
  void AllocateOutputs() ITK_OVERRIDE;
  void GenerateOutputInformation() ITK_OVERRIDE;

private:
  PersistentCorrelationImageFilter(const Self &); //purposely not implemented
  void operator =(const Self&); //purposely not implemented

  itk::Array<PixelType>       m_ThreadCor;
  itk::Array<PixelType>       m_ThreadSum;
  itk::Array<long>           m_ThreadCount;

  /* Ignored values */
  bool                       m_IgnoreInfiniteValues;
  bool                       m_IgnoreUserDefinedValue;
  RealType                   m_UserIgnoredValue;
  std::vector<unsigned long>  m_IgnoredInfinitePixelCount;
  std::vector<unsigned int>  m_IgnoredUserPixelCount;


}; // end of class PersistentCorrelationImageFilter

/*===========================================================================*/

/** \class SARStreamingCorrelationImageFilter
 * \brief This class streams the whole input image through the PersistentCorrelationImageFilter.
 *
 * This way, it allows computing the first order global statistics of this image. It calls the
 * Reset() method of the PersistentCorrelationImageFilter before streaming the image and the
 * Synthetize() method of the PersistentCorrelationImageFilter after having streamed the image
 * to compute the statistics. The accessor on the results are wrapping the accessors of the
 * internal PersistentCorrelationImageFilter.
 * By default infinite values are ignored, use IgnoreInfiniteValues accessor to consider
 * infinite values in the computation.
 *
 * This filter can be used as:
 * \code
 * typedef otb::StreamingCorrelationImageFilter<ImageType> StatisticsType;
 * StatisticsType::Pointer statistics = StatisticsType::New();
 * statistics->SetInput(reader->GetOutput());
 * statistics->Update();
 * std::cout << statistics-> GetSum() << std::endl;
 * std::cout << statistics-> GetCorrelation() << std::endl;
 * \endcode
 *
 * \sa PersistentCorrelationImageFilter
 * \sa PersistentImageFilter
 * \sa PersistentFilterStreamingDecorator
 * \sa StreamingImageVirtualWriter
 * \ingroup Streamed
 * \ingroup Multithreaded
 * \ingroup MathematicalStatisticsImageFilters
 *
 * \ingroup DiapOTBModule
 */

template<class TInputImage>
class ITK_EXPORT SARStreamingCorrelationImageFilter :
  public PersistentFilterStreamingDecorator<PersistentCorrelationImageFilter<TInputImage> >
{
public:
  /** Standard Self typedef */
  typedef SARStreamingCorrelationImageFilter Self;
  typedef PersistentFilterStreamingDecorator
  <PersistentCorrelationImageFilter<TInputImage> > Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Type macro */
  itkNewMacro(Self);

  /** Creation through object factory macro */
  itkTypeMacro(StreamingCorrelationImageFilter, PersistentFilterStreamingDecorator);

  typedef typename Superclass::FilterType    StatFilterType;
  typedef typename StatFilterType::PixelType PixelType;
  typedef typename StatFilterType::RealType  RealType;
  typedef TInputImage                        InputImageType;

  /** Type of DataObjects used for scalar outputs */
  typedef itk::SimpleDataObjectDecorator<RealType>  RealObjectType;
  typedef itk::SimpleDataObjectDecorator<long>  LongObjectType;
  typedef itk::SimpleDataObjectDecorator<PixelType> PixelObjectType;

  using Superclass::SetInput;
  void SetInput(InputImageType * input)
  {
    this->GetFilter()->SetInput(input);
  }
  const InputImageType * GetInput()
  {
    return this->GetFilter()->GetInput();
  }

  /** Return the computed Minimum. */
  PixelType GetCorrelation() const
  {
    return this->GetFilter()->GetCorrelationOutput()->Get();
  }
  PixelObjectType* GetCorrelationOutput()
  {
    return this->GetFilter()->GetCorrelationOutput();
  }
  const PixelObjectType* GetCorrelationOutput() const
  {
    return this->GetFilter()->GetCorrelationOutput();
  }
  /** Return the computed number of finite pixel. */
  long GetCount() const
  {
    return this->GetFilter()->GetCountOutput()->Get();
  }
  LongObjectType* GetCountOutput()
  {
    return this->GetFilter()->GetCountOutput();
  }
  const LongObjectType* GetCountOutput() const
  {
    return this->GetFilter()->GetCountOutput();
  }
  /** Return the computed Sum. */
  PixelType GetSum() const
  {
    return this->GetFilter()->GetSumOutput()->Get();
  }
  PixelObjectType* GetSumOutput()
  {
    return this->GetFilter()->GetSumOutput();
  }
  const PixelObjectType* GetSumOutput() const
  {
    return this->GetFilter()->GetSumOutput();
  }

  // Estimate Doppler0
  double GetDoppler()
  {
    // Get values estimated into our PersistentFilter
    PixelType sum = this->GetFilter()->GetSumOutput()->Get();
    PixelType correlationCoef = this->GetFilter()->GetCorrelationOutput()->Get();
    long count = this->GetFilter()->GetCountOutput()->Get();

    // Estimate Doppler0
    typename PixelType::value_type countConvert = count; // In order to match with float or double for the next
                                                         // division
    PixelType mean = sum/countConvert;
    correlationCoef /= count;
    double mod2 = static_cast<double>((-1) * (std::abs(mean)*std::abs(mean))); // abs eq to modulus for a complex 
    double correlationCoefReal = static_cast<double>(std::real(correlationCoef) + mod2);
    double correlationCoefImag = static_cast<double>(std::imag(correlationCoef));
    double dop = static_cast<double>(std::atan2(correlationCoefImag, correlationCoefReal) / (2*M_PI));
    
    return dop;

  }

  otbSetObjectMemberMacro(Filter, IgnoreInfiniteValues, bool);
  otbGetObjectMemberMacro(Filter, IgnoreInfiniteValues, bool);

  otbSetObjectMemberMacro(Filter, IgnoreUserDefinedValue, bool);
  otbGetObjectMemberMacro(Filter, IgnoreUserDefinedValue, bool);

  otbSetObjectMemberMacro(Filter, UserIgnoredValue, RealType);
  otbGetObjectMemberMacro(Filter, UserIgnoredValue, RealType);

protected:
  /** Constructor */
  SARStreamingCorrelationImageFilter() {};
  /** Destructor */
  ~SARStreamingCorrelationImageFilter() ITK_OVERRIDE {}

private:
  SARStreamingCorrelationImageFilter(const Self &); //purposely not implemented
  void operator =(const Self&); //purposely not implemented
};

} // end namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSARStreamingCorrelationImageFilter.txx"
#endif

#endif
