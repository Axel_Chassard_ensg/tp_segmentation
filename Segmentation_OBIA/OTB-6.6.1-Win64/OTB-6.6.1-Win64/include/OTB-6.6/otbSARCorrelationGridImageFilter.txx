/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARCorrelationGridImageFilter_txx
#define otbSARCorrelationGridImageFilter_txx

#include "otbSARCorrelationGridImageFilter.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageRegionIterator.h"
#include "itkImageRegionConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkProgressReporter.h"
#include "itkNumericTraitsPointPixel.h"
#include "itkContinuousIndex.h"

#include "otbDEMHandler.h"

#include "itkFFTWCommon.h"
#include "itkFFTWGlobalConfiguration.h"

#include <cmath>


namespace otb
{
  /** 
   * Constructor with default initialization
   */
  template <class TImageIn, class TImageOut> 
  SARCorrelationGridImageFilter< TImageIn ,TImageOut >::SARCorrelationGridImageFilter()
  {
    this->SetNumberOfRequiredInputs(2);
 
    // Default sizes
    m_PatchSizePerDim = 128;
  
    // Default sub-pixel precision
    m_SubPixelAccuracy = 1.0/10.0;

    // Grid Step
    m_GridStep.Fill(1);
    m_GridStep_ML.Fill(1);

    m_Rescale = false;

    m_MLran = 3;
    m_MLazi = 3;
  }

  /**
   * Set Master Image
   */ 
  template <class TImageIn, class TImageOut> 
  void
  SARCorrelationGridImageFilter< TImageIn ,TImageOut >
  ::SetMasterInput(const ImageInType* image )
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(0, const_cast<ImageInType *>(image));
  }

  /**
   * Set Slave Image
   */ 
  template <class TImageIn, class TImageOut> 
  void
  SARCorrelationGridImageFilter< TImageIn ,TImageOut >
  ::SetSlaveInput(const ImageInType* image)
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(1, const_cast<ImageInType *>(image));
  }

  /**
   * Get Master Image
   */ 
  template <class TImageIn, class TImageOut> 
  const typename SARCorrelationGridImageFilter< TImageIn ,TImageOut>::ImageInType *
  SARCorrelationGridImageFilter< TImageIn ,TImageOut >
  ::GetMasterInput() const
  {
    if (this->GetNumberOfInputs()<1)
      {
	return 0;
      }
    return static_cast<const ImageInType *>(this->itk::ProcessObject::GetInput(0));
  }

  /**
   * Get Slave Image
   */ 
  template <class TImageIn, class TImageOut> 
  const typename SARCorrelationGridImageFilter< TImageIn ,TImageOut>::ImageInType *
  SARCorrelationGridImageFilter< TImageIn ,TImageOut >
  ::GetSlaveInput() const
  {
    if (this->GetNumberOfInputs()<2)
      {
	return 0;
      }
    return static_cast<const ImageInType *>(this->itk::ProcessObject::GetInput(1));
  }

  /**
   * Print
   */
  template<class TImageIn, class TImageOut>
  void
  SARCorrelationGridImageFilter< TImageIn, TImageOut >
  ::PrintSelf(std::ostream & os, itk::Indent indent) const
  {
    Superclass::PrintSelf(os, indent);
    
    os << indent << "ML range factor : " << m_MLran << std::endl;
    os << indent << "ML azimut factor : " << m_MLazi << std::endl;
    os << indent << "Patch size for correlation estimation : " << m_PatchSizePerDim << std::endl;
    os << indent << "Step for the grid : " << m_GridStep[0] << ", " << m_GridStep[1] << std::endl;
  }

  /**
   * activateDEM
   */
  template<class TImageIn, class TImageOut>
  void
  SARCorrelationGridImageFilter< TImageIn, TImageOut >
  ::activateDEM(const std::string DEMDirectory)
  {
    DEMHandlerPointerType DEMHandler = DEMHandler::Instance();
    DEMHandler->ClearDEMs();
    DEMHandler->OpenDEMDirectory(DEMDirectory);    
  }

  /**
   * Method GenerateOutputInformaton()
   **/
  template<class TImageIn, class TImageOut>
  void
  SARCorrelationGridImageFilter< TImageIn, TImageOut >
  ::GenerateOutputInformation()
  {    
    // Call superclass implementation
    Superclass::GenerateOutputInformation();

    // Get pointers to the input and output
    ImageInType * masterPtr  = const_cast<ImageInType * >(this->GetMasterInput());
    ImageInType * slavePtr  = const_cast<ImageInType * >(this->GetSlaveInput());
    ImageOutPointer outputPtr = this->GetOutput();
  
    // Check GridSteps (must be a multiple of ML Factors)
    if (m_GridStep[0] % m_MLran)
      {
	itkExceptionMacro(<<"GridSteps range mot a multiple of MLRan.");
      }
    if (m_GridStep[1] % m_MLazi)
      {
	itkExceptionMacro(<<"GridSteps azimut mot a multiple of MLAzi.");
      }

    m_GridStep_ML[0] = m_GridStep[0]/m_MLran;
    m_GridStep_ML[1] = m_GridStep[1]/m_MLazi;

    ///////// Checks (with input keywordlists/metadata) /////////////
    // Check ML Factors (inputs of this filter with Master and Slave metadata)
    // Get Master and Slave Keywordlist
    ImageKeywordlist masterKWL = masterPtr->GetImageKeywordlist();
    ImageKeywordlist slaveKWL = slavePtr->GetImageKeywordlist();

    if (masterKWL.HasKey("support_data.ml_ran") && masterKWL.HasKey("support_data.ml_azi"))
      {
	// Get Master ML Factors
	unsigned int master_MLRan = atoi(masterKWL.GetMetadataByKey("support_data.ml_ran").c_str());
	unsigned int master_MLAzi = atoi(masterKWL.GetMetadataByKey("support_data.ml_azi").c_str());

	if (slaveKWL.HasKey("support_data.ml_ran") && slaveKWL.HasKey("support_data.ml_azi"))
	  {
	    // Get Slave ML Factors
	    unsigned int slave_MLRan = atoi(slaveKWL.GetMetadataByKey("support_data.ml_ran").c_str());
	    unsigned int slave_MLAzi = atoi(slaveKWL.GetMetadataByKey("support_data.ml_azi").c_str());

	    if ((slave_MLRan != master_MLRan) || (slave_MLAzi != master_MLAzi))
	      {
		itkExceptionMacro(<<"ML Factor betwwen master and slave are different.");
	      }
	  }
	
	if ((master_MLRan != m_MLran) || (master_MLAzi != m_MLazi))
	  {
	    itkExceptionMacro(<<"ML Factor betwwen master and inputs of this application are different.");
	  }
      }
    else
      {
	if (slaveKWL.HasKey("support_data.ml_ran") && slaveKWL.HasKey("support_data.ml_azi"))
	  {
	    // Get Slave ML Factors
	    unsigned int slave_MLRan = atoi(slaveKWL.GetMetadataByKey("support_data.ml_ran").c_str());
	    unsigned int slave_MLAzi = atoi(slaveKWL.GetMetadataByKey("support_data.ml_azi").c_str());
	    
	    if ((slave_MLRan != m_MLran) || (slave_MLAzi != m_MLazi))
	      {
		itkExceptionMacro(<<"ML Factor betwwen slave and inputs of this application are different.");
	      }
	  }
      }

    // Set the number of Components for the Output VectorImage
    // 3 Components :  
    //                _ range shift between Master and Slave
    //                _ azimut shift between Master and Slave
    //                _ correlation rate
    outputPtr->SetNumberOfComponentsPerPixel(3);

    // Update size and spacing according to grid step
    ImageOutRegionType largestRegion  = static_cast<ImageOutRegionType>(masterPtr->GetLargestPossibleRegion());
    ImageOutSizeType outputSize = static_cast<ImageOutSizeType>(largestRegion.GetSize());
    ImageOutSpacingType outputSpacing = static_cast<ImageOutSpacingType>(masterPtr->GetSpacing());
    ImageOutPointType outputOrigin = static_cast<ImageOutPointType>(masterPtr->GetOrigin());

    for(unsigned int dim = 0; dim < ImageOutType::ImageDimension; ++dim)
    {
      outputSize[dim] = (outputSize[dim] - outputSize[dim]%m_GridStep_ML[dim]) / m_GridStep_ML[dim];
      outputSpacing[dim] *= m_GridStep_ML[dim];
    }
    
    // Set spacing
    outputPtr->SetSpacing(outputSpacing);

    // Set origin
    outputPtr->SetOrigin(outputOrigin);

    // Set largest region size
    largestRegion.SetSize(outputSize);
    outputPtr->SetLargestPossibleRegion(largestRegion);

    // RSTransform
    m_rsTransform = RSTransformType2D::New();
    m_rsTransform->SetInputKeywordList(masterPtr->GetImageKeywordlist());
    m_rsTransform->SetOutputKeywordList(slavePtr->GetImageKeywordlist());
    m_rsTransform->InstantiateTransform();

    // Add GridSteps into KeyWordList
    ImageKeywordlist outputKWL;
    outputKWL.AddKey("support_data.gridstep.range", std::to_string(m_GridStep[0]));
    outputKWL.AddKey("support_data.gridstep.azimut", std::to_string(m_GridStep[1]));
    
    outputPtr->SetImageKeywordList(outputKWL); 
  }

  /** 
   * Method OutputRegionToInputRegion for GenerateInputRequestedRegion
   */
  template<class TImageIn, class TImageOut>
  void
  SARCorrelationGridImageFilter< TImageIn, TImageOut >
  ::OutputRegionToInputRegion(const ImageOutRegionType& outputRegion) const
  {
    // Get pointers to inputs
    ImageInType * masterPtr = const_cast<ImageInType * >(GetMasterInput());
    ImageInType * slavePtr = const_cast<ImageInType * >(GetSlaveInput());
    
    int nbColSlave = slavePtr->GetLargestPossibleRegion().GetSize()[0];
    int nbLinesSlave = slavePtr->GetLargestPossibleRegion().GetSize()[1];
    
    /////////////////////// Requested region for master ////////////////////
    // Correlation grid is bonded to the master image => should represent with the 
    // m_Grid_step the output requested region
    // get a copy of the master requested region (should equal the output
    // requested region)
    ImageInRegionType masterRequestedRegion, slaveRequestedRegion;
    masterRequestedRegion = outputRegion;

    // Apply grid step
    ImageInSizeType masterRequestedSize = masterRequestedRegion.GetSize();
    ImageInIndexType masterRequestedIndex = masterRequestedRegion.GetIndex();
  
    for(unsigned int dim = 0; dim < ImageOutType::ImageDimension; ++dim)
      {
	masterRequestedSize [dim] *= m_GridStep_ML[dim];
	masterRequestedIndex[dim] *= m_GridStep_ML[dim];
      }

    masterRequestedRegion.SetSize(masterRequestedSize);
    masterRequestedRegion.SetIndex(masterRequestedIndex);

    //////////////////// Requested region for slave ////////////////////
    //int max_shift = 10;
    int max_shift = 50;
    int firstL, firstC, lastL, lastC; 
    // Transform back into slave region index space with an maximum shift between master and slave
    ImageInIndexType slaveIndex;
   
    // Find requested region
    ImageInSizeType slaveSize;

    // Set the max_shift (margin)
    firstL = masterRequestedIndex[1] - max_shift;
    firstC = masterRequestedIndex[0] - max_shift;
    lastL = masterRequestedIndex[1] + masterRequestedSize[1] + max_shift;
    lastC =  masterRequestedIndex[0] + masterRequestedSize[0] + max_shift;

    // Check the limits
    if (firstC < 0)
      {
	firstC = 0;
      } 
    if (firstL < 0)
      {
	firstL = 0;
      } 
    if (lastC > nbColSlave-1)
      {
	lastC = nbColSlave-1;
      } 
    if (lastL > nbLinesSlave-1)
      {
	lastL = nbLinesSlave-1;
      } 

    
    slaveIndex[0] = static_cast<ImageInIndexValueType>(firstC);
    slaveIndex[1] = static_cast<ImageInIndexValueType>(firstL);
    slaveSize[0] = static_cast<ImageInIndexValueType>(lastC - firstC)+1;
    slaveSize[1] = static_cast<ImageInIndexValueType>(lastL - firstL)+1;

    slaveRequestedRegion.SetIndex(slaveIndex);
    slaveRequestedRegion.SetSize(slaveSize);
    //slaveRequestedRegion.SetIndex(masterRequestedIndex);
    //slaveRequestedRegion.SetSize(masterRequestedSize);

    /////////////////////////// Assigne regions /////////////////////////
    // Pad these input requested regions by the operator patchSize / 2
    masterRequestedRegion.PadByRadius( m_PatchSizePerDim / 2 );
    slaveRequestedRegion.PadByRadius( m_PatchSizePerDim / 2 );
    
    // Crop the fixed region at the fixed's largest possible region
     if ( masterRequestedRegion.Crop(masterPtr->GetLargestPossibleRegion()))
      {
	masterPtr->SetRequestedRegion( masterRequestedRegion );
      }
    else
      {
	// Couldn't crop the region (requested region is outside the largest
	// possible region).  Throw an exception.
	// store what we tried to request (prior to trying to crop)
	masterPtr->SetRequestedRegion( masterRequestedRegion );
	
	// Build an exception
	itk::InvalidRequestedRegionError e(__FILE__, __LINE__);
	std::ostringstream msg;
	msg << this->GetNameOfClass()
	    << "::GenerateInputRequestedRegion()";
	e.SetLocation(msg.str().c_str());
	e.SetDescription("Requested region is (at least partially) outside the largest possible region of image 1.");
	e.SetDataObject(masterPtr);
	throw e;
      }


    // crop the moving region at the moving's largest possible region
    if ( slaveRequestedRegion.Crop(slavePtr->GetLargestPossibleRegion()))
      {
	slavePtr->SetRequestedRegion( slaveRequestedRegion );
      }
    else
      {
	// Couldn't crop the region (requested region is outside the largest
	// possible region). This case might happen so we do not throw any exception but
	// request a null region instead
	slaveSize.Fill(0);
	slaveRequestedRegion.SetSize(slaveSize);
	slaveIndex.Fill(0);
	slaveRequestedRegion.SetIndex(slaveIndex);
	
	// store what we tried to request (prior to trying to crop)
	slavePtr->SetRequestedRegion(slaveRequestedRegion);
      }
  }


  /** 
   * Method GenerateInputRequestedRegion
   */
  template<class TImageIn, class TImageOut>
  void
  SARCorrelationGridImageFilter< TImageIn, TImageOut >
  ::GenerateInputRequestedRegion()
  {
    // Call the superclass' implementation of this method
    Superclass::GenerateInputRequestedRegion();
  
    ImageOutRegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();
    this->OutputRegionToInputRegion(outputRequestedRegion);
   }
 

  /**
   * Method BeforeThreadedGenerateData
   */
  template<class TImageIn, class TImageOut>
  void
  SARCorrelationGridImageFilter< TImageIn, TImageOut >
  ::BeforeThreadedGenerateData()
  { 
    // Get the number of threads
    unsigned int nbThreads = this->GetNumberOfThreads();

    // Allocate Tab
    m_masterPieceTab = new FFTProxyPixelType*[nbThreads];
    m_slavePieceTab = new FFTProxyPixelType*[nbThreads];
    m_inverseFFTPieceTab = new FFTProxyPixelType*[nbThreads];

    m_multipliedFFTTab = new FFTProxyComplexType*[nbThreads];
    m_slaveFFTTab = new FFTProxyComplexType*[nbThreads];
    m_masterFFTTab = new FFTProxyComplexType*[nbThreads];    

    m_masterPlanTab = new FFTProxyPlanType[nbThreads];
    m_slavePlanTab = new FFTProxyPlanType[nbThreads];
    m_inversePlanTab = new FFTProxyPlanType[nbThreads];

    // Extract Parameters
    m_masterExtractPieceTab = new ImageInPointer[nbThreads];
    m_slaveExtractPieceTab = new ImageInPointer[nbThreads];

    // Default size (for FFT execution)
    ImageInSizeType pieceSize;
    pieceSize.Fill(m_PatchSizePerDim/m_SubPixelAccuracy);
    m_pieceSize = pieceSize;

    unsigned int sizeFFT = (pieceSize[0] / 2 + 1) * pieceSize[1];
    unsigned int pieceNbOfPixel = pieceSize[0] * pieceSize[1];
    m_sizeFFT = sizeFFT;
     
    // Size and Index for extracted regions (with padding)
    ImageInSizeType patchSize;
    patchSize.Fill(m_PatchSizePerDim/m_SubPixelAccuracy);

    ImageInIndexType patchIndex;
    ImageInIndexValueType first_index = ((m_PatchSizePerDim/2)*(1/m_SubPixelAccuracy)) - (m_PatchSizePerDim/2);
    patchIndex.Fill(-first_index);

    // Memory allocation for each Thread
    for (unsigned int i = 0; i < nbThreads; i++) 
      {
	// For direct FFTs
	m_masterPieceTab[i] = static_cast<FFTProxyPixelType*>(fftw_malloc(pieceNbOfPixel * sizeof(FFTProxyPixelType)));
   
	m_masterFFTTab[i] = static_cast<FFTProxyComplexType*>(fftw_malloc(sizeFFT * sizeof(FFTProxyComplexType)));
  
	m_masterPlanTab[i] = FFTWProxyType::Plan_dft_r2c_2d(pieceSize[1],
							    pieceSize[0],
							    m_masterPieceTab[i],
							    m_masterFFTTab[i],
							    FFTW_ESTIMATE);

	m_slavePieceTab[i] = static_cast<FFTProxyPixelType*>(fftw_malloc(pieceNbOfPixel * sizeof(FFTProxyPixelType)));
   
	m_slaveFFTTab[i] = static_cast<FFTProxyComplexType*>(fftw_malloc(sizeFFT * sizeof(FFTProxyComplexType)));
   

	m_slavePlanTab[i] = FFTWProxyType::Plan_dft_r2c_2d(pieceSize[1],
							   pieceSize[0],
							   m_slavePieceTab[i],
							   m_slaveFFTTab[i],
							   FFTW_ESTIMATE);
     
	// For inverse FFT
	m_multipliedFFTTab[i] = static_cast<FFTProxyComplexType*>(fftw_malloc(sizeFFT * sizeof(FFTProxyComplexType)));
   
	m_inverseFFTPieceTab[i] = static_cast<FFTProxyPixelType*>(fftw_malloc(pieceNbOfPixel * sizeof(FFTProxyPixelType)));
   
	// Inverse FFT of the product of FFT 
	m_inversePlanTab[i] = FFTWProxyType::Plan_dft_c2r_2d(pieceSize[1],
							     pieceSize[0],
							     m_multipliedFFTTab[i],
							     m_inverseFFTPieceTab[i],
							     FFTW_ESTIMATE);
	// Extract parameters
	ImageInRegionType masterRegion;
	ImageInRegionType slaveRegion;

	masterRegion.SetSize(patchSize);
	slaveRegion.SetSize(patchSize);
	
	masterRegion.SetIndex(patchIndex);
	slaveRegion.SetIndex(patchIndex);

	m_masterExtractPieceTab[i] = ImageInType::New();
	m_slaveExtractPieceTab[i] = ImageInType::New();

	m_masterExtractPieceTab[i]->SetRegions(masterRegion);
	m_masterExtractPieceTab[i]->Allocate();

	m_slaveExtractPieceTab[i]->SetRegions(slaveRegion);
	m_slaveExtractPieceTab[i]->Allocate();
      }
  }

  /**
   * Method AfterThreadedGenerateData
   */
  template<class TImageIn, class TImageOut>
  void
  SARCorrelationGridImageFilter< TImageIn, TImageOut >
  ::AfterThreadedGenerateData()
  {
    // Get the number of threads
    unsigned int nbThreads = this->GetNumberOfThreads();
    
    for (unsigned int i = 0; i < nbThreads; i++) 
      {
	// Destroy the FFT plans
	FFTWProxyType::DestroyPlan(m_masterPlanTab[i]);
	FFTWProxyType::DestroyPlan(m_slavePlanTab[i]);
	FFTWProxyType::DestroyPlan(m_inversePlanTab[i]);

	// Free fftw buffer
	fftw_free(m_masterPieceTab[i]);
	fftw_free(m_slavePieceTab[i]);
	fftw_free(m_multipliedFFTTab[i]);
	fftw_free(m_masterFFTTab[i]);
	fftw_free(m_slaveFFTTab[i]);
	fftw_free(m_inverseFFTPieceTab[i]);
      }

    // Delete pointer arrays
    delete m_masterPieceTab;
    delete m_slavePieceTab;
    delete m_multipliedFFTTab;
    delete m_masterFFTTab;
    delete m_slaveFFTTab;
    delete m_inverseFFTPieceTab;

    delete m_masterPlanTab;
    delete m_slavePlanTab;
    delete m_inversePlanTab;
  }

 /**
   * Method BeforeThreadedGenerateData
   */
  template<class TImageIn, class TImageOut>
  void
  SARCorrelationGridImageFilter< TImageIn, TImageOut >
  ::ThreadedGenerateData(const ImageOutRegionType& outputRegionForThread,
			 itk::ThreadIdType threadId)
  {
    // Get pointers to inputs
    const ImageInType * masterPtr = this->GetMasterInput();
    const ImageInType * slavePtr = this->GetSlaveInput();

    // Typedef for iterators
    typedef itk::ImageScanlineIterator< ImageOutType > OutputIterator;
    typedef itk::ImageRegionConstIterator<ImageInType> MasterSlaveIt;
    typedef itk::ImageRegionIterator<ImageInType> ExtractIt;
    
    // Iterator on output (Grid geometry)
    OutputIterator OutIt(this->GetOutput(), outputRegionForThread);
    OutIt.GoToBegin();
    
    ImageOutPixelType pixelCorrelationGrid;
    pixelCorrelationGrid.Reserve(3);

    // Support progress methods/callbacks
    itk::ProgressReporter progress(this, threadId, this->GetOutput()->GetRequestedRegion().GetNumberOfPixels());

    // First index for padded regions. Padding create region with [-padIndex; padIndex+m_PatchSize] pixels into
    // the 2 dimensions. The "real values" are contains into index [0; m_PatchSize-1], the rest is 0.
    int padIndex = ((m_PatchSizePerDim/2)*(1/m_SubPixelAccuracy)) - (m_PatchSizePerDim/2);

    // Index to define the cycle for the shift of FFT results
    int shift_cycle = (m_PatchSizePerDim/2);
    int shift_value = ((m_PatchSizePerDim/2)*(1/m_SubPixelAccuracy));
    
    // For each line of output 
    while ( !OutIt.IsAtEnd())
      {
	OutIt.GoToBeginOfLine();

	// For each colunm of the grid
       	while (!OutIt.IsAtEndOfLine()) 
	  {
	    // Get physical point from index
	    Point2DType masterPoint;
	    ImageInIndexType masterIndex;
	    for(unsigned int dim = 0; dim < ImageInType::ImageDimension; ++dim)
	      {
		masterIndex[dim] = OutIt.GetIndex()[dim] * m_GridStep_ML[dim];
	      }

	    masterPtr->TransformIndexToPhysicalPoint(masterIndex, masterPoint);

	    // Transform physical point in master to physical point in slave
	    Point2DType slavePoint = m_rsTransform->TransformPoint(masterPoint);

	    // Get slave index from physical point
	    itk::ContinuousIndex<double, 2> slaveIndex;
	    slavePtr->TransformPhysicalPointToContinuousIndex(slavePoint, slaveIndex);
	   
	    // Get the pixel around (to extract regions)
	    ImageInIndexType currentMasterIndex = masterIndex;
	    ImageInIndexType currentSlaveIndex;
	    ImageInSizeType currentMasterSize;
	    ImageInSizeType currentSlaveSize;
	   	 
	    // Prepare regions 
	    // Center the region to the masterIndex and the slaveIndex 
	    for(unsigned int i = 0; i < ImageInType::ImageDimension; i++)
	      {
		currentMasterIndex[i] = masterIndex[i] - (m_PatchSizePerDim / 2);
		currentSlaveIndex[i] = static_cast<ImageOutIndexValueType>(slaveIndex[i]) - 
		 (m_PatchSizePerDim / 2);
	      }
	    
	    currentMasterSize.Fill(static_cast<ImageInSizeValueType>(m_PatchSizePerDim));
	    ImageInRegionType masterCurrentRegion;
	    masterCurrentRegion.SetIndex(currentMasterIndex);
	    masterCurrentRegion.SetSize(currentMasterSize);
	    masterCurrentRegion.Crop(masterPtr->GetLargestPossibleRegion());

	    currentSlaveSize.Fill(static_cast<ImageInSizeValueType>(m_PatchSizePerDim));
	    ImageInRegionType slaveCurrentRegion;
	    slaveCurrentRegion.SetIndex(currentSlaveIndex);
	    slaveCurrentRegion.SetSize(currentSlaveSize);
	    slaveCurrentRegion.Crop(slavePtr->GetLargestPossibleRegion());

	    // Check the size of our future extracted region (should be the same to execute the internal
	    // Pipeline)
	    if (slaveCurrentRegion.GetSize() != masterCurrentRegion.GetSize())
	      {
		for(unsigned int i = 0; i < ImageInType::ImageDimension; i++)
		  {
		    ImageInSizeValueType minSize = std::min(masterCurrentRegion.GetSize()[i], 
							    slaveCurrentRegion.GetSize()[i]);
		    currentMasterSize[i] = minSize;
		    currentSlaveSize[i] = minSize;
		  }
		masterCurrentRegion.SetSize(currentMasterSize);
		slaveCurrentRegion.SetSize(currentSlaveSize);
	      }

	    // Check Index
	    if (slaveCurrentRegion.GetIndex()[0] < 0 || slaveCurrentRegion.GetIndex()[1] < 0)
	      {
		for(unsigned int i = 0; i < ImageInType::ImageDimension; i++)
		  {
		    currentSlaveIndex[i] = slaveCurrentRegion.GetIndex()[i];
		    if (slaveCurrentRegion.GetIndex()[i] < 0)
		      {
			currentSlaveIndex[i] = 0;
		      }
		  }
		slaveCurrentRegion.SetIndex(currentSlaveIndex);
	      }

	    // Rescale values to adapt the deformation
	    double rescale_range = masterCurrentRegion.GetSize()[0];
	    double rescale_azimut = masterCurrentRegion.GetSize()[1];

	    // Extraction regions from images
	    ImageInPointer masterExtract = m_masterExtractPieceTab[threadId];
	    ImageInPointer slaveExtract = m_slaveExtractPieceTab[threadId];

	    // Iterators on master and slave Ptr
	    MasterSlaveIt masterIt(masterPtr, masterCurrentRegion);
	    masterIt.GoToBegin();
	    MasterSlaveIt slaveIt(slavePtr, slaveCurrentRegion);
	    slaveIt.GoToBegin();
	    
	    // Iterator on extracted regions
	    ExtractIt masterExtractIt(masterExtract, masterExtract->GetLargestPossibleRegion());
	    masterExtractIt.GoToBegin();
	    ExtractIt slaveExtractIt(slaveExtract, slaveExtract->GetLargestPossibleRegion());
	    slaveExtractIt.GoToBegin();

	    int first = -padIndex;
	    int last = padIndex + m_PatchSizePerDim;

	    // Extract regions with padding
	    for (int l = first; l < last; ++l)
	      {
		for (int k = first; k < last; ++k)
		      {
			// If under the extracted region
			if ((l >= 0 && l < static_cast<int>(m_PatchSizePerDim)) &&
          (k >= 0 && k < static_cast<int>(m_PatchSizePerDim)))
			  {
			    // Check masterIt (for bounderies)
			    if (!masterIt.IsAtEnd())
			      {
				masterExtractIt.Set(masterIt.Get());
				++masterExtractIt;
				++masterIt;
			      }
			    else
			      {
				masterExtractIt.Set(0.);
				++masterExtractIt;
			      }
			    
			    // Check slaveIt (for bounderies)
			    if (!slaveIt.IsAtEnd())
			      {
				slaveExtractIt.Set(slaveIt.Get());
				++slaveExtractIt;
				++slaveIt;
			      }
			    else
			      {
				slaveExtractIt.Set(0.);
				++slaveExtractIt;
			      }
			  }
			// Else padding
			else
			  {
			    masterExtractIt.Set(0.);
			    ++masterExtractIt;
			    slaveExtractIt.Set(0.);
			    ++slaveExtractIt;
			  }
		      }
	      }


	    ImageInPointer rescaleSlavePtr;

	    // Need to rescale slave with master to obtain the same dynamic
	    if (m_Rescale)
	      {
		typename MinMaxCalculatorType::Pointer minMaxMasterRegion = MinMaxCalculatorType::New();
		minMaxMasterRegion->SetImage(masterExtract);
		minMaxMasterRegion->ComputeMaximum();
		minMaxMasterRegion->ComputeMinimum();
	    
		typename RescaleFilterType::Pointer rescaleFilter = RescaleFilterType::New();
		rescaleFilter->SetInput(slaveExtract);
		rescaleFilter->SetOutputMinimum(minMaxMasterRegion->GetMinimum());
		rescaleFilter->SetOutputMaximum(minMaxMasterRegion->GetMaximum());
		rescaleFilter->Update();
		
		rescaleSlavePtr = rescaleFilter->GetOutput();
	      }
	    else
	      {
		rescaleSlavePtr = slaveExtract;
	      }

	    
	    // FFT implementation with FFTWProxy
	    // Get Pointers and Plans for current thread
	    FFTProxyPixelType * masterPiece =  m_masterPieceTab[threadId];
	    FFTProxyPixelType * slavePiece =  m_slavePieceTab[threadId];
	    FFTProxyPixelType *  inverseFFTPiece = m_inverseFFTPieceTab[threadId];
	    FFTProxyPlanType masterPlan = m_masterPlanTab[threadId]; 
	    FFTProxyPlanType slavePlan = m_slavePlanTab[threadId];
	    FFTProxyPlanType inversePlan = m_inversePlanTab[threadId];
	    FFTProxyComplexType * multipliedFFT = m_multipliedFFTTab[threadId];
	    FFTProxyComplexType * masterFFT = m_masterFFTTab[threadId];
	    FFTProxyComplexType * slaveFFT = m_slaveFFTTab[threadId];
    
	    masterExtractIt.GoToBegin();
	    slaveExtractIt.GoToBegin();

	    int sizePad = m_PatchSizePerDim/m_SubPixelAccuracy;
	    
	    // Copy into buffers the extracted regions
	    for (int l = 0; l < sizePad; ++l)
	      {
		for (int k = 0; k < sizePad; ++k)
		  {
		    masterPiece[m_pieceSize[0] * l + k] =  masterExtractIt.Get();
		    ++masterExtractIt;
		  }
	      }
	    
	    for (int l = 0; l < sizePad; ++l)
	      {
		for (int k = 0; k < sizePad; ++k)
		  {
		    slavePiece[m_pieceSize[0] * l + k] = slaveExtractIt.Get();
		    ++slaveExtractIt;
		  }
	      }
	 
	    // Execute master and slave FFT Plan
	    FFTWProxyType::Execute(masterPlan);
	    FFTWProxyType::Execute(slavePlan);

	     // Conjugate product
	     for (unsigned int j = 0; j < m_sizeFFT; ++j)
	       {
		 // Complex mutiplication
		   multipliedFFT[j][0] = masterFFT[j][0] * slaveFFT[j][0] + masterFFT[j][1] * slaveFFT[j][1];
		   multipliedFFT[j][1] = masterFFT[j][1] * slaveFFT[j][0] - masterFFT[j][0] * slaveFFT[j][1];
	       }
	     
	     // Normalization with the modulus
	     for (unsigned int j = 0; j < m_sizeFFT; ++j)
	       {
		 // Modulus
		double mod = std::sqrt(multipliedFFT[j][0]*multipliedFFT[j][0] +
				       multipliedFFT[j][1]*multipliedFFT[j][1]);
		
		if (mod != 0)
		  {
		    multipliedFFT[j][0] /= (std::sqrt(multipliedFFT[j][0]*multipliedFFT[j][0] +
						      multipliedFFT[j][1]*multipliedFFT[j][1]));
		    multipliedFFT[j][1] /= (std::sqrt(multipliedFFT[j][0]*multipliedFFT[j][0] +
						      multipliedFFT[j][1]*multipliedFFT[j][1]));
		  }
		else
		  {
		    multipliedFFT[j][0] = 0;
		    multipliedFFT[j][1] = 0;
		  }
		
		multipliedFFT[j][0] /= (m_pieceSize[0]*m_pieceSize[1]);
		multipliedFFT[j][1] /= (m_pieceSize[0]*m_pieceSize[1]);
	       }

	      m_multipliedFFTTab[threadId] = multipliedFFT;
	     
	     // Execute plan for inverse FFT
	     FFTWProxyType::Execute(inversePlan);

	     // Copy into masterExtract the result. Apply at the same time, a cyclic shift. 	     
	     masterExtractIt.GoToBegin();
	     
	     int ind_InverseFFTLine = 0;
	     int ind_InverseFFTColunm = 0;
	     
	     while (!masterExtractIt.IsAtEnd())
	       {
		 ImageInIndexType currentIndex = masterExtractIt.GetIndex();
		 
		 // Define index to copy result into masterExtract Image (cyclic shift)
 		 if (currentIndex[0] < shift_cycle)
		   {
		     ind_InverseFFTColunm = currentIndex[0] + shift_value; 
		   }
		 else
		   {
		     ind_InverseFFTColunm = currentIndex[0] - shift_value;
		   }

		 if (currentIndex[1] < shift_cycle)
		   {
		     ind_InverseFFTLine = currentIndex[1] + shift_value;
		   }
		 else
		   {
		     ind_InverseFFTLine = currentIndex[1] - shift_value;
		   }
		 
		 // padIndex to rescale with the inverseFFT buffer 
		 // (index of inverseFFT goes from 0 to m_PatchSize-1)
		 ind_InverseFFTColunm += padIndex;
		 ind_InverseFFTLine += padIndex;

		 // Copy
		 masterExtractIt.Set(inverseFFTPiece[m_pieceSize[0] * ind_InverseFFTLine + 
						    ind_InverseFFTColunm]);
		 ++masterExtractIt;
	       }
	    
	    // Get position of maximum correlation
	    typename MinMaxCalculatorType::Pointer minMax = MinMaxCalculatorType::New();
	    minMax->SetImage(masterExtract);
	    minMax->ComputeMaximum();

	    ImageInIndexType indexOfMaximumCorrelation = minMax->GetIndexOfMaximum();
	   
	    // Assigne the deformation grid
	    // Correlation between master and slave = Correlation_at_the_selected_point (at master_Index) + 
	    // Correlation_into_the_patch
	    // Correlation_at_the_selected_point[dim] = slaveIndex[dim] - masterIndex[dim]
	    // Correlation_into_the_patch[dim] = IndexOfMaximumCorrelation[dim] - rescale_dim/2 
	    // rescale_dim is often equal to m_PatchSizePerDim except for the limits of the grid
	    pixelCorrelationGrid[0] = slaveIndex[0] - masterIndex[0] + 
	      ((double(indexOfMaximumCorrelation[0])- rescale_range/2)*m_SubPixelAccuracy);
	    pixelCorrelationGrid[1] = slaveIndex[1] - masterIndex[1] + 
	      ((double(indexOfMaximumCorrelation[1]) - rescale_azimut/2 )*m_SubPixelAccuracy);
	    
	    // Correlation
	    pixelCorrelationGrid[2] = minMax->GetMaximum();
	      	    
	    // Correlation into SLC geometry and not ML geometry
	    pixelCorrelationGrid[0] *= m_MLran;
	    pixelCorrelationGrid[1] *= m_MLazi;

	    // Assign Output
	    OutIt.Set(pixelCorrelationGrid);
	    progress.CompletedPixel();
	    
	    // Next Colunm for output
	    ++OutIt;
	  }
	
	// Next Line for output 
	OutIt.NextLine();
      }
  }

} /*namespace otb*/

#endif
