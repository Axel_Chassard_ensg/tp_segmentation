/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARDEMGridImageFilter_h
#define otbSARDEMGridImageFilter_h

#include "itkImageToImageFilter.h"
#include "itkSmartPointer.h"
#include "itkPoint.h"

#include "otbImageKeywordlist.h"
#include "otbGenericRSTransform.h"
#
namespace otb
{
  /** \class SARDEMGridImageFilter 
   * \brief Estimates a deformation grid between two images (master and slave) related to the DEM for the azimut
   * and range dimension. For each grid point, a number of DEM points with contribution is also calculated.
   * 
   * This filter performs the estimation of deformations with DEM projections between two images. The inputs 
   * are the two projected DEM into Master SAR geometry and Slave SAR geometry. The projected DEM are vector 
   * images with at least two components : C (colunm into SAR image) and L (line into SAR image). 
   * The vector image can be composed of other components, not used here. 
   * 
   * \ingroup DiapOTBModule
   */

  template <typename TImageIn,  typename TImageSAR, typename TImageOut> class ITK_EXPORT SARDEMGridImageFilter :
    public itk::ImageToImageFilter<TImageIn,TImageOut>
  {
  public:

    // Standard class typedefs
    typedef SARDEMGridImageFilter                    Self;
    typedef itk::ImageToImageFilter<TImageIn,TImageOut>    Superclass;
    typedef itk::SmartPointer<Self>                        Pointer;
    typedef itk::SmartPointer<const Self>                  ConstPointer;

    // Method for creation through object factory
    itkNewMacro(Self);
    // Run-time type information
    itkTypeMacro(SARDEMGridFilter,ImageToImageFilter);

    /** Typedef to image input type : otb::VectorImage for DEM projections (master and slave) */
    typedef TImageIn                                  ImageInType;
    /** Typedef to describe the inout image pointer type. */
    typedef typename ImageInType::Pointer             ImageInPointer;
    typedef typename ImageInType::ConstPointer        ImageInConstPointer;
    /** Typedef to describe the inout image region type. */
    typedef typename ImageInType::RegionType          ImageInRegionType;
    /** Typedef to describe the type of pixel and point for inout image. */
    typedef typename ImageInType::PixelType           ImageInPixelType;
    typedef typename ImageInType::PointType           ImageInPointType;
    /** Typedef to describe the image index, size types and spacing for inout image. */
    typedef typename ImageInType::IndexType           ImageInIndexType;
    typedef typename ImageInType::IndexValueType      ImageInIndexValueType;
    typedef typename ImageInType::SizeType            ImageInSizeType;
    typedef typename ImageInType::SizeValueType       ImageInSizeValueType;
    typedef typename ImageInType::SpacingType         ImageInSpacingType;
    typedef typename ImageInType::SpacingValueType    ImageInSpacingValueType;
  
    /** Typedef to image output type : otb::VectorImage with three components (range deformations, 
	azimut deformations and nb DEM points for contribution */
    typedef TImageOut                                  ImageOutType;
    /** Typedef to describe the output image pointer type. */
    typedef typename ImageOutType::Pointer             ImageOutPointer;
    typedef typename ImageOutType::ConstPointer        ImageOutConstPointer;
    /** Typedef to describe the output image region type. */
    typedef typename ImageOutType::RegionType          ImageOutRegionType;
    /** Typedef to describe the type of pixel and point for output image. */
    typedef typename ImageOutType::PixelType           ImageOutPixelType;
    typedef typename ImageOutType::PointType           ImageOutPointType;
    /** Typedef to describe the image index, size types and spacing for output image. */
    typedef typename ImageOutType::IndexType           ImageOutIndexType;
    typedef typename ImageOutType::IndexValueType      ImageOutIndexValueType;
    typedef typename ImageOutType::SizeType            ImageOutSizeType;
    typedef typename ImageOutType::SizeValueType       ImageOutSizeValueType;
    typedef typename ImageOutType::SpacingType         ImageOutSpacingType;
    typedef typename ImageOutType::SpacingValueType    ImageOutSpacingValueType;

    /** Typedef to SAR image type : otb::Image with Master SAR geometry (only metadata)  */
    typedef TImageSAR                                  ImageSARType;
    typedef typename ImageSARType::Pointer             ImageSARPointer;
  
    // Define Point2DType and Point3DType
    using Point2DType = itk::Point<double,2>;
    using Point3DType = itk::Point<double,3>;

    // Define Filter used here (ie : RSTRansform from Master to DEM)
    typedef typename otb::GenericRSTransform<double,2,2>                                    RSTransformType2D;
    
    // Setter/Getter
    /** Set/Get the size of the area on which correlation is computed */
    /** Set/Get unsigned int grid step */
    void SetGridStep(unsigned int stepRange, unsigned int stepAzimut)
    {
      m_GridStep[0] = stepRange;
      m_GridStep[1] = stepAzimut;
    }
    unsigned int GetGridStepRange()
    {
      return m_GridStep[0];
    }
    unsigned int GetGridStepAzimut()
    {
      return m_GridStep[1];
    }
    /** Set/Get ML factors */
    itkSetMacro(MLran, unsigned int);
    itkGetMacro(MLran, unsigned int);
    itkSetMacro(MLazi, unsigned int);
    itkGetMacro(MLazi, unsigned int);
    void SetSARImagePtr(ImageSARPointer sarPtr);

    // Setter/Getter for DEM projections on master and slave images (inputs)
    /** Connect one of the operands for registration */
    void SetDEMProjOnMasterInput( const ImageInType* image);

    /** Connect one of the operands for registration */
    void SetDEMProjOnSlaveInput(const ImageInType* image);

    /** Get the inputs */
    const ImageInType * GetDEMProjOnMasterInput() const;
    const ImageInType * GetDEMProjOnSlaveInput() const;

  protected:
    // Constructor
    SARDEMGridImageFilter();

    // Destructor
    virtual ~SARDEMGridImageFilter() ITK_OVERRIDE {};

    // Print
    void PrintSelf(std::ostream & os, itk::Indent indent) const ITK_OVERRIDE;
  
    /** SARDEMGridImageFilter produces an vector image to indicate the deformation related to DEM for the two 
     * dimensions and the nb DEM points for contribution. The differences between output and input images are 
     * the size of images and the dimensions. 
     * As such, SARDEMGridImageFilter needs to provide an implementation for 
     * GenerateOutputInformation() in order to inform the pipeline execution model. 
     */ 
    virtual void GenerateOutputInformation() ITK_OVERRIDE;

    /** SARDEMGridImageFilter needs input requested regions (for DEM projection on master and slave images) that 
     * corresponds to the projection into the requested region of the deformation grid (our output requested 
     * region).  
     * As such, SARQuadraticAveragingImageFilter needs to provide an implementation for 
     * GenerateInputRequestedRegion() in order to inform the pipeline execution model. 
     * \sa ProcessObject::GenerateInputRequestedRegion() */
    virtual void GenerateInputRequestedRegion() ITK_OVERRIDE;

    /**
     * OutputRegionToInputRegion assigne DEM projected (master and slave) 
     */
    ImageInRegionType OutputRegionToInputRegion(const ImageOutRegionType& outputRegion) const;

    /** SARDEMGridImageFilter can be implemented as a multithreaded filter.
     * Therefore, this implementation provides a ThreadedGenerateData() routine
     * which is called for each processing thread. The output image data is
     * allocated automatically by the superclass prior to calling
     * ThreadedGenerateData().  ThreadedGenerateData can only write to the
     * portion of the output image specified by the parameter
     * "outputRegionForThread"
     *
     * \sa ImageToImageFilter::ThreadedGenerateData(),
     *     ImageToImageFilter::GenerateData() */
    void ThreadedGenerateData(const ImageOutRegionType& outputRegionForThread,
			      itk::ThreadIdType threadId ) override;

  
  private:
    SARDEMGridImageFilter(const Self&); // purposely not implemented
    void operator=(const Self &); // purposely not 
    
    /** Grid step */
    ImageInSizeType m_GridStep;

    /** ML factors (in range and in azimut) */
    unsigned int m_MLran;
    unsigned int m_MLazi;
    
    // Master SAR Image (only metadata)
    ImageSARPointer m_SarImagePtr;

    // RSTransform
    RSTransformType2D::Pointer m_rsTransform;
  };

} // End namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSARDEMGridImageFilter.txx"
#endif



#endif
