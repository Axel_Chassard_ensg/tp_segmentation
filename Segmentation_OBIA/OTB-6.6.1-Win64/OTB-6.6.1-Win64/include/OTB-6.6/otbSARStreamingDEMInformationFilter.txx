/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARStreamingDEMInformationFilter_txx
#define otbSARStreamingDEMInformationFilter_txx
#include "otbSARStreamingDEMInformationFilter.h"

#include "itkImageRegionIterator.h"
#include "itkImageScanlineConstIterator.h"
#include "itkProgressReporter.h"
#include "otbMacro.h"

#include <complex>

namespace otb
{

template<class TInputImage>
PersistentDEMInformationFilter<TInputImage>
::PersistentDEMInformationFilter()
  : m_Thread_DEMSide_0_0(1),
    m_Thread_DEMSide_0_nbLines(1),
    m_Thread_DEMSide_nbCol_0(1),
    m_Thread_DEMSide_nbCol_nbLines(1),
    m_UserIgnoredValue(-32768)
{
  // first output is a copy of the image, DataObject created by
  // superclass
  //
  // allocate the data objects for the outputs which are
  // just decorators around pixel types
  for (int i = 1; i <= 4; ++i)
    {
    typename PixelObjectType::Pointer output
      = static_cast<PixelObjectType*>(this->MakeOutput(1).GetPointer());
    this->itk::ProcessObject::SetNthOutput(i, output.GetPointer());
    }
    
  this->GetSide_0_0_Output()->Set(itk::NumericTraits<PixelType>::Zero);
  this->GetSide_0_nbLines_Output()->Set(itk::NumericTraits<PixelType>::Zero);
  this->GetSide_nbCol_0_Output()->Set(itk::NumericTraits<PixelType>::Zero);
  this->GetSide_nbCol_nbLines_Output()->Set(itk::NumericTraits<PixelType>::Zero);

  this->Reset();
}

template<class TInputImage>
typename itk::DataObject::Pointer
PersistentDEMInformationFilter<TInputImage>
::MakeOutput(DataObjectPointerArraySizeType output)
{
  switch (output)
    {
    case 0:
      return static_cast<itk::DataObject*>(TInputImage::New().GetPointer());
      break;
    case 1:
      return static_cast<itk::DataObject*>(PixelObjectType::New().GetPointer());
      break;
    case 2:
      return static_cast<itk::DataObject*>(PixelObjectType::New().GetPointer());
      break;
    case 3:
      return static_cast<itk::DataObject*>(LongObjectType::New().GetPointer());
      break;
    default:
      // might as well make an image
      return static_cast<itk::DataObject*>(TInputImage::New().GetPointer());
      break;
    }
}

template<class TInputImage>
typename PersistentDEMInformationFilter<TInputImage>::PixelObjectType*
PersistentDEMInformationFilter<TInputImage>
::GetSide_0_0_Output()
{
  return static_cast<PixelObjectType*>(this->itk::ProcessObject::GetOutput(1));
}

template<class TInputImage>
const typename PersistentDEMInformationFilter<TInputImage>::PixelObjectType*
PersistentDEMInformationFilter<TInputImage>
::GetSide_0_0_Output() const
{
  return static_cast<const PixelObjectType*>(this->itk::ProcessObject::GetOutput(1));
}

template<class TInputImage>
typename PersistentDEMInformationFilter<TInputImage>::PixelObjectType*
PersistentDEMInformationFilter<TInputImage>
::GetSide_0_nbLines_Output()
{
  return static_cast<PixelObjectType*>(this->itk::ProcessObject::GetOutput(2));
}

template<class TInputImage>
const typename PersistentDEMInformationFilter<TInputImage>::PixelObjectType*
PersistentDEMInformationFilter<TInputImage>
::GetSide_0_nbLines_Output() const
{
  return static_cast<const PixelObjectType*>(this->itk::ProcessObject::GetOutput(2));
}

template<class TInputImage>
typename PersistentDEMInformationFilter<TInputImage>::PixelObjectType*
PersistentDEMInformationFilter<TInputImage>
::GetSide_nbCol_0_Output()
{
  return static_cast<PixelObjectType*>(this->itk::ProcessObject::GetOutput(3));
}

template<class TInputImage>
const typename PersistentDEMInformationFilter<TInputImage>::PixelObjectType*
PersistentDEMInformationFilter<TInputImage>
::GetSide_nbCol_0_Output() const
{
  return static_cast<const PixelObjectType*>(this->itk::ProcessObject::GetOutput(3));
}

template<class TInputImage>
typename PersistentDEMInformationFilter<TInputImage>::PixelObjectType*
PersistentDEMInformationFilter<TInputImage>
::GetSide_nbCol_nbLines_Output()
{
  return static_cast<PixelObjectType*>(this->itk::ProcessObject::GetOutput(4));
}

template<class TInputImage>
const typename PersistentDEMInformationFilter<TInputImage>::PixelObjectType*
PersistentDEMInformationFilter<TInputImage>
::GetSide_nbCol_nbLines_Output() const
{
  return static_cast<const PixelObjectType*>(this->itk::ProcessObject::GetOutput(4));
}

template<class TInputImage>
void
PersistentDEMInformationFilter<TInputImage>
::GenerateOutputInformation()
{
  Superclass::GenerateOutputInformation();
  if (this->GetInput())
    {
    this->GetOutput()->CopyInformation(this->GetInput());
    this->GetOutput()->SetLargestPossibleRegion(this->GetInput()->GetLargestPossibleRegion());

    if (this->GetOutput()->GetRequestedRegion().GetNumberOfPixels() == 0)
      {
      this->GetOutput()->SetRequestedRegion(this->GetOutput()->GetLargestPossibleRegion());
      }
    }
}
template<class TInputImage>
void
PersistentDEMInformationFilter<TInputImage>
::AllocateOutputs()
{
  // This is commented to prevent the streaming of the whole image for the first stream strip
  // It shall not cause any problem because the output image of this filter is not intended to be used.
  //InputImagePointer image = const_cast< TInputImage * >( this->GetInput() );
  //this->GraftOutput( image );
  // Nothing that needs to be allocated for the remaining outputs
}

template<class TInputImage>
void
PersistentDEMInformationFilter<TInputImage>
::Synthetize()
{
  int numberOfThreads = this->GetNumberOfThreads();
  
  // Assign the side and set the outputs (if m_Threads* != 0) 
  for (int i = 0; i < numberOfThreads; ++i)
    {
      if (m_Thread_DEMSide_0_0[i] !=  itk::NumericTraits<PixelType>::Zero)
	{
	  this->GetSide_0_0_Output()->Set(m_Thread_DEMSide_0_0[i]);
	}
      
      if (m_Thread_DEMSide_0_nbLines[i] !=  itk::NumericTraits<PixelType>::Zero)
	{
	  this->GetSide_0_nbLines_Output()->Set(m_Thread_DEMSide_0_nbLines[i]);
	}
      
      if (m_Thread_DEMSide_nbCol_0[i] !=  itk::NumericTraits<PixelType>::Zero)
	{
	  this->GetSide_nbCol_0_Output()->Set(m_Thread_DEMSide_nbCol_0[i]);
	}

      if (m_Thread_DEMSide_nbCol_nbLines[i] !=  itk::NumericTraits<PixelType>::Zero)
	{
	  this->GetSide_nbCol_nbLines_Output()->Set(m_Thread_DEMSide_nbCol_nbLines[i]);
	}
      
    } 

}

template<class TInputImage>
void
PersistentDEMInformationFilter<TInputImage>
::Reset()
{
  int numberOfThreads = this->GetNumberOfThreads();

  // Resize the thread temporaries
  m_Thread_DEMSide_0_0.SetSize(numberOfThreads);
  m_Thread_DEMSide_0_nbLines.SetSize(numberOfThreads);
  m_Thread_DEMSide_nbCol_0.SetSize(numberOfThreads);
  m_Thread_DEMSide_nbCol_nbLines.SetSize(numberOfThreads);
  // Initialize the temporaries
  m_Thread_DEMSide_0_0.Fill(itk::NumericTraits<PixelType>::Zero);
  m_Thread_DEMSide_0_nbLines.Fill(itk::NumericTraits<PixelType>::Zero);
  m_Thread_DEMSide_nbCol_0.Fill(itk::NumericTraits<PixelType>::Zero);
  m_Thread_DEMSide_nbCol_nbLines.Fill(itk::NumericTraits<PixelType>::Zero);
}


/** 
 * Method GenerateInputRequestedRegion
 */
template<class TInputImage>
void
PersistentDEMInformationFilter< TInputImage >
::GenerateInputRequestedRegion()
{
  RegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();
  RegionType inputRequestedRegion = outputRequestedRegion;

  InputImagePointer  inputPtr = const_cast< ImageType * >( this->GetInput() );
  
  inputPtr->SetRequestedRegion(inputRequestedRegion);
}



template<class TInputImage>
void
PersistentDEMInformationFilter<TInputImage>
::ThreadedGenerateData(const RegionType& outputRegionForThread,
                       itk::ThreadIdType threadId)
{
  /**
   * Grab the input
   */
  //InputImagePointer inputPtr =  const_cast<TInputImage *>(this->GetInput(0));
  // Compute corresponding input region (same as output here)
  RegionType inputRegionForThread = outputRegionForThread;

  // support progress methods/callbacks
  itk::ProgressReporter progress(this, threadId, outputRegionForThread.GetNumberOfPixels());

  //  Define/declare an iterator that will walk the input region for this
  // thread. 
  typedef itk::ImageScanlineConstIterator< ImageType > InputIterator;
  InputIterator  inIt(this->GetInput(), inputRegionForThread);
  
  inIt.GoToBegin();
  
  // Retrive the DEM dimensions for the side
  int nbColDEM =  this->GetInput()->GetLargestPossibleRegion().GetSize()[0];
  int nbLinesDEM =  this->GetInput()->GetLargestPossibleRegion().GetSize()[1];
  
  // For each line 
  while ( !inIt.IsAtEnd())
  {
    inIt.GoToBeginOfLine();
   
    // For each column
    while (!inIt.IsAtEndOfLine())
    {
      // Get the Index
      IndexType index_current = inIt.GetIndex();

      
      // Check if the current index is corresponding to a side : 
      // (0,0), (0,nbLinesDEM-1), (nbColDEM-1,0), (nbColDEM-1,nbLinesDEM-1) 
      if (index_current[0] == 0 && index_current[1] == 0)
	{
	  m_Thread_DEMSide_0_0[threadId] = inIt.Get();
	}
      if (index_current[0] == 0 && index_current[1] == nbLinesDEM-1)
	{
	  m_Thread_DEMSide_0_nbLines[threadId] = inIt.Get();
	}
      if (index_current[0] == nbColDEM-1 && index_current[1] == 0)
	{
	  m_Thread_DEMSide_nbCol_0[threadId] = inIt.Get();
	}
      if (index_current[0] == nbColDEM-1 && index_current[1] == nbLinesDEM-1)
	{
	  m_Thread_DEMSide_nbCol_nbLines[threadId] = inIt.Get();
	}
             
       // Next colunm
       ++inIt;
    }

    // Next Line
    inIt.NextLine();
  }
}

template <class TImage>
void
PersistentDEMInformationFilter<TImage>
::PrintSelf(std::ostream& os, itk::Indent indent) const
{
  Superclass::PrintSelf(os, indent);

  os << indent << "Side (0,0): "
     << static_cast<typename itk::NumericTraits<PixelType>::PrintType>(this->GetSide_0_0()) << std::endl;
  os << indent << "Side (0,nbLines): "
     << static_cast<typename itk::NumericTraits<PixelType>::PrintType>(this->GetSide_0_nbLines()) << std::endl;
  os << indent << "Side (nbCol,0): "
     << static_cast<typename itk::NumericTraits<PixelType>::PrintType>(this->GetSide_nbCol_0()) << std::endl;
  os << indent << "Side (nbCol,nbLines): "
     << static_cast<typename itk::NumericTraits<PixelType>::PrintType>(this->GetSide_nbCol_nbLines()) << std::endl;
}
} // end namespace otb
#endif
