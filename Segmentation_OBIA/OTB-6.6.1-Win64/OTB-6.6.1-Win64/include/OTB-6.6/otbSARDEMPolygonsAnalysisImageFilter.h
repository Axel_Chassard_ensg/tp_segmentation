/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARDEMPolygonsAnalysisImageFilter_h
#define otbSARDEMPolygonsAnalysisImageFilter_h

#include "itkImageToImageFilter.h"
#include "itkSmartPointer.h"
#include "itkPoint.h"
#include "itkSimpleFastMutexLock.h"

#include "otbSarSensorModelAdapter.h"
#include "otbImageKeywordlist.h"
#include "otbGenericRSTransform.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkImageRegionIterator.h"


namespace otb
{
/** \class SARDEMPolygonsAnalysisImageFilter 
 * \brief Analyses the projected DEM. 
 * 
 * This filter :
 * _ builds polygons with projected DEM points.
 * _ select DEM polygons thanks to intersection with each SAR line. 
 * _ scan all selected polygons to extract wanted information for each pixel into output geometry. The "wanted 
 * information" is defined by the TFunction. This functor has to be instanciated outside this filter and is set 
 * with SetFunctorPtr. 
 * 
 * This filter can be used to estimate the amplitude image of a SAR image. Two kinds of output geometries are
 * available : SAR geometry (same as TImageSAR) or ML geometry (thanks to ML factors). A optional image defined 
 * as a vector can be estimated. 
 * 
 * The output geometry and the optional image are required by the functor itself. Outputs can be a simple images
 * or vector images.
 *
 * \ingroup DiapOTBModule
 */

  template <typename TImageIn,  typename TImageOut, typename TImageDEM, typename TImageSAR, typename TFunction> 
class ITK_EXPORT SARDEMPolygonsAnalysisImageFilter :
    public itk::ImageToImageFilter<TImageIn,TImageOut>
{
public:

  // Standard class typedefs
  typedef SARDEMPolygonsAnalysisImageFilter                    Self;
  typedef itk::ImageToImageFilter<TImageIn,TImageOut>    Superclass;
  typedef itk::SmartPointer<Self>                        Pointer;
  typedef itk::SmartPointer<const Self>                  ConstPointer;

  // Method for creation through object factory
  itkNewMacro(Self);
  // Run-time type information
  itkTypeMacro(SARDEMPolygonsAnalysisImageFilter,ImageToImageFilter);

  /** Typedef to image input type : otb::VectorImage */
  typedef TImageIn                                  ImageInType;
  /** Typedef to describe the inout image pointer type. */
  typedef typename ImageInType::Pointer             ImageInPointer;
  typedef typename ImageInType::ConstPointer        ImageInConstPointer;
  /** Typedef to describe the inout image region type. */
  typedef typename ImageInType::RegionType          ImageInRegionType;
  /** Typedef to describe the type of pixel and point for inout image. */
  typedef typename ImageInType::PixelType           ImageInPixelType;
  typedef typename ImageInType::PointType           ImageInPointType;
  /** Typedef to describe the image index, size types and spacing for inout image. */
  typedef typename ImageInType::IndexType           ImageInIndexType;
  typedef typename ImageInType::IndexValueType      ImageInIndexValueType;
  typedef typename ImageInType::SizeType            ImageInSizeType;
  typedef typename ImageInType::SizeValueType       ImageInSizeValueType;
  typedef typename ImageInType::SpacingType         ImageInSpacingType;
  typedef typename ImageInType::SpacingValueType    ImageInSpacingValueType;
  
  /** Typedef to image output type : otb::Image or otb::VectorImage with SAR or ML geometry  */
  typedef TImageOut                                  ImageOutType;
  /** Typedef to describe the output image pointer type. */
  typedef typename ImageOutType::Pointer             ImageOutPointer;
  typedef typename ImageOutType::ConstPointer        ImageOutConstPointer;
  /** Typedef to describe the output image region type. */
  typedef typename ImageOutType::RegionType          ImageOutRegionType;
  /** Typedef to describe the type of pixel and point for output image. */
  typedef typename ImageOutType::PixelType           ImageOutPixelType;
  typedef typename ImageOutType::PointType           ImageOutPointType;
  /** Typedef to describe the image index, size types and spacing for output image. */
  typedef typename ImageOutType::IndexType           ImageOutIndexType;
  typedef typename ImageOutType::IndexValueType      ImageOutIndexValueType;
  typedef typename ImageOutType::SizeType            ImageOutSizeType;
  typedef typename ImageOutType::SizeValueType       ImageOutSizeValueType;
  typedef typename ImageOutType::SpacingType         ImageOutSpacingType;
  typedef typename ImageOutType::SpacingValueType    ImageOutSpacingValueType;

  /** Typedef to optionnal image output type : otb::Image or otb::VectorImage. This output is a vector 
   * with dimensions 1 x nbLineOfMainOutput */
  typedef TImageOut                            ImageOptionnalType;
  /** Typedef to describe the optionnal output image pointer type. */
  typedef typename ImageOptionnalType::Pointer             ImageOptionnalPointer;
  typedef typename ImageOptionnalType::ConstPointer        ImageOptionnalConstPointer;
  typedef typename ImageOptionnalType::RegionType          ImageOptionnalRegionType;
  typedef typename ImageOptionnalType::IndexType           ImageOptionnalIndexType;
  typedef typename ImageOptionnalType::SizeType            ImageOptionnalSizeType;


  /** Typedef to DEM image type : otb::Image with DEM geometry (only metadata)  */
  typedef TImageDEM                                  ImageDEMType;
  typedef typename ImageDEMType::Pointer             ImageDEMPointer;

  /** Typedef to SAR image type : otb::Image with SAR geometry (only metadata)  */
  typedef TImageSAR                                  ImageSARType;
  typedef typename ImageSARType::Pointer             ImageSARPointer;

  // Define Point2DType and Point3DType
  using Point2DType = itk::Point<double,2>;
  using Point3DType = itk::Point<double,3>;

  // Define RSTRansform For Localisation inverse (SAR to DEM)
  typedef otb::GenericRSTransform<double,2,2> RSTransformType2D;

  typedef TFunction                                  FunctionType;
  typedef typename FunctionType::Pointer             FunctionPointer;

  typedef itk::SimpleFastMutexLock         MutexType;

  // Typedef for iterators
  typedef itk::ImageScanlineConstIterator< ImageInType > InputIterator;
  typedef itk::ImageScanlineIterator< ImageOutType > OutputIterator;
  typedef itk::ImageRegionIterator< ImageOutType > OutputOptIterator;

  // Setter
  void SetSARImageKeyWorList(ImageKeywordlist sarImageKWL);
  void SetSARImagePtr(ImageSARPointer sarPtr);
  void SetDEMImagePtr(ImageDEMPointer demPtr);
  void SetDEMInformation(double gain, int DEMDirC, int DEMDirL);
  void SetFunctorPtr(FunctionPointer functor);
  itkSetMacro(NoData, int);
 // Getter
  itkGetConstMacro(Gain, double);
  itkGetMacro(NoData, int);

  // Initialize margin and GenericRSTransform
  void initializeMarginAndRSTransform();

  /** Returns the const image */
  const TImageOut * GetOutput() const;
  /** Returns the image */
  TImageOut * GetOutput();
  /** Returns the const optionnal output image */
  //const ImageOptionnalType * GetImageOptionnalTypeOutput() const;
  /** Returns the optionnal output image */
  ImageOptionnalType * GetOptionnalOutput();
 
  
protected:
  // Constructor
  SARDEMPolygonsAnalysisImageFilter();

  // Destructor
  virtual ~SARDEMPolygonsAnalysisImageFilter() ITK_OVERRIDE {};

  // Print
  void PrintSelf(std::ostream & os, itk::Indent indent) const ITK_OVERRIDE;
  
  /** 
   * SARDEMPolygonsAnalysisImageFilter produces an image which are into SAR geometry or ML geometry. 
   * The differences between output and input images are the size of images and the dimensions. The input image 
   * are a otb::VectorImage and the output can be a classic image or a vector image.
   * As such, SARDEMPolygonsAnalysisImageFilter needs to provide an implementation for 
   * GenerateOutputInformation() in order to inform the pipeline execution model. 
   */ 
  virtual void GenerateOutputInformation() ITK_OVERRIDE;

  /**
   * SARDEMPolygonsAnalysisImageFilter needs a input requested region that corresponds to the projection 
   * into our main output requested region.  
   * As such, SARQuadraticAveragingImageFilter needs to provide an implementation for 
   * GenerateInputRequestedRegion() in order to inform the pipeline execution model. 
   * \sa ProcessObject::GenerateInputRequestedRegion() */
  virtual void GenerateInputRequestedRegion() ITK_OVERRIDE;

  /**
   * OutputRegionToInputRegion returns the input region. This input region corresponds to the outputRegion.
   */
  ImageInRegionType OutputRegionToInputRegion(const ImageOutRegionType& outputRegion) const;

  /**
   * SARDEMPolygonsAnalysisImageFilter can produce an optionnal image according to the functor. The requested 
   * region for this optionnal output is set to the largest possible region.
   */
  void EnlargeOutputRequestedRegion( itk::DataObject *output ) ITK_OVERRIDE;

  /**
   * SARDEMPolygonsAnalysisImageFilter can produce an optionnal image according to the functor. An allocation of
   * this optionnal output is made into BeforeThreadedGenerateData.
   */
  void BeforeThreadedGenerateData() ITK_OVERRIDE;


  /** 
   * SARDEMPolygonsAnalysisImageFilter can be implemented as a multithreaded filter.
   * Therefore, this implementation provides a ThreadedGenerateData() routine
   * which is called for each processing thread. The main output image data is
   * allocated automatically by the superclass prior to calling
   * ThreadedGenerateData().  ThreadedGenerateData can only write to the
   * portion of the output image specified by the parameter
   * "outputRegionForThread"
   *
   * \sa ImageToImageFilter::ThreadedGenerateData(),
   *     ImageToImageFilter::GenerateData() */
  virtual void ThreadedGenerateData(const ImageOutRegionType& outputRegionForThread, itk::ThreadIdType threadId) ITK_OVERRIDE;


  /** 
   * Select for the current SAR Line (ind_LineSAR), the polygons that intersect the current SAR line. 
   * This selection is always made with the SAR Line (even in ML geometry for the main output).
   */
  bool PolygonAndSarLineIntersection(const int ind_LineSAR, const double ligUL, const double colUL, 
				     const double ligUR, const double colUR, 
				     const double ligLR, const double colLR, const double ligLL, 
				     const double colLL,
				     int & i_InSideUp, int & i_InSideDown,
				     int & i_OutSideUp, int & i_OutSideDown);

  /** 
   * Scan all selected polygons and apply on it, the choosen functor.
   */
  void PolygonsScan(const int ind_LineSAR,
		    std::vector<std::vector<ImageInPixelType> *> * CLZY_InSideUp_Vec, 
		    std::vector<std::vector<ImageInPixelType> *> * CLZY_InSideDown_Vec, 
		    std::vector<std::vector<ImageInPixelType> *> * CLZY_OutSideUp_Vec, 
		    std::vector<std::vector<ImageInPixelType> *> * CLZY_OutSideDown_Vec, 
		    int firstCol_into_outputRegion, 
		    int nbCol_into_outputRegion, ImageOutPixelType * outValueTab, int threadId);

 private:
  SARDEMPolygonsAnalysisImageFilter(const Self&); // purposely not implemented
  void operator=(const Self &); // purposely not 
  
  // Instance of SarSensorModelAdapter
  SarSensorModelAdapter::Pointer m_SarSensorModelAdapter; 

  // SAR Image (only metadata)
  ImageSARPointer m_SarImagePtr;

  // DEM Image (only metadata)
  ImageDEMPointer m_DemImagePtr;

  // SAR Image KeyWorldList
  ImageKeywordlist m_SarImageKwl;

  // Multiplying gain to obtain a mean radiometry at 100
  double m_Gain;

  // Directions to scan the DEM in function of SAR geometry (from near to far range)
  int m_DEMdirL;
  int m_DEMdirC;

  // Margin for polygon selection
  int m_Margin;

  // Value for NoDATA 
  int m_NoData;

  // Function to apply on DEM Polygon
  FunctionPointer m_FunctionOnPolygon;
  
  // Output Geometry (SAR or ML)
  std::string m_OutGeometry;
  // ML factor (for ML geometry)
  unsigned int m_MLRan;
  unsigned int m_MLAzi;
  
  // Sar dimensions
  int m_NbLinesSAR;
  int m_NbColSAR; 

  // Store optionnalImage
  ImageOutPixelType * m_OptionnalResults;
  int m_OutputCounter;

  // Mutex for optionnalImage
  MutexType * m_Mutex; 

  // RSTransform (for inverse localisation)
  RSTransformType2D::Pointer m_RSTransform; 
  
};

} // End namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSARDEMPolygonsAnalysisImageFilter.txx"
#endif



#endif
