/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARStreamingGridInformationImageFilter_txx
#define otbSARStreamingGridInformationImageFilter_txx
#include "otbSARStreamingGridInformationImageFilter.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageRegionIterator.h"
#include "itkProgressReporter.h"
#include "otbMacro.h"

#include <complex>

namespace otb
{

template<class TInputImage>
PersistentGridInformationImageFilter<TInputImage>
::PersistentGridInformationImageFilter()
 : m_ThreadSumRange(1),
   m_ThreadSumAzimut(1),
   m_ThreadMinRange(1),
   m_ThreadMinAzimut(1),
   m_ThreadMaxRange(1),
   m_ThreadMaxAzimut(1),
   m_ThreadCount(1),
   m_IgnoreInfiniteValues(true),
   m_IgnoreUserDefinedValue(false),
   m_Threshold(0.3),
   m_NbComponentForInput(1),
   m_EstimateMean(false)
{
  //this->itk::ProcessObject::SetNumberOfRequiredOutputs(3);

  // first output is a copy of the image, DataObject created by
  // superclass
  //
  // allocate the data objects for the outputs which are
  // just decorators around pixel types
  typename ValueObjectType::Pointer output2
    = static_cast<ValueObjectType*>(this->MakeOutput(1).GetPointer());
  this->itk::ProcessObject::SetNthOutput(2, output2.GetPointer());
  typename ValueObjectType::Pointer output3
    = static_cast<ValueObjectType*>(this->MakeOutput(1).GetPointer());
  this->itk::ProcessObject::SetNthOutput(3, output3.GetPointer());
  // allocate the data objects for the outputs which are
  // just decorators around long types
  typename LongObjectType::Pointer output1
    = static_cast<LongObjectType*>(this->MakeOutput(0).GetPointer());
  this->itk::ProcessObject::SetNthOutput(1, output1.GetPointer());

  // For Min/Max shifts
  typename ValueObjectType::Pointer output4
    = static_cast<ValueObjectType*>(this->MakeOutput(1).GetPointer());
  this->itk::ProcessObject::SetNthOutput(4, output4.GetPointer());
  typename ValueObjectType::Pointer output5
    = static_cast<ValueObjectType*>(this->MakeOutput(1).GetPointer());
  this->itk::ProcessObject::SetNthOutput(5, output5.GetPointer());
  typename ValueObjectType::Pointer output6
    = static_cast<ValueObjectType*>(this->MakeOutput(1).GetPointer());
  this->itk::ProcessObject::SetNthOutput(6, output6.GetPointer());
  typename ValueObjectType::Pointer output7
    = static_cast<ValueObjectType*>(this->MakeOutput(1).GetPointer());
  this->itk::ProcessObject::SetNthOutput(7, output7.GetPointer());

  // Initialisation
  this->GetCountOutput()->Set(static_cast<long>(0));
  this->GetSumRangeOutput()->Set(itk::NumericTraits<ValueType>::Zero);
  this->GetSumAzimutOutput()->Set(itk::NumericTraits<ValueType>::Zero);
  this->GetMinRangeOutput()->Set(10E20);
  this->GetMinAzimutOutput()->Set(10E20);
  this->GetMaxRangeOutput()->Set(-10E20);
  this->GetMaxAzimutOutput()->Set(-10E20);

  // Initiate the infinite ignored pixel counters
  m_IgnoredInfinitePixelCount= std::vector<unsigned long>(this->GetNumberOfThreads(), 0);
  m_IgnoredUserPixelCount= std::vector<unsigned int>(this->GetNumberOfThreads(), 0);

  this->Reset();
}

template<class TInputImage>
typename itk::DataObject::Pointer
PersistentGridInformationImageFilter<TInputImage>
::MakeOutput(DataObjectPointerArraySizeType output)
{
  switch (output)
    {
    case 0:
      return static_cast<itk::DataObject*>(LongObjectType::New().GetPointer());
      break;
    case 1:
      return static_cast<itk::DataObject*>(ValueObjectType::New().GetPointer());
      break;
    case 2:
      return static_cast<itk::DataObject*>(PixelObjectType::New().GetPointer());
      break;
    default:
      // might as well make an image
      return static_cast<itk::DataObject*>(TInputImage::New().GetPointer());
      break;
    }
}


template<class TInputImage>
typename PersistentGridInformationImageFilter<TInputImage>::LongObjectType*
PersistentGridInformationImageFilter<TInputImage>
::GetCountOutput()
{
  return static_cast<LongObjectType*>(this->itk::ProcessObject::GetOutput(1));
}

template<class TInputImage>
const typename PersistentGridInformationImageFilter<TInputImage>::LongObjectType*
PersistentGridInformationImageFilter<TInputImage>
::GetCountOutput() const
{
  return static_cast<const LongObjectType*>(this->itk::ProcessObject::GetOutput(1));
}

template<class TInputImage>
typename PersistentGridInformationImageFilter<TInputImage>::ValueObjectType*
PersistentGridInformationImageFilter<TInputImage>
::GetSumRangeOutput()
{
  return static_cast<ValueObjectType*>(this->itk::ProcessObject::GetOutput(2));
}

template<class TInputImage>
const typename PersistentGridInformationImageFilter<TInputImage>::ValueObjectType*
PersistentGridInformationImageFilter<TInputImage>
::GetSumRangeOutput() const
{
  return static_cast<const ValueObjectType*>(this->itk::ProcessObject::GetOutput(2));
}

template<class TInputImage>
typename PersistentGridInformationImageFilter<TInputImage>::ValueObjectType*
PersistentGridInformationImageFilter<TInputImage>
::GetSumAzimutOutput()
{
  return static_cast<ValueObjectType*>(this->itk::ProcessObject::GetOutput(3));
}

template<class TInputImage>
const typename PersistentGridInformationImageFilter<TInputImage>::ValueObjectType*
PersistentGridInformationImageFilter<TInputImage>
::GetSumAzimutOutput() const
{
  return static_cast<const ValueObjectType*>(this->itk::ProcessObject::GetOutput(3));
}

template<class TInputImage>
typename PersistentGridInformationImageFilter<TInputImage>::ValueObjectType*
PersistentGridInformationImageFilter<TInputImage>
::GetMinRangeOutput()
{
  return static_cast<ValueObjectType*>(this->itk::ProcessObject::GetOutput(4));
}

template<class TInputImage>
const typename PersistentGridInformationImageFilter<TInputImage>::ValueObjectType*
PersistentGridInformationImageFilter<TInputImage>
::GetMinRangeOutput() const
{
  return static_cast<const ValueObjectType*>(this->itk::ProcessObject::GetOutput(4));
}

template<class TInputImage>
typename PersistentGridInformationImageFilter<TInputImage>::ValueObjectType*
PersistentGridInformationImageFilter<TInputImage>
::GetMinAzimutOutput()
{
  return static_cast<ValueObjectType*>(this->itk::ProcessObject::GetOutput(5));
}

template<class TInputImage>
const typename PersistentGridInformationImageFilter<TInputImage>::ValueObjectType*
PersistentGridInformationImageFilter<TInputImage>
::GetMinAzimutOutput() const
{
  return static_cast<const ValueObjectType*>(this->itk::ProcessObject::GetOutput(5));
}

template<class TInputImage>
typename PersistentGridInformationImageFilter<TInputImage>::ValueObjectType*
PersistentGridInformationImageFilter<TInputImage>
::GetMaxRangeOutput()
{
  return static_cast<ValueObjectType*>(this->itk::ProcessObject::GetOutput(6));
}

template<class TInputImage>
const typename PersistentGridInformationImageFilter<TInputImage>::ValueObjectType*
PersistentGridInformationImageFilter<TInputImage>
::GetMaxRangeOutput() const
{
  return static_cast<const ValueObjectType*>(this->itk::ProcessObject::GetOutput(6));
}

template<class TInputImage>
typename PersistentGridInformationImageFilter<TInputImage>::ValueObjectType*
PersistentGridInformationImageFilter<TInputImage>
::GetMaxAzimutOutput()
{
  return static_cast<ValueObjectType*>(this->itk::ProcessObject::GetOutput(7));
}

template<class TInputImage>
const typename PersistentGridInformationImageFilter<TInputImage>::ValueObjectType*
PersistentGridInformationImageFilter<TInputImage>
::GetMaxAzimutOutput() const
{
  return static_cast<const ValueObjectType*>(this->itk::ProcessObject::GetOutput(7));
}


template<class TInputImage>
void
PersistentGridInformationImageFilter<TInputImage>
::GenerateOutputInformation()
{
  Superclass::GenerateOutputInformation();
  if (this->GetInput())
    {
      this->GetOutput()->CopyInformation(this->GetInput());
      this->GetOutput()->SetLargestPossibleRegion(this->GetInput()->GetLargestPossibleRegion());

      if (this->GetOutput()->GetRequestedRegion().GetNumberOfPixels() == 0)
	{
	  this->GetOutput()->SetRequestedRegion(this->GetOutput()->GetLargestPossibleRegion());
	}

      m_NbComponentForInput = this->GetInput()->GetNumberOfComponentsPerPixel();
    }
}
template<class TInputImage>
void
PersistentGridInformationImageFilter<TInputImage>
::AllocateOutputs()
{
  // This is commented to prevent the streaming of the whole image for the first stream strip
  // It shall not cause any problem because the output image of this filter is not intended to be used.
  //InputImagePointer image = const_cast< TInputImage * >( this->GetInput() );
  //this->GraftOutput( image );
  // Nothing that needs to be allocated for the remaining outputs
}

template<class TInputImage>
void
PersistentGridInformationImageFilter<TInputImage>
::Synthetize()
{
  int numberOfThreads = this->GetNumberOfThreads();
  
  long     count;
  ValueType sumRange, sumAzimut;
  
  sumRange = sumAzimut = itk::NumericTraits<ValueType>::Zero;
  count = 0;

  ValueType minRange, minAzimut, maxRange, maxAzimut;
  minRange = this->GetMinRange();
  minAzimut = this->GetMinAzimut();
  maxRange = this->GetMaxRange();
  maxAzimut = this->GetMaxAzimut();

  // For each threads, get information into our Thread arrays
  for (int i = 0; i < numberOfThreads; ++i)
    {
      count += m_ThreadCount[i];
      sumRange += m_ThreadSumRange[i];
      sumAzimut += m_ThreadSumAzimut[i];

      // Conditions for min/max
      if (minRange > m_ThreadMinRange[i])
	{
	  minRange = m_ThreadMinRange[i];
	}
      if (minAzimut > m_ThreadMinAzimut[i])
	{
	  minAzimut = m_ThreadMinAzimut[i];
	}
      if (maxRange < m_ThreadMaxRange[i])
	{
	  maxRange = m_ThreadMaxRange[i];
	}
      if (maxAzimut < m_ThreadMaxAzimut[i])
	{
	  maxAzimut = m_ThreadMaxAzimut[i];
	}
    }
 

  // Set the outputs
  this->GetCountOutput()->Set(count);
  this->GetSumRangeOutput()->Set(sumRange);
  this->GetSumAzimutOutput()->Set(sumAzimut);
  this->GetMinRangeOutput()->Set(minRange);
  this->GetMinAzimutOutput()->Set(minAzimut);
  this->GetMaxRangeOutput()->Set(maxRange);
  this->GetMaxAzimutOutput()->Set(maxAzimut);
}

template<class TInputImage>
void
PersistentGridInformationImageFilter<TInputImage>
::Reset()
{
  int numberOfThreads = this->GetNumberOfThreads();

  // Resize the thread temporaries
  m_ThreadCount.SetSize(numberOfThreads);
  m_ThreadSumRange.SetSize(numberOfThreads);
  m_ThreadSumAzimut.SetSize(numberOfThreads);
  m_ThreadMinRange.SetSize(numberOfThreads);
  m_ThreadMinAzimut.SetSize(numberOfThreads);
  m_ThreadMaxRange.SetSize(numberOfThreads);
  m_ThreadMaxAzimut.SetSize(numberOfThreads);
  // Initialize the temporaries
  m_ThreadCount.Fill(itk::NumericTraits<long>::Zero);
  m_ThreadSumRange.Fill(itk::NumericTraits<ValueType>::Zero);
  m_ThreadSumAzimut.Fill(itk::NumericTraits<ValueType>::Zero);
  m_ThreadMinRange.Fill(10E20);
  m_ThreadMinAzimut.Fill(10E20);
  m_ThreadMaxRange.Fill(-10E20);
  m_ThreadMaxAzimut.Fill(-10E20);

  if (m_IgnoreInfiniteValues)
    {
      m_IgnoredInfinitePixelCount= std::vector<unsigned long>(numberOfThreads, 0);
    }

  if (m_IgnoreUserDefinedValue)
    {
      m_IgnoredUserPixelCount= std::vector<unsigned int>(this->GetNumberOfThreads(), 0);
    }
}



/** 
 * Method OutputRegionToInputRegion for GenerateInputRequestedRegion
 */
template<class TInputImage >
typename PersistentGridInformationImageFilter< TInputImage >::RegionType 
PersistentGridInformationImageFilter< TInputImage >
::OutputRegionToInputRegion(const RegionType& outputRegion) const
{
  // Compute the input requested region (size and start index)
  // Use the image transformations to insure an input requested region
  // that will provide the proper range
  const SizeType & outputRequestedRegionSize = outputRegion.GetSize();
  const IndexType & outputRequestedRegionIndex = outputRegion.GetIndex();

  // Compute input index 
  IndexType  inputRequestedRegionIndex = outputRequestedRegionIndex;

  // Compute input size
  SizeType inputRequestedRegionSize = outputRequestedRegionSize; 
 
  // Input region
  RegionType inputRequestedRegion = outputRegion;
  inputRequestedRegion.SetIndex(inputRequestedRegionIndex);
  inputRequestedRegion.SetSize(inputRequestedRegionSize);

  return inputRequestedRegion;  
}
/** 
 * Method OutputRegionToInputRegion
 */
template<class TInputImage>
void
PersistentGridInformationImageFilter< TInputImage >
::GenerateInputRequestedRegion()
{
  RegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();
  RegionType inputRequestedRegion = OutputRegionToInputRegion(outputRequestedRegion);

  InputImagePointer  inputPtr = const_cast< ImageType * >( this->GetInput() );
  
  inputPtr->SetRequestedRegion(inputRequestedRegion);
}



template<class TInputImage>
void
PersistentGridInformationImageFilter<TInputImage>
::ThreadedGenerateData(const RegionType& outputRegionForThread,
                       itk::ThreadIdType threadId)
{ 
  // Grab the input
  InputImagePointer inputPtr =  const_cast<TInputImage *>(this->GetInput(0));
  // Compute corresponding input region
  RegionType inputRegionForThread = OutputRegionToInputRegion(outputRegionForThread);

  // support progress methods/callbacks
  itk::ProgressReporter progress(this, threadId, outputRegionForThread.GetNumberOfPixels());

  //  Define/declare an iterator that will walk the input region for this
  // thread. 
  typedef itk::ImageScanlineConstIterator< ImageType > InputIterator;
  InputIterator  inIt(this->GetInput(), inputRegionForThread);
  
  inIt.GoToBegin();
  
  ValueType valueCurrentRange, valueCurrentAzimut;
 
  // For each line 
  while ( !inIt.IsAtEnd())
  {
    inIt.GoToBeginOfLine();

    // For each column
    while (!inIt.IsAtEndOfLine())
    {
      //The input grid is a vector image composed of three values :
      // [0] : range deformations 
      // [1] : azimut deformations
      // [2] : correlation rate (for certain grid only => m_NbComponentForInput > 2).
      valueCurrentRange = inIt.Get()[0];
      valueCurrentAzimut = inIt.Get()[1];
            
      bool valueCurrentRangeIsFinite = (vnl_math_isfinite(valueCurrentRange));
      bool valueCurrentAzimutIsFinite = (vnl_math_isfinite(valueCurrentAzimut));
      
      // Assigne if finite
       if (valueCurrentRangeIsFinite && valueCurrentAzimutIsFinite)
	 {
	   // Compare correlation rate with the threshold
	   if (m_EstimateMean)
	     {
	       if (m_NbComponentForInput > 2)
		 {
		   if (inIt.Get()[2] >= m_Threshold)
		     {
		       m_ThreadSumRange[threadId] += valueCurrentRange;
		       m_ThreadSumAzimut[threadId] += valueCurrentAzimut;
		       ++m_ThreadCount[threadId];
		     }
		 }
	       else
		 {
		   m_ThreadSumRange[threadId] += valueCurrentRange;
		   m_ThreadSumAzimut[threadId] += valueCurrentAzimut;
		   ++m_ThreadCount[threadId];
		 }
	     }

	   // Conditions for min/max
	   if (m_ThreadMinRange[threadId] > valueCurrentRange)
	     {
	       m_ThreadMinRange[threadId] = valueCurrentRange;
	     }
	   if (m_ThreadMinAzimut[threadId] > valueCurrentAzimut)
	     {
	       m_ThreadMinAzimut[threadId] = valueCurrentAzimut;
	     }
	   if (m_ThreadMaxRange[threadId] < valueCurrentRange)
	     {
	       m_ThreadMaxRange[threadId] = valueCurrentRange;
	     }
	   if (m_ThreadMaxAzimut[threadId] < valueCurrentAzimut)
	     {
	       m_ThreadMaxAzimut[threadId] = valueCurrentAzimut;
	     }
	 }
       
     
       // Next colunm
       ++inIt;
    }
     // Next Line
    inIt.NextLine();
    
  }

}

template <class TInputImage>
void
PersistentGridInformationImageFilter<TInputImage>
::PrintSelf(std::ostream& os, itk::Indent indent) const
{
  Superclass::PrintSelf(os, indent);

  os << indent << "Sum Range: "
     << static_cast<typename itk::NumericTraits<ValueType>::PrintType>(this->GetSumRange()) << std::endl;
  os << indent << "Sum Azimut: "
   << static_cast<typename itk::NumericTraits<ValueType>::PrintType>(this->GetSumAzimut()) << std::endl;
  os << indent << "Count: "      << this->GetCount() << std::endl;
}
} // end namespace otb
#endif
