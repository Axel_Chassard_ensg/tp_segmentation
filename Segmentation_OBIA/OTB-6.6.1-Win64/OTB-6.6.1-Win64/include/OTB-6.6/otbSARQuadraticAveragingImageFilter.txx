/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARQuadraticAveragingImageFilter_txx
#define otbSARQuadraticAveragingImageFilter_txx

#include "otbSARQuadraticAveragingImageFilter.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkProgressReporter.h"
#include "itkNumericTraitsPointPixel.h"
#include "otbImageKeywordlist.h"

#include <cmath>

namespace otb
{
/** 
 * Constructor with default initialization
 */
template <class TImage> 
SARQuadraticAveragingImageFilter<TImage>::SARQuadraticAveragingImageFilter()
  : m_AveragingFactor(1), m_Direction(DirectionType::AZIMUTH)
{}

/**
 * Print
 */
template<class TImage>
void
SARQuadraticAveragingImageFilter< TImage >
::PrintSelf(std::ostream & os, itk::Indent indent) const
{
  Superclass::PrintSelf(os, indent);

  std::string direction = (m_Direction == DirectionType::AZIMUTH) ? "Azimuth" : "Range";
  os << indent << "Averaging factor : " << m_AveragingFactor << std::endl;
  os << indent << "Direction : " << direction << std::endl;
}

/**
 * Set averaging factor
 */ 
template<class TImage>
void
SARQuadraticAveragingImageFilter< TImage >
::SetAveragingFactor(unsigned int factor)
{
  // Check if factor is greater than 0
  assert(factor > 0 && "averaging factor must be greater than 0.");
  m_AveragingFactor = factor;
}

/**
 * Method GenerateOutputInformaton()
 *         */
template<class TImage>
void
SARQuadraticAveragingImageFilter< TImage >
::GenerateOutputInformation()
{
  // Call the superclass' implementation of this method
  Superclass::GenerateInputRequestedRegion();
  
  // Get pointers to the input and output
  ImageConstPointer inputPtr = this->GetInput();
  ImagePointer     outputPtr = this->GetOutput();
  
  // Compute axis
  unsigned int transformedAxis = (m_Direction == DirectionType::AZIMUTH) ? 1 : 0;
  unsigned int untransformedAxis = (m_Direction == DirectionType::AZIMUTH) ? 0 : 1;

  // Compute the output image size
  const ImageSizeType &   inputSize = inputPtr->GetLargestPossibleRegion().GetSize();
  ImageSizeType outputSize;
  outputSize[untransformedAxis] = inputSize[untransformedAxis];
  outputSize[transformedAxis] = std::max<ImageSizeValueType>(static_cast<ImageSizeValueType>(
   std::floor( (double) inputSize[transformedAxis]/ (double) m_AveragingFactor)),1);

  // Compute origin (See if origin changes)
  ImagePointType origin = this->GetInput()->GetOrigin();

  // Compute Spacing
  const ImageSpacingType & inSP = this->GetInput()->GetSpacing();
  ImageSpacingType outSP;
  outSP[untransformedAxis] = inSP[untransformedAxis];
  outSP[transformedAxis] = static_cast<ImageSpacingValueType>(inSP[transformedAxis] * m_AveragingFactor);

  // Set region
  ImageRegionType outputLargestPossibleRegion = inputPtr->GetLargestPossibleRegion();
  outputLargestPossibleRegion.SetSize(outputSize);
  outputPtr->SetLargestPossibleRegion(outputLargestPossibleRegion);
  outputPtr->SetOrigin(origin);
  outputPtr->SetSpacing(outSP);

  // Override the geom file with MultiLook information
  // Retrieve input image keywordlist
  ImageKeywordlist inputKwl = inputPtr->GetImageKeywordlist();
  // According to direction, fill the corresponding information into geom file
  if (m_Direction == DirectionType::AZIMUTH)
    {
      inputKwl.AddKey("support_data.ml_azi", std::to_string(m_AveragingFactor));
      inputKwl.AddKey("support_data.ml_number_lines", std::to_string(outputSize[transformedAxis]));
    }
  else
    {
      inputKwl.AddKey("support_data.ml_ran", std::to_string(m_AveragingFactor));
      inputKwl.AddKey("support_data.ml_number_samples", std::to_string(outputSize[transformedAxis]));
    }
  // Set new keyword list to output image
   outputPtr->SetImageKeywordList(inputKwl);

}

/** 
 * Method OutputRegionToInputRegion for GenerateInputRequestedRegion
 */
template<class TImage >
typename SARQuadraticAveragingImageFilter< TImage >::ImageRegionType 
SARQuadraticAveragingImageFilter< TImage >
::OutputRegionToInputRegion(const ImageRegionType& outputRegion) const
{
  // Compute the input requested region (size and start index)
  // Use the image transformations to insure an input requested region
  // that will provide the proper range
  const ImageSizeType & outputRequestedRegionSize = outputRegion.GetSize();
  const ImageIndexType & outputRequestedRegionIndex = outputRegion.GetIndex();

  // Compute axis
  unsigned int transformedAxis = (m_Direction == DirectionType::AZIMUTH) ? 1 : 0;
  unsigned int untransformedAxis = (m_Direction == DirectionType::AZIMUTH) ? 0 : 1;

  // Compute input index 
  ImageIndexType  inputRequestedRegionIndex;
  inputRequestedRegionIndex[untransformedAxis] = outputRequestedRegionIndex[untransformedAxis];
  inputRequestedRegionIndex[transformedAxis] = static_cast<ImageIndexValueType>(
      outputRequestedRegionIndex[transformedAxis] * m_AveragingFactor );

  // Compute input size
  ImageSizeType inputRequestedRegionSize;
  inputRequestedRegionSize[untransformedAxis] = outputRequestedRegionSize[untransformedAxis];
  inputRequestedRegionSize[transformedAxis] = static_cast<ImageSizeValueType>(
      outputRequestedRegionSize[transformedAxis] * m_AveragingFactor ) ;
 
  // Input region
  ImageRegionType inputRequestedRegion = outputRegion;
  inputRequestedRegion.SetIndex(inputRequestedRegionIndex);
  inputRequestedRegion.SetSize(inputRequestedRegionSize);

  return inputRequestedRegion;  
}
/** 
 * Method OutputRegionToInputRegion
 */
template<class TImage>
void
SARQuadraticAveragingImageFilter< TImage >
::GenerateInputRequestedRegion()
{
  ImageRegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();
  ImageRegionType inputRequestedRegion = OutputRegionToInputRegion(outputRequestedRegion);

  ImagePointer  inputPtr = const_cast< ImageType * >( this->GetInput() );
  
  inputPtr->SetRequestedRegion(inputRequestedRegion);
}


/**
 * Method ThreadedGenerateData
 */
template<class TImage >
void
SARQuadraticAveragingImageFilter< TImage >
::ThreadedGenerateData(const ImageRegionType & outputRegionForThread,
                       itk::ThreadIdType threadId)
{
  if (m_Direction == DirectionType::AZIMUTH)
  {
    // Process for line
    this->ThreadedGenerateDataForLine(outputRegionForThread,threadId);
  } else
  {
    // Process for column
    this->ThreadedGenerateDataForColumn(outputRegionForThread,threadId);
  } 
}


/**
 * Method ThreadedGenerateDataForLine (direction Azimut)
 */
template<class TImage >
void
SARQuadraticAveragingImageFilter< TImage >
::ThreadedGenerateDataForLine(const ImageRegionType & outputRegionForThread,
                       itk::ThreadIdType threadId)
{
  // Compute corresponding input region
  ImageRegionType inputRegionForThread = OutputRegionToInputRegion(outputRegionForThread);
  
  //  Define/declare an iterator that will walk the output region for this
  // thread. 
  typedef itk::ImageScanlineConstIterator< ImageType > InputIterator;
  InputIterator  inIt(this->GetInput(), inputRegionForThread);

  // Define/declare an iterator that will walk the output region for this
  // thread.
  typedef itk::ImageScanlineIterator< ImageType > OutputIterator;
  OutputIterator outIt(this->GetOutput(), outputRegionForThread);

  // Support progress methods/callbacks
  itk::ProgressReporter progress( this, threadId, outputRegionForThread.GetNumberOfPixels() );

  inIt.GoToBegin();
  outIt.GoToBegin();

  // Get the number of column of the region
  unsigned int untransformedAxis = (m_Direction == DirectionType::AZIMUTH) ? 0 : 1;
  const ImageSizeType &   outputRegionForThreadSize = outputRegionForThread.GetSize();
  unsigned int nbCol = static_cast<unsigned int>(outputRegionForThreadSize[untransformedAxis]); 
  
  // Allocate outValueTab (size = nbCol)
  ImagePixelType *outValueTab = new ImagePixelType[nbCol];

 
  // For each Line
  while ( !inIt.IsAtEnd() && !outIt.IsAtEnd())
  {
    // reinitialize outValueTab
    for (unsigned int j = 0; j < nbCol; j++) 
      {
	outValueTab[j] = static_cast<ImagePixelType>(0);
      }


      // GenerateInputRequestedRegion function requires an input region with In_nbLine = m_Averaging * Out_nbLine
      for (unsigned int i =0 ; i < m_AveragingFactor; i++) 
	{
	  inIt.GoToBeginOfLine();
	  outIt.GoToBeginOfLine();
	  
	  int colCounter = 0;

	  // For each column
	  while (!inIt.IsAtEndOfLine() && !outIt.IsAtEndOfLine())
	    {
	      outValueTab[colCounter] += inIt.Get()*inIt.Get();
	      	      
	      // Assigne outIt
	      if (i == (m_AveragingFactor-1))
		{
		  ImagePixelType outV = outValueTab[colCounter]/(static_cast<ImagePixelType>(m_AveragingFactor));
		  outV = static_cast<ImagePixelType>(std::sqrt(outV));
		  
		  // Assigne output 
		  outIt.Set(outV);
		  progress.CompletedPixel();		  
		}

	      // Next colunm
	      ++inIt;
	      ++outIt;
	      colCounter++;
	    }
	  
	  inIt.NextLine();
	  
	}
      
      outIt.NextLine();
   
  }
  delete [] outValueTab;
}

/**
 * Method ThreadedGenerateDataForColumn
 */
template<class TImage >
void
SARQuadraticAveragingImageFilter< TImage >
::ThreadedGenerateDataForColumn(const ImageRegionType & outputRegionForThread,
                       itk::ThreadIdType threadId)
{
  // Compute corresponding input region
  ImageRegionType inputRegionForThread = OutputRegionToInputRegion(outputRegionForThread);
  
  //  Define/declare an iterator that will walk the output region for this
  // thread. 
  typedef itk::ImageScanlineConstIterator< ImageType > InputIterator;
  InputIterator  inIt(this->GetInput(), inputRegionForThread);

  // Define/declare an iterator that will walk the output region for this
  // thread.
  typedef itk::ImageScanlineIterator< ImageType > OutputIterator;
  OutputIterator outIt(this->GetOutput(), outputRegionForThread);

  // Support progress methods/callbacks
  itk::ProgressReporter progress( this, threadId, outputRegionForThread.GetNumberOfPixels() );
  
  inIt.GoToBegin();
  outIt.GoToBegin();

  // For each Line
  while ( !inIt.IsAtEnd() && !outIt.IsAtEnd())
  {
    inIt.GoToBeginOfLine();
    outIt.GoToBeginOfLine();

    // For each column
    while (!inIt.IsAtEndOfLine() && !outIt.IsAtEndOfLine())
    {
      ImagePixelType outValue = static_cast<ImagePixelType>(0);
      
      // GenerateInputRequestedRegion function requires an input region with In_nbColunm = m_Averaging * Out_nbColunm 
      for (unsigned int i =0 ; i < m_AveragingFactor; i++) 
	{
	  outValue += inIt.Get()*inIt.Get();
	  ++inIt;
	}
      
      // Quadraic Averaging 
      outValue /= static_cast<ImagePixelType>(m_AveragingFactor); 
      outValue = static_cast<ImagePixelType>(std::sqrt(outValue)); 

      // Assigne output image pixel
      outIt.Set(outValue);
      ++outIt;

      progress.CompletedPixel();
      
    }

    inIt.NextLine();
    outIt.NextLine();
    
  }
}
/**
 *  Check if input line is in output image
 */
template<class TImage >
bool
SARQuadraticAveragingImageFilter< TImage >
::IsInOutput(ImageIndexType inIndex)
{
  const unsigned int zero = 0;
  return ( (static_cast<unsigned int>(inIndex[static_cast<unsigned int>(m_Direction)]) % m_AveragingFactor) == zero);

}

} /*namespace otb*/

#endif
