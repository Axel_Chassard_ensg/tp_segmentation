/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARGainImageFilter_h
#define otbSARGainImageFilter_h

#include "itkUnaryFunctorImageFilter.h"
#include "otbImageKeywordlist.h"

#include <string>
#include <algorithm>

namespace otb
{
  
/** \class SARSGainImageFilter
 * \brief This is a pixel-wise filter that multiplies every
 * pixel in the input image by a user-provided number (ML_GAIN). 
 *
 * \ingroup DiapOTBModule
 */
namespace Functor {  
  
template< class TInput, class TOutput>
class Gain
{
public:
  Gain() {}
  ~Gain() {}
 
  inline TOutput operator()( const TInput & A )
    {
      TInput val = m_Gain * A + 0.5;
      val=std::min(val,static_cast<TInput>(255));
      val=std::max(val,static_cast<TInput>(0));
      return static_cast<TOutput>(val);
    }
  void SetGain( const double & gain )
    {
      assert(gain > 0. && "Gain factor must be greater than 0.");
      m_Gain = gain;
    }
private:
  //TInput m_Gain;
  double m_Gain;
}; 
}

template <class TInputImage, class TOutputImage>
class ITK_EXPORT SARGainImageFilter :
    public
itk::UnaryFunctorImageFilter<TInputImage,TOutputImage, 
           Functor::Gain< typename TInputImage::PixelType, 
                             typename TOutputImage::PixelType>   >
{
public:
  /** Standard class typedefs. */
  typedef SARGainImageFilter  Self;
  typedef itk::UnaryFunctorImageFilter<TInputImage,TOutputImage, 
                 Functor::Gain< typename TInputImage::PixelType, 
                                   typename TOutputImage::PixelType> > Superclass;
  typedef itk::SmartPointer<Self>   Pointer;
  typedef itk::SmartPointer<const Self>  ConstPointer;

  /** Typedef to image types */
  typedef TInputImage                           InImageType;
  typedef TOutputImage                          OutImageType;
  /** Typedef to describe the image pointer types. */
  typedef typename InImageType::Pointer         InImagePointer;
  typedef typename InImageType::ConstPointer    InImageConstPointer;
  typedef typename OutImageType::Pointer        OutImagePointer;
  typedef typename OutImageType::ConstPointer   OutImageConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);
  
  /** Explicit typedef for the FunctorType from the Superclass */
  typedef typename  Superclass::FunctorType    FunctorType;

  /** Get the scale type from the functor type */
  void SetGain( const double & gain )
    {
      m_GainFactor = gain;
      this->GetFunctor().SetGain( gain );
    }

protected:
  SARGainImageFilter() {}
  virtual ~SARGainImageFilter() {}
  
private:
  SARGainImageFilter(const Self&); //purposely not implemented
  void operator=(const Self&); //purposely not implemented

  double m_GainFactor;

};

} // end namespace otb


#endif
