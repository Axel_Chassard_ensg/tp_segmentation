/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARStreamingDEMInformationFilter_h
#define otbSARStreamingDEMInformationFilter_h

#include "otbPersistentImageFilter.h"
#include "itkNumericTraits.h"
#include "itkArray.h"
#include "itkSimpleDataObjectDecorator.h"
#include "itkPoint.h"

#include "otbPersistentFilterStreamingDecorator.h"

#include "otbSarSensorModelAdapter.h"
#include "otbImageKeywordlist.h"

#include <complex>
#include <cmath>

namespace otb
{

/** \class PersistentDEMInformationFilter
 * \brief Retrive some pixels from the input DEM
 *
 *  This filter persists its temporary data. It means that if you Update it n times on n different
 * requested regions, the output statistics will be the statitics of the whole set of n regions.
 *
 * To reset the temporary data, one should call the Reset() function.
 *
 * To get the wanted values once the regions have been processed via the pipeline, use the Synthetize() method.
 *
 * \ingroup DiapOTBModule
 */
template<class TInputImage>
class ITK_EXPORT PersistentDEMInformationFilter :
  public PersistentImageFilter<TInputImage, TInputImage>
{
public:
  /** Standard Self typedef */
  typedef PersistentDEMInformationFilter                 Self;
  typedef PersistentImageFilter<TInputImage, TInputImage> Superclass;
  typedef itk::SmartPointer<Self>                         Pointer;
  typedef itk::SmartPointer<const Self>                   ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Runtime information support. */
  itkTypeMacro(PersistentDEMInformationFilter, PersistentImageFilter);

  /** Image related typedefs. */
  typedef TInputImage                   ImageType;
  typedef typename TInputImage::Pointer InputImagePointer;

  typedef typename TInputImage::RegionType RegionType;
  typedef typename TInputImage::SizeType   SizeType;
  typedef typename TInputImage::IndexType  IndexType;
  typedef typename TInputImage::PixelType  PixelType;
  typedef typename ImageType::IndexValueType IndexValueType;
  typedef typename ImageType::SizeValueType  SizeValueType;

  itkStaticConstMacro(InputImageDimension, unsigned int,
                      TInputImage::ImageDimension);

  /** Image related typedefs. */
  itkStaticConstMacro(ImageDimension, unsigned int,
                      TInputImage::ImageDimension);

  /** Type to use for computations. */
  //  typedef typename itk::NumericTraits<PixelType>::RealType RealType;
  typedef typename itk::NumericTraits<double>::RealType RealType;
 
  /** Smart Pointer type to a DataObject. */
  typedef typename itk::DataObject::Pointer DataObjectPointer;
  typedef itk::ProcessObject::DataObjectPointerArraySizeType DataObjectPointerArraySizeType;

  /** Type of DataObjects used for scalar outputs */
  typedef itk::SimpleDataObjectDecorator<RealType>  RealObjectType;
  typedef itk::SimpleDataObjectDecorator<long>  LongObjectType;
  typedef itk::SimpleDataObjectDecorator<PixelType> PixelObjectType;

  /** Return the four sides of DEM to estiamte the gain. */
  PixelType GetSide_0_0() const
  {
    return this->GetSide_0_0_Output()->Get();
  }
  PixelObjectType* GetSide_0_0_Output();
  const PixelObjectType* GetSide_0_0_Output() const;

  PixelType GetSide_0_nbLines() const
  {
    return this->GetSide_0_nbLines_Output()->Get();
  }
  PixelObjectType* GetSide_0_nbLines_Output();
  const PixelObjectType* GetSide_0_nbLines_Output() const;
  
  PixelType GetSide_nbCol_0() const
  {
    return this->GetSide_nbCol_0_Output()->Get();
  }
  PixelObjectType* GetSide_nbCol_0_Output();
  const PixelObjectType* GetSide_nbCol_0_Output() const;

  PixelType GetSide_nbCol_nbLines() const
  {
    return this->GetSide_nbCol_nbLines_Output()->Get();
  }
  PixelObjectType* GetSide_nbCol_nbLines_Output();
  const PixelObjectType* GetSide_nbCol_nbLines_Output() const;

  /** Make a DataObject of the correct type to be used as the specified
   * output. */
  DataObjectPointer MakeOutput(DataObjectPointerArraySizeType idx) ITK_OVERRIDE;
  using Superclass::MakeOutput;

  /**
   * Synthetize and Reset function called by our PersistentFilterStreamingDecorator
   */
  void Synthetize(void) ITK_OVERRIDE;
  void Reset(void) ITK_OVERRIDE;

  itkSetMacro(UserIgnoredValue, RealType);
  itkGetMacro(UserIgnoredValue, RealType);

protected:
  PersistentDEMInformationFilter();
  ~PersistentDEMInformationFilter() ITK_OVERRIDE {}
  void PrintSelf(std::ostream& os, itk::Indent indent) const ITK_OVERRIDE;

  
  /** PersistentDEMInformationFilter needs a input requested region with the same size of
      the output requested region.
   * \sa ProcessObject::GenerateInputRequestedRegion() */
  virtual void GenerateInputRequestedRegion() ITK_OVERRIDE;

  /** Multi-thread version GenerateData. */
  void  ThreadedGenerateData(const RegionType&
                             outputRegionForThread,
                             itk::ThreadIdType threadId) ITK_OVERRIDE;

  /** Pass the input through unmodified. Do this by Grafting in the
   *  AllocateOutputs method.
   */
  void AllocateOutputs() ITK_OVERRIDE;
  void GenerateOutputInformation() ITK_OVERRIDE;

private:
  PersistentDEMInformationFilter(const Self &); //purposely not implemented
  void operator =(const Self&); //purposely not implemented

  itk::Array<PixelType>       m_Thread_DEMSide_0_0;
  itk::Array<PixelType>       m_Thread_DEMSide_0_nbLines;
  itk::Array<PixelType>       m_Thread_DEMSide_nbCol_0;
  itk::Array<PixelType>       m_Thread_DEMSide_nbCol_nbLines;

  /* Ignored values */
  RealType                   m_UserIgnoredValue;
  

}; // end of class PersistentDEMInformationFilter

/*===========================================================================*/

/** \class SARStreamingDEMInformationFilter
 * \brief This class streams the whole input image through the PersistentDEMInformationFilter.
 *
 * This way, it allows computing the first order global statistics of this image. It calls the
 * Reset() method of the PersistentDEMInformationFilter before streaming the image and the
 * Synthetize() method of the PersistentDEMInformationFilter after having streamed the image
 * to compute the statistics. The accessor on the results are wrapping the accessors of the
 * internal PersistentDEMInformationFilter.
 * By default infinite values are ignored, use IgnoreInfiniteValues accessor to consider
 * infinite values in the computation.
 *
 * This filter can be used as:
 * \code
 * typedef otb::StreamingDEMInformationFilter<ImageType> StatisticsType;
 * StatisticsType::Pointer statistics = StatisticsType::New();
 * statistics->SetInput(reader->GetOutput());
 * statistics->Update();
 * std::cout << statistics-> GetDEMInformation() << std::endl;
 * \endcode
 *
 * \sa PersistentDEMInformationFilter
 * \sa PersistentImageFilter
 * \sa PersistentFilterStreamingDecorator
 * \sa StreamingImageVirtualWriter
 * \ingroup Streamed
 * \ingroup Multithreaded
 * \ingroup MathematicalStatisticsImageFilters
 *
 * \ingroup DiapOTBModule
 */

template<class TInputImage>
class ITK_EXPORT SARStreamingDEMInformationFilter :
  public PersistentFilterStreamingDecorator<PersistentDEMInformationFilter<TInputImage> >
{
public:
  /** Standard Self typedef */
  typedef SARStreamingDEMInformationFilter Self;
  typedef PersistentFilterStreamingDecorator
  <PersistentDEMInformationFilter<TInputImage> > Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Type macro */
  itkNewMacro(Self);

  /** Creation through object factory macro */
  itkTypeMacro(StreamingDEMInformationFilter, PersistentFilterStreamingDecorator);

  typedef typename Superclass::FilterType    StatFilterType;
  typedef typename StatFilterType::PixelType PixelType;
  typedef typename StatFilterType::RealType  RealType;
  typedef TInputImage                        InputImageType;

  /** Type of DataObjects used for scalar outputs */
  typedef itk::SimpleDataObjectDecorator<RealType>  RealObjectType;
  typedef itk::SimpleDataObjectDecorator<long>  LongObjectType;
  typedef itk::SimpleDataObjectDecorator<PixelType> PixelObjectType;

  /** Image related typedefs. */
  typedef TInputImage                   ImageType;
  typedef typename TInputImage::Pointer InputImagePointer;

  typedef typename TInputImage::RegionType RegionType;
  typedef typename TInputImage::SizeType   SizeType;
  typedef typename TInputImage::IndexType  IndexType;

   // Define Point2DType and Point3DType
  using Point2DType = itk::Point<double,2>;
  using Point3DType = itk::Point<double,3>;

  using Superclass::SetInput;
  void SetInput(InputImageType * input)
  {
    this->GetFilter()->SetInput(input);
  }
  const InputImageType * GetInput()
  {
    return this->GetFilter()->GetInput();
  }

  
  /** Return the side. */
  PixelType GetSide_0_0() const
  {
    return this->GetFilter()->GetSide_0_0_Output()->Get();
  }
  PixelObjectType* GetSide_0_0_Output()
  {
    return this->GetFilter()->GetSide_0_0_Output();
  }
  const PixelObjectType* GetSide_0_0_Output() const
  {
    return this->GetFilter()->GetSide_0_0_Output();
  }

  PixelType GetSide_0_nbLines() const
  {
    return this->GetFilter()->GetSide_0_nbLines_Output()->Get();
  }
  PixelObjectType* GetSide_0_nbLines_Output()
  {
    return this->GetFilter()->GetSide_0_nbLines_Output();
  }
  const PixelObjectType* GetSide_0_nbLines_Output() const
  {
    return this->GetFilter()->GetSide_0_nbLines_Output();
  }

  PixelType GetSide_nbCol_0() const
  {
    return this->GetFilter()->GetSide_nbCol_0_Output()->Get();
  }
  PixelObjectType* GetSide_nbCol_0_Output()
  {
    return this->GetFilter()->GetSide_nbCol_0_Output();
  }
  const PixelObjectType* GetSide_nbCol_0_Output() const
  {
    return this->GetFilter()->GetSide_nbCol_0_Output();
  }

  PixelType GetSide_nbCol_nbLines() const
  {
    return this->GetFilter()->GetSide_nbCol_nbLines_Output()->Get();
  }
  PixelObjectType* GetSide_nbCol_nbLines_Output()
  {
    return this->GetFilter()->GetSide_nbCol_nbLines_Output();
  }
  const PixelObjectType* GetSide_nbCol_nbLines_Output() const
  {
    return this->GetFilter()->GetSide_nbCol_nbLines_Output();
  }
  
  // Get some information abour DEM (gain estimation, direction to scan the DEM in function of SAR geometry)
  void GetDEMInformation(double & gain, int & direction_toScan_DEMColunms,  int & direction_toScan_DEMLines)
  {
    double deltaC[4];
    double incidence[4];
  
    // Retrive the DEM dimensions for the side
    int nbColDEM =  this->GetInput()->GetLargestPossibleRegion().GetSize()[0];
    int nbLinesDEM =  this->GetInput()->GetLargestPossibleRegion().GetSize()[1];

    int nbColSAR = atoi(m_SarImageKwl.GetMetadataByKey("support_data.number_samples").c_str());

    // Create and Initilaze the SarSensorModelAdapter
    m_SarSensorModelAdapter = SarSensorModelAdapter::New();
    bool loadOk = m_SarSensorModelAdapter->LoadState(m_SarImageKwl);
  
    if(!loadOk || !m_SarSensorModelAdapter->IsValidSensorModel())
      {
	itkExceptionMacro(<<"SAR image does not contain a valid SAR sensor model.");
      }

    // 4 sides of DEM
    int ind_lines[4] = {0, nbLinesDEM-1, 0, nbLinesDEM-1};
    int ind_col[4] = {0, 0, nbColDEM-1, nbColDEM-1};

    PixelType pixel_side[4];
  
    // Fill the tab with selected values into the persistant filter
    pixel_side[0] = this->GetFilter()->GetSide_0_0_Output()->Get();
    pixel_side[1] = this->GetFilter()->GetSide_0_nbLines_Output()->Get();
    pixel_side[2] = this->GetFilter()->GetSide_nbCol_0_Output()->Get();
    pixel_side[3] = this->GetFilter()->GetSide_nbCol_nbLines_Output()->Get();
  

    Point2DType mntLatLonPoint(0);
    Point3DType mntGeoPoint(0);
    Point3DType MntPointCartesien(0);
    Point3DType SatPos(0);
    Point3DType SatVel(0);
    Point3DType R(0);
    Point2DType col_row(0);
    Point2DType y_z(0);
    
    double incidence_moy = 0.;
    double coef1 = 0.;
    double coef2 = 0.;

    IndexType idCurrent;
    
    std::map<std::string,int> DEMSidesOrder_Into_SARGeometry;
    double firstLine;
    double lastLine;
    double firstY;
    double lastY;

    // Loop on the four sides
    for (int k = 0; k < 4; k++)
      {
	idCurrent[0] = ind_col[k];
	idCurrent[1] = ind_lines[k];

	// Trnasform index to Lat/Lon Point
	this->GetInput()->TransformIndexToPhysicalPoint(idCurrent, mntLatLonPoint);
	mntGeoPoint[0] = mntLatLonPoint[0];
	mntGeoPoint[1] = mntLatLonPoint[1];
	mntGeoPoint[2] = pixel_side[k];

	// DeltaC (with colomn into SaR image)
	m_SarSensorModelAdapter->WorldToLineSampleYZ(mntGeoPoint, col_row, y_z);
	deltaC[k] = col_row[0]  - (nbColSAR/2);

	// Compare each side of DEM to find the orientation related to SAR image 
	if (k == 0)
	  {
	    firstLine = col_row[1];
	    lastLine = col_row[1];
	    firstY = y_z[0];
	    lastY = y_z[0];
	    DEMSidesOrder_Into_SARGeometry["SideOf_L0"] = 0;
	    DEMSidesOrder_Into_SARGeometry["SideOf_Llast"] = 0;
	    DEMSidesOrder_Into_SARGeometry["SideOf_NR"] = 0;
	    DEMSidesOrder_Into_SARGeometry["SideOf_FR"] = 0;
	  }
	else
	  {
	    if (col_row[1] < firstLine)
	      {
		firstLine = col_row[1];
		DEMSidesOrder_Into_SARGeometry["SideOf_L0"] = k;
	      }
	     if (col_row[1] > lastLine)
	      {
		lastLine = col_row[1];
		DEMSidesOrder_Into_SARGeometry["SideOf_Llast"] = k;
	      }
	     // The Near and Far side is defined by Y (Y is calculated in function of the sensor and can 
	     // change of sign with the side looking) 
	     if (y_z[0] < firstY)
	      {
		firstY = y_z[0];
		DEMSidesOrder_Into_SARGeometry["SideOf_NR"] = k;
	      }
	     if (y_z[0] > lastY)
	      {
		lastY = y_z[0];
		DEMSidesOrder_Into_SARGeometry["SideOf_FR"] = k;
	      }
	  }

	// Satelite Position
	m_SarSensorModelAdapter->WorldToSatPositionAndVelocity(mntGeoPoint, SatPos, SatVel); 

	// Cartesian conversion
	otb::SarSensorModelAdapter::WorldToCartesian(mntGeoPoint, MntPointCartesien);

	// Incidence 
	R[0] = MntPointCartesien[0] - SatPos[0];
	R[1] = MntPointCartesien[1] - SatPos[1];
	R[2] = MntPointCartesien[2] - SatPos[2];
      
	double NormeS = sqrt(SatPos[0]*SatPos[0] + SatPos[1]*SatPos[1] + SatPos[2]*SatPos[2]);
	double NormeCible = sqrt(MntPointCartesien[0]*MntPointCartesien[0] + 
				 MntPointCartesien[1]*MntPointCartesien[1] + 
				 MntPointCartesien[2]*MntPointCartesien[2]);
	double  NormeR = sqrt(R[0]*R[0] + R[1]*R[1] + R[2]*R[2]);
      
	incidence[k] = acos((NormeS*NormeS - NormeR*NormeR - NormeCible *NormeCible) / 
			    (2 * NormeCible * NormeR) ) * 180. / M_PI;
      }

    // Define the direction into DEM geometry in order to across SAR geometry from Near range to Far range
    int NR_side,  FR_side;
    NR_side = DEMSidesOrder_Into_SARGeometry["SideOf_NR"];
    FR_side = DEMSidesOrder_Into_SARGeometry["SideOf_FR"];

    direction_toScan_DEMColunms = (ind_col[FR_side] > ind_col[NR_side]);
    direction_toScan_DEMLines = (ind_lines[FR_side] > ind_lines[NR_side]); 
        
    if (direction_toScan_DEMColunms == 0) 
      {
	direction_toScan_DEMColunms = -1;
      }
    
    if (direction_toScan_DEMLines == 0) 
      {
	direction_toScan_DEMLines = -1;
      }
    
    // Fit function
    this->fit2(deltaC, incidence, 4, &incidence_moy, &coef1, &coef2);

    // Gain Estimation 
   gain = 100.0 * tan(incidence_moy * M_PI / 180.);
  }


  void SetSARImageKeyWorList(ImageKeywordlist sarImageKWL)
  {
    // Check if sarImageKWL not NULL
    assert(&sarImageKWL && "SAR Image Metadata don't exist.");
    m_SarImageKwl = sarImageKWL;
  }

  otbSetObjectMemberMacro(Filter, UserIgnoredValue, RealType);
  otbGetObjectMemberMacro(Filter, UserIgnoredValue, RealType);

protected:
  /** Constructor */
  SARStreamingDEMInformationFilter() {};
  /** Destructor */
  ~SARStreamingDEMInformationFilter() ITK_OVERRIDE {}

private:
  SARStreamingDEMInformationFilter(const Self &); //purposely not implemented
  void operator =(const Self&); //purposely not implemented

  // Function fit with a second polynome
  void fit2(double * x,double * y, int n, double * a, double * b, double * c)
  {
    double	x1,x2,y1;
    double	s1,s2,s3,s4,r1,r2,r3,c1,c2,c3,c4,c5;
    int i;
	
    *a=0;
    *b=0;
    *c=0;
    s1=0;
    s2=0;
    s3=0;
    s4=0;
    r1=0;
    r2=0;
    r3=0;
	
    for (i=0; i<n; i++)
      {
	x1=x[i];
	y1=y[i];
	s1=s1+x1;
	x2=x1*x1;
	s2=s2+x2;
        s3=s3+x2*x1;
        s4=s4+x2*x2;
        r1=r1+y1;
        r2=r2+y1*x1;
        r3=r3+y1*x2;
      }
	
    if (s4 ==0)
      return;
	
    c1=r1-r3*s2/s4;
    c2=s2-s3*s3/s4;
    c3=s1-s2*s3/s4;
    c4=r2-s3*r3/s4;
    c5=n-s2*s2/s4;
    c5=c5*c2-c3*c3;
    *a = (c1 * c2 - c3 * c4) / c5;
    
    if (c2 == 0)
      {
	return;
      }
	
    *b = (c4 - (*a) * c3) / c2;
    *c = (r3 - (*b) * s3 - (*a) * s2) / s4;
    
  }

  // Instance of SarSensorModelAdapter
  SarSensorModelAdapter::Pointer m_SarSensorModelAdapter; 

  // SAR Image KeyWorldList
  ImageKeywordlist m_SarImageKwl;
  
};

} // end namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSARStreamingDEMInformationFilter.txx"
#endif

#endif
