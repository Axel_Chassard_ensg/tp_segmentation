
#ifndef OTBImageIO_EXPORT_H
#define OTBImageIO_EXPORT_H

#ifdef OTB_STATIC
#  define OTBImageIO_EXPORT
#  define OTBImageIO_HIDDEN
#else
#  ifndef OTBImageIO_EXPORT
#    ifdef OTBImageIO_EXPORTS
        /* We are building this library */
#      define OTBImageIO_EXPORT __declspec(dllexport)
#    else
        /* We are using this library */
#      define OTBImageIO_EXPORT __declspec(dllimport)
#    endif
#  endif

#  ifndef OTBImageIO_HIDDEN
#    define OTBImageIO_HIDDEN 
#  endif
#endif

#ifndef OTBIMAGEIO_DEPRECATED
#  define OTBIMAGEIO_DEPRECATED __declspec(deprecated)
#endif

#ifndef OTBIMAGEIO_DEPRECATED_EXPORT
#  define OTBIMAGEIO_DEPRECATED_EXPORT OTBImageIO_EXPORT OTBIMAGEIO_DEPRECATED
#endif

#ifndef OTBIMAGEIO_DEPRECATED_NO_EXPORT
#  define OTBIMAGEIO_DEPRECATED_NO_EXPORT OTBImageIO_HIDDEN OTBIMAGEIO_DEPRECATED
#endif

#define DEFINE_NO_DEPRECATED 0
#if DEFINE_NO_DEPRECATED
# define OTBIMAGEIO_NO_DEPRECATED
#endif

#endif
