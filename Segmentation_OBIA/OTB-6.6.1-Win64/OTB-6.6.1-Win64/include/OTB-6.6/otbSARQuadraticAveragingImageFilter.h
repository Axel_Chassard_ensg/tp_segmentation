/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARQuadraticAveragingImageFilter_h
#define otbSARQuadraticAveragingImageFilter_h

#include "itkImageToImageFilter.h"
#include "itkSmartPointer.h"

namespace otb
{
/** \class SARQuadraticAveragingImageFilter 
 * \brief Performs a quadratic averaging on a direction (RANGE or AZIMUT)
 * 
 * This filter performs a a quadratic averaging.
 * 
 * \ingroup DiapOTBModule
 */

template <typename TImage> class ITK_EXPORT SARQuadraticAveragingImageFilter :
    public itk::ImageToImageFilter<TImage,TImage>
{
public:
  // Direction
  enum class DirectionType {RANGE=0,AZIMUTH=1};

  // Standard class typedefs
  typedef SARQuadraticAveragingImageFilter              Self;
  typedef itk::ImageToImageFilter<TImage,TImage>        Superclass;
  typedef itk::SmartPointer<Self>                       Pointer;
  typedef itk::SmartPointer<const Self>                 ConstPointer;

  // Method for creation through object factory
  itkNewMacro(Self);
  // Run-time type information
  itkTypeMacro(SARDQuadraticAveragingFilter,ImageToImageFilter);

  /** Typedef to image type */
  typedef TImage                             ImageType;
  /** Typedef to describe the image pointer type. */
  typedef typename ImageType::Pointer        ImagePointer;
  typedef typename ImageType::ConstPointer   ImageConstPointer;
  /** Typedef to describe the image region type. */
  typedef typename ImageType::RegionType     ImageRegionType;
  /** Typedef to describe the type of pixel and point. */
  typedef typename ImageType::PixelType      ImagePixelType;
  typedef typename ImageType::PointType      ImagePointType;
  /** Typedef to describe the image index, size types and spacing. */
  typedef typename ImageType::IndexType      ImageIndexType;
  typedef typename ImageType::IndexValueType ImageIndexValueType;
  typedef typename ImageType::SizeType       ImageSizeType;
  typedef typename ImageType::SizeValueType  ImageSizeValueType;
  typedef typename ImageType::SpacingType    ImageSpacingType;
  typedef typename ImageType::SpacingValueType    ImageSpacingValueType;
  
  // Setter
  void SetAveragingFactor(unsigned int factor);
  itkSetEnumMacro(Direction, DirectionType);
  // Getter
  itkGetConstMacro(AveragingFactor, unsigned int);
  itkGetEnumMacro(Direction, DirectionType);
  
  
protected:
  // Constructor
  SARQuadraticAveragingImageFilter();

  // Destructor
  virtual ~SARQuadraticAveragingImageFilter() ITK_OVERRIDE {};

  // Print
  void PrintSelf(std::ostream & os, itk::Indent indent) const ITK_OVERRIDE;
  
  /** SARQuadraticAveragingImageFilter produces an image which is different size
   * size than its input image.
   * As such, SARQuadraticAveragingImageFilter needs to provide
   * an implementation for GenerateOutputInformation() in order to
   * inform the pipeline execution model. 
   */ 
  virtual void GenerateOutputInformation() ITK_OVERRIDE;

  /** SARQuadraticAveragingImageFilter needs a larger input requested region than the output
   * requested region.  As such, SARQuadraticAveragingImageFilter needs to provide an
   * implementation for GenerateInputRequestedRegion() in order to inform the
   * pipeline execution model.
   * \sa ProcessObject::GenerateInputRequestedRegion() */
  virtual void GenerateInputRequestedRegion() ITK_OVERRIDE;

  /**
   * OutputRegionToInputRegion returns the input region 
   */
  ImageRegionType OutputRegionToInputRegion(const ImageRegionType& outputRegion) const;

  /** SARQuadraticAveragingImageFilter can be implemented as a multithreaded filter.
   * Therefore, this implementation provides a ThreadedGenerateData() routine
   * which is called for each processing thread. The output image data is
   * allocated automatically by the superclass prior to calling
   * ThreadedGenerateData().  ThreadedGenerateData can only write to the
   * portion of the output image specified by the parameter
   * "outputRegionForThread"
   *
   * \sa ImageToImageFilter::ThreadedGenerateData(),
   *     ImageToImageFilter::GenerateData() */
  virtual void ThreadedGenerateData(const ImageRegionType& outputRegionForThread, itk::ThreadIdType threadId) ITK_OVERRIDE;
  void ThreadedGenerateDataForLine(const ImageRegionType& outputRegionForThread, itk::ThreadIdType threadId);
  void ThreadedGenerateDataForColumn(const ImageRegionType& outputRegionForThread, itk::ThreadIdType threadId);

 private:
  SARQuadraticAveragingImageFilter(const Self&); // purposely not implemented
  void operator=(const Self &); // purposely not 
  /**
   * The method IsInOutput returns true if the input line is in the 
   * output image and false if not.
   */
  bool IsInOutput(ImageIndexType outIndex);
  
  // Averaging factor
  unsigned int m_AveragingFactor;
  // Direction for decimation
  DirectionType m_Direction;
  
};

} // End namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSARQuadraticAveragingImageFilter.txx"
#endif



#endif
