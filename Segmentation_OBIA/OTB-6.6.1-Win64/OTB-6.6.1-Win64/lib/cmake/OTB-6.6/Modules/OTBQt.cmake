# This file is modified by OTB after installation.
      
#
# Copyright (C) 2005-2017 Centre National d'Etudes Spatiales (CNES)
#
# This file is part of Orfeo Toolbox
#
#     https://www.orfeo-toolbox.org/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

set(OTBQt_LOADED 1)
set(OTBQt_DEPENDS "")
set(OTBQt_LIBRARIES "Qt5::Widgets;Qt5::Core;Qt5::Gui;Qt5::OpenGL")
set(OTBQt_INCLUDE_DIRS "${OTB_INSTALL_PREFIX}/include/OTB-6.6;${OTB_INSTALL_PREFIX}/include/;${OTB_INSTALL_PREFIX}/include/QtWidgets;${OTB_INSTALL_PREFIX}/include/QtGui;${OTB_INSTALL_PREFIX}/include/QtCore;${OTB_INSTALL_PREFIX}/.//mkspecs/win32-msvc;${OTB_INSTALL_PREFIX}/include/;${OTB_INSTALL_PREFIX}/include/QtCore;${OTB_INSTALL_PREFIX}/.//mkspecs/win32-msvc;${OTB_INSTALL_PREFIX}/include/;${OTB_INSTALL_PREFIX}/include/QtGui;${OTB_INSTALL_PREFIX}/include/QtCore;${OTB_INSTALL_PREFIX}/.//mkspecs/win32-msvc;${OTB_INSTALL_PREFIX}/include/;${OTB_INSTALL_PREFIX}/include/QtOpenGL;${OTB_INSTALL_PREFIX}/include/QtWidgets;${OTB_INSTALL_PREFIX}/include/QtGui;${OTB_INSTALL_PREFIX}/include/QtCore;${OTB_INSTALL_PREFIX}/.//mkspecs/win32-msvc")
set(OTBQt_LIBRARY_DIRS "")
find_package(Qt5Core REQUIRED HINTS ${OTB_INSTALL_PREFIX}/lib/cmake/Qt5Core)
find_package(Qt5Gui REQUIRED HINTS ${OTB_INSTALL_PREFIX}/lib/cmake/Qt5Gui)
find_package(Qt5Widgets REQUIRED HINTS ${OTB_INSTALL_PREFIX}/lib/cmake/Qt5Widgets)
find_package(Qt5OpenGL REQUIRED HINTS ${OTB_INSTALL_PREFIX}/lib/cmake/Qt5OpenGL)
find_package(Qt5LinguistTools HINTS ${OTB_INSTALL_PREFIX}/lib/cmake/Qt5LinguistTools)

