VertexComponentAnalysis|outendm
Given a set of mixed spectral vectors, estimatereference substances also known as endmembers using the VertexComponent Analysis algorithm.
Miscellaneous
QgsProcessingParameterRasterLayer|in|Input Image|None|False
QgsProcessingParameterNumber|ne|Number of endmembers|QgsProcessingParameterNumber.Integer|1|True
QgsProcessingParameterRasterDestination|outendm|Output Endmembers
QgsProcessingParameterNumber|rand|set user defined seed|QgsProcessingParameterNumber.Integer|0|True
*QgsProcessingParameterEnum|outputpixeltype|Output pixel type|uint8;int;float;double|False|2|True
