DSFuzzyModelEstimation
Estimate feature fuzzy model parameters using 2 vector data (ground truth samples and wrong samples).
Feature Extraction
QgsProcessingParameterVectorLayer|psin|Input Positive Vector Data|-1|None|False
QgsProcessingParameterVectorLayer|nsin|Input Negative Vector Data|-1|None|False
QgsProcessingParameterString|belsup|Belief Support|None|True|True
QgsProcessingParameterString|plasup|Plausibility Support|None|True|True
QgsProcessingParameterString|cri|Criterion|None|False|True
QgsProcessingParameterNumber|wgt|Weighting|QgsProcessingParameterNumber.Double|0.5|True
QgsProcessingParameterFile|initmod|initialization model|QgsProcessingParameterFile.File|None|None|True
QgsProcessingParameterString|desclist|Descriptor list|None|True|True
QgsProcessingParameterNumber|maxnbit|Maximum number of iterations|QgsProcessingParameterNumber.Integer|200|True
QgsProcessingParameterBoolean|optobs|Optimizer Observer|false|True
QgsProcessingParameterFileDestination|out|Output filename
