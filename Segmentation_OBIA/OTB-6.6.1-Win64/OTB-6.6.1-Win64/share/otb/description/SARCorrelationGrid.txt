SARCorrelationGrid|out
Computes SAR correlation grid.
SAR
QgsProcessingParameterRasterLayer|inmaster|Input Master image (real image)|None|False
QgsProcessingParameterRasterLayer|inslave|Input Slave image (real image)|None|False
QgsProcessingParameterFile|indem|Input DEM directory|QgsProcessingParameterFile.File|None|None|True
QgsProcessingParameterRasterDestination|out|Output Correlation grid (Vector Image)
QgsProcessingParameterNumber|mlran|MultiLook factor on distance|QgsProcessingParameterNumber.Integer|3|True
QgsProcessingParameterNumber|mlazi|MultiLook factor on azimut|QgsProcessingParameterNumber.Integer|3|True
QgsProcessingParameterNumber|patchsize|Size of patch into each dimension for correlation estimation|QgsProcessingParameterNumber.Integer|20|True
QgsProcessingParameterNumber|gridsteprange|Grid step for range dimension (into SLC/SAR geometry)|QgsProcessingParameterNumber.Integer|150|True
QgsProcessingParameterNumber|gridstepazimut|Grid step for azimut dimension (into SLC/SAR geometry)|QgsProcessingParameterNumber.Integer|150|True
QgsProcessingParameterNumber|subpixel|Subpixel measure|QgsProcessingParameterNumber.Integer|10|True
*QgsProcessingParameterEnum|outputpixeltype|Output pixel type|uint8;int;float;double|False|2|True
