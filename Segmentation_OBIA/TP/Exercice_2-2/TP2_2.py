# -*- coding: utf-8 -*-
"""
Éditeur de Spyder
TP SEGMENTATION 
Exercice 2.2
Axel CHASSARD et Laura WENCLIK ING2
"""

###############################################################################
#Import des modules nécessaires
###############################################################################

from skimage import io
import scipy.ndimage as nd
import numpy as np 
import scipy.misc  as nm
import imageio


###############################################################################
#Fonctions
###############################################################################

def RecupImg(link):
    '''
    Fonction permettant de récuperer une image et la convertir en numpy array
    
    param link : chemin de l'image 
    type link : str
    
    return : l'image convertit en tenseur 
    rtype : numpy array de float
    '''
    
    I = io.imread(link)
    I = np.array(I,int)
    return I

def CreationDico(img):
    '''
    Cette fonction permet de créer un dictionnaire associant à chaque valeur de l'image son indice afin de connaître
    une énumération séquentielle des valeurs de l'image.
    
    param img : l'image que l'on souhaité traiter
    type img : numpy array de float
    
    return : le dictionnaire associant à une valeur un indice
    rtype : dictionnaire
    '''
    
    dico = {}
    for i,v in enumerate(np.unique(img)):
        dico.update({v:i})
    return dico
    
def RecupNbVal(img):
    '''
    Cette fonction permet de récupérer le nombre de valeur différente dans l'image
    
    param img : l'image que l'on souhaité traiter
    type img : numpy array de float
    
    return : le nombre de valeur différente de notre image
    rtype : int
    '''
    
    #on créé un vecteur avec contenant une fois chaque valeur possible et on renvoie la taille de ce vecteur
    return np.unique(img).shape[0]



def DictionnaireDecompte(imgIRC,imgSegmentation,nbs,dicoSeg):
    '''
    Fonction permettant de récupérer un dictionnaire associant à chaque segment les pixels de celui-ci (avec la valeur de chaque canal)
    
    param imgIRC : l'image de la zone en infrarouge composé
    type imgIRC : numpy array de float de taille (nombre de lignes, nombre de colonnes, nombre de canaux)
    param imgSegmentation : l'image de segmentation
    type imgSegmentation : numpy array de float de taille (nombre de lignes, nombre de colonnes)
    param nbs : le nombre de segment
    type nbs : int
    param dicoSeg : dictionnaire associant à chaque segment un indice
    type dicoSeg : dictionnaire
    
    return : le dictionnaire associant à chaque segment les pixels de celui-ci
    rtype : dictionnaire
    
    '''
    
    #On créé un dictionnaire qui va asscoier à chaque indice de segment un tableau
    dicoNbPx = {}
    for i in range(nbs):
        dicoNbPx.update({i:[]})
    
    #On ajoute dans le tableau de chaque segment la valeur du pixel (avec chaque canaux) de l'image 
    for i in range(imgIRC.shape[0]):
        for j in range(imgIRC.shape[1]):
            dicoNbPx[dicoSeg[imgSegmentation[i,j]]].append(imgIRC[i,j])
    
    return dicoNbPx
            
            
def DictionnaireMoyenne(dicoNbPx):
    '''
    Fonction permettant de récupérer un dictionnaire associant à chaque segment la valeur moyenne de ces pixels
    
    param dicoNbPx : un dictionnaire associant à chaque segment les pixels de celui-ci
    type dicoNbPx : dictionnaire
    
    return : un dictionnaire associant à chaque segment la valeur moyenne de ces pixels
    rtype : dctionnaire
    '''          
    
    dicoMoyen = {}
    for i in range(len(dicoNbPx)):
        dicoMoyen.update({i:[]})
    
    
    for seg in dicoNbPx :
        RSomme = 0
        VSomme = 0
        BSomme = 0
        for i in range(len(dicoNbPx[seg])):
            RSomme += dicoNbPx[seg][i][0] 
            VSomme += dicoNbPx[seg][i][1]
            BSomme += dicoNbPx[seg][i][2]
        RMoy = RSomme/len(dicoNbPx[seg])
        VMoy = VSomme/len(dicoNbPx[seg])
        BMoy = BSomme/len(dicoNbPx[seg])
        dicoMoyen[seg] = [RMoy,VMoy,BMoy]
    
    return dicoMoyen
        
def CreationImage(imgS,dicoMoyen, dicoSeg):
    '''
    Fonction permettant de créer la nouvelle image avec une segmentation contenant la valeur moyenne des pixels
    
    param imgS : l'image de segmentation
    type imgS : numpy array de float de la taille de l'image
    param dicoMoyen : dictionnaire associant à chaque segment la valeur moyenne de ces pixels
    type dicoMoyen : dictionnaire
    param dicoSeg : dictionnaire associant à chaque segment un indice
    type dicoSeg : dictionnaire
    
    return : la nouvelle image
    rtype : numpy array de float
    '''
    
    
    NM=np.zeros((imgS.shape[0],imgS.shape[1],3))
    for i in range(NM.shape[0]):
        for j in range(NM.shape[1]):
            seg = imgS[i][j]
            NM[i][j][0] = dicoMoyen[dicoSeg[seg]][0]
            NM[i][j][1] = dicoMoyen[dicoSeg[seg]][1]
            NM[i][j][2] = dicoMoyen[dicoSeg[seg]][2]
    
    return NM
    

###############################################################################
#Exécution principale
###############################################################################

def Run2():
    '''
    Fonction réalisant la classification orientée objet 2.2.
    '''
    
    imgSegmentation= RecupImg("IRC_Segmentation.tif")
    imgIRC= RecupImg("ImageIRC.tif")
    nbs=RecupNbVal(imgSegmentation)
    dicoSeg = CreationDico(imgSegmentation)
    
    dicoNbPx = DictionnaireDecompte(imgIRC,imgSegmentation,nbs,dicoSeg)
    dicoMoyen = DictionnaireMoyenne(dicoNbPx)
    
    NM = CreationImage(imgSegmentation,dicoMoyen,dicoSeg)
    
    return NM

if __name__ == '__main__':
    
    

    
    NM = Run2()
    
    imageio.imwrite('NMbis.png',NM)
    
    