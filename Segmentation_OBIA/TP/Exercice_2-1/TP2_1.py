# -*- coding: utf-8 -*-
"""
Éditeur de Spyder
TP SEGMENTATION 
Exercice 2.1
Axel CHASSARD et Laura WENCLIK ING2
"""

###############################################################################
#Import des modules nécessaires
###############################################################################


from skimage import io
import scipy.ndimage as nd
import numpy as np 
import scipy.misc  as nm
import imageio


###############################################################################
#Fonctions
###############################################################################

def RecupImg(link):
    '''
    Fonction permettant de récuperer une image et la convertir en numpy array
    
    param link : chemin de l'image 
    type link : str
    
    return : l'image convertit en matrice 
    rtype : numpy array de float
    '''
    
    I = io.imread(link)
    I = np.array(I,float)
    
    return I

def RecupNbVal(img):
    '''
    Cette fonction permet de récupérer le nombre de valeur différente dans l'image
    
    param img : l'image que l'on souhaité traiter
    type img : numpy array de float
    
    return : le nombre de valeur différente de notre image
    rtype : int
    '''
    
    #on créé un vecteur avec contenant une fois chaque valeur possible et on renvoie la taille de ce vecteur
    return np.unique(img).shape[0]


def CreationDico(img):
    '''
    Cette fonction permet de créer un dictionnaire associant à chaque valeur de l'image son indice afin de connaître
    une énumération séquentielle des valeurs de l'image.
    
    param img : l'image que l'on souhaité traiter
    type img : numpy array de float
    
    return : le dictionnaire associant à une valeur un indice
    rtype : dictionnaire
    '''
    
    dico = {}
    for i,v in enumerate(np.unique(img)):
        dico.update({v:i})
    return dico
    

def Matricecompte(imgC,imgS,nbc,nbs,dicoSeg,dicoClassif):
    '''
    Fonction permettant de récupérer une matrice contenant pour chaque segment le nombre de pixel de chaque classe proposée
    
    
    param imgC : l'image de classification
    type imgC : numpy array
    param imgS : l'image de segmentation
    type imgS : numpy array
    param nbc : le nombre de classe 
    type nbc : int
    param nbs : le nombre de segment
    type nbs : int
    param dicoSeg : le dictionnaire associant à chaque segment un indice
    type dicoSeg : dictionnaire
    param dicoClassif : le dictionnaire associant à chaque classe un indice
    type dicoClassif : dictionnaire
    
    return : une matrice associant à chaque segment le nombre de pixel de chaque classe 
    rtype : numpy array de float de taille (nbs,nbc)
    '''
    
    #On parcourt chaque pixel de l'image, on récupére le segment et la classe et on incrémente de 1
    #le scalaire situé au croisement du segment et de la classe dans la matrice M
    M=np.zeros((nbs,nbc))
    for i in range(imgC.shape[0]):
        for j in range(imgC.shape[1]):
            idSegment=dicoSeg[imgS[i][j]]
            idClasse=dicoClassif[imgC[i][j]]
            M[idSegment][idClasse]+=1
    return M

def Vecteur(M,nbs,nbc):
    '''
    Création du vecteur V associant à chaque segment la classe majoritaire
    
    param  M : la matrice associant à chaque segment le nombre de pixel de chaque classe proposée
    type M : numpy array de float de taille (nbs,nbc)
    param nbc : le nombre de classe 
    type nbc : int
    param nbs : le nombre de segment
    type nbs : int
    
    return : le vecteur V 
    rtype : numpy array de float de taille (nbs,1)
    '''
    
    #pour chaque segment, on récupère la valeur maximale de la ligne dans la matrice M, on cherche à 
    #quelle classe cela correspond et on affecte cette classe au vecteur V 
    V=np.zeros(nbs)
    for i in range(nbs):
        max=np.max(M[i])
        for j in range(nbc):
            if(M[i][j]==max):
                V[i]=j
    return V
    

def CreationImage(imgS,V,dicoSeg,dicoClassif):
    '''
    Fonction permettant de créer la nouvelle image avec une segmentation contenant la classe majoritaire.
    
    param imgS : l'image de segmentation
    type imgS : numpy array de float de la taille de l'image
    param V : le vecteur associant à chaque segment sa classe majoritaire
    param dicoSeg : le dictionnaire associant à chaque segment un indice
    type dicoSeg : dictionnaire
    param dicoClassif : le dictionnaire associant à chaque classe un indice
    type dicoClassif : dictionnaire
    
    return : la nouvelle image 
    rtype : numpy array de float
    '''
    
    #Pour chaque pixel, on récupère le segment, on regarde quel est sa classe maoritaire et on l'affecte dans notre nouvelle matrice
    NM=np.zeros((imgS.shape[0],imgS.shape[1]))
    for i in range(NM.shape[0]):
        for j in range(NM.shape[1]):
            seg = imgS[i][j]
            NM[i][j] = V[dicoSeg[seg]]+1
    
    return NM


###############################################################################
#Exécution principale
###############################################################################

def Run1():
    '''
    Fonction réalisant la classification orientée objet 2.1.
    '''
    
    imgClassif= RecupImg("IRC_Classif.tif")
    imgSegmentation= RecupImg("IRC_Segmentation.tif")
    nbc= RecupNbVal(imgClassif)
    nbs= RecupNbVal(imgSegmentation)
    dicoSeg = CreationDico(imgSegmentation)
    dicoClassif = CreationDico(imgClassif)
    
    
    Matrice=Matricecompte(imgClassif,imgSegmentation,nbc,nbs,dicoSeg,dicoClassif)
    V=Vecteur(Matrice,nbs,nbc)
    
    NM = CreationImage(imgSegmentation,V,dicoSeg,dicoClassif)
    
    return NM
    
    

if __name__ == '__main__':

    
    NM = Run1()
    
    #on exporte l'image au format png
    imageio.imwrite('NM.png',NM)
    
    