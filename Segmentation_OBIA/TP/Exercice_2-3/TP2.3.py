# -*- coding: utf-8 -*-
"""
Created on Mon Dec  9 12:23:24 2019
TD SEGMENTATION 2.1
Axel CHASSARD et Laura WENCLIK ING2
@author: Formation
"""
from skimage import io
import scipy.ndimage as nd
import numpy as np 
import scipy.misc  as nm
import imageio

def RecupImg(link):
    '''
    Cette fonction permet de récuperer une image et la convertir en Array
    in : le chemin de l'image
    out: image en Array
    
    '''
    
    I = io.imread(link)
    I = np.array(I,float)
    
    return I

def RecupNbVal(img):
    '''
    Cette fonction permet de récupérer le nombre de valeur diférente dans l'image
    in: image en Array
    out: int 
    '''
    
    return np.unique(img).shape[0]


def CreationDico(img):
    '''
    Cette fonction permet de créer un dico en fonction des valeurs prise dans l'image
    in : image
    out: dico
    '''
    dico = {}

    for i,v in enumerate(np.unique(img)):
        dico.update({v:i})
    return dico

def CreationDicoClasse(img):
    '''
    '''
    
    dico = CreationDico(img);
    dico.update({6:5}) #la classe 6 est une nouvelle classe correspondant aux vergers
    dico.update({7:6}) #la classe 7 est une nouvelle classe correspondant aux champs cultivés
    dico.update({8:7}) #la classe 8 est une nouvelle classe correspondant aux arbres denses
    dico.update({9:8}) #la classe 9 est une nouvelle classe correspondant aux arbres semi-denses
    
    return dico
    
def Matricecompte(imgC,imgS,nbc,nbs,dicoSeg,dicoClassif):
    '''
    Cette fonction permet de compter les itérations de chaque segment
    in : image segment, image classe, nbc ,nbs, dicoSegmentation , dicClassif
    out: Matrice de taille(nbc,nbs)
    '''
#    imgSegmentationClasse=np.unique(imgS)
    M=np.zeros((nbs,nbc))
    for i in range(imgC.shape[0]):
        for j in range(imgC.shape[1]):
            idSegment=dicoSeg[imgS[i][j]]
            idClasse=dicoClassif[imgC[i][j]]
            M[idSegment][idClasse]+=1
    return M

def MatriceProportion(Mc):
    '''
    '''
    
    Mp = np.ones((Mc.shape[0],Mc.shape[1]))
    for i,v in enumerate(Mp):
        somme = np.sum(Mc[i])
        v /= somme
    
    return Mc*Mp
    
def vecteur(MP,nbs,nbc):
    '''
    '''
    
    V=np.zeros(nbs)
    for i in range(nbs):
        max=np.max(MP[i])
        if(MP[i][3]>0.2 and MP[i][3]<0.3 and MP[i][0]>0.4):
            V[i]=5
        elif(MP[i][0]>0.6 and MP[i][4]>0.15):
            V[i]=6
        elif(MP[i][3]+MP[i][4]>0.95):
            V[i]=7
        elif(MP[i][3]+MP[i][4]>0.75):
            V[i]=8
        else:
            for j in range(nbc):
                if(MP[i][j]==max):
                    V[i]=j
    
    return V





def CreationImage(imgS,V,dicoSeg,dicoClassif):
    '''
    Fonction permettant de créer la nouvelle image
    in : imgS,V,dicoSeg,dicoClassif
    out: image
    '''
    
    
    NM=np.zeros((imgS.shape[0],imgS.shape[1]))
    for i in range(NM.shape[0]):
        for j in range(NM.shape[1]):
            seg = imgS[i][j]
            NM[i][j] = V[dicoSeg[seg]]+1
    
    return NM

if __name__ == '__main__':

    
    imgClassif= RecupImg("imageIRC_classification.tif")
    imgSegmentation= RecupImg("imageIRC_segmentation.tif")
    nbc= RecupNbVal(imgClassif)
    nbs= RecupNbVal(imgSegmentation)
    dicoSeg = CreationDico(imgSegmentation)
    dicoClassif = CreationDicoClasse(imgClassif)
    Matrice=Matricecompte(imgClassif,imgSegmentation,nbc,nbs,dicoSeg,dicoClassif)
    MatriceProportion = MatriceProportion(Matrice)
    
    V=vecteur(MatriceProportion,nbs,nbc)
    
    NM = CreationImage(imgSegmentation,V,dicoSeg,dicoClassif)
    
    imageio.imwrite('NM.png',NM)
